/*
 * File: app.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

// @require @packageOverrides
Ext.Loader.setConfig({
    enabled: true
});


Ext.application({
    models: [
        'trap001CLDatosMdl'
    ],
    stores: [
        'trap001CLMarcaStr',
        'trap001CLsimboloUnidadStr',
        'tra001CLPatioStr',
        'trap001CLColoresStr',
        'trap001CLDistribuidorStr',
        'trap001CLUnidadesGrdStr',
        'trap001CLVINStr',
        'trap001CLLocalizacionUnidad',
        'trap001CLGeneralesStr',
        'trap001CLConsultaUnidadesStr',
        'trap001CLSimboloStr',
        'trap001CLDesbloqueoUnidadStr',
        'trap001CLDatosStr',
        'trap001ClLeerUnidadesStr',
        'trap001CLValidaAvanzadaStr',
        'trap001CLTarifaPorSimboloStr',
        'trap001CLExisteColorSimDistStr'
    ],
    views: [
        'trap001Win'
    ],
    name: 'MyApp',

    launch: function() {
        Ext.create('MyApp.view.trap001Win', {renderTo: Ext.getBody()});
    }

});
