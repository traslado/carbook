    /*
 * File: app/view/complementoPolizaDextraWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.viajesForaneos.complementoPoliza.complementoPolizaDextraWin', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.grid.Panel',
        'Ext.grid.View',
        'Ext.toolbar.Separator',
        'Ext.grid.column.Column'
    ],

    height: 397,
    id: 'complementoPolizaDextraWin',
    width: 475,
    title: 'Detalle Carga en Bombas',
    resizable:false,
    iconCls:'tractorModule',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            tools: [{
                xtype: 'tool',
                handler: function(event, toolEl, owner, tool) {
                    Ext.data.StoreManager.lookup('complementoPolizaConceptoDiStr').reload();
                },
                type: 'refresh'
            }],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                this.up('window').close();
                            },
                            iconCls: 'ux-btnSave',
                            text: 'Aceptar'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    height: 320,
                    id: 'complementoPolizaDextraFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'combobox',
                            x: 20,
                            y: 10,
                            id: 'complementoPolizaDextraConceptoCmb',
                            width: 290,
                            fieldLabel: 'Concepto',
                            labelWidth: 70,
                            name: 'complementoPolizaDextraConceptoCmb',
                            allowBlank: false,
                            enableKeyEvents: true,
                            displayField: 'nombreValor',
                            forceSelection: true,
                            queryMode: 'local',
                            store: 'complementoPolizaConceptoDiStr',
                            valueField: 'valor'
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 40,
                            id: 'complementoPolizaDextraLitrosTxt',
                            width: 160,
                            fieldLabel: 'Litros',
                            labelWidth: 70,
                            name: 'complementoPolizaDextraLitrosTxt',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            enableKeyEvents: true,
                            maskRe: /[0-9]+/
                        },
                        {
                            xtype: 'gridpanel',
                            x: 20,
                            y: 90,
                            height: 210,
                            id: 'complementoPolizaDextraGrd',
                            title: 'Descuento en Bombas',
                            store: 'complementoPolizaDextraGrd',
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    items: [
                                        {
                                            xtype: 'button',
                                            handler: function(button, e) {
                                                var form = Ext.getCmp('complementoPolizaDextraFrm').getForm();
                                                if(form.isValid()){
                                                    Ext.Ajax.request({
                                                        url: '../../carbookBck/json/trDieselExtra.php?',
                                                        params: {
                                                            dieselExtraHdn:'addConcepto',
                                                            dieselExtraIdViajeHdn: Ext.getCmp('complementoPolizaIdViajeHdn').getValue(),
                                                            dieselExtraLitrosTxt: Ext.getCmp('complementoPolizaDextraLitrosTxt').getValue(),
                                                            dieselExtraConceptoCmb: Ext.getCmp('complementoPolizaDextraConceptoCmb').getValue()
                                                        }});

                                                        var talonStr = Ext.getCmp("complementoPolizaDextraGrd").getStore();
                                                        talonStr.load({params:{dieselExtraHdn:'sqlGetConsultas',
                                                        dieselExtraIdViajeHdn:Ext.getCmp('complementoPolizaIdViajeHdn').getValue()}});
                                                        Ext.getCmp('complementoPolizaDextraConceptoCmb').setValue('');
                                                        Ext.getCmp('complementoPolizaDextraLitrosTxt').setValue('');
                                                        Ext.getCmp('complementoPolizaDextraConceptoCmb').focus();
                                                    }else{
                                                        Ext.Msg.alert("Captura de Diesel","Falta capturar campos requeridos");
                                                    }
                                            },
                                            iconCls: 'ux-btnSave',
                                            text: 'Agregar'
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'button',
                                            handler: function(button, e) {
                                                var selection = Ext.getCmp('complementoPolizaDextraGrd').getView().getSelectionModel().getSelection()[0];
                                                var numConcepto = selection.get('concepto');
                                                var extraeCadena = numConcepto.substring(0,4);

                                                Ext.Ajax.request({
                                                    url: '../../carbookBck/json/trDieselExtra.php?',
                                                    params: {
                                                        dieselExtraHdn:'sqlDltDiesel',
                                                        dieselExtraIdViajeHdn: Ext.getCmp('complementoPolizaIdViajeHdn').getValue(),
                                                        dieselExtraConceptoCmb: extraeCadena
                                                    }});

                                                    var talonStr = Ext.getCmp("complementoPolizaDextraGrd").getStore();
                                                    talonStr.load({params:{dieselExtraHdn:'sqlGetConsultas',
                                                    dieselExtraIdViajeHdn:Ext.getCmp('complementoPolizaIdViajeHdn').getValue()}});

                                            },
                                            iconCls: 'ux-btnDel',
                                            text: 'Eliminar'
                                        }
                                    ]
                                }
                            ],
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'tractor',
                                    text: 'Tractor'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'concepto',
                                    text: 'Concepto'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'fechaEvento',
                                    text: 'FechaEvento'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'litros',
                                    text: 'Litros'
                                }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
                boxready: {
                    fn: me.oncomplementoPolizaDextraWinBoxReady,
                    scope: me
                },
                render: {
                    fn: me.oncomplementoPolizaDextraWinRender,
                    scope: me
                },
                beforeclose: {
                        fn: function(panel, eOpts) {

                                var window = Ext.WindowManager.get('complementoPolizaCapturadieselWin');

                                Ext.Ajax.request({
                                    url: '../../carbookBck/json/trcomplementoPoliza.php?',
                                    params: {
                                        complementoActionHdn: 'getMontosDiesel',
                                        complementoPolizaIdViajeHdn:Ext.getCmp('complementoPolizaIdViajeHdn').getValue(),
                                        polizaGastosIdTractorHdn: Ext.getCmp('polizaGastosTractorDsp').idTractor
                                    },
                                    success: function(response){
                                        response = Ext.JSON.decode(response.responseText);
                                        if (response.records > 0) {

                                            var km = parseFloat(Ext.getCmp('complementoPolizaKilometrosTxt').getValue());
                                            var rendimiento = parseFloat(Ext.getCmp('complementoPolizaRendimientoHdn').getValue());


                                            Ext.getCmp('complementoPolizaCapturadieselCaluloLtsDsp').setValue(Math.round(km/rendimiento).toFixed(2));


                                            Ext.getCmp('dieselExtraTicketDsp').setValue(response.root[0].litrosTk);
                                            Ext.getCmp('dieselExtraBombasDsp').setValue(response.root[0].numLitrosResta);
                                             Ext.getCmp('dieselExtraSumaDsp').setValue(response.root[0].numLitrosSuma);
                                            //Ext.getCmp('complementoPolizaCapturadieselCaluloLtsDsp').setValue(response.root[0].numLitrosSuma);

                                            var calcLitros = parseFloat(Ext.getCmp('complementoPolizaCapturadieselCaluloLtsDsp').getValue());
                                            var dieselBombas = parseFloat(Ext.getCmp('dieselExtraBombasDsp').getValue());
                                            var dieselTicket = parseFloat(Ext.getCmp('dieselExtraTicketDsp').getValue());
                                            var dieselExtra = parseFloat(Ext.getCmp('dieselExtraSumaDsp').getValue());

                                            var valFinal = parseFloat(calcLitros-dieselBombas-dieselTicket+dieselExtra);
                                            var vNeutro = Math.sign(valFinal);
                                            Ext.getCmp('complementoPolizaWin').difCombustible = valFinal;
                                            if(vNeutro == -1){
                                                var positivo = parseFloat(valFinal* -1);

                                                Ext.getCmp('complementoPolizaCapturadieselDescBombasDsp').setValue(positivo.toFixed(0));
                                                Ext.getCmp('complementoPolizaCapturaDieselDpendienteDsp').setValue('00.00');
                                            }else{
                                                Ext.getCmp('complementoPolizaCapturaDieselDpendienteDsp').setValue(valFinal.toFixed(0));
                                                Ext.getCmp('complementoPolizaCapturadieselDescBombasDsp').setValue('00.00');
                                            }


                                             if (dieselBombas != 0 || dieselTicket != 0 || dieselExtra != 0 )
                                             {
                                                Ext.getCmp('polizaGastorCapturaDieselGuardarBtn').enable();
                                             }
                                             else{
                                                 Ext.getCmp('polizaGastorCapturaDieselGuardarBtn').disable();
                                             }
                                        }
                                    }
                                });

                        }
                    }
            }
        });

        me.callParent(arguments);
    },

    oncomplementoPolizaDextraWinBoxReady: function(component, width, height, eOpts) {
        var elements = Ext.ComponentQuery.query('[xtype=textfield]').concat(Ext.ComponentQuery.query('[xtype=combobox]')).concat(Ext.ComponentQuery.query('[xtype=numberfield]'));

        //Por cada elemento lo suscribo a un evento keypress, con un eventHandler llamado nextFocus
        Ext.each(elements, function(el){
            el.on('keypress', nextFocus);
        });

        //Event Handler que se encarga de recorrer y hacer FOCO a los elementos del formulario.
        function nextFocus(field, e){
            var actual = field;

            if (e.getKey() === Ext.EventObject.ENTER) { //Si el input del teclado es Enter...
                while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){//Recorro los elementos. Si existe el elemento siguiente y es Visible (por los hiddens fields).
                    actual = actual.next();
                }
                if(actual.next()){
                    actual.next().focus();
                } else {//Si estoy en el elemento final regreso al primero...NO SÉ si sea el comportamiento requerido.

                    while(actual.prev()){//Recorro mientras exista elemento
                        actual = actual.prev();
                    }
                    if(actual.isVisible()){//Si no es un hidden field le hago foco
                        actual.focus();
                    } else {// de lo contrario vuelvo a recorrer hasta encontrar algún elemento visible
                        while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){
                            actual = actual.next();
                        }
                        actual.next().focus();
                    }
                }
            }
        }
    },

    oncomplementoPolizaDextraWinRender: function(component, eOpts) {
        var talonStr = Ext.getCmp("complementoPolizaDextraGrd").getStore();
        talonStr.load({params:{dieselExtraHdn:'sqlGetConsultas',
                               dieselExtraIdViajeHdn:Ext.getCmp('complementoPolizaIdViajeHdn').getValue()}});

        Ext.getCmp('complementoPolizaDextraConceptoCmb').focus();
    }

});