/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.catGenMenuModel100', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Catalogos',
            iconCls: 'catGeneralesMenuIco',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
					{
						text: 'Unidades',
						iconCls: 'catalogosAutIco',
						menu:
						{
							items:
							[
								{
									text: 'Simbolos Unidades',
									iconCls:'catSimbolosUnidades',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catSimbolosWin');
										win = module.createWindow_catSimbolosWin();
									},
									scope: this,
									windowId: 'catSimbolosWin'
								}
							]
						}
					},
					{
						text: 'Operativos',
						iconCls: 'catOperativos',
						menu:
						{
							items:
							[
								{
									text: 'Distribuidores',
									iconCls:'catDistribuidores',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catDistribuidoresWin');
										win = module.createWindow_catDistribuidoresWin();
									},
									scope: this,
									windowId: 'catDistribuidoresWin'
								},
								{
									text: 'Destinos Especiales',
									iconCls:'catDestinosEspeciales',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catDestinosEspecialesWin');
										win = module.createWindow_catDestinosEspecialesWin();
									},
									scope: this,
									windowId: 'catDestinosEspecialesWin'
								}
							]
						}
					}
				]
            }
        };
    }
});