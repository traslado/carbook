/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.polizaMenu13', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Polizas',
            iconCls: 'tractorModule',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Polizas',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('polizaGastosWin');
										win = module.createWindow_polizaGastosWin();
									},
									scope: this,
									windowId: 'polizaGastosWin'
								},
								{
									text: 'Carga Ticket Card',
									iconCls:'catGeneralesMenuIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('cargaTicketWin');
										win = module.createWindow_cargaTicketWin();
									},
									scope: this,
									windowId: 'cargaTicketWin'
								},
								{
									text: 'Carga diesel Extra',
									iconCls:'catGeneralesMenuIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('dieselExtraWin');
										win = module.createWindow_dieselExtraWin();
									},
									scope: this,
									windowId: 'dieselExtraWin'
								},
								{
									text: 'Pago Diesel en Bombas',
									iconCls:'catGeneralesMenuIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('pagoDieselWin');
										win = module.createWindow_pagoDieselWin();
									},
									scope: this,
									windowId: 'pagoDieselWin'
								},
								{
									text: 'Cancelacion Polizas',
									iconCls:'catGeneralesMenuIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('canPolizaWin');
										win = module.createWindow_canPolizaWin();
									},
									scope: this,
									windowId: 'canPolizaWin'
								}
				]
            }
        };
    }
});