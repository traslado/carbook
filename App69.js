/*!
 * Ext JS Library 4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

// Prevent the backspace key from navigating back.
Ext.EventManager.on(Ext.isIE ? document : window, 'keydown', function(e, t) {
	if (e.getKey() == e.BACKSPACE && ((!/^input$/i.test(t.tagName) && !/^textarea$/i.test(t.tagName)) || t.disabled || t.readOnly)) {
		e.stopEvent();
	}
});

Ext.define('MyDesktop.App69', {
    extend: 'Ext.ux.desktop.App',

    requires: [
		'Ext.window.MessageBox',
		'Ext.ux.desktop.ShortcutModel',
		
		'MyDesktop.moduloComodato69',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLDatosMdl',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLMarcaStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLsimboloUnidadStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.tra001CLPatioStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLColoresStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLDistribuidorStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLUnidadesGrdStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLVINStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLLocalizacionUnidad',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLGeneralesStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLConsultaUnidadesStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLSimboloStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLDesbloqueoUnidadStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLDatosStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001ClLeerUnidadesStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLValidaAvanzadaStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLTarifaPorSimboloStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLExisteColorSimDistStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001CLExisteColorSimDistStr',
		'MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001Win',
		'MyDesktop.modules.recepcionUnidades.FacturacionEntradaPatio.entradaPatioUnidadesMdl',
		'MyDesktop.modules.recepcionUnidades.FacturacionEntradaPatio.entradaPatioCentrosValidosStr',
		'MyDesktop.modules.recepcionUnidades.FacturacionEntradaPatio.entradaPatioUnidadesStr',
		'MyDesktop.modules.recepcionUnidades.FacturacionEntradaPatio.entradaPatioVinStr',
		'MyDesktop.modules.recepcionUnidades.FacturacionEntradaPatio.entradaPatioBloqueoStr',
		'MyDesktop.modules.recepcionUnidades.FacturacionEntradaPatio.entradaPatioWin',
		'MyDesktop.modules.recepcionUnidades.Observaciones.trap063HistoricoStr',
		'MyDesktop.modules.recepcionUnidades.Observaciones.trap063VinStr',
		'MyDesktop.modules.recepcionUnidades.Observaciones.trap063Win',
		'MyDesktop.modules.recepcionUnidades.cambioDistribuidor.alCambioDistribuidorVinStr',
		'MyDesktop.modules.recepcionUnidades.cambioDistribuidor.alCambioDistribuidorDistribuidorStr',
		'MyDesktop.modules.recepcionUnidades.cambioDistribuidor.alCambioDistribuidorVinDetalleStr',
		'MyDesktop.modules.recepcionUnidades.cambioDistribuidor.alCambioDistribuidorHistoriaUnidadStr',
		'MyDesktop.modules.recepcionUnidades.cambioDistribuidor.alCambioDistribuidorWin',
		'MyDesktop.modules.recepcionUnidades.CambioEstatus.trapCambioEstatusEstatusStr',
		'MyDesktop.modules.recepcionUnidades.CambioEstatus.trapCambioEstatusVinStr',
		'MyDesktop.modules.recepcionUnidades.CambioEstatus.trapCambioEstatusBloqueoStr',
		'MyDesktop.modules.recepcionUnidades.CambioEstatus.trapCambioEstatusWin',
		'MyDesktop.modules.recepcionUnidades.detencionUnidades.detencionUnidadesUnidadesStr',
		'MyDesktop.modules.recepcionUnidades.detencionUnidades.detencionUnidadesDistribuidorStr',
		'MyDesktop.modules.recepcionUnidades.detencionUnidades.detencionUnidadesSimboloStr',
		'MyDesktop.modules.recepcionUnidades.detencionUnidades.detencionUnidadesWin',
		'MyDesktop.modules.recepcionUnidades.ReimpresionEtiquetas.reEtiquetasVinStr',
		'MyDesktop.modules.recepcionUnidades.ReimpresionEtiquetas.reEtiquetasLocalizacionStr',
		'MyDesktop.modules.recepcionUnidades.ReimpresionEtiquetas.reEtiquetasWind',
		'MyDesktop.modules.comoDato.impresionComodato.impresionComodatoStr',
		'MyDesktop.modules.comoDato.impresionComodato.impresionComodatoWin',
		'MyDesktop.modules.comoDato.localizacionUnidadesComodato.localizacionUnidadBahiaStr',
		'MyDesktop.modules.comoDato.localizacionUnidadesComodato.localizacionUnidadesStr',
		'MyDesktop.modules.comoDato.localizacionUnidadesComodato.localizacionPatioWin',
		'MyDesktop.modules.comoDato.comodatoProdStatus.prodStatusGeneralStr',
		'MyDesktop.modules.comoDato.comodatoProdStatus.prodStatusWin',
		'MyDesktop.modules.comoDato.reImpresionEtiquetas.reImpresionEtiquetasWin',
		'MyDesktop.modules.comoDato.ActializacionDeLocalizaciones.actalizacionDeLocalizacionWin',
		'MyDesktop.modules.comoDato.cambioMercado.tipoMercadoStr',
		'MyDesktop.modules.comoDato.cambioMercado.tipoMercadoWin',
		'MyDesktop.modules.comoDato.cambioSalida.tipoSalidaSalidaStr',
		'MyDesktop.modules.comoDato.cambioSalida.tipoSalidaWin',
		'MyDesktop.modules.comoDato.bajaUnidades.bajaUndsWin',
		'MyDesktop.modules.recepcionUnidades.impresionPlacard.impresionPlacardStr',
		'MyDesktop.modules.recepcionUnidades.impresionPlacard.impPlacardMarcaStr',
		'MyDesktop.modules.recepcionUnidades.impresionPlacard.impresionPlacardWin',
		
		'MyDesktop.modules.security.segUsuariosModulos.segUsuariosModulosDialogWin',
		'MyDesktop.modules.security.login.app.store.segUsuarioConfigStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlSegUsuariosStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlModulosStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlEscritorioStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlWn'
    ],

    init: function() {
        // custom logic before getXYZ methods get called...

        this.callParent();

        // now ready...
    },

    getModules : function(){
        return [
            // Menues
            new MyDesktop.modules.recepcionUnidades.altaGlobalUnidades.trap001Win(),
            new MyDesktop.modules.recepcionUnidades.FacturacionEntradaPatio.entradaPatioWin(),
            new MyDesktop.modules.recepcionUnidades.Observaciones.trap063Win(),
            new MyDesktop.modules.recepcionUnidades.cambioDistribuidor.alCambioDistribuidorWin(),
            new MyDesktop.modules.recepcionUnidades.CambioEstatus.trapCambioEstatusWin(),
            new MyDesktop.modules.recepcionUnidades.detencionUnidades.detencionUnidadesWin(),
            new MyDesktop.modules.recepcionUnidades.ReimpresionEtiquetas.reEtiquetasWind(),
            new MyDesktop.modules.comoDato.impresionComodato.impresionComodatoWin(),
            new MyDesktop.modules.comoDato.localizacionUnidadesComodato.localizacionPatioWin(),
            new MyDesktop.modules.comoDato.comodatoProdStatus.prodStatusWin(),
            new MyDesktop.modules.comoDato.reImpresionEtiquetas.reImpresionEtiquetasWin(),
            new MyDesktop.modules.comoDato.ActializacionDeLocalizaciones.actalizacionDeLocalizacionWin(),
            new MyDesktop.modules.comoDato.cambioMercado.tipoMercadoWin(),
            new MyDesktop.modules.comoDato.cambioSalida.tipoSalidaWin(),
            new MyDesktop.modules.comoDato.bajaUnidades.bajaUndsWin(),
            new MyDesktop.modules.recepcionUnidades.impresionPlacard.impresionPlacardWin(),
            new MyDesktop.moduloComodato69(),
            new MyDesktop.modules.security.segUsuariosModulos.segUsuariosModulosDialogWin(),
            new MyDesktop.modules.security.login.app.store.segUsuarioConfigStr()
        ];
    },

    getDesktopConfig: function () {
        var me = this, ret = me.callParent();
		
        return Ext.apply(ret, {

            contextMenuItems: [
                { text: 'Personalizar', iconCls:'personalizar', handler: me.onSettings, scope: me }
            ],

            shortcuts: Ext.create('Ext.data.Store', {
                model: 'Ext.ux.desktop.ShortcutModel',
				data: accesosDirArr
            }),

            wallpaper: 'wallpapers/Chrysler.jpg',
            wallpaperStretch: true
        });
    },

    // config for the start menu
    getStartConfig : function() {
        var me = this, ret = me.callParent();
        Ext.EventManager.on(document, 'keydown', function(e, t) {
    	   	if (e.getKey() == e.BACKSPACE && (!/^input$/i.test(t.tagName) || t.disabled || t.readOnly)) {
     		e.stopEvent();
    		}
   	   });

        return Ext.apply(ret, {
            title: usuNombre,
            iconCls: 'user',
            height: 300,
            toolConfig: {
                width: 100,
                items: [
                    {
                        text:'Personalizar',
                        iconCls:'personalizar',
                        handler: me.onSettings,
                        scope: me
                    },
                    '-',
                    {
                        text:'Presto',
                        iconCls:'personalizar',
                        handler: me.getCosnulta,
                        scope: me
                    },
                    '-',
                    {
                        text:'Salir',
                        iconCls:'salir',
                        handler: me.onLogout,
                        scope: me
                    }
                ]
            }
        });
    },

    getTaskbarConfig: function () {
        var ret = this.callParent();

        return Ext.apply(ret, {
            quickStart: quickStartArr,
            trayItems: [
                { xtype: 'trayclock', flex: 1 }
            ]
        });
    },

    getCosnulta: function () {
        var dlg = new MyDesktop.modules.system.panelPresto.MyWindow({
            desktop: this.desktop
        });
        dlg.show();
    },

    onLogout: function () {
        Ext.Msg.confirm('Salir', '¿Esta seguro que desea salir?<br>El trabajo que no haya guardado se <br>perder&aacute;', function(btn, text){
        	if (btn == 'yes')
				window.location = 'modules/security/login/logout.php';
			});
    },

    onSettings: function () {
        var dlg = new MyDesktop.modules.system.panelControl.sisPanelControlWn({
            desktop: this.desktop
        });
        dlg.show();
    }
});
