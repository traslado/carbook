/*
 * File: app/view/chequesCancelacionWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.cheques.chequesCancelacion.chequesCancelacionWin', {
    extend: 'Ext.ux.desktop.Module',
    
    id:'chequesCancelacionWin',

    /*init : function(){
        this.launcher = {
            text: rhTRelLabTittle,
            iconCls:'auCatColores'
        };
    },*/
    
    createWindow_chequesCancelacionWin : function(){
        var desktop = this.app.getDesktop();
        var win_chequesCancelacionWin = desktop.getWindow('chequesCancelacionWin');
        if(!win_chequesCancelacionWin){
            // Integrar aquí los Stores que ocupará la pantalla
            var store = new MyDesktop.modules.cheques.chequesCancelacion.chequesCancelacionChequeStr();
            var store = new MyDesktop.modules.cheques.chequesCancelacion.chequesCancelacionCtaBancariaStr();
            
            win_chequesCancelacionWin = desktop.createWindow({
                height: 301,
                id: 'chequesCancelacionWin',
                width: 554,
                layout: {
                    type: 'fit'
                },
                title: 'Generacion de Cheques',
                maximizable: true,
                minimizable: true,
                
                dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'chequesCancelacionToolBr',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var win = Ext.WindowMgr.get('chequesCancelacionWin');
                                var form = Ext.getCmp('chequesCancelacionFrm');
                                if(form.getForm().isValid()){
                                    Ext.Msg.confirm({
                                        title: win.title,
                                        buttonText:{
                                            'yes': 'Normal',
                                            'no': 'Físico'
                                        },
                                        msg: 'Seleccione el Tipo de Cancelación',
                                        fn: function(btn){
                                            var tipoCancelacion = 'N';
                                            if(btn === 'no') tipoCancelacion = 'F';//Fisico
                                            if(btn === 'cancel') return;
                                            form.getForm().submit({
                                                params:{
                                                    facturacionTipoCancelacion: tipoCancelacion
                                                },
                                                success: function(form, action){
                                                    Ext.Msg.alert(win.title, action.result.successMessage);
                                                    Ext.getCmp('chequesCancelacionCancelarBtn').fireHandler();
                                                },
                                                failure: function(form, action){
                                                    Ext.Msg.alert(win.title, action.result.errorMessage);
                                                }
                                            });
                                            Ext.Msg.wait('Cancelando el Cheque...', win.title);
                                        }
                                    });

                                } else{
                                    Ext.Msg.alert(win.title, 'Faltan Campos requeridos o contiene el Formato Incorrecto');
                                }
                            },
                            cls: 'ux-btnSave',
                            id: 'chequesCancelacionAceptarBtn',
                            text: ''
                        },
                        {
                            xtype: 'tbseparator',
                            id: 'chequesCancelacionSep001'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.getCmp('chequesCancelacionFrm').getForm().reset();
                            },
                            cls: 'ux-btnCancel',
                            id: 'chequesCancelacionCancelarBtn',
                            text: ''
                        }
                    ]
                },
                {
                    xtype: 'form',
                    dock: 'top',
                    height: 234,
                    id: 'chequesCancelacionFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    url: '../../carbookBck/json/chCheques.php?chChequesActionHdn=cancelarCheque',
                    items: [
                        {
                            xtype: 'combobox',
                            x: 20,
                            y: 20,
                            id: 'chequesCancelacionCtaBancariaCmb',
                            width: 320,
                            fieldLabel: 'Cta. Bancaria',
                            msgTarget: 'side',
                            fieldStyle: 'text-transform: uppercase',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            displayField: 'descCuenta',
                            forceSelection: true,
                            queryMode: 'local',
                            store: 'chequesCancelacionCtaBancariaStr',
                            valueField: 'idCuentaBancaria',
                            listeners: {
                                select: {
                                    fn: function(combo, records, eOpts) {
                                        var win = Ext.WindowMgr.get('chequesCancelacionWin');
                                        var folio = Ext.getCmp('chequesCancelacionChequeTxt').getValue();
                                        if(folio){
                                            var chequeStr = Ext.StoreMgr.lookup('chequesCancelacionChequeStr');
                                            chequeStr.load({
                                                params:{
                                                    facturacionIdCtaBancaria: combo.getValue(),
                                                    facturacionFolioCheque: folio
                                                },
                                                callback: function(records, operation, success){
                                                    if(records && records.length === 1){
                                                        var fechaElaboracion = records[0].get('fechaElaboracion').replace('-','/');
                                                        fechaElaboracion = new Date(fechaElaboracion).toLocaleDateString('es');
                                                        Ext.getCmp('chequesCancelacionNombreDsp').setValue(records[0].get('descripcionCentro'));
                                                        Ext.getCmp('chequesCancelacionConceptoDsp').setValue('BONIFICACIÓN');
                                                        Ext.getCmp('chequesCancelacionImporteDsp').setValue(records[0].get('importe'));
                                                        Ext.getCmp('chequesCancelacionFechaElaboracionDsp').setValue(fechaElaboracion);
                                                        Ext.getCmp('chequesCancelacionNumUnidadesDsp').setValue(records[0].get('numeroUnidades'));
                                                        Ext.getCmp('chequesCancelacionIdChequeHdn').setValue(records[0].get('idCheque'));
                                                    } else{
                                                        Ext.Msg.alert(win.title,'No. de Cheque no encontrado');
                                                        Ext.getCmp('chequesCancelacionNombreDsp').reset();
                                                        Ext.getCmp('chequesCancelacionConceptoDsp').reset();
                                                        Ext.getCmp('chequesCancelacionImporteDsp').reset();
                                                        Ext.getCmp('chequesCancelacionFechaElaboracionDsp').reset();
                                                        Ext.getCmp('chequesCancelacionNumUnidadesDsp').reset();
                                                        Ext.getCmp('chequesCancelacionIdChequeHdn').reset();
                                                        combo.reset();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 360,
                            y: 20,
                            id: 'chequesCancelacionChequeTxt',
                            width: 160,
                            fieldLabel: 'No. Cheque',
                            labelWidth: 80,
                            msgTarget: 'side',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            enforceMaxLength: true,
                            maskRe: /[0-9]/,
                            maxLength: 8,
                            listeners: {
                                blur: {
                                    fn: function(component, e, eOpts) {
                                        var win = Ext.WindowMgr.get('chequesCancelacionWin');
                                        var cuenta = Ext.getCmp('chequesCancelacionCtaBancariaCmb').getValue();
                                        if(cuenta && component.getValue()){
                                            var chequeStr = Ext.StoreMgr.lookup('chequesCancelacionChequeStr');
                                            chequeStr.load({
                                                params:{
                                                    facturacionIdCtaBancaria: cuenta,
                                                    facturacionFolioCheque: component.getValue()
                                                },
                                                callback: function(records, operation, success){
                                                    if(records && records.length === 1){
                                                        if(records[0].get('estatusCheque') === 'C'){//Cheque Cancelado
                                                            Ext.Msg.alert(win.title,'El No. Cheque: ' + component.getValue() + ' está Cancelado', function(){
                                                                component.reset();
                                                            });
                                                        } else{
                                                            var fechaElaboracion = records[0].get('fechaElaboracion').replace('-','/');
                                                            fechaElaboracion = new Date(fechaElaboracion).toLocaleDateString('es');
                                                            Ext.getCmp('chequesCancelacionNombreDsp').setValue(records[0].get('descripcionCentro'));
                                                            Ext.getCmp('chequesCancelacionConceptoDsp').setValue('BONIFICACIÓN');
                                                            Ext.getCmp('chequesCancelacionImporteDsp').setValue(records[0].get('importe'));
                                                            Ext.getCmp('chequesCancelacionFechaElaboracionDsp').setValue(fechaElaboracion);
                                                            Ext.getCmp('chequesCancelacionNumUnidadesDsp').setValue(records[0].get('numeroUnidades'));
                                                            Ext.getCmp('chequesCancelacionIdChequeHdn').setValue(records[0].get('idCheque'));
                                                        }
                                                    } else{
                                                        Ext.Msg.alert(win.title,'No. de Cheque no encontrado');
                                                        Ext.getCmp('chequesCancelacionNombreDsp').reset();
                                                        Ext.getCmp('chequesCancelacionConceptoDsp').reset();
                                                        Ext.getCmp('chequesCancelacionImporteDsp').reset();
                                                        Ext.getCmp('chequesCancelacionFechaElaboracionDsp').reset();
                                                        Ext.getCmp('chequesCancelacionNumUnidadesDsp').reset();
                                                        Ext.getCmp('chequesCancelacionIdChequeHdn').reset();
                                                        component.reset();
                                                    }

                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 50,
                            hidden: true,
                            id: 'chequesCancelacionTipoChequeTxt',
                            width: 140,
                            fieldLabel: 'Tipo Cheque',
                            msgTarget: 'side',
                            fieldStyle: 'text-transform: uppercase',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            enforceMaxLength: true,
                            maskRe: /[A-Za-z]/,
                            maxLength: 1
                        },
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 80,
                            id: 'chequesCancelacionNombreDsp',
                            width: 480,
                            fieldLabel: 'A Nombre',
                            labelWidth: 110,
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 110,
                            id: 'chequesCancelacionConceptoDsp',
                            width: 200,
                            fieldLabel: 'Concepto',
                            labelWidth: 110,
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 140,
                            id: 'chequesCancelacionFechaElaboracionDsp',
                            fieldLabel: 'Fecha Elaboración',
                            labelWidth: 110,
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 170,
                            id: 'chequesCancelacionFechaEntregaDsp',
                            fieldLabel: 'Fecha Entrega',
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 200,
                            id: 'chequesCancelacionFechaCobroDsp',
                            fieldLabel: 'Fecha Cobro',
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 360,
                            y: 200,
                            id: 'chequesCancelacionNumUnidadesDsp',
                            fieldLabel: 'No. Unidades',
                            labelWidth: 80,
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 360,
                            y: 170,
                            id: 'chequesCancelacionImporteDsp',
                            fieldLabel: 'Importe',
                            labelWidth: 80,
                            value: ''
                        },
                        {
                            xtype: 'hiddenfield',
                            x: 252,
                            y: 103,
                            id: 'chequesCancelacionIdChequeHdn',
                            fieldLabel: 'Label',
                            name: 'facturacionIdChequeHdn'
                        }
                    ]
                }
            ],
            listeners: {
                render: {
                    fn: function(component, eOpts) {
                        /* Función que simula un TAB en inputFields con un ENTER y recorre como Linked Lists.
                        * REQUERIMIENTOS PREVIOS: settear la propiedad enableKeyEvents de los componentes a utilizar.
                        * PENDIENTES: No funciona en checkboxes por su comportamiento nativo. Desconosco aún si funciona
                        *             con varias Tab-Panel y con demás controles dentros del Form.
                        */
                        //Hago un query con los elementos que sean textfields, numberfields o combobox y obtengo un array de ellos.
                        var elements = Ext.ComponentQuery.query('[xtype=textfield]').concat(Ext.ComponentQuery.query('[xtype=combobox]')).concat(Ext.ComponentQuery.query('[xtype=numberfield]'));
                        elements = elements.concat(Ext.ComponentQuery.query('[xtype=datefield]'));

                        //Por cada elemento lo suscribo a un evento keypress, con un eventHandler llamado nextFocus
                        Ext.each(elements, function(el){
                            el.on('keypress', nextFocus);
                        });

                        //Event Handler que se encarga de recorrer y hacer FOCO a los elementos del formulario.
                        function nextFocus(field, e){
                            var actual = field;
                            if (e.getKey() === Ext.EventObject.ENTER) { //Si el input del teclado es Enter...
                                while(actual.next() && (!actual.next().isVisible() || actual.next().xtype === 'displayfield' || actual.next().xtype === 'button')){//Recorro los elementos. Si existe el elemento siguiente y es Visible (por los hiddens fields).
                                    actual = actual.next();
                                }
                                if(actual.next() && actual.next().id !== 'catPreViajesDatosGrdPnl'){
                                    actual.next().focus();
                                }
                                else{//Si estoy en el elemento final regreso al primero...NO SÉ si sea el comportamiento requerido.

                                    while(actual.prev()){//Recorro mientras exista elemento
                                        actual = actual.prev();
                                    }
                                    if(actual.isVisible() && actual.xtype !== 'displayfield'){//Si no es un hidden field le hago foco
                                        actual.focus();
                                    }
                                    else{// de lo contrario vuelvo a recorrer hasta encontrar algún elemento visible
                                        while(actual.next() && (!actual.next().isVisible() || actual.next().xtype === 'displayfield' || actual.next().xtype === 'button')){
                                            actual = actual.next();
                                        }
                                        actual.next().focus();
                                    }
                                }
                            }
                        }
                    }
                },
                beforeclose: {
                    fn: function(panel, eOpts) {
                        if(panel.allowDestroy){
                            delete panel.allowDestroy;
                            return true;
                        }
                        var titulo = this.title;
                        Ext.Msg.confirm(titulo,'¿Está seguro que desea salir de<br> '+ titulo+'?',function(btn){
                            if(btn == 'yes'){
                                panel.allowDestroy = true;
                                panel.destroy();
                            }
                        }, panel);
                        return false;
                    }
                }
            }
        });

        }
        win_chequesCancelacionWin.show();
        return win_chequesCancelacionWin;
    }
});