/*
 * File: app/view/cambiodestinoWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

 Ext.define('MyDesktop.modules.serviciosEspeciales.cambioDestinoSE.cambiodestinoWin', {
   extend: 'Ext.ux.desktop.Module',

   id:'cambiodestinoWin',

   /*init : function(){
       this.launcher = {
           text: rhTRelLabTittle,
           iconCls:'auCatColores'
       };
   },*/

   createWindow_cambiodestinoWin : function(){
       var desktop = this.app.getDesktop();
       var win_cambiodestinoWin = desktop.getWindow('cambiodestinoWin');
       if(!win_cambiodestinoWin){
           // Integrar aquí los Stores que ocupará la pantalla
           var store = new MyDesktop.modules.serviciosEspeciales.cambioDestinoSE.canServCambiodestinoStr();
           var store = new MyDesktop.modules.serviciosEspeciales.cambioDestinoSE.canServCentroStr();
           var store = new MyDesktop.modules.serviciosEspeciales.cambioDestinoSE.canServUndGridStr();

           win_cambiodestinoWin = desktop.createWindow({
               height: 518,
               id: 'cambiodestinoWin',
               width: 874,
               title: 'Modificacion de Destinos S.E.',
               maximizable: false,
               minimizable: true,
               razisable: false,
               iconCls:'modificacionDestinoSE',
               dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.Msg.wait("Espere efectuando la modificación ... ", 'Modificación de Destinos S.E.');
                                Ext.Ajax.request({
                                    url: '../../carbookBck/json/alDestinosEspeciales.php?',
                                    params: {
                                        alDestinosEspecialesActionHdn: 'addCambioDeSE',
                                        modDestinoDestinoCmb: Ext.getCmp('modDestinoDestinoCmb').getValue()
                                    },
                                    success: function(response) {
				                                var unidadesStr = Ext.StoreMgr.lookup('canServUndGridStr');
				                                unidadesStr.load({
				                                    params: {
				                                        alDestinosEspecialesActionHdn:'getUndTmp'
				                                    },
				                                    callback: function(records, operation, success){
								                                Ext.getCmp('canServCancelarBtn').fireHandler();
								                                Ext.Msg.alert('Modificacion de Destinos S.E.','Modificación efectuada.');                                    		
				                                    }
				                                });
                                    }
                                });
                            },
                            id: 'canServAceptarBtn',
                            iconCls: 'ux-btnSave',
                            text: 'Aceptar'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.Ajax.request({
                                    url: '../../carbookBck/json/alDestinosEspeciales.php?',
                                    params: {
                                        alDestinosEspecialesActionHdn: 'DltCancTmp'
                                    }
                                });

                                var unidadesStr = Ext.StoreMgr.lookup('canServUndGridStr');
                                unidadesStr.load({
                                    params: {
                                        alDestinosEspecialesActionHdn:'getUndTmp'
                                    }
                                });

                                Ext.getCmp('canServCentroCmb').setValue('');
                                Ext.getCmp('modDestinoDestinoCmb').setValue('');
                                Ext.getCmp('canServAceptarCmb').disable();
                                Ext.getCmp('canServAceptarBtn').disable();
                            },
                            id: 'canServCancelarBtn',
                            iconCls: 'ux-btnDel',
                            text: 'Cancelar'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    height: 450,
                    id: 'canServFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'combobox',
                            x: 20,
                            y: 20,
                            id: 'canServCentroCmb',
                            width: 440,
                            fieldLabel: 'C. Distribucion',
                            name: 'canServCentroCmb',
                            allowBlank: false,
                            enableKeyEvents: true,
                            displayField: 'distDesc',
                            fieldCls: 'upperCase-field',
                            forceSelection: true,
                            queryMode: 'local',
                            store: 'canServCentroStr',
                            valueField: 'distribuidorCentro',
                            listeners: {
                                blur: {
                                    fn: function(component, e, eOpts) {
                                        if(Ext.getCmp('canServCentroCmb').getValue() !== null){
                                            Ext.getCmp('canServAceptarCmb').enable();

                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'combobox',
                            x: 20,
                            y: 60,
                            id: 'modDestinoDestinoCmb',
                            width: 440,
                            fieldLabel: 'Cambio Destino',
                            name: 'modDestinoDestinoCmb',
                            fieldCls: 'upperCase-field',
                            allowBlank: false,
                            displayField: 'distDesc',
                            forceSelection: true,
                            queryMode: 'local',
                            store: 'canServCambiodestinoStr',
                            valueField: 'distribuidorCentro'
                        },
                        {
                            xtype: 'gridpanel',
                            x: 20,
                            y: 100,
                            height: 330,
                            id: 'canServUnidadesGrd',
                            title: 'Unidades',
                            store: 'canServUndGridStr',
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    items: [
                                        {
                                            xtype: 'button',
                                            handler: function(button, e) {
                                                var unidadesStore = Ext.data.StoreManager.lookup('canServUndGridStr');
                                                Ext.apply(Ext.getCmp('canServAvanzadaTxt'), {allowBlank: true}, {});
                                                Ext.apply(Ext.getCmp('canServVinDsp'), {allowBlank: true}, {});
                                                Ext.apply(Ext.getCmp('canServSimboloDsp'), {allowBlank: true}, {});
                                                Ext.apply(Ext.getCmp('canServDistribuidorDsp'), {allowBlank: true}, {});
                                                Ext.apply(Ext.getCmp('canServOrigenDsp'), {allowBlank: true}, {});
                                                Ext.apply(Ext.getCmp('canServDestinoDsp'), {allowBlank: true}, {});

                                                var newRecord = {
                                                    avanzada:'',
                                                    vin:'',
                                                    simbolo:'',
                                                    distribuidor:'',
                                                    origen:'',
                                                    destino:''
                                                };

                                                var edit = Ext.getCmp('canServUnidadesGrd').getPlugin('canServRowEditing');
                                                edit.cancelEdit();
                                                unidadesStore.add(newRecord);
                                                edit.startEdit(unidadesStore.getCount()-1,0);
                                                Ext.getCmp('canServAvanzadaTxt').focus();
                                            },
                                            id: 'canServAceptarCmb',
                                            iconCls: 'ux-btnSave',
                                            text: 'Agregar'
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'button',
                                            handler: function(button, e) {
                                                var window = Ext.WindowManager.get('canServWin');
                                                var recToRemove = Ext.getCmp('canServUnidadesGrd').getSelectionModel().getSelection()[0];

                                                if(recToRemove){
                                                    Ext.Msg.confirm(window.title, 'Deseas Eliminar: ' + recToRemove.get('avanzada'), function(btn){
                                                        if(btn === 'yes'){
                                                            //Ext.getCmp('asigEspecialesAgregarBtn').enable();
                                                            //Ext.getCmp('asigEspecialesUnidadesGrd').getStore().remove(recToRemove);

                                                            Ext.Ajax.request({
                                                                url: '../../carbookBck/json/alDestinosEspeciales.php?',
                                                                params: {
                                                                    alDestinosEspecialesActionHdn:'DltUndTmpMD',
                                                                    canServAvanzadaTxt: recToRemove.get('avanzada')
                                                                }});

                                                                var unidadesStr = Ext.StoreMgr.lookup('canServUndGridStr');
                                                                unidadesStr.load({
                                                                    params: {
                                                                        alDestinosEspecialesActionHdn:'getUndTmpMD'
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                            },
                                            id: 'canServCancelarCmb',
                                            iconCls: 'ux-btnDel',
                                            text: 'Eliminar'
                                        }
                                    ]
                                }
                            ],
                            plugins: [
                                Ext.create('Ext.grid.plugin.RowEditing', {
                                    pluginId: 'canServRowEditing',
                                    listeners: {
                                        edit: {
                                            fn: function(editor, context, eOpts) {
                                                // Valida que no este repetida la funcion
                                                var index = 0;
                                                var recToRemove = Ext.getCmp('canServUnidadesGrd').getSelectionModel().getSelection()[0];
                                                context.grid.getStore().each(function(validaRepetido){
                                                    index = context.grid.getStore().indexOf(validaRepetido);
                                                    console.log(validaRepetido.get('avanzada'));

                                                    if (index != context.rowIdx && Ext.getCmp('canServAvanzadaTxt').getValue() == validaRepetido.get('avanzada')){
                                                        Ext.getCmp('canServUnidadesGrd').getStore().remove(recToRemove);

                                                        editor.startEdit(context.rowIdx,0);
                                                        context.record.reject();
                                                        Ext.Msg.alert("Cancelacion Servicio Especial","Launidad ya esta capturada Previamente");

                                                    }

                                                });
                                            }
                                        },
                                        validateedit: {
                                            fn: function(editor, context, eOpts) {
                                                //valida blancos antes de actualizar
                                                Ext.apply(Ext.getCmp('canServAvanzadaTxt'), {allowBlank: false}, {});

                                                //---------------------------------------------------------------------------
                                                var talonesFrm = Ext.getCmp('canServFrm').getForm();
                                                if(!talonesFrm.isValid())
                                                {
                                                    Ext.Msg.alert("Servicios Especiales","Campos Requeridos");
                                                }
                                            }
                                        },
                                        canceledit: {
                                            fn: function(editor, context, eOpts) {

                                            }
                                        }
                                    }
                                })
                            ],
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'avanzada',
                                    text: 'avanzada',
                                    editor: {
                                        xtype: 'textfield',
                                        id: 'canServAvanzadaTxt',
                                        name: 'canServAvanzadaTxt',
                                        listeners: {
                                            blur: {
                                                fn: function(component, e, eOpts) {
                                                    var str = Ext.getCmp('canServAvanzadaTxt').getValue();
                                                    var n = str.length;

                                                    console.log("numero de caracteres"+n);

                                                    if(n === 8){
                                                        Ext.Ajax.request({
                                                            url: '../../carbookBck/json/alDestinosEspeciales.php?',
                                                            params: {
                                                                alDestinosEspecialesActionHdn: 'sqlGetCancelacion',
                                                                canServAvanzadaTxt:Ext.getCmp('canServAvanzadaTxt').getValue(),
                                                                canServCentroCmb:Ext.getCmp('canServCentroCmb').getValue()
                                                            },
                                                            success: function(response){
                                                                response = Ext.JSON.decode(response.responseText);
                                                                if (response.records > 0){
                                                                    Ext.getCmp('canServVinDsp').setValue(response.root[0].vin);
                                                                    Ext.getCmp('canServSimboloDsp').setValue(response.root[0].simboloUnidad);
                                                                    Ext.getCmp('canServDistribuidorDsp').setValue(response.root[0].distribuidorUnidad);
                                                                    Ext.getCmp('canServOrigenDsp').setValue(response.root[0].distribuidorOrigen);
                                                                    Ext.getCmp('canServDestinoDsp').setValue(response.root[0].distribuidordestino);
                                                                    Ext.getCmp('canServAceptarBtn').enable();

                                                                    Ext.Ajax.request({
                                                                        url: '../../carbookBck/json/alDestinosEspeciales.php?',
                                                                        params: {
                                                                            alDestinosEspecialesActionHdn: 'addUndTmp',
                                                                            canServVinDsp:Ext.getCmp('canServVinDsp').getValue(),
                                                                            canServAvanzadaTxt:Ext.getCmp('canServAvanzadaTxt').getValue()
                                                                        }
                                                                    });

                                                                    var unidadesStr = Ext.StoreMgr.lookup('canServUndGridStr');
                                                                    unidadesStr.load({
                                                                        params: {
                                                                            alDestinosEspecialesActionHdn:'getUndTmp'
                                                                        }
                                                                    });

                                                                }else{
                                                                    Ext.Msg.alert('Modificacion Destinos S.E.','No Existe la unidad o no corresponde al centro distribucion');
                                                                    Ext.getCmp('canServAvanzadaTxt').setValue('');
                                                                }
                                                            }
                                                        });

                                                    }else if(n === 17){
                                                        var numAvanzada = Ext.getCmp('canServAvanzadaTxt').getValue();
                                                        var numCadena = numAvanzada.substr(9,8);
                                                        console.log(numCadena);

                                                        Ext.Ajax.request({
                                                            url: '../../carbookBck/json/alDestinosEspeciales.php?',
                                                            params: {
                                                                alDestinosEspecialesActionHdn: 'sqlGetCancelacion',
                                                                canServAvanzadaTxt:Ext.getCmp('canServAvanzadaTxt').getValue(),
                                                                canServCentroCmb:Ext.getCmp('canServCentroCmb').getValue()
                                                            },
                                                            success: function(response){
                                                                response = Ext.JSON.decode(response.responseText);
                                                                if (response.records > 0){
                                                                    Ext.getCmp('canServVinDsp').setValue(response.root[0].vin);
                                                                    Ext.getCmp('canServSimboloDsp').setValue(response.root[0].simboloUnidad);
                                                                    Ext.getCmp('canServDistribuidorDsp').setValue(response.root[0].distribuidorUnidad);
                                                                    Ext.getCmp('canServOrigenDsp').setValue(response.root[0].distribuidorOrigen);
                                                                    Ext.getCmp('canServDestinoDsp').setValue(response.root[0].distribuidordestino);
                                                                    Ext.getCmp('canServAceptarBtn').enable();

                                                                    Ext.Ajax.request({
                                                                        url: '../../carbookBck/json/alDestinosEspeciales.php?',
                                                                        params: {
                                                                            alDestinosEspecialesActionHdn: 'addUndTmp',
                                                                            canServVinDsp:Ext.getCmp('canServVinDsp').getValue(),
                                                                            canServAvanzadaTxt:numCadena
                                                                        }
                                                                    });

                                                                    var unidadesStr = Ext.StoreMgr.lookup('canServUndGridStr');
                                                                    unidadesStr.load({
                                                                        params: {
                                                                            alDestinosEspecialesActionHdn:'getUndTmp'
                                                                        }
                                                                    });
                                                                }else{
                                                                    Ext.Msg.alert('Modificacion Destinos S.E.','No se encontro un servicio especial para la unidad');
                                                                }
                                                            }
                                                        });

                                                    }else if(n === 0){

                                                    }else{
                                                        Ext.Msg.alert('Cancelacion de Servicios','No se encuentra la unidad con estas Caracteristicas');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'vin',
                                    text: 'VIN',
                                    editor: {
                                        xtype: 'displayfield',
                                        id: 'canServVinDsp',
                                        name: 'canServVinDsp',
                                        value: 'Display Field'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 134,
                                    dataIndex: 'simboloUnidad',
                                    text: 'Simbolo',
                                    editor: {
                                        xtype: 'displayfield',
                                        id: 'canServSimboloDsp',
                                        name: 'canServSimboloDsp',
                                        value: 'Display Field'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 169,
                                    dataIndex: 'distribuidorUnidad',
                                    text: 'Distribuidor',
                                    editor: {
                                        xtype: 'displayfield',
                                        id: 'canServDistribuidorDsp',
                                        name: 'canServDistribuidorDsp',
                                        value: 'Display Field'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 129,
                                    dataIndex: 'distribuidorOrigen',
                                    text: 'Origen',
                                    editor: {
                                        xtype: 'displayfield',
                                        id: 'canServOrigenDsp',
                                        name: 'canServOrigenDsp',
                                        value: 'Display Field'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 140,
                                    dataIndex: 'distribuidordestino',
                                    text: 'destino',
                                    editor: {
                                        xtype: 'displayfield',
                                        id: 'canServDestinoDsp',
                                        name: 'canServDestinoDsp',
                                        value: 'Display Field'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
                beforeclose: {
                    fn: function(panel, eOpts) {
                        if(panel.allowDestroy){
                            delete panel.allowDestroy;
                            return true;
                        }

                        Ext.Msg.confirm(this.title,'¿Está seguro que desea salir de<br> '+ this.title+'?',function(btn){

                            if(btn == 'yes'){
                                panel.allowDestroy = true;
                                panel.destroy();
                            }
                        }, panel);

                        return false;
                    }
                },
                boxready: {
                    fn: function(component, width, height, eOpts) {
                        var elements = Ext.ComponentQuery.query('[xtype=textfield]').concat(Ext.ComponentQuery.query('[xtype=combobox]')).concat(Ext.ComponentQuery.query('[xtype=numberfield]'));

                        //Por cada elemento lo suscribo a un evento keypress, con un eventHandler llamado nextFocus
                        Ext.each(elements, function(el){
                            el.on('keypress', nextFocus);
                        });

                        //Event Handler que se encarga de recorrer y hacer FOCO a los elementos del formulario.
                        function nextFocus(field, e){
                            var actual = field;

                            if (e.getKey() === Ext.EventObject.ENTER) { //Si el input del teclado es Enter...
                                while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){//Recorro los elementos. Si existe el elemento siguiente y es Visible (por los hiddens fields).
                                    actual = actual.next();
                                }
                                if(actual.next()){
                                    actual.next().focus();
                                } else {//Si estoy en el elemento final regreso al primero...NO SÉ si sea el comportamiento requerido.

                                    while(actual.prev()){//Recorro mientras exista elemento
                                        actual = actual.prev();
                                    }
                                    if(actual.isVisible()){//Si no es un hidden field le hago foco
                                        actual.focus();
                                    } else {// de lo contrario vuelvo a recorrer hasta encontrar algún elemento visible
                                        while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){
                                            actual = actual.next();
                                        }
                                        actual.next().focus();
                                    }
                                }
                            }
                        }
                    }
                },
                render: {
                    fn: function(component, eOpts) {
                        Ext.getCmp('canServAceptarCmb').disable();
                        Ext.getCmp('canServAceptarBtn').disable();
                    }
                }
            }
        });
      }
      win_cambiodestinoWin.show();
      return win_cambiodestinoWin;
  }
});
