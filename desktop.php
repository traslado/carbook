
<?php
    date_default_timezone_set ("America/Mexico_City");

    require("segUsuConfig.php");
    require("segUsuariosMenu.php");

    session_start();
    
    if ((!isset($_SESSION['idUsuario'])) || (isset($_SESSION['idUsuario']) && $_SESSION['idUsuario'] == ""))
    {
        header('Location:index.html');
    }
    else
    {
        generaOpciones();
        generaArchivoApp();
    }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Carbook - <?php echo $_SESSION['usuCompania']." -".$_SESSION['usuCiaAut']?></title>

    <link rel="stylesheet" type="text/css" href="css/desktop.css" />
    <link rel="shortcut icon" href="images/traNormales.png" />
    <link rel="icon" href="images/traNormales.png" />
    <script type="text/javascript" src="js/funcionesGlobales.js"></script>

    <!-- GC -->

    <!-- <x-compile> -->
    <!-- <x-bootstrap> -->
    <script type="text/javascript">
        var inactivityTime = function () {
            var t;
            window.onload = resetTimer;
            // DOM Events
            document.onmousemove = resetTimer;
            document.onkeypress = resetTimer;

            function logout() {
                var lb_reload = false;
                if (myDesktopApp.desktop.windows.length > 0) {
                    var arrWinDesc = Ext.Array.pluck(myDesktopApp.desktop.windows.items, 'id'),
                        arrWinBloq = ['asigT12AvanzadaWin', 'asignacionUnidadesWin', 'susTalonesWin', 'asigEspecialesWin', 'trModificacionTalonesViajeWin', 'cambioCentroWin', 'cambiodestinoWin', 'canServWin', 'trCanTalonesWin', 'trap001Win', 'alCambioDistribuidorWin', 'trapCambioEstatusWin', 'entradaPatioWin', 'trap010Win','trap484Win','gastosViajeVacioWin'];
                    Ext.each(arrWinDesc, function(itemWin, idx) {
                        if (arrWinBloq.indexOf(itemWin) >-1) {
                            lb_reload = true;
                            return false;
                        }
                    });
                }
                if (lb_reload) {
                    if (document.hasFocus()) {
                        Ext.Ajax.request({
                            url: '../../carbookBck/json/segLogin.php',
                            waitTitle: 'Carbook',
                            waitMsg: 'Actualizando sesión por inactividad prolongada.',
                            params: { idUsuario: usuValor },
                            success: function(response) {
                                response = response.responseText;
                                if (Ext.isEmpty(response)) {
                                    alert("La sesión ha caducado. Se reiniciara la aplicación");
                                    location.reload();
                                } else {
                                    response = Ext.JSON.decode(response);
                                    if (response.success) {
                                        inactivityTime();
                                        resetTimer();
                                    } else {
                                        alert("La sesión ha caducado. Se reiniciara la aplicación");
                                        location.reload();
                                    }
                                }
                            }
                        });
                    }
                } else {
                    Ext.Msg.alert('Carbook', "La sesión ha caducado. Se reiniciara la aplicación", function(btn) {
                        Ext.Ajax.request({
                            url: 'segInicioSesion.php',
                            params: {
                                ciaSesVal: ciaSesVal,
                                ciaAutVal: ciaAutVal,
                                usuValor: usuValor,
                                usuClave: usuClave,
                                usuNombre: usuNombre,
                                temaApp: temaApp,
                                tipoUsuario: tipoUsuario,
                                loginHdn: '2'
                            },
                            success: function(form, action) {
                                gb_refreshCia = false;
                                location.reload();
                            }
                        });
                    });
                }
            }

            function resetTimer() {
                clearTimeout(t);
                t = setTimeout(logout, 1380000);
                // 1000 milisec = 1 sec
            }
        };
        var gb_refreshCia = true;
        var inactivityCia = function () {
            function refreshSesCia() {
                // console.log('focus segInicioSesion');
                if (document.hasFocus() == false) {
                    gb_refreshCia = true;
                }
                if (document.hasFocus() && gb_refreshCia) {
                    Ext.Ajax.request({
                        url: 'segInicioSesion.php',
                        params: {
                            ciaSesVal: ciaSesVal,
                            ciaAutVal: ciaAutVal,
                            usuValor: usuValor,
                            usuClave: usuClave,
                            usuNombre: usuNombre,
                            temaApp: temaApp,
                            tipoUsuario: tipoUsuario,
                            loginHdn: '2'
                        },
                        success: function(form, action) {
                            gb_refreshCia = false;
                        }
                    });
                }
            }

            var task = Ext.TaskManager.start({
                run: refreshSesCia,
                interval: 1000
            });
        };

        inactivityTime();
        // para valores globales al seleccionar.
        <?php
            echo "      var ciaSesVal     = '" . $_SESSION['usuCompania'] . "';\n";
            echo "      var ciaAutVal     = '" . $_SESSION['usuCiaAut'] . "';\n";
            echo "      var accesosDirArr = " . getUsuarioConfig() . ";\n";
            echo "      var quickStartArr = " . getUsuarioConfig() . ";\n";
            echo "      var usuNombre = '" . $_SESSION['nombreUsr'] . "';\n";
            echo "      var usuValor  = '" . $_SESSION['idUsuario'] . "';\n";
            echo "      var usuClave  = '" . $_SESSION['usuario'] . "';\n";
            echo "      var fondoEsc  = '" . $_SESSION['wallpaper'] . "';\n";
            echo "      var temaApp   = '" . $_SESSION['theme'] . "';\n";
            echo "      var tipoUsuario   = '" . $_SESSION['tipoUsuario'] . "';\n";
                
        ?>
    </script>
    
    <script type="text/javascript" src="lib/ext-4.2.1/examples/shared/include-ext.js"></script>

<!--<style type="text/css">

@import url(https://fonts.googleapis.com/css?family=Poiret+One);
@import url(https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons.min.css);

.widget {
  position: absolute;
  opacity: 0.8;
  top:10%;
  left:25%; 
  display: flex;
  height: 40px;
  width: 350px;
  transform: translate(-80%, -80%);
  flex-wrap: wrap;
  cursor: default;
  border-radius: 20px;
}

.widget .cronometro {
  flex: 0 0 55%;
  height: 90%;
  background: #080705;
  border-bottom-left-radius: 10px;
  border-top-left-radius: 10px;
  display: flex;
  align-items: center;
  color: white;
}
.widget .cronometro .description {
  flex: 0 100%;
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  justify-content: center;
}
.widget .cronometro .description .ms2 {
  text-transform: uppercase;
  font-size: 20px;
  align-items: center;
  font-weight: 100;
}
.widget .tiempo {
  flex: 0 0 30%;
  height: 90%;
  background: #75B5FF;
  border-bottom-right-radius: 10px;
  border-top-right-radius: 10px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  color: white;
  font-size: 30px;
  font-weight: 800;
}



</style>

 <script type="text/javascript" src="cromometro.js"></script>  
<article class="widget">
  <div class="cronometro">
    <div class="description">    
      <div class="ms2"> &nbsp;Tiempo de Sesi&oacuten:</div>
    </div>
  </div>
  <div class="tiempo" id="ms" ></div>
</article>
-->

<?php If ($_SESSION['tipoUsuario'] != '1')
{
?>
    <script type="text/javascript" src="lib/ext-4.2.1/examples/shared/options-toolbar.js"></script>

<?php } ?>


    <!-- </x-bootstrap> -->
    <script type="text/javascript">

    inactivityCia();
    document.onkeydown = function(e){ 
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 116 ||(tecla == 17 && tecla == 91)) return false;

    };
    function myFunction(){
        var r = "Se perderan todos lo cambios no guardados.";
        return r;
    }
    function monitorUI() {
        // 
    }
    Ext.Loader.setPath({
        'Ext.ux.desktop': 'js',
        MyDesktop: ''
    });

<?php
    echo "      Ext.require('MyDesktop.App" . $_SESSION['idUsuario'] . "');\n";
?>
        var myDesktopApp;
        Ext.onReady(function () {
<?php
    echo "            myDesktopApp = new MyDesktop.App" . $_SESSION['idUsuario'] . "();\n";
?>
            setTimeout(function(){
                Ext.get('loading').hide();
                Ext.get('loading-mask').fadeOut({hide:true});
            }, 250);
        });
    </script>
    <!-- </x-compile> -->
</head>

<body onbeforeunload="return myFunction();">
    <div id="loading-mask"></div>
    <div id="loading">
        <div class="loading-indicator">
            Sistema Carbook ...
        </div>
    </div>

    <a href="http://www.tracomex.com.mx" target="_blank" alt="Powered by tracomex S.A de C.V"
       id="poweredby"><div></div></a>
</body>
</html>
