/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.predimentosPDF13', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Generacion Pedimento',
            iconCls: 'traNormales',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'GeneracionP',
									iconCls:'traNormales',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('shipperWin');
										win = module.createWindow_shipperWin();
									},
									scope: this,
									windowId: 'shipperWin'
								}
				]
            }
        };
    }
});