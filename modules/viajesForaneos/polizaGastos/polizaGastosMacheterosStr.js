/*
 * File: app/store/polizaGastosMacheterosStr.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.viajesForaneos.polizaGastos.polizaGastosMacheterosStr', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'polizaGastosMacheterosStr',
            proxy: {
                type: 'ajax',
                reader: {
                    type: 'json',
                    root: 'root'
                }
            },
            fields: [
                {
                    name: 'cantidad'
                },
                {
                    name: 'concepto'
                },
                {
                    name: 'importe'
                }
            ],
            listeners:{
                load: {
                    fn: function(store, records, successful, eOpts){
                       
                    }
                },
                add: {
                    fn: function(store, record, operation, modifiedFieldNames, eOpts) {
                        
                    }
                },
                update: {
                    fn: function(store, record, operation, modifiedFieldNames, eOpts) {
                        if(modifiedFieldNames[0] == "importe"){
                            var total = 0.00;
                            store.each(function(rec){
                                total += parseFloat(rec.data.importe);
                            });
                            Ext.getCmp('polizaGastosMacheterosManiobrasTotalDsp').setValue(total.toFixed(2));

                            Ext.getCmp('polizaGastosMacheterosManiobrasMacheterosDsp').setValue(
                                (total - parseFloat(Ext.getCmp('polizaGastosMacheterosManiobrasTaxiDsp').getValue())).toFixed(2));
                        }
                    }
                }
            }
        }, cfg)]);
    }
});