/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.ViajesTractorModel13', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Viajes Tractor',
            iconCls: 'tractorModule',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Pre - Viaje',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catPreViajesWin');
										win = module.createWindow_catPreViajesWin();
									},
									scope: this,
									windowId: 'catPreViajesWin'
								},
								{
									text: 'Cancelacion Pre-Viaje',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('cancelacionViajesWin');
										win = module.createWindow_cancelacionViajesWin();
									},
									scope: this,
									windowId: 'cancelacionViajesWin'
								},
								{
									text: 'Cancelacion Asignacion',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('cancelacionAsignacionWin');
										win = module.createWindow_cancelacionAsignacionWin();
									},
									scope: this,
									windowId: 'cancelacionAsignacionWin'
								},
								{
									text: 'Cancelacion Gastos',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trCancelacionGastosWin');
										win = module.createWindow_trCancelacionGastosWin();
									},
									scope: this,
									windowId: 'trCancelacionGastosWin'
								},
								{
									text: 'Modificacion Viajes',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trModificacionTalonesViajeWin');
										win = module.createWindow_trModificacionTalonesViajeWin();
									},
									scope: this,
									windowId: 'trModificacionTalonesViajeWin'
								},
								{
									text: 'Asignacion Unidades',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('asignacionUnidadesWin');
										win = module.createWindow_asignacionUnidadesWin();
									},
									scope: this,
									windowId: 'asignacionUnidadesWin'
								},
								{
									text: 'Recepcion Viajes',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('recepcionViajeWin');
										win = module.createWindow_recepcionViajeWin();
									},
									scope: this,
									windowId: 'recepcionViajeWin'
								},
								{
									text: 'Liberar Tractores',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('libTractorWin');
										win = module.createWindow_libTractorWin();
									},
									scope: this,
									windowId: 'libTractorWin'
								},
								{
									text: 'Impresion Bitacora',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('reBitacoraWin');
										win = module.createWindow_reBitacoraWin();
									},
									scope: this,
									windowId: 'reBitacoraWin'
								},
								{
									text: 'Asignacion Trac. 12',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('asigT12AvanzadaWin');
										win = module.createWindow_asigT12AvanzadaWin();
									},
									scope: this,
									windowId: 'asigT12AvanzadaWin'
								},
								{
									text: 'Modificacion Pre Viaje',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('modPreviajeWin');
										win = module.createWindow_modPreviajeWin();
									},
									scope: this,
									windowId: 'modPreviajeWin'
								},
								{
									text: 'Primera Asignacion SE',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('asigEspecialesWin');
										win = module.createWindow_asigEspecialesWin();
									},
									scope: this,
									windowId: 'asigEspecialesWin'
								}
				]
            }
        };
    }
});