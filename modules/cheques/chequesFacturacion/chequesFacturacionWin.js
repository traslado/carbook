/*
 * File: app/view/chequesFacturacionWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.cheques.chequesFacturacion.chequesFacturacionWin', {
    extend: 'Ext.ux.desktop.Module',
    
    id:'chequesFacturacionWin',

    /*init : function(){
        this.launcher = {
            text: rhTRelLabTittle,
            iconCls:'auCatColores'
        };
    },*/
    
    createWindow_chequesFacturacionWin : function(){
        var desktop = this.app.getDesktop();
        var win_chequesFacturacionWin = desktop.getWindow('chequesFacturacionWin');
        if(!win_chequesFacturacionWin){
            // Integrar aquí los Stores que ocupará la pantalla
            var store = new MyDesktop.modules.cheques.chequesFacturacion.chequesFacturacionDireccionStr();
            var store = new MyDesktop.modules.cheques.chequesFacturacion.chequesFacturacionCtaBancariasStr();
            var store = new MyDesktop.modules.cheques.chequesFacturacion.chequesFacturacionChequeStr();
            var store = new MyDesktop.modules.cheques.chequesFacturacion.chequesFacturacionSigChequeStr();
            var store = new MyDesktop.modules.cheques.chequesFacturacion.chequesFacturacionFacturacionStr();         
            
            win_chequesFacturacionWin = desktop.createWindow({
                height: 301,
                id: 'chequesFacturacionWin',
                width: 554,
                layout: {
                    type: 'fit'
                },
                title: 'Generacion de Facturacion de Cheques',
                maximizable: true,
                minimizable: true,
                
                dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'chequesFacturacionToolBr',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var form = Ext.getCmp('chequesFacturacionFrm');
                                var win  = Ext.WindowMgr.get('chequesFacturacionWin');
                                if(form.getForm().isValid()){
                                    if(Ext.getCmp('chequesFacturacionPorPagarDsp').getValue() < 1){
                                        Ext.Msg.alert(win.title, 'No hay Unidades por Pagar');
                                        return;
                                    }
                                    var ctaRec = Ext.getCmp('chequesFacturacionCtaContableCmb').getPicker().getSelectionModel().getSelection()[0];
                                    form.getForm().submit({
                                        params:{
                                            facturacionIdBanco: ctaRec.get('idBanco')
                                        },
                                        success: function(form, action){
                                            Ext.Msg.alert(win.title, action.result.successMessage);
                                            Ext.getCmp('chequesFacturacionCancelarBtn').fireHandler();
                                            window.open('../../carbookBck/json/chCheques.php?chChequesActionHdn=printCheque&facturacionIdCheque=' + action.result.idCheque);
                                        },
                                        failure: function(form, action){
                                            var errors = action.result.errors;
                                            var simb = '';
                                            if(errors && errors.length > 0){
                                                errors.forEach(function(error){
                                                    simb += '</br>' + error.simbolo + ': ' + '$ ' + error.importe;
                                                });
                                            }
                                            Ext.Msg.alert(win.title, (simb) ? (action.result.errorMessage + '</br>' + simb ): action.result.errorMessage);
                                        }
                                    });
                                    Ext.Msg.wait('Generando el Cheque...', win.title);
                                } else{
                                    Ext.Msg.alert(win.title, 'Faltan Campos Requeridos o contienen el Formato incorrecto');
                                }
                            },
                            cls: 'ux-btnSave',
                            id: 'chequesFacturacionAceptarBtn',
                            text: ''
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var form = Ext.getCmp('chequesFacturacionFrm');
                                form.getForm().reset();
                                Ext.getCmp('chequesFacturacionFolioTxt').setReadOnly(true);
                            },
                            cls: 'ux-btnCancel',
                            id: 'chequesFacturacionCancelarBtn',
                            text: ''
                        }
                    ]
                },
                {
                    xtype: 'form',
                    dock: 'top',
                    height: 242,
                    id: 'chequesFacturacionFrm',
                    layout: 'absolute',
                    title: '',
                    url: '../../carbookBck/json/chCheques.php?chChequesActionHdn=addCheque',
                    items: [
                        {
                            xtype: 'combobox',
                            x: 20,
                            y: 20,
                            id: 'chequesFacturacionCtaContableCmb',
                            width: 340,
                            fieldLabel: 'Cta. Contable',
                            msgTarget: 'side',
                            name: 'facturacionCtaBancaria',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            displayField: 'descCuenta',
                            forceSelection: true,
                            queryMode: 'local',
                            store: 'chequesFacturacionCtaBancariasStr',
                            valueField: 'idCuentaBancaria',
                            listeners: {
                                select: {
                                    fn: function(combo, records, eOpts) {
                                        var sigChequeStr = Ext.StoreMgr.get('chequesFacturacionSigChequeStr');
                                        sigChequeStr.load({
                                            params:{
                                                facturacionIdCtaBancaria: combo.getValue()
                                            },
                                            callback: function(records, operation, success){
                                                if(!records){
                                                    Ext.Msg.alert('ERROR', 'ERROR');
                                                    combo.reset();
                                                    return;
                                                }
                                                Ext.getCmp('chequesFacturacionFolioTxt').setValue(records[0].get('sigCheque')).setReadOnly(false);
                                            }
                                        });
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 50,
                            id: 'chequesFacturacionFolioTxt',
                            width: 180,
                            fieldLabel: 'Folio',
                            msgTarget: 'side',
                            name: 'facturacionFolioCheque',
                            readOnly: true,
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            maskRe: /[0-9]/,
                            listeners: {
                                blur: {
                                    fn: function(component, e, eOpts) {
                                        var win = Ext.WindowManager.get('chequesFacturacionWin');
                                        var chequeStr = Ext.StoreMgr.lookup('chequesFacturacionChequeStr');
                                        if(component.getValue() && component.prev().getValue()){
                                            chequeStr.load({
                                                params:{
                                                    facturacionIdCtaBancaria: component.prev().getValue(),
                                                    facturacionFolioCheque: component.getValue()
                                                },
                                                callback: function(records, operation, success){
                                                    if(!records){
                                                        Ext.Msg.alert('ERROR','ERROR');
                                                        component.reset();
                                                    } else if(records.length > 0){
                                                        Ext.Msg.alert(win.title,'No. Cheque: ' + component.getValue() + ' ya Existente', function(){
                                                            component.reset();
                                                            Ext.StoreMgr.lookup('chequesFacturacionSigChequeStr').reload();
                                                        });

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 80,
                            id: 'chequesFacturacionDistribuidorTxt',
                            width: 180,
                            fieldLabel: 'Distribuidor',
                            msgTarget: 'side',
                            name: 'facturacionDistribuidor',
                            fieldStyle: 'text-transform: uppercase',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            enforceMaxLength: true,
                            maskRe: /[0-9a-zA-Z]/,
                            maxLength: 5,
                            minLength: 5,
                            listeners: {
                                change: {
                                    fn: function(field, newValue, oldValue, eOpts) {
                                        var win = Ext.WindowMgr.get('chequesFacturacionWin');
                                        if(newValue.length > 4){
                                            var direccion = '';
                                            var direccionStr = Ext.StoreMgr.lookup('chequesFacturacionDireccionStr');
                                            Ext.getCmp('chequesFacturacionPorPagarDsp').setValue('');
                                            Ext.getCmp('chequesFacturacionDelPeriodoDsp').setValue('');
                                            Ext.getCmp('chequesFacturacionCanceladasDsp').setValue('');
                                            direccionStr.load({
                                                params:{
                                                    catDireccionesDistribuidorHdn: newValue
                                                },
                                                callback: function(records, operation, success){
                                                    if(!records || records.length < 1){
                                                        Ext.Msg.alert(win.title, 'Distribuidor No Existente', function(){
                                                            field.focus();
                                                        });
                                                        field.reset();
                                                    } else{
                                                        //concatena la direccion en 3 renglones y centrada
                                                        var rec = records[0].data;
                                                        direccion =   rec.calleNumero + '</br>';
                                                        direccion +=  rec.municipio + ', ' + rec.estado + '</br>';
                                                        direccion +=  rec.pais + ', C.P. ' + rec.cp;

                                                        Ext.getCmp('chequesFacturacionDireccionDsp').setValue(direccion);

                                                    }
                                                }
                                            });
                                        } else{
                                            Ext.getCmp('chequesFacturacionDireccionDsp').setValue('');
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'displayfield',
                            x: 220,
                            y: 80,
                            id: 'chequesFacturacionDireccionDsp',
                            width: 310,
                            fieldLabel: 'Dirección',
                            labelWidth: 60,
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 380,
                            y: 200,
                            id: 'chequesFacturacionPorPagarDsp',
                            fieldLabel: 'Por Pagar',
                            labelWidth: 80,
                            name: 'facturacionNumUnidades',
                            submitValue: true,
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 200,
                            id: 'chequesFacturacionDelPeriodoDsp',
                            fieldLabel: 'Del Periodo',
                            labelWidth: 80,
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 200,
                            y: 200,
                            id: 'chequesFacturacionCanceladasDsp',
                            fieldLabel: 'Canceladas',
                            labelWidth: 80,
                            value: ''
                        },
                        {
                            xtype: 'datefield',
                            x: 20,
                            y: 130,
                            id: 'chequesFacturacionDelDate',
                            width: 280,
                            fieldLabel: 'Del',
                            msgTarget: 'side',
                            name: 'facturacionFechaInicio',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            submitFormat: 'Y/m/d',
                            listeners: {
                                change: {
                                    fn: function(field, newValue, oldValue, eOpts) {
                                        Ext.getCmp('chequesFacturacionPorPagarDsp').setValue('');
                                        Ext.getCmp('chequesFacturacionDelPeriodoDsp').setValue('');
                                        Ext.getCmp('chequesFacturacionCanceladasDsp').setValue('');
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'datefield',
                            x: 20,
                            y: 160,
                            id: 'chequesFacturacionAlDate',
                            width: 280,
                            fieldLabel: 'Al',
                            msgTarget: 'side',
                            name: 'facturacionFechaFin',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            submitFormat: 'Y/m/d',
                            listeners: {
                                change: {
                                    fn: function(field, newValue, oldValue, eOpts) {
                                        Ext.getCmp('chequesFacturacionPorPagarDsp').setValue('');
                                        Ext.getCmp('chequesFacturacionDelPeriodoDsp').setValue('');
                                        Ext.getCmp('chequesFacturacionCanceladasDsp').setValue('');
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var facturasStr = Ext.StoreMgr.lookup('chequesFacturacionFacturacionStr');
                                var desdeDt = Ext.getCmp('chequesFacturacionDelDate').getValue();
                                var hastaDt = Ext.getCmp('chequesFacturacionAlDate').getValue();
                                var distribuidor = Ext.getCmp('chequesFacturacionDistribuidorTxt').getValue();
                                if(desdeDt && hastaDt && distribuidor){
                                    facturasStr.load({
                                        params:{
                                            chChequesActionHdn: 'getFacturasInfo',
                                            facturacionDistribuidor: distribuidor,
                                            facturacionFechaInicio: '>=' + desdeDt.getFullYear() + '/' + (parseInt(desdeDt.getMonth()) + 1) + '/' + desdeDt.getDate(),
                                            facturacionFechaFin:  '<=' + hastaDt.getFullYear() + '/' + (parseInt(hastaDt.getMonth()) + 1) + '/' + hastaDt.getDate()
                                        },
                                        callback: function(records, operation, success){
                                            Ext.getCmp('chequesFacturacionCanceladasDsp').setValue(records[0].get('canceladas'));
                                            Ext.getCmp('chequesFacturacionDelPeriodoDsp').setValue(records[0].get('delPeriodo'));
                                            Ext.getCmp('chequesFacturacionPorPagarDsp').setValue(records[0].get('porPagar'));
                                        }
                                    });
                                }
                            },
                            x: 320,
                            y: 145,
                            height: 20,
                            id: 'chequesFacturacionGenerarBtn',
                            text: 'Generar'
                        }
                    ]
                }
            ],
            listeners: {
                beforeclose: {
                    fn: function(panel, eOpts) {
                        if(panel.allowDestroy){
                            delete panel.allowDestroy;
                            return true;
                        }
                        var titulo = this.title;
                        Ext.Msg.confirm(titulo,'¿Está seguro que desea salir de<br> '+ titulo+'?',function(btn){
                            if(btn == 'yes'){
                                panel.allowDestroy = true;
                                panel.destroy();
                            }
                        }, panel);
                        return false;
                    }
                },
                render: {
                    fn: function(component, eOpts) {
                        /* Se agrego componentes DATE
                        * Función que simula un TAB en inputFields con un ENTER y recorre como Linked Lists.
                        * REQUERIMIENTOS PREVIOS: settear la propiedad enableKeyEvents de los componentes a utilizar.
                        * PENDIENTES: No funciona en checkboxes por su comportamiento nativo. Desconosco aún si funciona
                        *             con varias Tab-Panel y con demás controles dentros del Form.
                        */
                        //Hago un query con los elementos que sean textfields, numberfields o combobox y obtengo un array de ellos.
                        var elements = Ext.ComponentQuery.query('[xtype=textfield]').concat(Ext.ComponentQuery.query('[xtype=combobox]')).concat(Ext.ComponentQuery.query('[xtype=numberfield]'));
                        elements = elements.concat(Ext.ComponentQuery.query('[xtype=datefield]'));

                        //Por cada elemento lo suscribo a un evento keypress, con un eventHandler llamado nextFocus
                        Ext.each(elements, function(el){
                            el.on('keypress', nextFocus);
                        });

                        //Event Handler que se encarga de recorrer y hacer FOCO a los elementos del formulario.
                        function nextFocus(field, e){
                            var actual = field;
                            if (e.getKey() === Ext.EventObject.ENTER) { //Si el input del teclado es Enter...
                                while(actual.next() && (!actual.next().isVisible() || actual.next().xtype === 'displayfield' || actual.next().tabIndex === -1 ||
                                                        actual.next().xtype === 'button' || actual.next().xtype === 'checkboxfield' || actual.next().isDisabled())){//Recorro los elementos. Si existe el elemento siguiente y es Visible (por los hiddens fields).
                                    actual = actual.next();
                                }
                                if(actual.next()){
                                    actual.next().focus();
                                }
                                else{//Si estoy en el elemento final regreso al primero...NO SÉ si sea el comportamiento requerido.

                                    while(actual.prev()){//Recorro mientras exista elemento
                                        actual = actual.prev();
                                    }
                                    if(actual.isVisible() && actual.xtype !== 'displayfield'){//Si no es un hidden field le hago foco
                                        actual.focus();
                                    }
                                    else{// de lo contrario vuelvo a recorrer hasta encontrar algún elemento visible
                                        while(actual.next() && (!actual.next().isVisible() || actual.next().xtype === 'displayfield' ||
                                                                actual.next().xtype === 'button' || actual.next().xtype === 'checkboxfield' || actual.next().isDisabled())){
                                            actual = actual.next();
                                        }
                                        actual.next().focus();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        }
        win_chequesFacturacionWin.show();
        return win_chequesFacturacionWin;
    }
});