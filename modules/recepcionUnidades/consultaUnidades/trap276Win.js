Ext.define("MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276Win",{extend:"Ext.ux.desktop.Module",id:"trap276Win",createWindow_trap276Win:function(){var a=this.app.getDesktop(),b=a.getWindow("trap276Win");if(!b){new MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276HistoricoStr,new MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276VinStr,new MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276EspecialesStr,new MyDesktop.modules.recepcionUnidades.consultaUnidades.alDstServiciosEspecialesWin;b=a.createWindow({height:531,id:"trap276Win",width:854,layout:"fit",title:"Consulta de Unidades",maximizable:false,minimizable:true,resizable:false,iconCls:'trap276',items:[{xtype:"panel",id:"trap276Pnl",layout:"absolute",title:"",dockedItems:[{xtype:"toolbar",x:80,y:19,dock:"top",id:"trap276tlb",items:[{
	xtype:"button",
			handler:function(a,b){
					var c=Ext.getCmp("trap276AvanzadaTxt").getValue();
					Ext.getCmp("trap276PagTlb").moveFirst(),c&&(c.length>7?(Ext.getCmp("trap276PagTlb").moveFirst(),
					Ext.getCmp("trap276VinCmb").enable(!0),
					Ext.getCmp("trap276AvanzadaTxt").disable(!0),
					Ext.Msg.wait("Espere buscando información ... ", "Consulta de Unidades"),
					Ext.data.StoreManager.lookup("trap276VinStr").load({
							params:{
									alUnidadesAvanzadaHdn:c
							},
							callback:function(a,b,c){
									Ext.Msg.hide();
									if(a)
											if(a.length>1)
													Ext.Msg.alert("Consulta de Unidades","Seleccione un VIN."),
													Ext.getCmp("trap276VinCmb").setReadOnly(!1);
											else if(1===a.length){
													var d=a[0];
													d&&(Ext.getCmp("trap276VinCmb").setValue(d.get("vin")),
													Ext.getCmp("trap276VinCmb").setReadOnly(!0))
											}
									else 
											Ext.Msg.alert("Trap 276 - Consulta de Unidades","No existe Avanzada"),
											Ext.getCmp("trap276CancelarBtn").fireHandler()}})):
											(Ext.Msg.alert("Trap 276 - Consulta de Unidades","Avanzada de 8 caracteres"),
											Ext.getCmp("trap276CancelarBtn").fireHandler()))
			},
			id:"trap276ConsultarBtn",
			iconCls:"ux-btnSearch",
			text:"Consultar"
	},
	{
		xtype:"tbseparator",id:"trap276Sep1"},{xtype:"button",handler:function(a,b){Ext.getCmp("trap276AvanzadaTxt").enable(!0),Ext.getCmp("trap276AvanzadaTxt").setValue(""),Ext.getCmp("trap276VinCmb").enable(!0),Ext.getCmp("trap276Grd").getStore().removeAll(),Ext.getCmp("trap276VinCmb").setValue(""),Ext.getCmp("trap276MarcaDsp").setValue(""),Ext.getCmp("trap276SimboloDsp").setValue(""),Ext.getCmp("trap276ColorDsp").setValue(""),Ext.getCmp("trap276TarifaDsp").setValue(""),Ext.getCmp("trap276DistDsp").setValue(""),Ext.getCmp("trap276DetenidaDsp").hide(),Ext.data.StoreManager.lookup("trap276VinStr").removeAll(),Ext.data.StoreManager.lookup("trap276HistoricoStr").removeAll(),Ext.getCmp("trap276PagTlb").moveFirst(),Ext.getCmp("trap276AvanzadaTxt").focus()},id:"trap276CancelarBtn",iconCls:"ux-btnDel",text:"Cancelar"},{xtype:"tbseparator",id:"trap276Sep2"},{xtype:"button",handler:function(a,b){Ext.getStore("trap276EspecialesStr").load({params:{alDestinosEspecialesActionHdn:"getDestinosEspeciales",alDestinosEspecialesVinTxt:Ext.getCmp("trap276VinCmb").getValue()},callback:function(a,b,c){a?Ext.create("MyDesktop.modules.recepcionUnidades.consultaUnidades.alDstServiciosEspecialesWin",{store:Ext.getStore("trap276EspecialesStr"),posicion:0}).show():Ext.Msg.alert("Consulta de Unidades","No se encontraron unidades")}})},disabled:!0,id:"trap276ServEspBtn",text:"Ser. Esp."},{xtype:"tbseparator",id:"trap276Sep21"}]}],items:[{xtype:"textfield",x:10,y:20,id:"trap276AvanzadaTxt",width:190,fieldLabel:"Avanzada",labelSeparator:" ",name:"trap276AvanzadaTxt",fieldStyle:"text-transform: uppercase;",enableKeyEvents:!0,enforceMaxLength:!0,maxLength:18,listeners:{blur:{fn:function(){
	if (Ext.getCmp('trap276AvanzadaTxt').getValue().length ==18) {
        newValue = Ext.getCmp('trap276AvanzadaTxt').getValue().substring(10,18);
        Ext.getCmp('trap276AvanzadaTxt').setValue(newValue);
    }
    if (Ext.getCmp('trap276AvanzadaTxt').getValue().length ==17) {
        newValue = Ext.getCmp('trap276AvanzadaTxt').getValue().substring(9,17);
        Ext.getCmp('trap276AvanzadaTxt').setValue(newValue);
    }Ext.getCmp("trap276ConsultarBtn").fireHandler()}}}},{xtype:"combobox",x:300,y:20,id:"trap276VinCmb",width:230,fieldLabel:"VIN",labelSeparator:" ",labelWidth:70,name:"trap276VinCmb",fieldStyle:"text-transform: uppercase;",readOnly:!0,enableKeyEvents:!0,displayField:"vin",enableRegEx:!1,forceSelection:!0,queryMode:"local",store:"trap276VinStr",valueField:"vin",listeners:{change:{fn:function(a,b,c,d){if(b&&""!=b){var e=Ext.data.StoreManager.lookup("trap276VinStr"),f=e.findExact(b);if(f>-1){var g=e.getAt(f);Ext.getCmp("trap276MarcaDsp").setValue(g.get("marcaDesc")),Ext.getCmp("trap276SimboloDsp").setValue(g.get("descSimbolo")),Ext.getCmp("trap276ColorDsp").setValue(g.get("descColor")),Ext.getCmp("trap276TarifaDsp").setValue(g.get("descTarifa")),Ext.getCmp("trap276DistDsp").setValue(g.get("descDist")),g.get("detenida")?Ext.getCmp("trap276DetenidaDsp").setValue("Unidad Detenida").show():Ext.getCmp("trap276DetenidaDsp").setValue("").hide();var h=Ext.data.StoreManager.lookup("trap276HistoricoStr");h.load({params:{trap276VinHdn:b},callback:function(a,b,c){var d=0;a?(a.forEach(function(a){("00"==a.get("claveTarifa")||"13"==a.get("claveTarifa"))&&(d+=1)}),d>0?Ext.getCmp("trap276ServEspBtn").enable():Ext.getCmp("trap276ServEspBtn").disable()):Ext.getCmp("trap276ServEspBtn").disable()}})}}}}}},{xtype:"displayfield",x:10,y:50,id:"trap276MarcaDsp",fieldLabel:"Marca",labelSeparator:" ",labelWidth:80,name:"trap276MarcaDsp"},{xtype:"displayfield",x:300,y:50,id:"trap276SimboloDsp",fieldLabel:"Símbolo",labelSeparator:" ",labelWidth:70,name:"trap276SimboloDsp"},{xtype:"displayfield",x:10,y:80,id:"trap276ColorDsp",fieldLabel:"Color",labelSeparator:" ",labelWidth:80,name:"trap276ColorDsp"},{xtype:"displayfield",x:300,y:80,id:"trap276TarifaDsp",fieldLabel:"Tarifa",labelSeparator:" ",labelWidth:70,name:"trap276TarifaDsp"},{xtype:"displayfield",x:10,y:110,id:"trap276DistDsp",fieldLabel:"Distribuidor",labelSeparator:" ",labelWidth:80,name:"trap276DistDsp"},{xtype:"displayfield",x:200,y:140,hidden:!0,id:"trap276DetenidaDsp",value:"Unidad Detenida",fieldCls:"rd-dspColor"},{xtype:"gridpanel",anchor:"98% 98%",x:10,y:160,height:280,id:"trap276Grd",width:720,autoScroll:!0,title:"",disableSelection:!0,store:"trap276HistoricoStr",viewConfig:{id:"trap276GrdVw"},dockedItems:[{xtype:"pagingtoolbar",dock:"bottom",id:"trap276PagTlb",width:360,displayInfo:!0,store:"trap276HistoricoStr"}],columns:[{xtype:"gridcolumn",dataIndex:"centroDistribucion",text:"Centro Distribucion"},{xtype:"gridcolumn",dataIndex:"descClaveMov",text:"Movimiento"},{xtype:"gridcolumn",dataIndex:"tractor",text:"Tractor"},{xtype:"gridcolumn",dataIndex:"folio",text:"Folio"},{xtype:"gridcolumn",dataIndex:"fechaEvento",text:"Fecha"},{xtype:"gridcolumn",dataIndex:"distribuidor",text:"Distribuidor"},{xtype:"gridcolumn",dataIndex:"descTarifa",text:"Tarifa"},{xtype:"gridcolumn",dataIndex:"claveTarifa",text:"Tarifa"},{xtype:"gridcolumn",hidden:!0,dataIndex:"localizacionUnidad",text:"LocalizacionUnidad"},{xtype:"gridcolumn",dataIndex:"localizacionCompleta",text:"Localizacion"}]}]}],listeners:{beforeclose:{fn:function(a,b){if(a.allowDestroy)return delete a.allowDestroy,!0;var c=this.title;return Ext.Msg.confirm(c,"¿Está seguro que desea salir de<br> "+c+"?",function(b){"yes"==b&&(a.allowDestroy=!0,a.destroy())},a),!1}},render:{fn:function(a,b){function c(a,b){var c=a;if(b.getKey()===Ext.EventObject.ENTER){for(;c.next()&&!c.next().isVisible();)c=c.next();if(c.next())c.next().focus();else{for(;c.prev();)c=c.prev();if(c.isVisible())c.focus();else{for(;c.next()&&!c.next().isVisible();)c=c.next();c.next().focus()}}}}var d=Ext.ComponentQuery.query("[xtype=textfield]").concat(Ext.ComponentQuery.query("[xtype=combobox]")).concat(Ext.ComponentQuery.query("[xtype=numberfield]"));Ext.each(d,function(a){a.on("keypress",c)})}}}})}return b.show(),b}});