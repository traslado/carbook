/*
 * File: app/store/segUsuariosIPStr.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.security.segUsuarios.segUsuariosIPStr', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.modules.security.segUsuarios.segUsuariosIPMdl'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'MyDesktop.modules.security.segUsuarios.segUsuariosIPMdl',
            storeId: 'segUsuariosIPStr',
            proxy: {
                type: 'ajax',
                url: '../../carbookBck/json/segUsuarios.php?segUsuariosActionHdn=getIPUsuario',
                reader: {
                    type: 'json',
                    root: 'root',
                    totalProperty: 'records'
                }
            }
        }, cfg)]);
    }
});