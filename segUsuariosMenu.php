<?php
	function generaOpciones(){
		$menuSql = "SELECT mn.nombre AS nombreMenu, sm.modulo, sm.tipoModulo, sm.nombre, md.idModulo, md.idModuloPadre, sm.icono " .
				   "FROM segMenusDetalleTbl md, sisModulosTbl sm, segUsuariosModulosTbl um, segMenusTbl mn " . 
				   "WHERE md.idModulo  = sm.idModulo " .
				   "AND   um.idModulo  = sm.idModulo " .
				   "AND   md.idMenu    = mn.idMenu " .
				   "AND   um.idUsuario = " . $_SESSION['idUsuario'] . " " .
				   "ORDER BY md.secuencial;";

		$menuRs = fn_ejecuta_query($menuSql);
		$menu = $menuRs['root'];

		for ($iInt=0; $iInt < sizeof($menu); $iInt++) { 
			$tempChildOne = "";
			$count = 0;
			if ($menu[$iInt]['tipoModulo'] == 1) {
				$ruta = str_replace('segUsuariosMenu.php', $menu[$iInt]['modulo'] . $_SESSION['idUsuario'] . '.js', __FILE__);
				$archivoStr  = "/*!\n";
				$archivoStr .= "* Ext JS Library 4.0\n";
				$archivoStr .= "* Copyright(c) 2006-2011 Sencha Inc.\n";
				$archivoStr .= "* licensing@sencha.com\n";
				$archivoStr .= "* http://www.sencha.com/license\n";
				$archivoStr .= "*/\n";
				$archivoStr .= "\n";
				$archivoStr .= "Ext.define('" . $menu[$iInt]['nombreMenu'] . "." . $menu[$iInt]['modulo'] . $_SESSION['idUsuario'] . "', {\n";
				$archivoStr .= "	extend: 'Ext.ux.desktop.Module',\n";
				$archivoStr .= "\n";
				$archivoStr .= "    init : function() {\n";
				$archivoStr .= "\n";
				$archivoStr .= "        this.launcher = {\n";
				$archivoStr .= "            text: '" . $menu[$iInt]['nombre'] . "',\n";
				$archivoStr .= "            iconCls: '" . $menu[$iInt]['icono'] . "',\n";
				$archivoStr .= "            handler: function() {\n";
				$archivoStr .= "                return false;\n";
				$archivoStr .= "            },\n";
				$archivoStr .= "            menu: {\n";
				$archivoStr .= "                items: \n";
				$archivoStr .= "				[\n";
				//ITEMS
				for ($jInt=0; $jInt < sizeof($menu); $jInt++) {
					if ($menu[$jInt]['idModuloPadre'] != 1 && ($menu[$jInt]['idModuloPadre'] == $menu[$iInt]['idModulo'])) {
						if ($menu[$jInt]['tipoModulo'] == 2) {
							if ($count != 0) {
								$tempChildOne .= ",\n";
							}
							$tempChildOne .= "					{\n";
							$tempChildOne .= "						text: '" . $menu[$jInt]['nombre'] . "',\n";
							$tempChildOne .= "						iconCls: '" . $menu[$jInt]['icono'] . "',\n";
							$tempChildOne .= "						menu:\n";
							$tempChildOne .= "						{\n";
							$tempChildOne .= "							items:\n";
							$tempChildOne .= "							[\n";
							$tempChildOne .= checkLevelTwo($menu, $jInt);
							$tempChildOne .= "							]\n";
							$tempChildOne .= "						}\n";
							$tempChildOne .= "					}";
							$count += 1;
						} else {
							if ($count != 0) {
								$tempChildOne .= ",\n";
							}
							$tempChildOne .= "								{\n";
							$tempChildOne .= "									text: '" . $menu[$jInt]['nombre'] . "',\n";
							$tempChildOne .= "									iconCls:'" . $menu[$jInt]['icono'] . "',\n";
							$tempChildOne .= "									handler : function(button, event) {\n";
							$tempChildOne .= "										var me = this; \n";
							$tempChildOne .= "										module = me.app.getModule('" . $menu[$jInt]['modulo'] . "');\n";
							$tempChildOne .= "										win = module.createWindow_" . $menu[$jInt]['modulo'] . "();\n";
							$tempChildOne .= "									},\n";
							$tempChildOne .= "									scope: this,\n";
							$tempChildOne .= "									windowId: '" . $menu[$jInt]['modulo'] . "'\n";
							$tempChildOne .= "								}";
							$count += 1;
						}
					}
				}
				$archivoStr .= $tempChildOne."\n";
				$archivoStr .= "				]\n";
				$archivoStr .= "            }\n";
				$archivoStr .= "        };\n";
				$archivoStr .= "    }\n";
				$archivoStr .= "});";

				$archivo = fopen($ruta, "w");
				fwrite($archivo, $archivoStr);
				fclose($archivo);
			}
		}
	}

	function checkLevelTwo($menu, $parent){
		$temp = "";
		$count = 0;

    	for ($iInt=0; $iInt < sizeof($menu); $iInt++) { 
    		if ($menu[$iInt]['idModuloPadre'] == $menu[$parent]['idModulo']) {
    			if ($menu[$iInt]['tipoModulo'] == 2) {
    				if ($count > 0) {
						$temp .= ",\n";
					}
    				$temp .= "					{\n";
					$temp .= "						text: '" . $menu[$iInt]['nombre'] . "',\n";
					$temp .= "						iconCls: '" . $menu[$iInt]['icono'] . "',\n";
					$temp .= "						menu:\n";
					$temp .= "						{\n";
					$temp .= "							items:\n";
					$temp .= "							[\n";
					$temp .= checkLevelTwo($menu, $iInt);
					$temp .= "							]\n";
					$temp .= "						}\n";
					$temp .= "					}";
					$count += 1;
    			} else {
    				if ($count > 0) {
						$temp .= ",\n";
					}
    				$temp .= "								{\n";
					$temp .= "									text: '" . $menu[$iInt]['nombre'] . "',\n";
					$temp .= "									iconCls:'" . $menu[$iInt]['icono'] . "',\n";
					$temp .= "									handler : function(button, event) {\n";
					$temp .= "										var me = this; \n";
					$temp .= "										module = me.app.getModule('" . $menu[$iInt]['modulo'] . "');\n";
					$temp .= "										win = module.createWindow_" . $menu[$iInt]['modulo'] . "();\n";
					$temp .= "									},\n";
					$temp .= "									scope: this,\n";
					$temp .= "									windowId: '" . $menu[$iInt]['modulo'] . "'\n";
					$temp .= "								}";
					$count += 1;
    			}
    		}
    	}
    	return $temp."\n";
	}
	
	function generaArchivoApp()
	{
		$appSql = "SELECT mn.nombre, sm.modulo, sm.nombreMenuNavegacion, sm.banderasEjecucion, sm.tipoModulo " . 
				   "FROM segMenusDetalleTbl md, sisModulosTbl sm, segUsuariosModulosTbl um, segMenusTbl mn " . 
				   "WHERE md.idModulo   = sm.idModulo " .
				   "AND   um.idModulo   = sm.idModulo " .
				   "AND   md.idMenu     = mn.idMenu " .
				   "AND   um.idUsuario  = " . $_SESSION['idUsuario'] . " " .
				   "AND   sm.tipoModulo IN (1, 3) " . 
				   "ORDER BY md.secuencial;";
	
		$appRs = fn_ejecuta_query($appSql);
		$appArr = $appRs['root'];
		$totalRegistros = sizeof($appArr);
		$modulosImportar = "";
		$modulosCrear    = "";
		$menuesImportar  = "";
		$menuesCrear     = "";
		$aplicacion = "";
		for($j = 0; $j < $totalRegistros; $j++)
		{
			$aplicacion = $appArr[$j]['nombre'];
			if($appArr[$j]['tipoModulo'] == '3')
			{
				$storesModelos = explode('|', substr($appArr[$j]['banderasEjecucion'], 0, -1));
				if(sizeof($storesModelos)>0){
					for ($iInt = 0; $iInt < sizeof($storesModelos); $iInt++)
					{
						if($storesModelos[$iInt]!=""){
							$modulosImportar .= "		'" . $appArr[$j]['nombre'] . "." . $appArr[$j]['nombreMenuNavegacion'] . "." . $storesModelos[$iInt] . "',\n";
							//$modulosCrear    .= "            new " . mysql_result($appRs, $j, "mn.nombre") . "." . mysql_result($appRs, $j, "sm.nombreMenuNavegacion") . "." . $storesModelos[$iInt] . "(),\n";
						}
					}
				}
				
				$modulosImportar .= "		'" . $appArr[$j]['nombre'] . "." . $appArr[$j]['nombreMenuNavegacion'] . "." . $appArr[$j]['modulo'] . "',\n";
				$modulosCrear    .= "            new " . $appArr[$j]['nombre'] . "." . $appArr[$j]['nombreMenuNavegacion'] . "." . $appArr[$j]['modulo'] . "(),\n";
			}
			else
			{
				$menuesImportar .= "		'" . $appArr[$j]['nombre'] . "." . $appArr[$j]['modulo'] . $_SESSION['idUsuario'] . "',\n";
				$menuesCrear    .= "            new " . $appArr[$j]['nombre'] . "." . $appArr[$j]['modulo'] . $_SESSION['idUsuario'] . "(),\n";
			}
		}
		
		$appArchivoStr  = "/*!\n";
		$appArchivoStr .= " * Ext JS Library 4.0\n";
		$appArchivoStr .= " * Copyright(c) 2006-2011 Sencha Inc.\n";
		$appArchivoStr .= " * licensing@sencha.com\n";
		$appArchivoStr .= " * http://www.sencha.com/license\n";
		$appArchivoStr .= " */\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "var lg_contextMnu;\n";
		$appArchivoStr .= "// Prevent the backspace key from navigating back.\n";
		$appArchivoStr .= "Ext.EventManager.on(Ext.isIE ? document : window, 'keydown', function(e, t) {\n";
		$appArchivoStr .= "	if (e.getKey() == e.BACKSPACE && ((!/^input$/i.test(t.tagName) && !/^textarea$/i.test(t.tagName)) || t.disabled || t.readOnly)) {\n";
		$appArchivoStr .= "		e.stopEvent();\n";
		$appArchivoStr .= "	}\n";
		$appArchivoStr .= "});\n";
		$appArchivoStr .= "\n"; 
		$appArchivoStr .= "Ext.define('" . $aplicacion . ".App" . $_SESSION['idUsuario'] . "', {\n";
		$appArchivoStr .= "    extend: 'Ext.ux.desktop.App',\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "    requires: [\n";
		$appArchivoStr .= "		'Ext.window.MessageBox',\n";
		$appArchivoStr .= "		'Ext.ux.desktop.ShortcutModel',\n";
		$appArchivoStr .= "		\n";
		$appArchivoStr .= $menuesImportar;
		$appArchivoStr .= $modulosImportar;
		$appArchivoStr .= "		\n";
		$appArchivoStr .= "		'" . $aplicacion . ".modules.security.segUsuariosModulos.segUsuariosModulosDialogWin',\n";
		$appArchivoStr .= "		'" . $aplicacion . ".modules.security.login.app.store.segUsuarioConfigStr',\n";
		$appArchivoStr .= "		'" . $aplicacion . ".modules.system.panelControl.sisPanelControlSegUsuariosStr',\n";
		$appArchivoStr .= "		'" . $aplicacion . ".modules.system.panelControl.sisPanelControlModulosStr',\n";
		$appArchivoStr .= "		'" . $aplicacion . ".modules.system.panelControl.sisPanelControlEscritorioStr',\n";
		$appArchivoStr .= "		'" . $aplicacion . ".modules.system.panelControl.sisPanelControlWn'\n";
		$appArchivoStr .= "    ],\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "    init: function() {\n";
		$appArchivoStr .= "        // custom logic before getXYZ methods get called...\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "        this.callParent();\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "        // now ready...\n";
		$appArchivoStr .= "    },\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "    getModules : function(){\n";
		$appArchivoStr .= "        return [\n";
		$appArchivoStr .= "            // Menues\n";
		$appArchivoStr .= $modulosCrear;
		$appArchivoStr .= $menuesCrear;
		$appArchivoStr .= "            new " . $aplicacion . ".modules.security.segUsuariosModulos.segUsuariosModulosDialogWin(),\n";
		$appArchivoStr .= "            new " . $aplicacion . ".modules.security.login.app.store.segUsuarioConfigStr()\n";
		$appArchivoStr .= "        ];\n";
		$appArchivoStr .= "    },\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "    getDesktopConfig: function () {\n";
		$appArchivoStr .= "        var me = this, ret = me.callParent();\n";
		$appArchivoStr .= "		\n";
		$appArchivoStr .= "        return Ext.apply(ret, {\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "            contextMenuItems: [\n";
		$appArchivoStr .= "                { text: 'Personalizar', iconCls:'personalizar', handler: me.onSettings, scope: me }\n";
		$appArchivoStr .= "            ],\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "            shortcuts: Ext.create('Ext.data.Store', {\n";
		$appArchivoStr .= "                model: 'Ext.ux.desktop.ShortcutModel',\n";
		$appArchivoStr .= "				data: accesosDirArr\n";
		$appArchivoStr .= "            }),\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "            wallpaper: '" . $_SESSION['wallpaper'] . "',\n";
		$appArchivoStr .= "            wallpaperStretch: true\n";
		$appArchivoStr .= "        });\n";
		$appArchivoStr .= "    },\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "    // config for the start menu\n";
		$appArchivoStr .= "    getStartConfig : function() {\n";
		$appArchivoStr .= "        var me = this, ret = me.callParent();\n";
		$appArchivoStr .= "        Ext.EventManager.on(document, 'keydown', function(e, t) {\n";
     	$appArchivoStr .= "    	   	if (e.getKey() == e.BACKSPACE && (!/^input$/i.test(t.tagName) || t.disabled || t.readOnly)) {\n";
        $appArchivoStr .= "     		e.stopEvent();\n";
     	$appArchivoStr .= "    		}\n";
  		$appArchivoStr .= "   	   });\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "        return Ext.apply(ret, {\n";
		$appArchivoStr .= "            title: usuNombre,\n";
		$appArchivoStr .= "            iconCls: 'user',\n";
		$appArchivoStr .= "            height: 300,\n";
		$appArchivoStr .= "            toolConfig: {\n";
		$appArchivoStr .= "                width: 100,\n";
		$appArchivoStr .= "                items: [\n";
		$appArchivoStr .= "                    {\n";
		$appArchivoStr .= "                        text:'Personalizar',\n";
		$appArchivoStr .= "                        iconCls:'personalizar',\n";
		$appArchivoStr .= "                        handler: me.onSettings,\n";
		$appArchivoStr .= "                        scope: me\n";
		$appArchivoStr .= "                    },\n";
		$appArchivoStr .= "                    '-',\n";
		$appArchivoStr .= "                    {\n";
		$appArchivoStr .= "                        text:'Presto',\n";
		$appArchivoStr .= "                        iconCls:'logoPresto',\n";
		$appArchivoStr .= "                        handler: me.getPagina,\n";
		$appArchivoStr .= "                        scope: me\n";
		$appArchivoStr .= "                    },\n";
		$appArchivoStr .= "                    {\n";
		$appArchivoStr .= "                        text:'Tracking ',\n";
		$appArchivoStr .= "                        iconCls:'logoPresto',\n";
		$appArchivoStr .= "                        handler: me.getVentanaTracking,\n";
		$appArchivoStr .= "                        scope: me\n";
		$appArchivoStr .= "                    },\n";
		$appArchivoStr .= "                    {\n";
		$appArchivoStr .= "                        text:'Rep VIN',\n";
		$appArchivoStr .= "                        iconCls:'logoPresto',\n";
		$appArchivoStr .= "                        handler: me.getVentanaRepVin,\n";
		$appArchivoStr .= "                        scope: me\n";
		$appArchivoStr .= "                    },\n";	
		$appArchivoStr .= "                    {\n";
		$appArchivoStr .= "                        text:'Madrinas',\n";
		$appArchivoStr .= "                        iconCls:'catMunicipios',\n";
		$appArchivoStr .= "                        handler: me.getVentanaMadrina,\n";
		$appArchivoStr .= "                        scope: me\n";
		$appArchivoStr .= "                    },\n";			
		$appArchivoStr .= "                    '-',\n";						
		$appArchivoStr .= "                    {\n";		
		$appArchivoStr .= "                        text:'Salir',\n";
		$appArchivoStr .= "                        iconCls:'salir',\n";
		$appArchivoStr .= "                        handler: me.onLogout,\n";
		$appArchivoStr .= "                        scope: me\n";
		$appArchivoStr .= "                    }\n";
		$appArchivoStr .= "                ]\n";
		$appArchivoStr .= "            }\n";
		$appArchivoStr .= "        });\n";
		$appArchivoStr .= "    },\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "    getTaskbarConfig: function () {\n";
		$appArchivoStr .= "        var ret = this.callParent();\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "        return Ext.apply(ret, {\n";
		$appArchivoStr .= "            quickStart: quickStartArr,\n";
		$appArchivoStr .= "            trayItems: [\n";
		$appArchivoStr .= "                { xtype: 'trayclock', flex: 1 }\n";
		$appArchivoStr .= "            ]\n";
		$appArchivoStr .= "        });\n";
		$appArchivoStr .= "    },\n";
		$appArchivoStr .= "\n";		
		$appArchivoStr .= "    getPagina: function () {\n";
		$appArchivoStr .= "    window.open('http://201.163.112.112:8014/presto/hub/signinCB.html'); \n";
		$appArchivoStr .= "    },\n";
		$appArchivoStr .= "\n";		
		$appArchivoStr .= "    getVentanaTracking: function () {\n";
		$appArchivoStr .= "    var visualizar = new Ext.Window({
        							iconCls:'logoPresto',        							
        							title: 'Tracking Series',
        							id: '',
        							layout: 'fit',
        							width: 700,
        							height: 500,        							
        							closeAction: 'destroy',
        							items: [{
            							xtype: 'component',
            							autoEl: {
                							tag: 'iframe',
                							style: 'height:100%; width:100%; border:none',
                							src: 'http://201.163.112.112:8014/presto/files/system/mashlets/Tracking_Series_CB_Interno/index.html'
            							}
        							}]
    							}).show(); \n";
		$appArchivoStr .= "    },\n";		
		$appArchivoStr .= "\n";		
		$appArchivoStr .= "    getVentanaRepVin: function () {\n";
		$appArchivoStr .= "    var visualizar = new Ext.Window({
        							iconCls:'logoPresto',        							
        							title: 'REP VIN',
        							id: '',
        							layout: 'fit',
        							width: 1200,
        							height: 700,        							
        							closeAction: 'destroy',
        							items: [{
            							xtype: 'component',
            							autoEl: {
                							tag: 'iframe',
                							style: 'height:100%; width:100%; border:none',
                							src: 'http://201.163.112.112:8014/presto/files/system/mashlets/REPVIN_CB_Interno/index.html'
            							}
        							}]
    							}).show(); \n";
		$appArchivoStr .= "    },\n";
		$appArchivoStr .= "\n";		
		$appArchivoStr .= "    getVentanaMadrina: function () {\n";
		$appArchivoStr .= "    var visualizar = new Ext.Window({
        							iconCls:'catMunicipios',        							
        							title: 'Localizacion Madrinas',
        							id: '',
        							layout: 'fit',
        							width: 1200,
        							height: 700,        							
        							closeAction: 'destroy',
        							items: [{
            							xtype: 'component',
            							autoEl: {
                							tag: 'iframe',
                							style: 'height:100%; width:100%; border:none',
                							src: 'http://201.163.112.112:8014/presto/files/system/mashlets/MainTracomexMobileCB/index.html'
            							}
        							}]
    							}).show(); \n";
		$appArchivoStr .= "    },\n";				
		$appArchivoStr .= "\n";
		$appArchivoStr .= "    getCosnulta: function () {\n";
		$appArchivoStr .= "        var dlg = new " . $aplicacion . ".modules.system.panelPresto.MyWindow({\n";
		$appArchivoStr .= "            desktop: this.desktop\n";
		$appArchivoStr .= "        });\n";
		$appArchivoStr .= "        dlg.show();\n";
		$appArchivoStr .= "    },\n";
		$appArchivoStr .= "\n";			
		$appArchivoStr .= "    onLogout: function () {\n";
		$appArchivoStr .= "        Ext.Msg.confirm('Salir', '¿Esta seguro que desea salir?<br>El trabajo que no haya guardado se <br>perder&aacute;', function(btn, text){\n";
		$appArchivoStr .= "        	if (btn == 'yes')\n";
		$appArchivoStr .= "				window.location = 'modules/security/login/logout.php';\n";
		$appArchivoStr .= "			});\n";
		$appArchivoStr .= "    },\n";
		$appArchivoStr .= "\n";
		$appArchivoStr .= "    onSettings: function () {\n";
		$appArchivoStr .= "        var dlg = new " . $aplicacion . ".modules.system.panelControl.sisPanelControlWn({\n";
		$appArchivoStr .= "            desktop: this.desktop\n";
		$appArchivoStr .= "        });\n";
		$appArchivoStr .= "        dlg.show();\n";
		$appArchivoStr .= "    }\n";
		$appArchivoStr .= "});\n";
		
		$ruta = str_replace('segUsuariosMenu.php', "App" . $_SESSION['idUsuario'] . '.js', __FILE__);
		$archivo = fopen($ruta, "w");
		fwrite($archivo, $appArchivoStr);
		fclose($archivo);
	}
?>
