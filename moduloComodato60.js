/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.moduloComodato60', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'moduloComodato',
            iconCls: 'cxc',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Impresion de Unidades',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('impresionComodatoWin');
										win = module.createWindow_impresionComodatoWin();
									},
									scope: this,
									windowId: 'impresionComodatoWin'
								},
								{
									text: 'Localizacion Unidades',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('localizacionPatioWin');
										win = module.createWindow_localizacionPatioWin();
									},
									scope: this,
									windowId: 'localizacionPatioWin'
								},
								{
									text: 'Cambio de Estatus',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('prodStatusWin');
										win = module.createWindow_prodStatusWin();
									},
									scope: this,
									windowId: 'prodStatusWin'
								},
								{
									text: 'Reimpresion Etiquetas',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('reImpresionEtiquetasWin');
										win = module.createWindow_reImpresionEtiquetasWin();
									},
									scope: this,
									windowId: 'reImpresionEtiquetasWin'
								},
								{
									text: 'Actualizacion Unidades en Patio',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('actalizacionDeLocalizacionWin');
										win = module.createWindow_actalizacionDeLocalizacionWin();
									},
									scope: this,
									windowId: 'actalizacionDeLocalizacionWin'
								},
								{
									text: 'Cambio Mercado',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('tipoMercadoWin');
										win = module.createWindow_tipoMercadoWin();
									},
									scope: this,
									windowId: 'tipoMercadoWin'
								},
								{
									text: 'Salida Masiva',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('alTsalidasWin');
										win = module.createWindow_alTsalidasWin();
									},
									scope: this,
									windowId: 'alTsalidasWin'
								}
				]
            }
        };
    }
});