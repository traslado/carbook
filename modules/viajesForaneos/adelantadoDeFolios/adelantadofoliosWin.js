/*
 * File: app/view/adelantadofoliosWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.viajesForaneos.adelantadoDeFolios.adelantadofoliosWin', {
    extend: 'Ext.ux.desktop.Module', 
    
    id:'adelantadofoliosWin',


createWindow_adelantadofoliosWin: function(){
    var desktop = this.app.getDesktop();
    var win_adelantadofoliosWin= desktop.getWindow('adelantadofoliosWin');
    if(!win_adelantadofoliosWin){
        var store = new MyDesktop.modules.viajesForaneos.adelantadoDeFolios.adelantadoFoliosCompaniaStr();
          
        win_adelantadofoliosWin= desktop.createWindow({
            height: 265,
            id: 'adelantadofoliosWin',
            width: 376,
            title: 'Adelantado De Folios (Remisiones)',
            minimizable: true,
            maximizable: false,
            resizable:false,
            iconCls:'tractorModule',
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'adelantadofoliosTlb',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                if(Ext.getCmp('adelantadofoliosCompaniaCmb').getValue() === null ||
                                Ext.getCmp('adelantadofoliosFolioNuevoTxt').getValue() === ''  )
                                {
                                    Ext.Msg.alert("Adelantado De Folios", "Falta llenar campos requeridos");
                                }
                                else
                                {
                                    var nuevo = Ext.getCmp('adelantadofoliosFolioNuevoTxt').getValue();
                                    var actual = Ext.getCmp('adelantadofoliosFolioActualTxt').getValue();

                                    if(actual >= nuevo)
                                    {
                                        Ext.Msg.alert("Adelantado De Folios", "El nuevo folio no puede ser menor o igual al actual, solo es posible adelantar folios.");
                                        Ext.getCmp('adelantadofoliosFolioNuevoTxt').setValue('');
                                        Ext.getCmp('adelantadofoliosFolioNuevoTxt').setValue().focus();
                                    }
                                    else
                                    {
                                        var folios_ciaSesVal = typeof(ciaSesVal)=='undefined'?'CDTOL':ciaSesVal;
                                        var compania = Ext.getCmp('adelantadofoliosCompaniaCmb').getValue();
																				Ext.Msg.wait("Espere ... actualizando el folio", "Adelantado De Folios");
                                        Ext.Ajax.request({
                                            url: '../../carbookBck/json/tradelantadoFolios.php?',
                                            params: {

                                                alfoliosCompaniasHdn: 'updFolio',
                                                folioNuevo: Ext.getCmp('adelantadofoliosFolioNuevoTxt').getValue(),
                                                centroDist: folios_ciaSesVal,
                                                compania: compania

                                            },
                                            success: function(response) {                                           
                                            			Ext.Msg.hide();
					                                        Ext.Msg.alert("Adelantado De Folios", "Folio Modificado Correctamente.");
					                                        Ext.getCmp('adelantadofoliosFrm').getForm().reset();                                            			
                                            }                                            
                                        });
                                    }
                                }
                            },
                            id: 'adelantadofoliosAceptarBtn',
                            iconCls: 'ux-btnSave',
                            text: 'Aceptar'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.getCmp('adelantadofoliosFrm').getForm().reset();
                            },
                            id: 'adelantadofoliosCancelarBtn',
                            iconCls: 'ux-btnDel',
                            text: 'Cancelar'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    height: 189,
                    id: 'adelantadofoliosFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'combobox',
                            x: 40,
                            y: 30,
                            id: 'adelantadofoliosCompaniaCmb',
                            width: 300,
                            fieldLabel: 'Compañia:',
                            labelWidth: 80,
                            name: 'adelantadofoliosCompaniaCmb',
                            displayField: 'descCiaTractor',
                            store: 'adelantadoFoliosCompaniaStr',
                            valueField: 'compania',
                            listeners: {
                                select: {
                                    fn: function(combo, records, eOpts) {
                                        var folios_ciaSesVal = typeof(ciaSesVal)=='undefined'?'CDTOL':ciaSesVal;
                                        var compania = Ext.getCmp('adelantadofoliosCompaniaCmb').getValue();

                                        Ext.Ajax.request({
                                            url: '../../carbookBck/json/tradelantadoFolios.php?',
                                            params: {

                                                alfoliosCompaniasHdn: 'SelectFolioCompania',
                                                centroDist: folios_ciaSesVal,
                                                compania: compania

                                            },

                                            success: function(response){

                                                response = Ext.JSON.decode(response.responseText);

                                                if(response.records !== 0){


                                                    Ext.getCmp('adelantadofoliosFolioActualTxt').setValue(response.root[0].folio);
                                                    Ext.getCmp('adelantadofoliosFolioActualTxt').disable(true);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 40,
                            y: 70,
                            disabled: true,
                            id: 'adelantadofoliosFolioActualTxt',
                            width: 190,
                            fieldLabel: 'Folio Actual',
                            labelWidth: 80,
                            name: 'adelantadofoliosFolioActualTxt'
                        },
                        {
                            xtype: 'textfield',
                            x: 40,
                            y: 110,
                            id: 'adelantadofoliosFolioNuevoTxt',
                            width: 190,
                            fieldLabel: 'Folio Nuevo:',
                            labelWidth: 80,
                            name: 'adelantadofoliosFolioNuevoTxt',
                            maskRe: /[0-9]/
                        }
                    ]
                }
            ],
             listeners: {
                beforeclose: {
                    fn:  function(panel, eOpts) {
                                var titulo = this.title;
                                Ext.Msg.confirm(titulo,'¿Está seguro que desea salir de<br> '+ titulo+'?',function(btn){
                                if(!Ext.getCmp('adelantadofoliosFrm').isDisabled()){
                                                if(btn == 'yes'){
                                                    panel.allowDestroy = true;
                                                    panel.destroy();
                                                }
                                            }else{                               
                                            }
                                }, panel);
                                return false;

                            }
                }
            
          }
        });
    }
        win_adelantadofoliosWin.show();
        return win_adelantadofoliosWin;
    }
})