/*!
 * Ext JS Library 4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

// Prevent the backspace key from navigating back.
Ext.EventManager.on(Ext.isIE ? document : window, 'keydown', function(e, t) {
	if (e.getKey() == e.BACKSPACE && ((!/^input$/i.test(t.tagName) && !/^textarea$/i.test(t.tagName)) || t.disabled || t.readOnly)) {
		e.stopEvent();
	}
});

Ext.define('MyDesktop.App100', {
    extend: 'Ext.ux.desktop.App',

    requires: [
		'Ext.window.MessageBox',
		'Ext.ux.desktop.ShortcutModel',
		
		'MyDesktop.catGenMenuModel100',
		'MyDesktop.avanzadasMenuModel100',
		'MyDesktop.especialesMenuModel100',
		'MyDesktop.ViajesTractorModel100',
		'MyDesktop.desbloqueoUnidades100',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosOrigenStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosTipoStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosMarcasCdStore',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosClasificacionStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosSimboloStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosWin',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistrbuidoresDirecionesMdl',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresTipoStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresPlazasStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresSucursalStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresRegionesStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresEstatusStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresPaisStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresEstadoStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresMunicipiosStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresColoniasStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresDireccionStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresDireccionesStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresZonaStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresWin',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesDireccionesMdl',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesTipoStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesPlazasStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesSucursalStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesRegionesStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesEstatusStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesPaisStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesEstadoStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesMunicipiosStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesColoniasStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesDireccionStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesDireccionesStr',
		'MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesWin',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010BloqueoStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010ColorStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010ConceptosStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010DestinoStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010DistribuidorStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010GastosStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010OrigenStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010SimboloStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010UnidadesMdl',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010UnidadesStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010VinStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010Win',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoUnidadesMdl',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoDestinoStr',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoUnidadesStr',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoVinStr',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoSimboloStr',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoColorStr',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoBloqueoStr',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoConceptosStr',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoGastosStr',
		'MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoWin',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010BloqueoStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010ColorStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010ConceptosStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010DestinoStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010DistribuidorStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010GastosStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010OrigenStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010SimboloStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010UnidadesMdl',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010UnidadesStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010VinStr',
		'MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010Win',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesDatosMdl',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesCompaniasStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesDatosStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesTractorStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesDistCentroStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesCompaniaGrdStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesViajeStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesBloyDesTractorStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajeTipoMovGeneralesStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesChoferesDisStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesMantenimientoStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesTractorViajeStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesGastosStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajeGeneralesStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesUnidadesVinStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesUnidadesVinBloqYDesbloStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesUnidadesVinHistoricoStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesHoldsVinStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesUnidadesAgregadosVinStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesListaEsperaStr',
		'MyDesktop.modules.viajesForaneos.preViajes.catPreViajesWin',
		'MyDesktop.modules.viajesForaneos.cancelacionPreviaje.cancelacionViajesGastosStr',
		'MyDesktop.modules.viajesForaneos.cancelacionPreviaje.cancelacionViajeCompaniaStr',
		'MyDesktop.modules.viajesForaneos.cancelacionPreviaje.cancelacionViajeTractorStr',
		'MyDesktop.modules.viajesForaneos.cancelacionPreviaje.viajeCanceladoViajeStr',
		'MyDesktop.modules.viajesForaneos.cancelacionPreviaje.viajeCanceladoConsultaStr',
		'MyDesktop.modules.viajesForaneos.cancelacionPreviaje.viajeCanceladoGastoStr',
		'MyDesktop.modules.viajesForaneos.cancelacionPreviaje.viajeCanceladoTalonStr',
		'MyDesktop.modules.viajesForaneos.cancelacionPreviaje.cancelacionViajeBloqueoStr',
		'MyDesktop.modules.viajesForaneos.cancelacionPreviaje.cancelacionViajesWin',
		'MyDesktop.modules.viajesForaneos.cancelacionAsignacion.cancelacionViajesGastosStr',
		'MyDesktop.modules.viajesForaneos.cancelacionAsignacion.cancelacionViajeCompaniaStr',
		'MyDesktop.modules.viajesForaneos.cancelacionAsignacion.cancelacionViajeTractorStr',
		'MyDesktop.modules.viajesForaneos.cancelacionAsignacion.viajeCanceladoViajeStr',
		'MyDesktop.modules.viajesForaneos.cancelacionAsignacion.viajeCanceladoConsultaStr',
		'MyDesktop.modules.viajesForaneos.cancelacionAsignacion.viajeCanceladoGastoStr',
		'MyDesktop.modules.viajesForaneos.cancelacionAsignacion.viajeCanceladoTalonStr',
		'MyDesktop.modules.viajesForaneos.cancelacionAsignacion.cancelacionAsignacionWin',
		'MyDesktop.modules.desbloqueo.desbloqueoTractor.desbloqueoTractorConsultaStr',
		'MyDesktop.modules.desbloqueo.desbloqueoTractor.desbloqueoTractorWin',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesCompaniaStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesTractorStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesChoferStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesTalonesStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesDetallesTalonesStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesCdValidosStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesUnidadesConsultaStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesUnidadesStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesListaEsperaStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesBloqueoTractorStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesUnidadesDltUnidadTmpStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesEliminaUnidadesTmpStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesEmbarcadasStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesConsulta12Str',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesCentroStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesContStr',
		'MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesWin',
		'MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeCIAStr',
		'MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeDisplaysStr',
		'MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeEstatusStr',
		'MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeMercedesStr',
		'MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeOrigenStr',
		'MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeTipoRecepcionStr',
		'MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeTalonStatusStr',
		'MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeUnidadesStr',
		'MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeWin',
		'MyDesktop.modules.viajesForaneos.liberarTractores.libTractorCompaniaStr',
		'MyDesktop.modules.viajesForaneos.liberarTractores.libTractorTractorStr',
		'MyDesktop.modules.viajesForaneos.liberarTractores.libTractorTalonesStr',
		'MyDesktop.modules.viajesForaneos.liberarTractores.libTractorBloqueoTractorStr',
		'MyDesktop.modules.viajesForaneos.liberarTractores.libTractorWin',
		'MyDesktop.modules.viajesForaneos.viTractor12.asigT12UnidadValidaStr',
		'MyDesktop.modules.viajesForaneos.viTractor12.asigT12ConsultaUnidadStr',
		'MyDesktop.modules.viajesForaneos.viTractor12.asigT12DistribuidorStr',
		'MyDesktop.modules.viajesForaneos.viTractor12.asigT12AvanzadaWin',
		'MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeModel',
		'MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeCompaniaStr',
		'MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeTractorStr',
		'MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeDescipcionStr',
		'MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeUnidadStr',
		'MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeWin',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asiEspecialesTalonesMdl',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asigEspecialesCompaniaStr',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asigEspecialesTractorStr',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asigEspecialesViajeStr',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asigEspecialesUnidadStr',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asigEspecialesTalonStr',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asiEspecialesDistribuidorStr',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asiEspecialesDistribuidoresStr',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asigEspacialesValidaUnidadStr',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asigEspecialesRemitenteStr',
		'MyDesktop.modules.viajesForaneos.asignacionEspeciales.asigEspecialesWin',
		
		'MyDesktop.modules.security.segUsuariosModulos.segUsuariosModulosDialogWin',
		'MyDesktop.modules.security.login.app.store.segUsuarioConfigStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlSegUsuariosStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlModulosStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlEscritorioStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlWn'
    ],

    init: function() {
        // custom logic before getXYZ methods get called...

        this.callParent();

        // now ready...
    },

    getModules : function(){
        return [
            // Menues
            new MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosWin(),
            new MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresWin(),
            new MyDesktop.modules.catalogs.catalogoDestinosEspeciales.catDestinosEspecialesWin(),
            new MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010Win(),
            new MyDesktop.modules.serviciosEspeciales.cambioDestino.cambioDestinoWin(),
            new MyDesktop.modules.serviciosEspeciales.servicioEspecial.trap010Win(),
            new MyDesktop.modules.viajesForaneos.preViajes.catPreViajesWin(),
            new MyDesktop.modules.viajesForaneos.cancelacionPreviaje.cancelacionViajesWin(),
            new MyDesktop.modules.viajesForaneos.cancelacionAsignacion.cancelacionAsignacionWin(),
            new MyDesktop.modules.desbloqueo.desbloqueoTractor.desbloqueoTractorWin(),
            new MyDesktop.modules.viajesForaneos.asignacionUnidades.asignacionUnidadesWin(),
            new MyDesktop.modules.viajesForaneos.recepcionViajes.recepcionViajeWin(),
            new MyDesktop.modules.viajesForaneos.liberarTractores.libTractorWin(),
            new MyDesktop.modules.viajesForaneos.viTractor12.asigT12AvanzadaWin(),
            new MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeWin(),
            new MyDesktop.modules.viajesForaneos.asignacionEspeciales.asigEspecialesWin(),
            new MyDesktop.catGenMenuModel100(),
            new MyDesktop.avanzadasMenuModel100(),
            new MyDesktop.especialesMenuModel100(),
            new MyDesktop.ViajesTractorModel100(),
            new MyDesktop.desbloqueoUnidades100(),
            new MyDesktop.modules.security.segUsuariosModulos.segUsuariosModulosDialogWin(),
            new MyDesktop.modules.security.login.app.store.segUsuarioConfigStr()
        ];
    },

    getDesktopConfig: function () {
        var me = this, ret = me.callParent();
		
        return Ext.apply(ret, {

            contextMenuItems: [
                { text: 'Personalizar', iconCls:'personalizar', handler: me.onSettings, scope: me }
            ],

            shortcuts: Ext.create('Ext.data.Store', {
                model: 'Ext.ux.desktop.ShortcutModel',
				data: accesosDirArr
            }),

            wallpaper: 'wallpapers/Carbook-Inicial.jpg',
            wallpaperStretch: true
        });
    },

    // config for the start menu
    getStartConfig : function() {
        var me = this, ret = me.callParent();
        Ext.EventManager.on(document, 'keydown', function(e, t) {
    	   	if (e.getKey() == e.BACKSPACE && (!/^input$/i.test(t.tagName) || t.disabled || t.readOnly)) {
     		e.stopEvent();
    		}
   	   });

        return Ext.apply(ret, {
            title: usuNombre,
            iconCls: 'user',
            height: 300,
            toolConfig: {
                width: 100,
                items: [
                    {
                        text:'Personalizar',
                        iconCls:'personalizar',
                        handler: me.onSettings,
                        scope: me
                    },
                    '-',
                    {
                        text:'Presto',
                        iconCls:'personalizar',
                        handler: me.getCosnulta,
                        scope: me
                    },
                    '-',
                    {
                        text:'Salir',
                        iconCls:'salir',
                        handler: me.onLogout,
                        scope: me
                    }
                ]
            }
        });
    },

    getTaskbarConfig: function () {
        var ret = this.callParent();

        return Ext.apply(ret, {
            quickStart: quickStartArr,
            trayItems: [
                { xtype: 'trayclock', flex: 1 }
            ]
        });
    },

    getCosnulta: function () {
        var dlg = new MyDesktop.modules.system.panelPresto.MyWindow({
            desktop: this.desktop
        });
        dlg.show();
    },

    onLogout: function () {
        Ext.Msg.confirm('Salir', '¿Esta seguro que desea salir?<br>El trabajo que no haya guardado se <br>perder&aacute;', function(btn, text){
        	if (btn == 'yes')
				window.location = 'modules/security/login/logout.php';
			});
    },

    onSettings: function () {
        var dlg = new MyDesktop.modules.system.panelControl.sisPanelControlWn({
            desktop: this.desktop
        });
        dlg.show();
    }
});
