/*
 * File: app/view/impresionCPWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.viajesForaneos.impresionCP.impresionCPWin', {
    extend: 'Ext.ux.desktop.Module', 
    
    id:'impresionCPWin',

    /*init : function(){
        this.launcher = {
            text: admCatGenTitle,
            iconCls:'icon-admCatGeg'
        };
    },*/    
createWindow_impresionCPWin: function(){
    var desktop = this.app.getDesktop();
    var win_impresionCPWin= desktop.getWindow('impresionCPWin');
    if(!win_impresionCPWin){
        var store = new MyDesktop.modules.viajesForaneos.impresionCP.impresionCPCompaniaStr();
        var titleWin = 'IMPRESION CARTA PORTE';

        win_impresionCPWin= desktop.createWindow({
            height: 350,
            id: 'impresionCPWin',
            width: 515,
            layout: 'fit',
            title: titleWin,
            maximizable: false,
            minimizable: true,
            razisable: false,
            iconCls:'tractorModule',
            buscaFolioFn: function(component) {
                  //console.log('GPO',Ext.getCmp('reimpresionTalonTipoTalonGpoRdo'));
                  Ext.Msg.wait("Espere validando el folio ... ", titleWin);
                  Ext.Ajax.request({
                      url: '../../carbookBck/json/impresionTalonPdfCP.php',
                      params: {
                          actionHdn: 'buscaFolio',
                          compania: Ext.getCmp('reimpresionTalonCompaniaCmb').getValue(),
                          folio: Ext.getCmp('reimpresionTalonFolioTxt').getValue(),
                          tipoTalon: Ext.getCmp('reImpresionTalonesActionTxt').tipoTalon
                      },
                      success: function(response) {
                          response = Ext.JSON.decode(response.responseText);
                          Ext.Msg.hide();
                          if (Ext.isEmpty(response.records) || response.records == 0) {
                              Ext.getCmp('reImpresionTalonesTractorTxt').setValue('');
                              Ext.getCmp('reImpresionTalonesViajeTxt').setValue('');
                              Ext.getCmp('reImpresionTalonesOperadorTxt').setValue('');
                              Ext.getCmp('reImpresionTalonesPlazaOrigenTxt').setValue('');
                              Ext.getCmp('reImpresionTalonesPlazaDestinoTxt').setValue('');
                              Ext.getCmp('reImpresionTalonesUnidadesTxt').setValue('');
                              Ext.getCmp('reImpresionTalonesFechaViajeTxt').setValue('');
                              Ext.Msg.alert(titleWin, '<b>Folio no existente.</b>', function(btn) {                                     
                                        component.focus();
                                });
                          } else {                            
                              var rec = response.root[0];
                              console.log('REG',rec.UUID);
                             //if (rec.UUID!=null) {
                                var aux = rec.plazaOrigen;
                                                        var pza = aux.substring(0, 26);
                                  Ext.getCmp('reImpresionTalonesTractorTxt').setValue(rec.tractor);
                                  Ext.getCmp('reImpresionTalonesViajeTxt').setValue(rec.viaje);
                                  Ext.getCmp('reImpresionTalonesOperadorTxt').setValue(rec.operador);
                                  Ext.getCmp('reImpresionTalonesPlazaOrigenTxt').setValue(pza);
                                  aux = rec.plazaDestino;
                                                            pza = aux.substring(0, 23);                           
                                  Ext.getCmp('reImpresionTalonesPlazaDestinoTxt').setValue(pza);
                                  Ext.getCmp('reImpresionTalonesUnidadesTxt').setValue(rec.numeroUnidades);
                                  Ext.getCmp('reImpresionTalonesFechaViajeTxt').setValue(rec.fechaEvento);
                                  Ext.getCmp('impresionCPHdn').setValue(rec.idViajeTractor);

                                  Ext.getCmp('impresionCPFechaHdn').setValue(rec.fechaEvento);
                                  Ext.getCmp('impresionCPCDHdn').setValue(rec.centroDistribucion);
                                  Ext.getCmp('impresionCPUUIDHdn').setValue(rec.UUID);
                                  Ext.getCmp('impresionFolioTimbradoHdn').setValue(rec.folioTimbrado);
                                  
                              /*}else{
                                Ext.Msg.alert(titleWin, "El Talón Aún No se Encuentra Timbrado.");
                                 Ext.getCmp('reimpresionTalonFolioTxt').setValue('');
                              Ext.getCmp('reimpresionTalonCompaniaCmb').setValue('');
                              
                              }*/
                              
                          }
                      }
                  });                   
            },
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'reimpresionTalonTlb',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var form = Ext.getCmp('reimpresionTalonCPFrm').getForm();
                                                                if(form.isValid() && !Ext.isEmpty(Ext.getCmp('reImpresionTalonesTractorTxt').getValue())){
                                                                    console.log("entra");
                                    /*var visualizar = new Ext.Window({
                                        title: titleWin,
                                        id: 'pdfWindow',
                                        layout: 'fit',
                                        width: 800,
                                        height: 600,
                                        modal: true,
                                        closeAction: 'destroy',
                                        items: [{
                                            xtype: 'component',
                                            autoEl: {
                                                tag: 'iframe',
                                                style: 'height:100%; width:100%; border:none',
                                                src: '../../carbookbck/json/generaTxtTalonComplementoUUID.php?'+'trViajesTractoresCompaniaHdn=' + Ext.getCmp('reimpresionTalonCompaniaCmb').getValue() + '&' +'trViajesTractoresFoliosHdn='+ Ext.getCmp('reimpresionTalonFolioTxt').getValue()+ '&' +'UUID='+ Ext.getCmp('uuidSica').getValue() + '&tipoTalon=' + Ext.getCmp('reImpresionTalonesActionTxt').tipoTalon
                                            }
                                     }).show();*/
                           
                                    var fecha=Ext.getCmp('impresionCPFechaHdn').getValue().substring(0,4) + Ext.getCmp('impresionCPFechaHdn').getValue().substring(5,7) + Ext.getCmp('impresionCPFechaHdn').getValue().substring(8,10);
                                    var patio=Ext.getCmp('impresionCPCDHdn').getValue().substring(2,5);
                                    var UUID=Ext.getCmp('impresionCPUUIDHdn').getValue();
                                    var folioTimbrado=Ext.getCmp('impresionFolioTimbradoHdn').getValue();
                                    
                                    var dia= parseFloat(Ext.getCmp('impresionCPFechaHdn').getValue().substring(8,10)) + parseFloat(1);
                                    var fecha1=Ext.getCmp('impresionCPFechaHdn').getValue().substring(0,4) + Ext.getCmp('impresionCPFechaHdn').getValue().substring(5,7) + dia;



                                                            console.log(folioTimbrado);



                                    window.open('../../carbookBck/json/impresionTalonPdf.php?trViajesTractoresIdViajeHdn=' + Ext.getCmp('impresionCPHdn').getValue() + '&' +'trViajesTractoresFoliosHdn=' + Ext.getCmp('reimpresionTalonFolioTxt').getValue());
                                    window.open('../cfdi/CFDI_PDF/'+ fecha + '/'+ folioTimbrado +'_' + UUID +'.pdf');
                                    window.open('../cfdi/CFDI_PDF/'+ fecha + '/'+ folioTimbrado +'_' + UUID +'_SS.pdf');

                                    ////
                                    window.open('../cfdi/CFDI_PDF/'+ fecha1 + '/'+ folioTimbrado +'_' + UUID +'.pdf');
                                    window.open('../cfdi/CFDI_PDF/'+ fecha1 + '/'+ folioTimbrado +'_' + UUID +'_SS.pdf');
                                        //Se limpia el formulario
                                        Ext.getCmp("reImpresionTalonesCancelarBtn").fireHandler();       
                                                                        //window.open('trViajesTractoresCompaniaHdn=' + Ext.getCmp('reimpresionTalonCompaniaCmb').getValue() + '&' +'trViajesTractoresFoliosHdn='+ Ext.getCmp('reimpresionTalonFolioTxt').getValue() + '&tipoTalon=' + Ext.getCmp('reImpresionTalonesActionTxt').tipoTalon);//Ext.getCmp('asiEspecialesTalonesFoliosHdn').getValue());                                                                                                      
                                } else {
                                    Ext.Msg.alert(titleWin, "Faltan datos requeridos.");
                                }
                            },
                            id: 'reimpresionTalonImprimirBtn',
                            iconCls: 'ux-btnSave',
                            text: 'Imprimir'
                        },
                        {
                            xtype: 'tbseparator'
                        },                        
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var borrarFrm = Ext.getCmp('reimpresionTalonCPFrm').getForm();
                                Ext.getCmp('reimpresionTalonFolioTxt').setValue('');
                                Ext.getCmp('reimpresionTalonCompaniaCmb').oldValue = '';
                                Ext.getCmp('reimpresionTalonFolioTxt').oldValue = 0;
                                borrarFrm.reset();
                                Ext.getCmp('reImpresionTalonesActionTxt').setValue('Talones normales');
                                Ext.getCmp('reImpresionTalonesActionTxt').tipoTalon = 'TN';
                            },
                            id: 'reImpresionTalonesCancelarBtn',
                            iconCls: 'ux-btnDel',
                            text: 'Cancelar'
                        },                        
                        {
                            xtype: 'tbfill'
                        },
                        //component.setTitle('titulo') ó Ext.getCmp('serPreCierreOrdenesPasswordPnl').setTitle('titulo);
                        {
                            xtype: 'textfield',
                            // x: 20,
                            // y: 10,
                            width: 135, 
                            id: 'reImpresionTalonesActionTxt',
                            readOnly: true,
                            labelStyle: 'font-weight: bold;',
                            fieldStyle: 'background-color: #8AEF9A; font-weight: bold; color: #003168;',
                            tipoTalon: 'TN',
                            value: 'Talones normales'
                        }                        
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    id: 'reimpresionTalonCPFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    url: '../../carbookBck/json/impresionTalonPdf.php',                    
                    title: '',
                    items: [
                        {
                            xtype: 'radiogroup',
                            x: 20,
                            y: 10,
                            id: 'reimpresionTalonTipoTalonGpoRdo',
                            width: 270,
                            labelWidth: 115,
                            fieldLabel: 'Tipo de Talón',
                            items: [{
                                    xtype: 'radiofield',
                                    handler: function(checkbox, checked) {
                                        if (checked) {
                                            titleWin = 'Reimpresión de Talones Normales';
                                            Ext.getCmp('impresionCPWin').setTitle(titleWin);
                                            Ext.getCmp('reImpresionTalonesActionTxt').setValue('Talones Normales');
                                            Ext.getCmp('reImpresionTalonesActionTxt').tipoTalon = 'TN';
                                            if (!Ext.isEmpty(Ext.getCmp('reimpresionTalonFolioTxt').getValue()) &&
                                                    !Ext.isEmpty(Ext.getCmp('reimpresionTalonCompaniaCmb').getValue())) {
                                                    win_impresionCPWin.buscaFolioFn(checkbox);
                                            }
                                        }
                                    },
                                    name: 'tipoTalon',
                                    width: 85,
                                    boxLabel: 'Normal',
                                    checked: true,
                                    inputValue: 'TN'
                                },
                                {
                                    xtype: 'radiofield',
                                    handler: function(checkbox, checked) {
                                        if (checked) {
                                            titleWin = 'Reimpresión de Talones Especiales';
                                            Ext.getCmp('impresionCPWin').setTitle(titleWin);
                                            Ext.getCmp('reImpresionTalonesActionTxt').setValue('Talones Especiales');
                                            Ext.getCmp('reImpresionTalonesActionTxt').tipoTalon = 'TE';
                                            if (!Ext.isEmpty(Ext.getCmp('reimpresionTalonFolioTxt').getValue()) &&
                                                    !Ext.isEmpty(Ext.getCmp('reimpresionTalonCompaniaCmb').getValue())) {
                                                    win_impresionCPWin.buscaFolioFn(checkbox);
                                            }                                            
                                        }
                                    },
                                    width: 80,
                                    name: 'tipoTalon',
                                    boxLabel: 'Especial',
                                    inputValue: 'TE',

                                }
                            ]
                        },                           
                        {
                            xtype: 'combobox',
                            x: 20,
                            y: 40,
                            id: 'reimpresionTalonCompaniaCmb',
                            width: 450,
                            oldValue: '',
                            fieldLabel: 'Compañía Tractor',
                            labelWidth: 120,
                            allowBlank: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            displayField: 'descCiaTractor',
                            forceSelection: true,
                            queryMode: 'local',
                            store: 'impresionCPCompaniaStr',
                            valueField: 'compania',
                            listeners: {
                                select: {
                                    fn: function(combo, records, eOpts) {
                                            if (combo.oldValue != combo.getValue() && !Ext.isEmpty(combo.getValue()) &&
                                                    !Ext.isEmpty(Ext.getCmp('reimpresionTalonFolioTxt').getValue())) {
                                                    win_impresionCPWin.buscaFolioFn(combo);
                                            }
                                            combo.oldValue = combo.getValue();                                          
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'numberfield',
                            x: 20,
                            y: 70,
                            id: 'reimpresionTalonFolioTxt',
                            width: 240,
                            oldValue: 0,
                            fieldLabel: 'Folio',
                            labelWidth: 120,
                            allowBlank: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            enforceMaxLength: true,
                            hideTrigger: true,
                            repeatTriggerClick: false,
                            keyNavEnabled: false,
                            mouseWheelEnabled: false,
                            spinDownEnabled: false,
                            spinUpEnabled: false,
                            allowDecimals: false,
                            allowExponential: false,
                            decimalPrecision: 0,
                            maxValue: 9999999999,
                            listeners: {
                                    blur: {
                                        fn: function(component, e, eOpts) {
                                            if (component.oldValue != component.getValue() && !Ext.isEmpty(component.getValue()) &&
                                                    !Ext.isEmpty(Ext.getCmp('reimpresionTalonCompaniaCmb').getValue())) {
                                                    win_impresionCPWin.buscaFolioFn(component);
                                            }
                                            component.oldValue = component.getValue();
                                        }
                                    }
                            }
                        },
                
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 100,
                            id: 'reImpresionTalonesTractorTxt',
                            fieldLabel: 'Tractor',
                                                        labelWidth: 60,          
                            name: 'reImpresionTalonesTractorTxt',
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 280,
                            y: 100,
                            id: 'reImpresionTalonesViajeTxt',
                            fieldLabel: 'Viaje',
                            labelWidth: 40,
                            name: 'reImpresionTalonesViajeTxt',
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 130,
                            id: 'reImpresionTalonesOperadorTxt',
                            fieldLabel: 'Operador',
                            labelWidth: 60,
                            name: 'reImpresionTalonesOperadorTxt',
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 160,
                            id: 'reImpresionTalonesPlazaOrigenTxt',
                            fieldLabel: 'Origen',
                            labelWidth: 60,
                            width: 250,
                            name: 'reImpresionTalonesPlazaOrigenTxt',
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 280,
                            y: 160,
                            id: 'reImpresionTalonesPlazaDestinoTxt',
                            fieldLabel: 'Destino',
                            labelWidth: 50,
                            name: 'reImpresionTalonesPlazaDestinoTxt',
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 190,
                            id: 'reImpresionTalonesUnidadesTxt',
                            fieldLabel: 'Unidades',
                            labelWidth: 60,
                            name: 'reImpresionTalonesUnidadesTxt',
                            value: ''
                        },
                        {
                            xtype: 'displayfield',
                            x: 280,
                            y: 190,
                            id: 'reImpresionTalonesFechaViajeTxt',
                            fieldLabel: 'Fecha Talón',
                            labelWidth: 80,
                            name: 'reImpresionTalonesFechaViajeTxt',
                            value: ''
                        }  ,

                        {
                            xtype: 'hiddenfield',
                            x: 20,
                            y: 220,
                            id: 'impresionCPHdn',
                            labelWidth: 80,
                            Width: 180,
                            name: 'impresionCPHdn',
                            value: ''
                        } ,

                        {
                            xtype: 'hiddenfield',
                            x: 20,
                            y: 220,
                            id: 'impresionCPFechaHdn',
                            labelWidth: 80,
                            Width: 180,
                            name: 'impresionCPFechaHdn',
                            value: ''
                        }  ,

                        {
                            xtype: 'hiddenfield',
                            x: 20,
                            y: 220,
                            id: 'impresionCPCDHdn',
                            labelWidth: 80,
                            Width: 180,
                            name: 'impresionCPCDHdn',
                            value: ''
                        }  ,
                        {
                            xtype: 'hiddenfield',
                            x: 20,
                            y: 220,
                            id: 'impresionFolioTimbradoHdn',
                            labelWidth: 80,
                            Width: 180,
                            name: 'impresionFolioTimbradoHdn',
                            value: ''
                        }  ,


                        {
                            xtype: 'hiddenfield',
                            x: 20,
                            y: 220,
                            id: 'impresionCPUUIDHdn',
                            labelWidth: 80,
                            Width: 180,
                            name: 'impresionCPUUIDHdn',
                            value: ''
                        }                                             
                    ]
                }
            ],
            listeners: {
                beforeclose: {
                    fn:  function(panel, eOpts) {
                        var titulo = this.title;
                        Ext.Msg.confirm(titulo,'¿Está seguro que desea salir de<br> '+ titulo+'?',function(btn){
                            if(!Ext.getCmp('reimpresionTalonCPFrm').isDisabled()){
                                if(btn == 'yes'){
                                    Ext.getCmp('reimpresionTalonCPFrm').getForm().reset();
                                    panel.allowDestroy = true;
                                    panel.destroy();
                                }
                            }else{
                                if(btn == 'yes'){
                                    panel.allowDestroy = true;
                                    panel.destroy();
                                }
                            }
                        }, panel);
                        return false;
                    }
                },
                boxready: {
                    fn: function(component, width, height, eOpts) {
                        var elements = Ext.ComponentQuery.query('[xtype=textfield]').concat(Ext.ComponentQuery.query('[xtype=combobox]')).concat(Ext.ComponentQuery.query('[xtype=numberfield]'));

                        //Por cada elemento lo suscribo a un evento keypress, con un eventHandler llamado nextFocus
                        Ext.each(elements, function(el){
                            el.on('keypress', nextFocus);
                        });

                        //Event Handler que se encarga de recorrer y hacer FOCO a los elementos del formulario.
                        function nextFocus(field, e){
                            var actual = field;

                            if (e.getKey() === Ext.EventObject.ENTER) { //Si el input del teclado es Enter...
                                while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){//Recorro los elementos. Si existe el elemento siguiente y es Visible (por los hiddens fields).
                                    actual = actual.next();
                                }
                                if(actual.next()){
                                    actual.next().focus();
                                } else {//Si estoy en el elemento final regreso al primero...NO SÉ si sea el comportamiento requerido.

                                    while(actual.prev()){//Recorro mientras exista elemento
                                        actual = actual.prev();
                                    }
                                    if(actual.isVisible()){//Si no es un hidden field le hago foco
                                        actual.focus();
                                    } else {// de lo contrario vuelvo a recorrer hasta encontrar algún elemento visible
                                        while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){
                                            actual = actual.next();
                                        }
                                        actual.next().focus();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }
        win_impresionCPWin.show();
        return win_impresionCPWin;
    }
})