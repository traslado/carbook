var numTractor = Ext.getCmp('trCancelacionGastosTractorFolioCmb').getValue();
var folio = Ext.getCmp('trCancelacionGastosFolioTxt').getValue();
var storeTractor = Ext.data.StoreManager.lookup('trCancelacionGastosViajesStr');
storeTractor.load({
    params:{
        trViajesTractoresIdTractorHdn:numTractor,
        trViajesTractoresActionHdn:'getViajes'},
    callback: function(records, operation, success){
        var index1 = storeTractor.find('idTractor',numTractor);
        if (index1 === 0)
        {
            var cveMovimiento = records[0].get('claveMovimiento');
            var idViaje = records[0].get('idViajeTractor');
            var nViaje = records[0].get('viaje');

            switch(cveMovimiento) {
                case 'VV':
                    Ext.Msg.alert('Cancelacion de Gastos','El tractor seleccionado tiene un estatus: PRE VIAJE, no se puede agregar una lista de espera',function(btn){
                        if(btn == 'ok' | btn === 'cancel')
                        {
                            Ext.getCmp('trCancelacionGastosCompaniaFolioCmb').setValue(null).focus();
                            Ext.getCmp('trCancelacionGastosTractorFolioCmb').setValue('');
                        }
                    });
                    break;
                case 'VC':
                    Ext.Msg.alert('Cancelacion de Gastos','El tractor seleccionado tiene un estatus: VIAJE VACIO,no se puede agregar una lista de espera',function(btn){
                        if(btn == 'ok' | btn === 'cancel')
                        {
                            Ext.getCmp('trCancelacionGastosCompaniaFolioCmb').setValue(null).focus();
                            Ext.getCmp('trCancelacionGastosTractorFolioCmb').setValue('');
                        }
                    });
                    break;
                case 'VF':
                    Ext.Msg.alert('Cancelacion de Gastos','No puedes Cancelar Anticipos ni Complementos en esta pantalla',function(btn){
                        if(btn == 'ok' | btn === 'cancel')
                        {
                            Ext.getCmp('trCancelacionGastosCompaniaFolioCmb').setValue(null).focus();
                            Ext.getCmp('trCancelacionGastosTractorFolioCmb').setValue('');
                        }
                    });

                    break;
                case 'VG':
                    Ext.Msg.alert('Cancelacion de Gastos','Se cancelaran los Gastos del Viaje',function(btn){
                        if(btn == 'ok' | btn === 'cancel')
                        {
                            //Ext.getCmp("trGastosViajeTractorIdViajeTractorHdn").setValue(idViaje);
                            Ext.getCmp('cancelacionAsignacionCompaniaHdn').setValue(Ext.getCmp('trCancelacionGastosCompaniaFolioCmb').getValue());
                            Ext.getCmp('trViajesTractoresIdTractorHdn').setValue(Ext.getCmp("trCancelacionGastosTractorFolioCmb").getValue());
                            Ext.getCmp("trCancelacionGastosCompaniaFolioCmb").disable(true);
                            Ext.getCmp("trCancelacionGastosTractorFolioCmb").disable(true);
                            Ext.getCmp('trCancelacionGastosFolioTxt').disable(true);
                            //Ext.getCmp('cancelacionAsignacionChoferHdn').setValue(records[0].get('claveChofer'));
                            Ext.getCmp("trCancelacionGastosChoferFolioLbl").setValue(records[0].get('nombreChofer'));
                            Ext.getCmp("trCancelacionGastosOrigenFolioLbl").setValue(records[0].get('descPlazaOrigen'));
                            Ext.getCmp("trCancelacionGastosDestinoFolioLbl").setValue(records[0].get('descPlazaDestino'));
                            Ext.getCmp("trCancelacionGastosKmFolioLbl").setValue(records[0].get('kilometrosTabulados'));
                            Ext.getCmp("trCancelacionGastosRepartosFolioLbl").setValue(records[0].get('numeroRepartos'));
                            //Ext.getCmp("cancelacionViajesUnidadesDsp").setValue(records[0].get('numeroUnidades'));
                            //Ext.getCmp("cancelacionViajesOrigenDsp").setValue(records[0].get('descPlazaOrigen'));
                            //Ext.getCmp("cancelacionViajesDestinoDsp").setValue(records[0].get('descPlazaDestino'));
                            //Ext.getCmp("trCancelacionViajesIdPadreHdn").setValue(records[0].get('idViajePadre'));
                            var consultaGrd = Ext.getCmp("trCancelacionGastosFolioGrd").getStore();

                            consultaGrd.load({
                                params:{
                                  trGastosViajeTractorIdViajeTractorHdn:idViaje,
                                    trGastosViajeTractorFolioTxt: folio,
                                  trGastosViajeTractorActionHdn:"getGastosViajeTractor"
                                },
                                callback: function(records, operation, success){
                                    if((consultaGrd.getCount()) === 0){
                                        Ext.Msg.alert('Error', 'Este viaje no tiene el folio: '+folio);
                                        //Fire event del boton cancelar.
                                    }
                                }
                            });
                        }
                    });
                    break;
                case 'VP':
                    Ext.Msg.alert('Cancelacion de Gastos','Viaje Pagado, no puedes cancelar el Viaje',function(btn){
                        if(btn == 'ok' | btn === 'cancel')
                        {
                            Ext.getCmp('trCancelacionGastosCompaniaViajesCmb').setValue(null).focus();
                            Ext.getCmp('trCancelacionGastosTractorViajesCmb').setValue('');
                        }
                    });
                    break;
                case 'VU':
                    Ext.Msg.alert('Cancelacion de Gastos','Viaje Comprobado, no puedes cancelar el Vaije',function(btn){
                        if(btn == 'ok' | btn === 'cancel')
                        {
                            Ext.getCmp('trCancelacionGastosCompaniaViajesCmb').setValue(null).focus();
                            Ext.getCmp('trCancelacionGastosTractorViajesCmb').setValue('');
                        }
                    });
                    break;
                case 'VX':
                    Ext.Msg.alert('Cancelacion de Gastos','Viaje Cancelado, no puedes cancelar el Viaje',function(btn){
                        if(btn == 'ok' | btn === 'cancel')
                        {
                            Ext.getCmp('trCancelacionGastosCompaniaViajesCmb').setValue(null).focus();
                            Ext.getCmp('trCancelacionGastosTractorViajesCmb').setValue('');
                        }
                    });
                    break;
                case 'VA':
                    Ext.Msg.alert('Cancelacion de Gastos','No puedes Cancelar los anticipos de Acompañante ni complementos',function(btn){
                        if(btn == 'ok' | btn === 'cancel')
                        {
                            Ext.getCmp('trCancelacionGastosCompaniaViajesCmb').setValue(null).focus();
                            Ext.getCmp('trCancelacionGastosTractorViajesCmb').setValue('');

                        }
                    });
                    break;
                default:
                    break;
            }
        }
    }
});