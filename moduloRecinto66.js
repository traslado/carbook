/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.moduloRecinto66', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Recinto LZC',
            iconCls: 'cxc',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Desembarque Unds',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('desL1Win');
										win = module.createWindow_desL1Win();
									},
									scope: this,
									windowId: 'desL1Win'
								},
								{
									text: 'Carga Estatus Tender',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('estatusTenderWin');
										win = module.createWindow_estatusTenderWin();
									},
									scope: this,
									windowId: 'estatusTenderWin'
								},
								{
									text: 'Carga Inicial KIA/HY',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('alCinicialWin');
										win = module.createWindow_alCinicialWin();
									},
									scope: this,
									windowId: 'alCinicialWin'
								},
								{
									text: 'Interfeces Exportacion',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('intExpMenuWin');
										win = module.createWindow_intExpMenuWin();
									},
									scope: this,
									windowId: 'intExpMenuWin'
								},
								{
									text: 'SalidasPorTren',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('UnidadesXPlataformaWin');
										win = module.createWindow_UnidadesXPlataformaWin();
									},
									scope: this,
									windowId: 'UnidadesXPlataformaWin'
								},
								{
									text: 'SalidasPorMadrina',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('salidaMadrinaWin');
										win = module.createWindow_salidaMadrinaWin();
									},
									scope: this,
									windowId: 'salidaMadrinaWin'
								},
								{
									text: 'Actualizacion Fechas Port In LC',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('portInWin');
										win = module.createWindow_portInWin();
									},
									scope: this,
									windowId: 'portInWin'
								},
								{
									text: 'Canc Transmisiones LZC',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('cancelacionTransmisionesLzcWin');
										win = module.createWindow_cancelacionTransmisionesLzcWin();
									},
									scope: this,
									windowId: 'cancelacionTransmisionesLzcWin'
								}
				]
            }
        };
    }
});