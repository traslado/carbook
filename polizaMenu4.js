/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.polizaMenu4', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Polizas',
            iconCls: 'tractorModule',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Polizas',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('polizaGastosWin');
										win = module.createWindow_polizaGastosWin();
									},
									scope: this,
									windowId: 'polizaGastosWin'
								}
				]
            }
        };
    }
});