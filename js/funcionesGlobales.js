// var ciaSesVal = 'CDTOL';
// var usuClave = 'omnitrac';
// var gIVA = 0.16;
/**
 * [formato_numero: Convierte los numeros en formato de pesos]
 * @param  {[INT]} numero: Numero que se desea Convertir
 * @param  {[INT]} decimales: Numero de decimales que se desea obtener 
 * @param  {[STRING]} separador_decimal: Valor con lo que se separara los decimales (','.';'...Etc)
 * @param  {[STRING]} separador_miles: Valor con lo que se separara los miles (','.';'...Etc)
 * @return {[STRING]} 
 */
/*function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }
    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return '$'+ numero;
}*/
// >:1:@fn decimal_round
function decimal_round(value_pi, precision_pi=2) {
    var li_value = (isNaN(value_pi) || Ext.isEmpty(value_pi) || value_pi == Infinity)?0:parseFloat(value_pi);
    var li_number = Ext.util.Format.round(li_value, precision_pi);
    return li_number;
}