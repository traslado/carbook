/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.desbloqueoUnidades4', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Desbloqueo',
            iconCls: 'catMarcasDistribuidor',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Desbloqueo Tractor',
									iconCls:'catMarcasDistribuidor',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('desbloqueoTractorWin');
										win = module.createWindow_desbloqueoTractorWin();
									},
									scope: this,
									windowId: 'desbloqueoTractorWin'
								},
								{
									text: 'Desbloqueo Unidad',
									iconCls:'catMarcasDistribuidor',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('desbloqueoUnidadesWin');
										win = module.createWindow_desbloqueoUnidadesWin();
									},
									scope: this,
									windowId: 'desbloqueoUnidadesWin'
								}
				]
            }
        };
    }
});