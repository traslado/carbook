/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.avanzadasMenuModel100', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Recepcion de Unidades',
            iconCls: 'avanzadaMenu',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[

				]
            }
        };
    }
});