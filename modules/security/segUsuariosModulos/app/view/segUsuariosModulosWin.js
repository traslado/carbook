/*
 * File: app/view/segUsuariosModulosWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.segUsuariosModulosWin', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.toolbar.Separator',
        'Ext.tab.Panel',
        'Ext.tab.Tab',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.tree.Panel',
        'Ext.tree.View',
        'Ext.form.field.Display',
        'Ext.form.field.Checkbox',
        'Ext.grid.Panel',
        'Ext.grid.View',
        'Ext.grid.column.Column'
    ],

    height: 428,
    hidden: false,
    id: 'segUsuariosModulosWin',
    width: 554,
    layout: 'fit',
    title: 'Usuarios Modulos',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'segUsuariosModulosTlb',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                if(Ext.getCmp('segUsuariosModulosDatosFrm').getForm().isValid()){
                                    Ext.getCmp('segUsuariosModulosDatosFrm').submit({
                                        success: function(form, action) {
                                            Ext.Msg.alert("Generado","Generado");
                                            Ext.getCmp('segUsuariosModulosActionHdn').setValue('updUsuariosModulo');
                                        },
                                        failure: function(form, action) {
                                            Ext.Msg.alert("No Generado","No Generado");
                                        }
                                    });
                                }else{
                                    Ext.Msg.alert('Error', 'Faltan valores');
                                }
                            },
                            id: 'segUsuariosModulosGuardarBtn',
                            iconCls: 'ux-btnSave',
                            text: 'Guardar'
                        },
                        {
                            xtype: 'tbseparator',
                            id: 'segUsuariosModulos01Sep'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.getCmp('segUsuariosModulosDatosFrm').getForm().reset();
                                Ext.getCmp('segUsuariosModulosTabPnl').setActiveTab('segUsuariosModulosDatosPnl');
                            },
                            id: 'segUsuariosModulosCancelarBtn',
                            iconCls: 'ux-btnCancel',
                            text: 'Cancelar'
                        },
                        {
                            xtype: 'tbseparator',
                            id: 'segUsuariosModulos02Sep'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var store = Ext.data.StoreManager.lookup('segUsuariosModulosStr');
                                store.load({params:{segUsuariosModulosActionHdn:'getUsuariosModulos',
                                    segUsuariosModulosUsuarioHdn:Ext.getCmp('segUsuariosModulosUsuarioCmb').getValue()
                                }});
                                Ext.getCmp('segUsuariosModulosTabPnl').setActiveTab('segUsuariosModulosConsultaPnl');
                            },
                            id: 'segUsuariosModulosBuscarBtn',
                            iconCls: 'ux-btnSearch',
                            text: 'Buscar'
                        },
                        {
                            xtype: 'tbseparator',
                            id: 'segUsuariosModulos03Sep'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                if(Ext.getCmp('segUsuariosModulosUsuarioCmb').getValue() && Ext.getCmp('segUsuariosModulosUsuarioCmb').getValue() !== '')
                                {
                                    if(Ext.getCmp('segUsuariosModulosDialogWin'))
                                    {
                                        Ext.getCmp('segUsuariosModulosDialogWin').show();
                                        Ext.getCmp('segUsuariosModulosWin').mask();
                                    }
                                    else
                                    {
                                        Ext.create('MyApp.view.segUsuariosModulosDialogWin').show();
                                        Ext.getCmp('segUsuariosModulosWin').mask();
                                    }
                                }
                                else
                                {
                                    Ext.Msg.alert('Error', 'Es Necesario seleccionar un Usuario');
                                }
                            },
                            id: 'segUsuariosModulosCopiarBtn',
                            iconCls: 'ux-btnCopiar',
                            text: 'Copiar'
                        },
                        {
                            xtype: 'tbseparator',
                            id: 'segUsuariosModulos04Sep'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'tabpanel',
                    id: 'segUsuariosModulosTabPnl',
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'panel',
                            id: 'segUsuariosModulosDatosPnl',
                            layout: 'fit',
                            title: 'Datos',
                            tabConfig: {
                                xtype: 'tab',
                                id: 'segUsuariosModulosDatosPnlCfg'
                            },
                            items: [
                                {
                                    xtype: 'form',
                                    border: 0,
                                    id: 'segUsuariosModulosDatosFrm',
                                    layout: 'absolute',
                                    bodyPadding: 10,
                                    title: '',
                                    url: '../../carbookBck/json/segUsuariosModulos.php',
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            x: 10,
                                            y: 10,
                                            id: 'segUsuariosModulosUsuarioCmb',
                                            width: 520,
                                            fieldLabel: 'Usuario',
                                            name: 'segUsuariosModulosUsuarioHdn',
                                            allowBlank: false,
                                            editable: false,
                                            displayField: 'nombre',
                                            forceSelection: true,
                                            queryMode: 'local',
                                            store: 'segUsuariosModulosUsuariosStr',
                                            valueField: 'idUsuario',
                                            listeners: {
                                                change: {
                                                    fn: me.onSegUsuariosModulosUsuarioCmbChange,
                                                    scope: me
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'treepanel',
                                            x: 10,
                                            y: 40,
                                            height: 290,
                                            id: 'segUsuariosModulosMenuTree',
                                            width: 210,
                                            title: 'Menu',
                                            store: 'segUsuariosModulosMenuTree',
                                            folderSort: true,
                                            viewConfig: {
                                                id: 'segUsuariosModulosMenuTvw',
                                                blockRefresh: false,
                                                deferInitialRefresh: true
                                            },
                                            listeners: {
                                                itemclick: {
                                                    fn: me.onSegUsuariosModulosMenuTreeItemClick,
                                                    scope: me
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            x: 240,
                                            y: 40,
                                            id: 'segUsuariosModulosClaveMenuLbl',
                                            width: 200,
                                            fieldLabel: 'Clave menu',
                                            labelWidth: 75,
                                            name: 'segUsuariosModulosClaveMenuLbl'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            handler: function(checkbox, checked) {
                                                if(checked == true){
                                                    Ext.getCmp('segUsuariosModulosInsertarChk').disabled=false;
                                                    Ext.getCmp('segUsuariosModulosEliminarChk').disabled=false;
                                                    Ext.getCmp('segUsuariosModulosModificarChk').disabled=false;
                                                    Ext.getCmp('segUsuariosModulosConsultarChk').disabled=false;
                                                    Ext.getCmp('segUsuariosModulosEjecutarChk').disabled=false;
                                                    Ext.getCmp('segUsuariosModulosReporteChk').disabled=false;
                                                    Ext.getCmp('segUsuariosModulosActionHdn').setValue('addUsuariosModulos');
                                                }else{
                                                    Ext.getCmp('segUsuariosModulosInsertarChk').setValue(0);
                                                    Ext.getCmp('segUsuariosModulosEliminarChk').setValue(0);
                                                    Ext.getCmp('segUsuariosModulosModificarChk').setValue(0);
                                                    Ext.getCmp('segUsuariosModulosConsultarChk').setValue(0);
                                                    Ext.getCmp('segUsuariosModulosEjecutarChk').setValue(0);
                                                    Ext.getCmp('segUsuariosModulosReporteChk').setValue(0);

                                                    Ext.getCmp('segUsuariosModulosInsertarChk').disabled=true;
                                                    Ext.getCmp('segUsuariosModulosEliminarChk').disabled=true;
                                                    Ext.getCmp('segUsuariosModulosModificarChk').disabled=true;
                                                    Ext.getCmp('segUsuariosModulosConsultarChk').disabled=true;
                                                    Ext.getCmp('segUsuariosModulosEjecutarChk').disabled=true;
                                                    Ext.getCmp('segUsuariosModulosReporteChk').disabled=true;

                                                    Ext.getCmp('segUsuariosModulosActionHdn').setValue('dltUsuariosModulos');
                                                }
                                            },
                                            x: 460,
                                            y: 40,
                                            id: 'segUsuariosModulosClaveMenuActivoChk',
                                            fieldLabel: '',
                                            name: 'segUsuariosModulosClaveMenuActivoChk',
                                            boxLabel: 'Habilitar',
                                            inputValue: '1',
                                            uncheckedValue: '0'
                                        },
                                        {
                                            xtype: 'displayfield',
                                            x: 240,
                                            y: 70,
                                            id: 'segUsuariosModulosClaveMenuDescripcionLbl',
                                            width: 280,
                                            fieldLabel: 'Descripcion',
                                            labelWidth: 75,
                                            name: 'segUsuariosModulosClaveMenuDescripcionLbl'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            x: 240,
                                            y: 130,
                                            id: 'segUsuariosModulosInsertarChk',
                                            fieldLabel: '',
                                            name: 'segUsuariosModulosInsertarChk',
                                            boxLabel: 'Insertar',
                                            inputValue: '1',
                                            uncheckedValue: '0'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            x: 400,
                                            y: 130,
                                            id: 'segUsuariosModulosEliminarChk',
                                            fieldLabel: '',
                                            name: 'segUsuariosModulosEliminarChk',
                                            boxLabel: 'Eliminar',
                                            inputValue: '1',
                                            uncheckedValue: '0'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            x: 240,
                                            y: 170,
                                            id: 'segUsuariosModulosConsultarChk',
                                            fieldLabel: '',
                                            name: 'segUsuariosModulosConsultarChk',
                                            boxLabel: 'Consultar',
                                            inputValue: '1',
                                            uncheckedValue: '0'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            x: 400,
                                            y: 170,
                                            id: 'segUsuariosModulosModificarChk',
                                            fieldLabel: '',
                                            name: 'segUsuariosModulosModificarChk',
                                            boxLabel: 'Modificar',
                                            inputValue: '1',
                                            uncheckedValue: '0'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            x: 240,
                                            y: 210,
                                            id: 'segUsuariosModulosEjecutarChk',
                                            fieldLabel: '',
                                            name: 'segUsuariosModulosEjecutarChk',
                                            boxLabel: 'Ejecutar',
                                            inputValue: '1',
                                            uncheckedValue: '0'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            x: 400,
                                            y: 210,
                                            id: 'segUsuariosModulosReporteChk',
                                            fieldLabel: '',
                                            name: 'segUsuariosModulosReporteChk',
                                            boxLabel: 'Reporte',
                                            inputValue: '1',
                                            uncheckedValue: '0'
                                        },
                                        {
                                            xtype: 'textfield',
                                            x: 280,
                                            y: 290,
                                            id: 'segUsuariosModulosActionHdn',
                                            fieldLabel: 'Label',
                                            name: 'segUsuariosModulosActionHdn',
                                            value: 'addUsuariosModulos'
                                        },
                                        {
                                            xtype: 'textfield',
                                            x: 280,
                                            y: 260,
                                            id: 'segUsuariosModulosClaveMenuHdn',
                                            fieldLabel: 'Clave',
                                            name: 'segUsuariosModulosClaveMenuHdn'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            id: 'segUsuariosModulosConsultaPnl',
                            layout: 'fit',
                            title: 'Consulta',
                            tabConfig: {
                                xtype: 'tab',
                                id: 'segUsuariosModulosConsultaPnlCfg'
                            },
                            items: [
                                {
                                    xtype: 'gridpanel',
                                    border: 0,
                                    id: 'segUsuariosModulosConsultaGrd',
                                    title: '',
                                    store: 'segUsuariosModulosStr',
                                    columns: [
                                        {
                                            xtype: 'gridcolumn',
                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                var storeCmb =Ext.StoreMgr.lookup('segUsuariosModulosUsuariosStr');
                                                var index = storeCmb.find('idUsuario', value);
                                                if(index>-1){
                                                    var registro = storeCmb.getAt(index);
                                                    return registro.get('nombre');
                                                }
                                            },
                                            dataIndex: 'idUsuario',
                                            text: 'Usuario'
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            dataIndex: 'nombreForma',
                                            text: 'Modulo'
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                if(value){
                                                    return 'Si';
                                                }else{
                                                    return 'No';
                                                }
                                            },
                                            width: 55,
                                            dataIndex: 'alta',
                                            text: 'Alta'
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                if(value){
                                                    return 'Si';
                                                }else{
                                                    return 'No';
                                                }
                                            },
                                            width: 55,
                                            dataIndex: 'baja',
                                            text: 'Baja'
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                if(value){
                                                    return 'Si';
                                                }else{
                                                    return 'No';
                                                }
                                            },
                                            width: 55,
                                            dataIndex: 'cambio',
                                            text: 'Cambio'
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                if(value){
                                                    return 'Si';
                                                }else{
                                                    return 'No';
                                                }
                                            },
                                            width: 55,
                                            dataIndex: 'consulta',
                                            text: 'Consulta'
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                if(value){
                                                    return 'Si';
                                                }else{
                                                    return 'No';
                                                }
                                            },
                                            width: 55,
                                            dataIndex: 'ejecutar',
                                            text: 'Ejecutar'
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                if(value){
                                                    return 'Si';
                                                }else{
                                                    return 'No';
                                                }
                                            },
                                            width: 55,
                                            dataIndex: 'reporte',
                                            text: 'Reporte'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onSegUsuariosModulosUsuarioCmbChange: function(field, newValue, oldValue, eOpts) {
        Ext.getCmp('segUsuariosModulosMenuTree').getStore().load({params:{
            segUsuariosModulosIdUsuarioHdn:field.getValue()
        }});
    },

    onSegUsuariosModulosMenuTreeItemClick: function(dataview, record, item, index, e, eOpts) {
        if(Ext.getCmp('segUsuariosModulosUsuarioCmb').getValue()){
            var store = Ext.data.StoreManager.lookup('segUsuariosModulosStr');
            store.load({params:{segUsuariosModulosActionHdn:'getUsuariosModulos',
                segUsuariosModulosUsuarioHdn:Ext.getCmp('segUsuariosModulosUsuarioCmb').getValue(),
            segUsuariosModulosClaveMenuHdn:record.get('id')},
            callback: function(records, operation, success) {
                Ext.getCmp('segUsuariosModulosClaveMenuHdn').setValue(record.get('id'));
                if(records.length>0){
                    Ext.getCmp('segUsuariosModulosActionHdn').setValue('updUsuariosModulo');
                    Ext.getCmp('segUsuariosModulosClaveMenuLbl').setValue(record.data['claveMenu']);
                    Ext.getCmp('segUsuariosModulosClaveMenuDescripcionLbl').setValue(record.data['text']);
                    Ext.getCmp('segUsuariosModulosClaveMenuActivoChk').setValue(1);
                    Ext.getCmp('segUsuariosModulosInsertarChk').setValue(records[0].get('alta'));
                    Ext.getCmp('segUsuariosModulosEliminarChk').setValue(records[0].get('baja'));
                    Ext.getCmp('segUsuariosModulosModificarChk').setValue(records[0].get('cambio'));
                    Ext.getCmp('segUsuariosModulosConsultarChk').setValue(records[0].get('consulta'));
                    Ext.getCmp('segUsuariosModulosEjecutarChk').setValue(records[0].get('ejecutar'));
                    Ext.getCmp('segUsuariosModulosReporteChk').setValue(records[0].get('reporte'));
                }else{
                    Ext.getCmp('segUsuariosModulosClaveMenuLbl').setValue(record.get('claveMenu'));
                    Ext.getCmp('segUsuariosModulosClaveMenuDescripcionLbl').setValue(record.get('text'));
                    Ext.getCmp('segUsuariosModulosClaveMenuActivoChk').setValue(0);
                }
            }
        });
        }
        else
        {
            Ext.Msg.alert("Alerta", "Debe seleccionar un usuario");
        }
        Ext.getCmp('segUsuariosModulosClaveMenuHdn').setValue(record.get('id'));
        //Ext.getCmp('segUsuariosModulosActionHdn').setValue('addUsuariosModulos');
        Ext.getCmp('segUsuariosModulosClaveMenuLbl').setValue(record.get('claveMenu'));
        Ext.getCmp('segUsuariosModulosClaveMenuDescripcionLbl').setValue(record.get('text'));
        Ext.getCmp('segUsuariosModulosClaveMenuActivoChk').setValue(record.get('habilitada'));
        Ext.getCmp('segUsuariosModulosInsertarChk').setValue(record.get('alta'));
        Ext.getCmp('segUsuariosModulosEliminarChk').setValue(record.get('baja'));
        Ext.getCmp('segUsuariosModulosModificarChk').setValue(record.get('cambio'));
        Ext.getCmp('segUsuariosModulosConsultarChk').setValue(record.get('consulta'));
        Ext.getCmp('segUsuariosModulosEjecutarChk').setValue(record.get('ejecutar'));
        Ext.getCmp('segUsuariosModulosReporteChk').setValue(record.get('reporte'));
    }

});