/*
 * File: app/view/complementoPolizaTjTelefonoWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.viajesForaneos.complementoPoliza.complementoPolizaTjTelefonoWin', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.toolbar.Separator',
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.View',
        'Ext.grid.column.Column'
    ],

    height: 449,
    id: 'complementoPolizaTjTelefonoWin',
    width: 492,
    constrain: true,
    title: 'Comprobantes de Tarjetas Telefónicas',
    modal: true,
    resizable:false,
    iconCls:'tractorModule',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'complementoPolizaTjTelefonoTlb',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                this.up('window').close();
                            },
                            id: 'complementoPolizaTjTelefonoAceptarBtn',
                            iconCls: 'ux-btnSave',
                            text: 'Guardar'
                        },
                        {
                            xtype: 'tbseparator',
                            id: 'complementoPolizaTjTelefonoSep01'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.getCmp('complementoPolizaTjTelefonoQrTxt').setReadOnly(false);
                                Ext.getCmp('complementoPolizaTjTelefonoFrm').getForm().reset();
                            },
                            id: 'complementoPolizaTjTelefonoCancelarBtn',
                            iconCls: 'ux-btnDel',
                            text: 'Cancelar'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    border: false,
                    height: 221,
                    id: 'complementoPolizaTjTelefonoFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    url: '../../carbookBck/json/trcomplementoPoliza.php?complementoActionHdn=addFacturaGastos',
                    items: [
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 10,
                            id: 'complementoPolizaTjTelefonoQrTxt',
                            width: 440,
                            fieldLabel: 'Leer QR',
                            labelWidth: 90,
                            msgTarget: 'side',
                            validateOnBlur: false,
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            listeners: {
                                change: {
                                    fn: me.oncomplementoPolizaTjTelefonoQrTxtChange,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 40,
                            id: 'complementoPolizaTjTelefonoRfcTxt',
                            fieldLabel: 'R.F.C Emisor',
                            labelWidth: 90,
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            x: 290,
                            y: 40,
                            id: 'complementoPolizaTjTelefonoImporteTxt',
                            width: 170,
                            fieldLabel: 'Importe',
                            labelWidth: 50,
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 70,
                            id: 'complementoPolizaTjTelefonoFolioFiscalTxt',
                            width: 400,
                            fieldLabel: 'Folio Fiscal',
                            labelWidth: 90,
                            readOnly: true
                        },
                        {
                            xtype: 'datefield',
                            x: 20,
                            y: 100,
                            id: 'complementoPolizaTjTelefonoFechaTxt',
                            fieldLabel: 'Fecha',
                            labelWidth: 90,
                            width: 200,
                            msgTarget: 'side',
                            name: 'complementoPolizaTjTelefonoFechaTxt',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            format: 'd/m/Y',
                            maxText: 'La fecha debe ser anterior o igual a  {0}',
                            minText: 'La fecha debe ser posterior o igual a {0}',
                            listeners: {
                                blur: {
                                    fn: function(component, e, eOpts) {
                                        if (component.isValid()) {
                                            if(!Ext.isEmpty(component.getValue())){
                                                var dteFac = Ext.Date.format(component.getValue(), 'Y-m-d');
                                                var mesAfe = Ext.getCmp('complementoPolizaMesAfectacionCmb').getValue();
                                                // if (dteFac.substring(5, 7) != mesAfe) {
                                                //     Ext.Msg.alert('Complemento de Póliza',"Mes diferente al mes seleccionado para comprobación.",function(btn){
                                                //         component.reset();
                                                //     });
                                                // }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 130,
                            id: 'complementoPolizaTjTelefonoSubtotalTxt',
                            width: 200,
                            fieldLabel: 'Subtotal',
                            labelWidth: 90,
                            msgTarget: 'side',
                            allowBlank: false,
                            blankText: 'Campo Requerido',
                            maskRe: /[0-9\.]/,
                            listeners: {
                                blur: {
                                    fn: me.oncomplementoPolizaTjTelefonoSubtotalTxtBlur,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 160,
                            id: 'complementoPolizaTjTelefonoIVATxt',
                            width: 200,
                            fieldLabel: 'IVA',
                            labelWidth: 90,
                            msgTarget: 'side',
                            allowBlank: false,
                            blankText: 'Campo Requerido',
                            maskRe: /[0-9\.]/,
                            listeners: {
                                blur: {
                                    fn: me.oncomplementoPolizaTjTelefonoIVATxtBlur,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 190,
                            id: 'complementoPolizaTjTelefonoTotalTxt',
                            width: 200,
                            fieldLabel: 'Total',
                            labelWidth: 90,
                            value: '0.00',
                            readOnly: true
                        },
                        {
                            xtype: 'toolbar',
                            x: 230,
                            y: 130,
                            height: 80,
                            id: 'complementoPolizaTjTelefonoBotonesTlb',
                            width: 90,
                            vertical: true,
                            items: [
                                {
                                    xtype: 'button',
                                    handler: function(button, e) {
                                        var form = Ext.getCmp('complementoPolizaTjTelefonoFrm').getForm();

                                        if(form.isValid()){
                                            var selection = Ext.getCmp('complementoPolizaComprobacionGrd').getView().getSelectionModel().getSelection()[0];
                                            var numCompra = 0;

                                            if(Ext.data.StoreManager.lookup('complementoPolizaTjTelefonoStr').getCount() > 0){
                                                var record = Ext.data.StoreManager.lookup('complementoPolizaTjTelefonoStr').getAt(
                                                Ext.data.StoreManager.lookup('complementoPolizaTjTelefonoStr').getCount()-1
                                                );
                                                numCompra = parseInt(record.data.folio)+1;
                                            } else {
                                                numCompra = 1;
                                            }

                                            form.submit({
                                                params:{
                                                    trcomplementoPolizaIdViajeHdn:Ext.getCmp('complementoPolizaIdViajeHdn').getValue(),
                                                    trcomplementoPolizaConceptoHdn:selection.data.concepto,
                                                    trcomplementoPolizaRFCHdn:Ext.getCmp('complementoPolizaTjTelefonoRfcTxt').getValue(),
                                                    trcomplementoPolizaFolioHdn:numCompra,
                                                    trcomplementoPolizaFolioFiscalHdn:Ext.getCmp('complementoPolizaTjTelefonoFolioFiscalTxt').getValue(),
                                                    trcomplementoPolizaSubtotalHdn:Ext.getCmp('complementoPolizaTjTelefonoSubtotalTxt').getValue(),
                                                    trcomplementoPolizaIvaHdn:Ext.getCmp('complementoPolizaTjTelefonoIVATxt').getValue(),
                                                    trcomplementoPolizaTotalHdn:Ext.getCmp('complementoPolizaTjTelefonoImporteTxt').getValue(),
                                                    trcomplementoPolizaMesAfectacionHdn:Ext.getCmp('complementoPolizaMesAfectacionCmb').getValue(),
                                                    trcomplementoPolizaFechaHdn:Ext.getCmp('complementoPolizaTjTelefonoFechaTxt').getValue()
                                                },
                                                success: function(form, action){
                                                    Ext.data.StoreManager.lookup('complementoPolizaTjTelefonoStr').add({
                                                        folio:numCompra,
                                                        rfc:Ext.getCmp('complementoPolizaTjTelefonoRfcTxt').getValue(),
                                                        subTotal:Ext.getCmp('complementoPolizaTjTelefonoSubtotalTxt').getValue(),
                                                        iva:Ext.getCmp('complementoPolizaTjTelefonoIVATxt').getValue(),
                                                        total:Ext.getCmp('complementoPolizaTjTelefonoImporteTxt').getValue(),
                                                        folioFiscal:Ext.getCmp('complementoPolizaTjTelefonoFolioFiscalTxt').getValue()
                                                    });

                                                    Ext.getCmp('complementoPolizaTjTelefonoCancelarBtn').handler();

                                                    if(Ext.data.StoreManager.lookup('complementoPolizaTjTelefonoStr').getCount() 	){

                                                    }
                                                },
                                                failure: function(form, action){
                                                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, action.result.errorMessage);
                                                }
                                            });
                                        } else {
                                            Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "Faltan Campos Requeridos");
                                        }
                                    },
                                    height: 25,
                                    id: 'complementoPolizaTjTelefonoAgregarBtn',
                                    width: 78,
                                    iconCls: 'ux-btnSave',
                                    text: 'Agregar'
                                },
                                {
                                    xtype: 'button',
                                    handler: function(button, e) {
                                        var selection = Ext.getCmp('complementoPolizaTjTelefonoGrd').getView().getSelectionModel().getSelection()[0];
                                        if(selection){
                                            Ext.Ajax.request({
                                                url: '../../carbookBck/json/trcomplementoPoliza.php',
                                                method: 'POST',
                                                params: {
                                                    complementoActionHdn:'dltFacturaGastos',
                                                    trcomplementoPolizaFolioFiscalHdn:selection.data.folioFiscal,
                                                    folioHdn: Ext.getCmp('complementoPolizaFolioTxt').getValue()
                                                },
                                                success: function(response){
                                                    if(selection.data.observaciones == 'LAVADA'){
                                                        var lavadaStore = Ext.getCmp('complementoPolizaComprobacionGrd').getStore();
                                                        var lavadasSelect = lavadaStore.getAt(lavadaStore.getCount()-2);

                                                        lavadasSelect.set('anticipo', parseFloat(lavadasSelect.data.anticipo) - 300);
                                                    }

                                                    Ext.getCmp('complementoPolizaTjTelefonoGrd').store.remove(selection);
                                                    button.disable();
                                                },
                                                failure: function(response){
                                                    var jsonRsp = JSON.parse(response.responseText);
                                                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, jsonRsp.errorMessage);

                                                }
                                            });
                                        }
                                    },
                                    disabled: true,
                                    height: 25,
                                    id: 'complementoPolizaTjTelefonoEliminarBtn',
                                    width: 78,
                                    iconCls: 'ux-btnDel',
                                    text: 'Eliminar'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    border: false,
                    height: 149,
                    id: 'complementoPolizaTjTelefonoPnl',
                    layout: 'absolute',
                    title: '',
                    items: [
                        {
                            xtype: 'gridpanel',
                            x: 10,
                            y: 10,
                            height: 130,
                            id: 'complementoPolizaTjTelefonoGrd',
                            width: 460,
                            title: '',
                            enableColumnHide: false,
                            enableColumnMove: false,
                            sortableColumns: false,
                            store: 'complementoPolizaTjTelefonoStr',
                            viewConfig: {
                                id: 'complementoPolizaTjTelefonoGrdVw'
                            },
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    width: 70,
                                    dataIndex: 'folio',
                                    text: '# Compra'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        return decimal_round(value);
                                    },
                                    width: 90,
                                    dataIndex: 'subTotal',
                                    text: 'Subtotal'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        return decimal_round(value);
                                    },
                                    width: 90,
                                    dataIndex: 'iva',
                                    text: 'IVA'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        return decimal_round(value);
                                    },
                                    width: 90,
                                    dataIndex: 'total',
                                    text: 'Total'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 150,
                                    dataIndex: 'rfc',
                                    text: 'RFC'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 250,
                                    dataIndex: 'folioFiscal',
                                    text: 'FolioFiscal'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'observaciones',
                                    text: 'Observaciones'
                                }
                            ],
                            listeners: {
                                itemclick: {
                                    fn: me.oncomplementoPolizaTjTelefonoGrdItemClick,
                                    scope: me
                                }
                            }
                        }
                    ]
                }
            ],
            listeners: {
                boxready: {
                    fn: me.oncomplementoPolizaTjTelefonoWinBoxReady,
                    scope: me
                },
                beforeclose: {
                    fn: me.oncomplementoPolizaTjTelefonoWinBeforeClose,
                    scope: me
                },
                render: {
                    fn: me.oncomplementoPolizaTjTelefonoWinRender,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    oncomplementoPolizaTjTelefonoQrTxtChange: function(field, newValue, oldValue, eOpts) {
        var lacturaQR = Ext.getCmp('complementoPolizaTjTelefonoQrTxt').getValue();
        var numCaracteres = lacturaQR.length;

        console.log("aqui entro");

        if(numCaracteres >= 79){
            var qr = lacturaQR;
            var match;

            Ext.getCmp('complementoPolizaTjTelefonoQrTxt').setReadOnly(true);
            console.log(lacturaQR);
            match = qr.match(/id¿(.*?)$/);

            Ext.Ajax.request({
                url: '../../carbookBck/json/trcomplementoPoliza.php?',
                method: 'POST',
                params: {
                    complementoActionHdn:'montoLavada',
                    montoConcepto:'rfcTraslado'
                },
                success: function(response){
                    var jsonRsp = JSON.parse(response.responseText);

                    if(jsonRsp && jsonRsp.root.length > 0){

                        var rfcEmpresa = jsonRsp.root[0].nombre;

                        match = qr.match(/rr¿(.*?)(?=\/)/);

                        if(rfcEmpresa === match[1].trim() ){
                            match = qr.match(/rr¿(.*?)(?=\/)/);
                            if(match && match.length > 0){
                                Ext.getCmp('complementoPolizaTjTelefonoQrTxt').setReadOnly(true);
                                match = qr.match(/id¿(.*?)$/);
                                var folFiscal = match[1].replace(/\'|\’/g, '-').toUpperCase().trim();
                                var rsFiscal = folFiscal.substring(0, 36);
                                if(match && match.length > 0){
                                    Ext.Ajax.request({
                                        url: '../../carbookBck/json/trcomplementoPoliza.php',
                                        method: 'POST',
                                        params: {
                                            complementoActionHdn:'getFacturasPoliza',
                                            trcomplementoPolizaFolioFiscalHdn:rsFiscal,
                                            folioHdn: Ext.getCmp('complementoPolizaFolioTxt').getValue()
                                        },
                                        success: function(response){
                                            var jsonRsp = JSON.parse(response.responseText);

                                            if(!jsonRsp || jsonRsp.records === 0){
                                               Ext.getCmp('complementoPolizaTjTelefonoFolioFiscalTxt').setValue(rsFiscal);
                                            
                                               // Ext.getCmp('complementoPolizaTjTelefonoFolioFiscalTxt').setValue(match[1].replace(/\'|\’/g, '-').toUpperCase());
                                                match = qr.match(/tt¿(.*?)(?=\/)/);
                                                if(match && match.length > 0){
                                                    Ext.getCmp('complementoPolizaTjTelefonoImporteTxt').setValue(decimal_round(match[1]));

                                                    match = qr.match(/re¿(.*?)(?=\/)/);
                                                    if(match && match.length > 0){
                                                        Ext.getCmp('complementoPolizaTjTelefonoRfcTxt').setValue(match[1].trim());
                                                    } else {
                                                        Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "No se encontró el Folio Fiscal en el QR");
                                                    }
                                                } else {
                                                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "No se encontró el Total en el QR");
                                                }
                                            } else {
                                                Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title,
                                                              "La Factura con el Folio Fiscal " +rsFiscal+" ya fue capturada");
                                                Ext.getCmp('complementoPolizaTjTelefonoQrTxt').setReadOnly(false);
                                                Ext.getCmp('complementoPolizaTjTelefonoQrTxt').setValue('');
                                                return;
                                            }
                                        },
                                        failure: function(response){
                                            Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "Error al verificar el Folio Fical");
                                            component.setValue('');
                                            Ext.getCmp('complementoPolizaTjTelefonoImporteTxt').setValue('');
                                            Ext.getCmp('complementoPolizaTjTelefonoRfcTxt').setValue('');
                                            component.focus();
                                            return;
                                        }
                                    });
                                } else {
                                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "No se encontró el RFC del emisor en el QR");
                                }
                            } else {
                                Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "No se encontró el RFC del emisor en el QR");
                            }
                        }else{
                            Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "El RFC de la factura no es el de la Empresa");
                        }
                    }
                },
                failure: function(response){
                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "Error al obtener el RFC de la empresa");
                    //Ext.getCmp('complementoPolizaAlimentosMontoLbl').disable(true);
                }
            });
        }
    },

    oncomplementoPolizaTjTelefonoSubtotalTxtBlur: function(component, e, eOpts) {
        Ext.getCmp('complementoPolizaTjTelefonoTotalTxt').setValue('0');
        var totImporte = Ext.getCmp('complementoPolizaTjTelefonoImporteTxt').getValue();
        if (!Ext.isEmpty(totImporte)) {
            totImporte = Ext.util.Format.round(totImporte, 2);
            var subtotal = decimal_round(component.getValue());
            var iva = decimal_round(Ext.getCmp('complementoPolizaTjTelefonoIVATxt').getValue());
            var suma = decimal_round(subtotal + iva);
            if (suma != parseFloat(totImporte)) {
                if (!Ext.isEmpty(component.getValue()) && Ext.getCmp('complementoPolizaTjTelefonoIVATxt').getValue() != '') {
                    suma = iva;
                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "El importe no coincide con los valores ingresados", function(btn) {
                        component.setValue('');
                        component.focus();
                    });
                }
            }
            Ext.getCmp('complementoPolizaTjTelefonoTotalTxt').setValue(suma);
        }
    },

    oncomplementoPolizaTjTelefonoIVATxtBlur: function(component, e, eOpts) {
        Ext.getCmp('complementoPolizaTjTelefonoTotalTxt').setValue('0');
        var totImporte = Ext.getCmp('complementoPolizaTjTelefonoImporteTxt').getValue();
        if (!Ext.isEmpty(totImporte)) {
            totImporte = Ext.util.Format.round(totImporte, 2);
            var subtotal = decimal_round(Ext.getCmp('complementoPolizaTjTelefonoSubtotalTxt').getValue());
            var iva = decimal_round(component.value);
            var suma = decimal_round(subtotal + iva);
            var ivaMax = decimal_round(parseFloat(totImporte) * 16 / 100);
            if (iva > ivaMax) {
                suma = subtotal;
                // Total máximo autorizado del IVA
                Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "Total del IVA acreditable es <b>"+ivaMax+"</b>", function(btn) {
                    component.setValue('');
                    component.focus();
                });
            } else {
                if (suma != parseFloat(totImporte)) {
                    if (!Ext.isEmpty(component.getValue()) && Ext.getCmp('complementoPolizaTjTelefonoSubtotalTxt').getValue() != '') {
                        suma = subtotal;
                        Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "El importe no coincide con los valores ingresados", function(btn) {
                            component.setValue('');
                            component.focus();
                        });
                    }
                }
            }
            Ext.getCmp('complementoPolizaTjTelefonoTotalTxt').setValue(suma);
        }
    },

    oncomplementoPolizaTjTelefonoGrdItemClick: function(dataview, record, item, index, e, eOpts) {
        if(index >= 0)
            Ext.getCmp('complementoPolizaTjTelefonoEliminarBtn').enable();
        else
            Ext.getCmp('complementoPolizaTjTelefonoEliminarBtn').disable();
    },

    oncomplementoPolizaTjTelefonoWinBoxReady: function(component, width, height, eOpts) {
        Ext.Ajax.request({
            url: '../../carbookBck/json/trcomplementoPoliza.php',
            method: 'POST',
            params: {
                complementoActionHdn:'getDatosPoliza',
                trcomplementoPolizaCentroDistHdn:ciaSesVal,
                conceptoHdn:'7019'
            },
            success: function(response){
                var jsonRsp = JSON.parse(response.responseText);
                if(!jsonRsp || jsonRsp.root.length <= 0){
                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "Error al obtener el concepto de Tarjetas Telefónicas");
                    this.up('window').close();
                } else {
                    Ext.getCmp('complementoPolizaTjTelefonoFechaTxt').setMinValue(Ext.getCmp('complementoPolizaFechaViajeDsp').getValue());
                    Ext.getCmp('complementoPolizaTjTelefonoFechaTxt').setMaxValue(new Date());
                    Ext.data.StoreManager.lookup('complementoPolizaTjTelefonoStr').load({
                        url: '../../carbookBck/json/trcomplementoPoliza.php',
                        params: {
                            complementoActionHdn: 'addFacturaGastos',
                            opcionHdn: '2',
                            idViajeTractorHdn: Ext.getCmp('complementoPolizaIdViajeHdn').getValue(),
                            conceptoHdn: '7019',
                            mesAfectacionHdn: Ext.getCmp('complementoPolizaMesAfectacionCmb').getValue(),
                            folioHdn: Ext.getCmp('complementoPolizaFolioTxt').getValue()
                        }
                    });
                }
            },
            failure: function(response){
                Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTjTelefonoWin').title, "Error al obtener el concepto de Tarjetas Telefónicas");
                this.up('window').close();
            }
        });
    },

    oncomplementoPolizaTjTelefonoWinBeforeClose: function(panel, eOpts) {
        var total = 0.00;
        var subtotal = 0.00;
        var iva = 0.00;

        Ext.data.StoreManager.lookup('complementoPolizaTjTelefonoStr').each(function(record){
            total += parseFloat(record.data.total);
            subtotal += parseFloat(record.data.subTotal);
            iva += parseFloat(record.data.iva);
        });

        var selection = Ext.getCmp('complementoPolizaComprobacionGrd').getView().getSelectionModel().getSelection()[0];
        if(selection && total >= 0.00){
            selection.set('total', decimal_round(total));
            selection.set('subtotal', decimal_round(subtotal));
            selection.set('iva', decimal_round(iva));
        }
    },

    oncomplementoPolizaTjTelefonoWinRender: function(component, eOpts) {
        /*------------------Funcion que simula el TAB con el ENTER en los campos--------------------------------*/

        //Hago un query con los elementos que sean textfields, numberfields o combobox y obtengo un array de ellos.
        var elements = Ext.ComponentQuery.query('[xtype=textfield]').concat(Ext.ComponentQuery.query('[xtype=combobox]')).concat(Ext.ComponentQuery.query('[xtype=numberfield]'));

        //Por cada elemento lo suscribo a un evento keypress, con un eventHandler llamado nextFocus
        Ext.each(elements, function(el){
            el.on('keypress', nextFocus);
        });

        //Event Handler que se encarga de recorrer y hacer FOCO a los elementos del formulario.
        function nextFocus(field, e){
            var actual = field;

            if (e.getKey() === Ext.EventObject.ENTER) { //Si el input del teclado es Enter...
                while(actual.next() && !actual.next().isVisible()){//Recorro los elementos. Si existe el elemento siguiente y es Visible (por los hiddens fields).
                    actual = actual.next();
                }
                if(actual.next()){
                    actual.next().focus();
                }
                else{//Si estoy en el elemento final regreso al primero...NO SÉ si sea el comportamiento requerido.

                    while(actual.prev()){//Recorro mientras exista elemento
                        actual = actual.prev();
                    }
                    if(actual.isVisible()){//Si no es un hidden field le hago foco
                        actual.focus();
                    }
                    else{// de lo contrario vuelvo a recorrer hasta encontrar algún elemento visible
                        while(actual.next() && !actual.next().isVisible()){
                            actual = actual.next();
                        }
                        actual.next().focus();
                    }
                }
            }
        }
    }

});