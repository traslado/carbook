/*
 * File: app/view/UnidadesXPlataformaWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.recintoLzc.UnidadesporPlataforma.UnidadesXPlataformaWin', {
    extend: 'Ext.window.Window',

    id:'UnidadesXPlataformaWin',

  createWindow_UnidadesXPlataformaWin: function(){
    var desktop = this.app.getDesktop();
    var win_UnidadesXPlataformaWin= desktop.getWindow('UnidadesXPlataformaWin');
    if(!win_UnidadesXPlataformaWin){
        var store = new MyDesktop.modules.recintoLzc.UnidadesporPlataforma.UnidadesxPlataformaTipoStr();
        var store = new MyDesktop.modules.recintoLzc.UnidadesporPlataforma.grdcargaPlataformaStr();
          
        win_UnidadesXPlataformaWin= desktop.createWindow({
            height: 635,
            id: 'UnidadesXPlataformaWin',
            width: 690,
            title: 'Salidas Por Tren (Recinto)',
            maximizable: false,
            resizable:false,
            iconCls:'hyundai',
            minimizable: true,
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                if((Ext.getCmp('UnidadesxPlatformaNoPlataformaTxt').getValue()=== '') || (Ext.getCmp('UnidadesxPlatformaNoWaybillTxt').getValue()=== '') || (Ext.getCmp('UnidadesxPlatformaFechaWaybillTxt').getValue()=== null) || (Ext.getCmp('UnidadesxPlatformaUnidadesTxt').getValue()=== '') || (
                                Ext.getCmp('UnidadesxPlatformaFechaSalidaTxt').getValue()=== null) || (
                                Ext.getCmp('UnidadesxPlatformaFechaHoraTxt').getValue()=== '') || (
                                Ext.getCmp('UnidadesxPlatformaFechaMinutoTxt').getValue()=== '') || (
                                Ext.getCmp('UnidadesxPlatformaFechaSegundoTxt').getValue()=== ''))

                                {
                                    Ext.Msg.alert("Error", "Falta Llenar campos requeridos o contienen Formato Incorrecto");
                                }

                                else {

                                    Ext.Ajax.request({
                                        url: '../../carbookBck/json/L3.php?',
                                        params: {
                                            alIngresoUnidadesLzcHdn:'addPlataformas',
                                            plataforma: Ext.getCmp('UnidadesxPlatformaNoPlataformaTxt').getValue(),
                                            tipo:Ext.getCmp('UnidadesxPlatformaTipoCmb').getValue(),
                                            Waybill:Ext.getCmp('UnidadesxPlatformaNoWaybillTxt').getValue(),
                                            WaybillDate:Ext.getCmp('UnidadesxPlatformaFechaWaybillTxt').getValue(),
                                            vines:Ext.getCmp('UnidadesxPlatformaUnidadesTxt').getValue(),
                                            fechaSalida:Ext.getCmp('UnidadesxPlatformaFechaSalidaTxt').getValue(),
                                            horaSalida: ' '+Ext.getCmp('UnidadesxPlatformaFechaHoraTxt').getValue() + ':' + Ext.getCmp('UnidadesxPlatformaFechaMinutoTxt').getValue() + ':' + Ext.getCmp('UnidadesxPlatformaFechaSegundoTxt').getValue()

                                        },

                                        success: function(response){
                                            response = Ext.JSON.decode(response.responseText);

                                            if(response.records !== 0){

                                                var text='';
                                                var text1='';

                                                for (i = 0 ; i < response.records; i++) {

                                                    if(response.root[i].cveStatus === 'SO') {

                                                        text += response.root[i].vin + "<br>";

                                                    }
                                                    else if (response.root[i].cveStatus === 'DH' || response.root[i].cveStatus === 'DK'){

                                                        text1 += response.root[i].vin + "<br>";

                                                    }

                                                }


                                                Ext.Msg.alert("Unidades por Plataforma", "Estas unidades ya cuentan con salida:<br>" + text + "Estas unidades no cuentan con tender:<br>" + text1);
                                                Ext.getCmp('UnidadesxPlatformaNoPlataformaTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaTipoCmb').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaNoWaybillTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaWaybillTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaUnidadesTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaSalidaTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaHoraTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaMinutoTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaSegundoTxt').setValue('');


                                                var store1 = Ext.StoreMgr.lookup('grdcargaPlataformaStr');



                                                store1.load

                                                ({

                                                });

                                            }
                                            else{
            
                                                  var fechaSalida =Ext.Date.format(Ext.getCmp('UnidadesxPlatformaFechaSalidaTxt').getValue(),'Y-m-d');

                                                  var horaSalida = ' '+Ext.getCmp('UnidadesxPlatformaFechaHoraTxt').getValue() + ':' + Ext.getCmp('UnidadesxPlatformaFechaMinutoTxt').getValue() + ':' + Ext.getCmp('UnidadesxPlatformaFechaSegundoTxt').getValue();

                                                  Ext.Msg.alert("Unidades por Plataforma", "Unidades Registradas Correctamente <br> Fecha de Salida:" + fechaSalida + horaSalida );

                                                Ext.getCmp('UnidadesxPlatformaNoPlataformaTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaTipoCmb').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaNoWaybillTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaWaybillTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaUnidadesTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaSalidaTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaHoraTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaMinutoTxt').setValue('');
                                                Ext.getCmp('UnidadesxPlatformaFechaSegundoTxt').setValue('');


                                                var store1 = Ext.StoreMgr.lookup('grdcargaPlataformaStr');



                                                store1.load

                                                ({

                                                });

                                                console.log(store1.getCount);
                                                //Ext.getCmp('UnidadesxPlatformatotUnidadesTxt').getValue=;
                                            }
                                        }
                                    });
                                }


                            },
                            id: 'UnidadesxPlatformaAceptarBtn',
                            iconCls: 'ux-btnSave',
                            text: 'Aceptar'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.getCmp('UnidadesxPlataformaFrm').getForm().reset();

                                Ext.Ajax.request({
                                    url: '../../carbookBck/json/L3.php?',
                                    params: {
                                        alIngresoUnidadesLzcHdn:'delPlataformas'

                                    },

                                });

                                var store = Ext.StoreMgr.lookup('grdcargaPlataformaStr');
                                store.load
                                ({
                                });






                            },
                            id: 'UnidadesxPlatformaCancelarBtn',
                            iconCls: 'ux-btnDel',
                            text: 'Cancelar'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    height: 567,
                    id: 'UnidadesxPlataformaFrm',
                    width: 696,
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 30,
                            id: 'UnidadesxPlatformaNoPlataformaTxt',
                            width: 240,
                            fieldLabel: 'No.Plataforma',
                            labelWidth: 85,
                            name: 'UnidadesxPlatformaNoPlataformaTxt',
                            enforceMaxLength: true,
                            maxLength: 10
                        },
                        {
                            xtype: 'combobox',
                            x: 20,
                            y: 70,
                            id: 'UnidadesxPlatformaTipoCmb',
                            width: 240,
                            focusOnToFront: false,
                            fieldLabel: 'Tipo',
                            labelWidth: 85,
                            name: 'UnidadesxPlatformaTipoCmb',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            enforceMaxLength: true,
                            maxLength: 15,
                            editable: false,
                            autoSelect: false,
                            displayField: 'nombre',
                            store: 'UnidadesxPlataformaTipoStr',
                            valueField: 'valor'
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 110,
                            id: 'UnidadesxPlatformaNoWaybillTxt',
                            width: 240,
                            fieldLabel: 'No.Guia',
                            labelWidth: 85,
                            name: 'UnidadesxPlatformaNoWaybillTxt',
                            enforceMaxLength: true,
                            maskRe: /[0-9]/,
                            maxLength: 12
                        },
                        {
                            xtype: 'datefield',
                            x: 20,
                            y: 150,
                            id: 'UnidadesxPlatformaFechaWaybillTxt',
                            width: 240,
                            fieldLabel: 'Fecha Documentacion',
                            labelWidth: 120,
                            name: 'UnidadesxPlatformaFechaWaybillTxt',
                            maxLength: 2,
                            minLength: 2,
                            editable: false,
                            format: 'Y-m-d',
                            maxText: 'Seleccione una fecha anterior o igual a:  {0}',
                            useStrict: true,
                            listeners: {
                                beforerender: {
                                    fn: function(component, eOpts) {
                                         var fechaProceso = new Date();
                                          Ext.getCmp("UnidadesxPlatformaFechaWaybillTxt").setMaxValue(fechaProceso);
                                    },
                                }
                            }
                        },
                        {
                            xtype: 'datefield',
                            x: 20,
                            y: 190,
                            id: 'UnidadesxPlatformaFechaSalidaTxt',
                            width: 240,
                            fieldLabel: 'Fecha Salida:',
                            labelWidth: 120,
                            name: 'UnidadesxPlatformaFechaSalidaTxt',
                            editable: false,
                            format: 'Y-m-d',
                            maxText: 'Seleccione una fecha anterior o igual a:  {0}',
                            useStrict: false,
                            listeners: {
                                beforerender: {
                                    fn: function(component, eOpts) {
                                         var fechaProceso = new Date();
                                          Ext.getCmp("UnidadesxPlatformaFechaSalidaTxt").setMaxValue(fechaProceso);
                                    },

                                }
                            }
                        },
                        {
                            xtype: 'label',
                            x: 350,
                            y: 10,
                            height: 20,
                            id: 'UnidadesxPlatformaUnidadesLbl',
                            text: 'Captura de unidades por No.Plataforma:'
                        },
                        {
                            xtype: 'textareafield',
                            x: 290,
                            y: 30,
                            height: 143,
                            id: 'UnidadesxPlatformaUnidadesTxt',
                            width: 380,
                            fieldLabel: '',
                            name: 'UnidadesxPlatformaUnidadesTxt'
                        },
                        {
                            xtype: 'textfield',
                            x: 310,
                            y: 190,
                            id: 'UnidadesxPlatformaFechaHoraTxt',
                            width: 80,
                            fieldLabel: 'Hora ',
                            labelWidth: 40,
                            name: 'UnidadesxPlatformaFechaHoraTxt',
                            enforceMaxLength: true,
                            maskRe: /[0-9]/,
                            maxLength: 2,
                            listeners: {
                                blur: {
                                    fn: function(component, e, eOpts) {
                                          var numHora = parseFloat(Ext.getCmp('UnidadesxPlatformaFechaHoraTxt').getValue());



                                                if(numHora >= 25 ){


                                                    Ext.Msg.alert('Unidades por plataforma','La Hora es Incorrecta');
                                                    Ext.getCmp('UnidadesxPlatformaFechaHoraTxt').setValue('');
                                                }



                                    },
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 420,
                            y: 190,
                            id: 'UnidadesxPlatformaFechaMinutoTxt',
                            width: 80,
                            fieldLabel: 'Minuto',
                            labelWidth: 40,
                            name: 'UnidadesxPlatformaFechaMinutoTxt',
                            enforceMaxLength: true,
                            maskRe: /[0-9]/,
                            maxLength: 2,
                            listeners: {
                                blur: {
                                    fn: function(component, e, eOpts) {
                                         var numMinuto = parseFloat(Ext.getCmp('UnidadesxPlatformaFechaMinutoTxt').getValue());

                                                if(numMinuto >= 60){
                                                    Ext.Msg.alert('Unidades por Plataforma','Los minutos son Incorrectos');
                                                    Ext.getCmp('UnidadesxPlatformaFechaMinutoTxt').setValue('');
                                                }else{

                                                }
                                    },
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 530,
                            y: 190,
                            id: 'UnidadesxPlatformaFechaSegundoTxt',
                            width: 90,
                            fieldLabel: 'Segundo',
                            labelWidth: 50,
                            name: 'UnidadesxPlatformaFechaSegundoTxt',
                            enforceMaxLength: true,
                            maskRe: /[0-9]/,
                            maxLength: 2,
                            listeners: {
                                blur: {
                                    fn:function(component, e, eOpts) {
                                          var numSegundo = parseFloat(Ext.getCmp('UnidadesxPlatformaFechaSegundoTxt').getValue());

                                                if(numSegundo >= 60){
                                                    Ext.Msg.alert('Unidades por Plataforma','Los segundos son Incorrectos');
                                                    Ext.getCmp('UnidadesxPlatformaFechaSegundoTxt').setValue('');
                                                }else{

                                                }
                                    },
                                }
                            }
                        },
                        {
                            xtype: 'gridpanel',
                            x: 20,
                            y: 230,
                            height: 320,
                            id: 'cargaPlataformaGrd',
                            width: 650,
                            title: 'Carga de unidades por Plataorma',
                            allowDeselect: true,
                            columnLines: true,
                            deferRowRender: false,
                            sealedColumns: true,
                            store: 'grdcargaPlataformaStr',
                            viewConfig: {
                                enableTextSelection: true
                            },
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    height: 35,
                                    id: 'cargaunidadesPlataformaTlb',
                                    items: [
                                        {
                                            xtype: 'button',
                                            handler: function(button, e) {
                                                var form = Ext.getCmp('UnidadesxPlataformaFrm').getForm();

                                                console.log(Ext.data.StoreManager.lookup('grdcargaPlataformaStr').data.keys['length']);

                                                if((Ext.data.StoreManager.lookup('grdcargaPlataformaStr').data.keys['length']) === 0){
                                                    Ext.Msg.alert("Unidades por Plataforma", "Sin unidades capturadas ");


                                                }else{

                                                    Ext.Ajax.request({
                                                        url: '../../carbookBck/json/L3.php?',
                                                        params: {
                                                            alIngresoUnidadesLzcHdn:'UpdPlataformas'

                                                        },
                                                    });

                                                    Ext.Msg.alert("Unidades por plataforma", "Unidades Registradas Correctamente");

                                                    var store = Ext.StoreMgr.lookup('grdcargaPlataformaStr');
                                                    store.load
                                                    ({
                                                    });
                                                }
                                            },
                                            id: 'CargaUnidadesxPlatformaguardarBtn',
                                            iconCls: 'ux-btnSave',
                                            text: 'Guardar'
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'button',
                                            handler: function(button, e) {
                                                var window = Ext.WindowManager.get('UnidadesXPlataformaWin');
                                                var recToRemove = Ext.getCmp('cargaPlataformaGrd').getSelectionModel().getSelection()[0];

                                                //console.log(Ext.getCmp('cargaPlataformaGrd').getSelectionModel().getSelection()[0]);

                                                if (Ext.getCmp('cargaPlataformaGrd').getSelectionModel().getSelection()[0] === undefined)
                                                {
                                                    Ext.Msg.alert("Error", "Seleccione una unidad para eliminar");

                                                }

                                                else {

                                                    if(recToRemove){
                                                        Ext.Msg.confirm(window.title, 'Deseas Eliminar: ' + recToRemove.get('Vin'), function(btn){
                                                            if(btn === 'yes'){

                                                                Ext.getCmp('cargaPlataformaGrd').getStore().remove(recToRemove);

                                                                console.log(recToRemove);

                                                                Ext.Ajax.request({
                                                                    method: 'POST',
                                                                    url: '../../carbookBck/json/L3.php?alIngresoUnidadesLzcHdn=eliminaTlTmp',
                                                                    params:{
                                                                        vin:recToRemove.get('Vin')
                                                                    }});

                                                                    Ext.Msg.alert(window.title, 'Unidad: ' + recToRemove.get('Vin') + 'Eliminada Correctamente');

                                                                    var store1 = Ext.StoreMgr.lookup('grdcargaPlataformaStr');

                                                                    store1.load

                                                                    ({

                                                                    });
                                                                }
                                                            });
                                                        }
                                                    }
                                            },
                                            id: 'CargaUnidadesxPlatformaEliminarBtn',
                                            iconCls: 'ux-btnCancel',
                                            text: 'Eliminar'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'pagingtoolbar',
                                    dock: 'bottom',
                                    width: 360,
                                    autoScroll: true,
                                    displayInfo: true,
                                    store: 'grdcargaPlataformaStr'
                                }
                            ],
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    width: 131,
                                    dataIndex: 'Vin',
                                    text: 'Vin'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 120,
                                    dataIndex: 'NoPlataforma',
                                    text: 'NoPlataforma'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 91,
                                    dataIndex: 'Tipo',
                                    text: 'Tipo'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'NoWaybill',
                                    text: 'NoWaybill'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 107,
                                    dataIndex: 'FechaWaybill',
                                    text: 'FechaWaybill'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 99,
                                    dataIndex: 'fechaSalida',
                                    text: 'FechaSalida'
                           }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
                beforeclose: {
                    fn:  function(panel, eOpts) {
                                var titulo = this.title;
                                Ext.Msg.confirm(titulo,'¿Está seguro que desea salir de<br> '+ titulo+'?',function(btn){
                                    if(!Ext.getCmp('UnidadesxPlataformaFrm').isDisabled()){
                                        if(btn == 'yes'){
                                            var idTractor = Ext.getCmp('UnidadesxPlatformaUnidadesTxt').getValue();
                                            Ext.getCmp('UnidadesxPlataformaFrm').getForm().reset();

                                            Ext.Ajax.request({
                                                url: '../../carbookBck/json/L3.php?',
                                                params: {
                                                    alIngresoUnidadesLzcHdn:'delPlataformas'

                                                },

                                            });

                                            var store = Ext.StoreMgr.lookup('grdcargaPlataformaStr');
                                            store.load
                                            ({
                                            });


                                            panel.allowDestroy = true;
                                            panel.destroy();
                                        }
                                    }else{
                                        if(btn == 'yes'){
                                            panel.allowDestroy = true;
                                            panel.destroy();
                                        }
                                    }
                                }, panel);
                                return false;

                            }
                }
            
          }
        });

    }
        win_UnidadesXPlataformaWin.show();
        return win_UnidadesXPlataformaWin;

}

});