Ext.define('MyDesktop.modules.comoDato.impresionComodato.impresionComodatoWin', {
    extend: 'Ext.ux.desktop.Module',
    
    id:'impresionComodatoWin',

    /*init : function(){
        this.launcher = {
            text: rhTRelLabTittle,
            iconCls:'auCatColores'
        };
    },*/
    
    createWindow_impresionComodatoWin : function(){
        var desktop = this.app.getDesktop();
        var win_impresionComodatoWin = desktop.getWindow('impresionComodatoWin');
        if(!win_impresionComodatoWin){

            // Integrar aquí los Stores que ocupará la pantalla
            var store = new MyDesktop.modules.comoDato.impresionComodato.impresionComodatoStr();
            
            win_impresionComodatoWin = desktop.createWindow({
                height: 473,
                hidden: false,
                maximizable: false,
                minimizable:true,
                resizable:false,
                id: 'impresionComodatoWin',
                width: 747,
                iconCls:'fca',
                layout: 'absolute',
                title: 'Impresión de unidades Comodato',
            items: [
                {
                    xtype: 'form',
                    height: 438,
                    id: 'impresionComodatoFrm',
                    layout: 'absolute',
                    bodyPadding: 10,                    
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            x: 71,
                            y: 39,
                            dock: 'top',
                            items: [
                                {
                                    xtype: 'button',
                                    handler: function(button, e) {
                                        var store = Ext.StoreMgr.lookup('impresionComodatoStr');
                                        Ext.Msg.wait("Espere buscando información ... ", 'Impresión de unidades Comodato');
                                        store.load({
                                            params:{
                                                getImpresionComodato: 'unidadesGrd'
                                            },
                                            callback: function (records, operation, success) {
                                            		Ext.Msg.hide();
                                            }
                                        });

                                    },
                                    id: 'impresionComodatoConsultarBtn',
                                    iconCls: 'ux-btnSave',
                                    text: 'Consultar'
                                },
                                {
                                    xtype: 'tbseparator'
                                },
                                {
                                    xtype: 'button',
                                    handler: function(button, e) {
                                        Ext.Msg.wait("Espere imprimiendo información ... ", 'Impresión de unidades Comodato');
                                        Ext.Ajax.request({
                                            url: '../../carbookBck/json/impresionComodato.php',
                                            params: {
                                                getImpresionComodato:'consultaVines'
                                            },
                                            success: function(response){
                                                Ext.Msg.hide();
                                                response = Ext.JSON.decode(response.responseText);
                                                window.open("../../carbookBck/json/etiquetasExportacion.php?alUnidadesVinHdn=" + response);
                                            }
                                        });
                                        Ext.Ajax.request({
                                            url: '../../carbookBck/json/impresionComodato.php',
                                            params: {
                                                getImpresionComodato:'updateUnidades'
                                            },
                                        });
                                        Ext.getCmp('impresionComodatoGrd').getStore().removeAll();
                                    },
                                    id: 'impresionComodatoImprimirBtn',
                                    iconCls: 'ux-btnPrint',
                                    text: 'Imprimir'
                                }
                            ]
                        }
                    ],
                    items: [
                        {
                            xtype: 'gridpanel',
                            x: 20,
                            y: 20,
                            height: 350,
                            id: 'impresionComodatoGrd',
                            title: 'Unidades',
                            store: 'impresionComodatoStr',
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    width: 139,
                                    dataIndex: 'vin',
                                    text: 'Vin',
                                    editor: {
                                        xtype: 'displayfield',
                                        id: 'impresionComodatoVinDsp',
                                        width: 100,
                                        fieldLabel: '',
                                        value: 'Display Field'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 206,
                                    dataIndex: 'distribuidor',
                                    text: 'Distribuidor',
                                    editor: {
                                        xtype: 'displayfield',
                                        id: 'impresionComodatoDistribuidorDsp',
                                        value: 'Display Field'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 196,
                                    dataIndex: 'simbolo',
                                    text: 'Simbolo',
                                    editor: {
                                        xtype: 'displayfield',
                                        id: 'impresionComodatoSimboloDsp',
                                        value: 'Display Field'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 155,
                                    dataIndex: 'observaciones',
                                    text: 'Observaciones',
                                    editor: {
                                        xtype: 'displayfield',
                                        id: 'impresionComodatoObservacionesDsp',
                                        value: 'Display Field'
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
                listeners: {
                beforeclose: {
                    fn:function(panel, eOpts) {
                        var titulo = this.title;
                        Ext.Msg.confirm(titulo,'¿Está seguro que desea salir de<br> '+ titulo+'?',function(btn){
                                if(btn == 'yes'){
                                    panel.allowDestroy = true;
                                    panel.destroy();
                                }
 
                        }, panel);
                        return false;

                    }
                }
            }

        });
        }
        win_impresionComodatoWin.show();
        return win_impresionComodatoWin;
    }
});