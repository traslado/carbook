/*
 * File: app/store/monitorFTPRubiconStr.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.transmisiones.monitorFTPRubicon.monitorFTPRubiconStr', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
            pageSize: 50,
            storeId: 'monitorFTPRubiconStr',
            proxy: {
                type: 'ajax',
                url: '../../carbookBck/json/verificaTransRUBICON.php?actionHdn=busca',
                reader: {
                    type: 'json',
                    root: 'root',
                    totalProperty: 'records'
                }
            },
            fields: [
                {
                    name: 'logName'
                },
                {
                    name: 'dirFte'
                },
                {
                    name: 'file'
                },
                {
                    name: 'dateFTP'
                },
                {
                    name: 'tipoTransaccion'
                }
            ]
            /*,
            listeners: {
                beforeload: {
                    fn: function(store, operation, eOpts) {
                        if(!Ext.getCmp("monitorFTPRubiconTipo").getValue().tipoTransmisionRdo){
                            return false;
                        }
                        operation.params = {
                            tipoTransmision: Ext.getCmp("monitorFTPRubiconTipo").getValue() || '',
                            fecha: Ext.getCmp("monitorFTPRubiconFechaTxt").getRawValue() || ''
                        };
                    }
                }
            }
            */
        }, cfg)]);
    }
});