/*!
 * Ext JS Library 4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

// Prevent the backspace key from navigating back.
Ext.EventManager.on(Ext.isIE ? document : window, 'keydown', function(e, t) {
	if (e.getKey() == e.BACKSPACE && ((!/^input$/i.test(t.tagName) && !/^textarea$/i.test(t.tagName)) || t.disabled || t.readOnly)) {
		e.stopEvent();
	}
});

Ext.define('MyDesktop.App66', {
    extend: 'Ext.ux.desktop.App',

    requires: [
		'Ext.window.MessageBox',
		'Ext.ux.desktop.ShortcutModel',
		
		'MyDesktop.catGenMenuModel66',
		'MyDesktop.avanzadasMenuModel66',
		'MyDesktop.moduloRepuve66',
		'MyDesktop.moduloRecinto66',
		'MyDesktop.moduloGMC66',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosOrigenStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosTipoStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosMarcasCdStore',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosClasificacionStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosSimboloStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosWin',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistrbuidoresDirecionesMdl',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresTipoStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresPlazasStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresSucursalStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresRegionesStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresEstatusStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresPaisStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresEstadoStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresMunicipiosStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresColoniasStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresDireccionStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresDireccionesStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresZonaStr',
		'MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresWin',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276HistoricoStr',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276VinStr',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276EspecialesStr',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.alDstServiciosEspecialesWin',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276Win',
		'MyDesktop.modules.recepcionUnidades.reactivacionUnidades.reUnidadesConsultaStr',
		'MyDesktop.modules.recepcionUnidades.reactivacionUnidades.reUnidadesWin',
		'MyDesktop.modules.recintoLzc.desembarque.lzcUnidadStr1',
		'MyDesktop.modules.recintoLzc.desembarque.desL1Win',
		'MyDesktop.modules.recintoLzc.reImpresionEtiquetasLazaro.reImpresionEtiquetasLazaroWin',
		'MyDesktop.modules.recintoLzc.cargaEstatusTender.cargaestatusTenderStr',
		'MyDesktop.modules.recintoLzc.cargaEstatusTender.estatusTenderWin',
		'MyDesktop.modules.recintoLzc.interfacesLzc.interfacesLzcRepuveWin',
		'MyDesktop.modules.recintoLzc.cargaInicial.alCargaInicialUndStr',
		'MyDesktop.modules.recintoLzc.cargaInicial.alCinicialWin',
		'MyDesktop.modules.recintoLzc.cargaInicialGM.alCargaInicialUndGMStr',
		'MyDesktop.modules.recintoLzc.cargaInicialGM.alCinicialGMWin',
		'MyDesktop.modules.recintoLzc.campanasGM.catCampaniaConsultaStr',
		'MyDesktop.modules.recintoLzc.campanasGM.catCampaniaDesactivadoStr',
		'MyDesktop.modules.recintoLzc.campanasGM.catCampaniasActivadaStr',
		'MyDesktop.modules.recintoLzc.campanasGM.catCampaniaWin',
		'MyDesktop.modules.recintoLzc.interfacesExp.intExpMenuWin',
		'MyDesktop.modules.recintoLzc.UnidadesporPlataforma.grdcargaPlataformaStr',
		'MyDesktop.modules.recintoLzc.UnidadesporPlataforma.UnidadesxPlataformaTipoStr',
		'MyDesktop.modules.recintoLzc.UnidadesporPlataforma.UnidadesXPlataformaWin',
		'MyDesktop.modules.recintoLzc.cargaManifiesto.cargaManifiestoGMWin',
		'MyDesktop.modules.recintoLzc.SalidasporMadrinaRecinto.cargaSalidasStr',
		'MyDesktop.modules.recintoLzc.SalidasporMadrinaRecinto.salidaMadrinaWin',
		'MyDesktop.modules.recepcionUnidades.impresionPlacard.impresionPlacardStr',
		'MyDesktop.modules.recepcionUnidades.impresionPlacard.impPlacardMarcaStr',
		'MyDesktop.modules.recepcionUnidades.impresionPlacard.impresionPlacardWin',
		'MyDesktop.modules.catalogs.CatdistribuidoresExp.carDisExpConsultaStr',
		'MyDesktop.modules.catalogs.CatdistribuidoresExp.catDisExpPlazaStr',
		'MyDesktop.modules.catalogs.CatdistribuidoresExp.catDisExpRegionStr',
		'MyDesktop.modules.catalogs.CatdistribuidoresExp.catDisExpWin',
		'MyDesktop.modules.recintoLzc.fechasPortInLc.portInfechasStr',
		'MyDesktop.modules.recintoLzc.fechasPortInLc.portInWin',
		'MyDesktop.modules.recintoLzc.transmisionesGMLZC.interfacesGMWin',
		'MyDesktop.modules.recintoLzc.cancelacionTransmisionesLzc.cancelacionTransmisionLzcPortInStr',
		'MyDesktop.modules.recintoLzc.cancelacionTransmisionesLzc.cancelacionTransmisionLzcRepuveStr',
		'MyDesktop.modules.recintoLzc.cancelacionTransmisionesLzc.cancelacionTransmisionLzcStr',
		'MyDesktop.modules.recintoLzc.cancelacionTransmisionesLzc.cancelacionTransmisionLzcTenderStr',
		'MyDesktop.modules.recintoLzc.cancelacionTransmisionesLzc.cancelacionTransmisionesLzcWin',
		
		'MyDesktop.modules.security.segUsuariosModulos.segUsuariosModulosDialogWin',
		'MyDesktop.modules.security.login.app.store.segUsuarioConfigStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlSegUsuariosStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlModulosStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlEscritorioStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlWn'
    ],

    init: function() {
        // custom logic before getXYZ methods get called...

        this.callParent();

        // now ready...
    },

    getModules : function(){
        return [
            // Menues
            new MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosWin(),
            new MyDesktop.modules.catalogs.catalogoDistribuidores.catDistribuidoresWin(),
            new MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276Win(),
            new MyDesktop.modules.recepcionUnidades.reactivacionUnidades.reUnidadesWin(),
            new MyDesktop.modules.recintoLzc.desembarque.desL1Win(),
            new MyDesktop.modules.recintoLzc.reImpresionEtiquetasLazaro.reImpresionEtiquetasLazaroWin(),
            new MyDesktop.modules.recintoLzc.cargaEstatusTender.estatusTenderWin(),
            new MyDesktop.modules.recintoLzc.interfacesLzc.interfacesLzcRepuveWin(),
            new MyDesktop.modules.recintoLzc.cargaInicial.alCinicialWin(),
            new MyDesktop.modules.recintoLzc.cargaInicialGM.alCinicialGMWin(),
            new MyDesktop.modules.recintoLzc.campanasGM.catCampaniaWin(),
            new MyDesktop.modules.recintoLzc.interfacesExp.intExpMenuWin(),
            new MyDesktop.modules.recintoLzc.UnidadesporPlataforma.UnidadesXPlataformaWin(),
            new MyDesktop.modules.recintoLzc.cargaManifiesto.cargaManifiestoGMWin(),
            new MyDesktop.modules.recintoLzc.SalidasporMadrinaRecinto.salidaMadrinaWin(),
            new MyDesktop.modules.recepcionUnidades.impresionPlacard.impresionPlacardWin(),
            new MyDesktop.modules.catalogs.CatdistribuidoresExp.catDisExpWin(),
            new MyDesktop.modules.recintoLzc.fechasPortInLc.portInWin(),
            new MyDesktop.modules.recintoLzc.transmisionesGMLZC.interfacesGMWin(),
            new MyDesktop.modules.recintoLzc.cancelacionTransmisionesLzc.cancelacionTransmisionesLzcWin(),
            new MyDesktop.catGenMenuModel66(),
            new MyDesktop.avanzadasMenuModel66(),
            new MyDesktop.moduloRepuve66(),
            new MyDesktop.moduloRecinto66(),
            new MyDesktop.moduloGMC66(),
            new MyDesktop.modules.security.segUsuariosModulos.segUsuariosModulosDialogWin(),
            new MyDesktop.modules.security.login.app.store.segUsuarioConfigStr()
        ];
    },

    getDesktopConfig: function () {
        var me = this, ret = me.callParent();
		
        return Ext.apply(ret, {

            contextMenuItems: [
                { text: 'Personalizar', iconCls:'personalizar', handler: me.onSettings, scope: me }
            ],

            shortcuts: Ext.create('Ext.data.Store', {
                model: 'Ext.ux.desktop.ShortcutModel',
				data: accesosDirArr
            }),

            wallpaper: 'wallpapers/dChallenger_01.jpg',
            wallpaperStretch: true
        });
    },

    // config for the start menu
    getStartConfig : function() {
        var me = this, ret = me.callParent();
        Ext.EventManager.on(document, 'keydown', function(e, t) {
    	   	if (e.getKey() == e.BACKSPACE && (!/^input$/i.test(t.tagName) || t.disabled || t.readOnly)) {
     		e.stopEvent();
    		}
   	   });

        return Ext.apply(ret, {
            title: usuNombre,
            iconCls: 'user',
            height: 300,
            toolConfig: {
                width: 100,
                items: [
                    {
                        text:'Personalizar',
                        iconCls:'personalizar',
                        handler: me.onSettings,
                        scope: me
                    },
                    '-',
                    {
                        text:'Presto',
                        iconCls:'personalizar',
                        handler: me.getCosnulta,
                        scope: me
                    },
                    '-',
                    {
                        text:'Salir',
                        iconCls:'salir',
                        handler: me.onLogout,
                        scope: me
                    }
                ]
            }
        });
    },

    getTaskbarConfig: function () {
        var ret = this.callParent();

        return Ext.apply(ret, {
            quickStart: quickStartArr,
            trayItems: [
                { xtype: 'trayclock', flex: 1 }
            ]
        });
    },

    getCosnulta: function () {
        var dlg = new MyDesktop.modules.system.panelPresto.MyWindow({
            desktop: this.desktop
        });
        dlg.show();
    },

    onLogout: function () {
        Ext.Msg.confirm('Salir', '¿Esta seguro que desea salir?<br>El trabajo que no haya guardado se <br>perder&aacute;', function(btn, text){
        	if (btn == 'yes')
				window.location = 'modules/security/login/logout.php';
			});
    },

    onSettings: function () {
        var dlg = new MyDesktop.modules.system.panelControl.sisPanelControlWn({
            desktop: this.desktop
        });
        dlg.show();
    }
});
