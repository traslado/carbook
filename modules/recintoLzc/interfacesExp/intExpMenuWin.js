/*
 * File: app/view/intExpMenuWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.recintoLzc.interfacesExp.intExpMenuWin', {
  extend: 'Ext.window.Window',

        id:'intExpMenuWin',

    createWindow_intExpMenuWin : function(){
        var desktop = this.app.getDesktop();
        var win_intExpMenuWin = desktop.getWindow('intExpMenuWin');
        if(!win_intExpMenuWin){
            // Integrar aquí los Stores que ocupará la pantalla
            //var store = new MyDesktop.modules.recintoLzc.cargaInicial.alCargaInicialUndStr();
            
            win_intExpMenuWin = desktop.createWindow({
                height: 300,
                id: 'intExpMenuWin',
                width: 180,
                title: 'GLOVIS Exportacion',
                maximizable: false,
                minimizable: true,
                resizable:false,

                iconCls:'hyundai',
                dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'intExpTlb',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var window = Ext.WindowManager.get('intExpMenuWin');
                                window.close();
                            },
                            id: 'intExpSalirBtn',
                            iconCls: 'ux-btnDel',
                            hidden:true,
                            text: 'Salir'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    height: 217,
                    id: 'intExpFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    items: [

                          {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.Ajax.request({
                                    url: '../../carbookBck/procesos/iRECEXP.php'
                                });

                                Ext.Msg.alert("PorTIn", "Interface Ejecutada");
                            },
                            x: 50,
                            y: 20,
                            id: 'intExpPortInBtn',
                            width: 80,
                            text: 'Port In'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.Ajax.request({
                                    url: '../../carbookBck/procesos/iACNEx.php'
                                });

                                  Ext.Msg.alert("PorTIn", "Interface Ejecutada");
                            },
                            x: 50,
                            y: 60,
                            id: 'intExpAcnBtn',
                            width: 80,
                            text: 'ACN'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.Ajax.request({
                                    url: '../../carbookBck/procesos/iEvvExp.php'
                                });

                              Ext.Msg.alert("PorTIn", "Interface Ejecutada");

                            },
                            x: 50,
                            y: 140,
                            id: 'intExpVVBtn',
                            width: 80,
                            text: 'EVE VV'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.Ajax.request({
                                    url: '../../carbookBck/procesos/iEvoExp.php'
                                });

                                  Ext.Msg.alert("PorTIn", "Interface Ejecutada");
                            },
                            x: 50,
                            y: 100,
                            id: 'intExpVOBtn',
                            width: 80,
                            text: 'EVE VO'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
    

                                var auxWin =  Ext.create('MyDesktop.modules.recintoLzc.interfacesExp.eveVAWin').show();
                            },
                            x: 50,
                            y: 180,
                            id: 'intExpVABtn',
                            width: 80,
                            text: 'EVE VA'
                        }
                    ]
                }
            ],
              listeners: {
                beforeclose: {
                    fn:function(panel, eOpts) {
                        var titulo = this.title;
                        Ext.Msg.confirm(titulo,'¿Está seguro que desea salir de<br> '+ titulo+'?',function(btn){
                            if(!Ext.getCmp('intExpFrm').isDisabled()){
                                if(btn == 'yes'){
                                    panel.allowDestroy = true;
                                    panel.destroy();
                                }
                            }else{                               
                            }
                        }, panel);
                        return false;

                    }
                }
            }
        });

        }
        win_intExpMenuWin.show();
        return win_intExpMenuWin;
    }
});