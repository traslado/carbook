/*
 * File: app/store/caChoferesChoferGrd.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.store.caChoferesChoferGrd', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
            storeId: 'caChoferesChoferGrd',
            proxy: {
                type: 'ajax',
                url: '../../carbookBck/json/catChoferes.php?catChoferesActionHdn=getChoferes',
                reader: {
                    type: 'json',
                    root: 'root'
                }
            },
            fields: [
                {
                    name: 'claveChofer'
                },
                {
                    name: 'apellidoPaterno'
                },
                {
                    name: 'apellidoMaterno'
                },
                {
                    name: 'nombre'
                },
                {
                    name: 'licencia'
                },
                {
                    name: 'cuentaContable'
                },
                {
                    name: 'estatus'
                },
                {
                    name: 'tipoChofer'
                },
                {
                    name: 'vigenciaLicencia'
                },
                {
                    name: 'centroDistribucionOrigen'
                },
                {
                    name: 'nombreDistOrigen'
                },
                {
                    name: 'nombreChofer'
                },
                {
                    name: 'nombreEstatus'
                },
                {
                    name: 'nombreTipo'
                },
                {
                    name: 'fechaIngreso'
                },
                {
                    name: 'certificado'
                },
                {
                    name: 'vigenciaCertificado'
                },
                {
                    name: 'numeroContrato'
                },
                {
                    name: 'vigenciaContrato'
                },
                {
                    name: 'calculoIstp'
                }
            ]
        }, cfg)]);
    }
});