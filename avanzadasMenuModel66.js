/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.avanzadasMenuModel66', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Recepcion de Unidades',
            iconCls: 'avanzadaMenu',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Consulta de Unidades por Avanzada',
									iconCls:'trap276',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trap276Win');
										win = module.createWindow_trap276Win();
									},
									scope: this,
									windowId: 'trap276Win'
								},
								{
									text: 'Reactivacion Unidades',
									iconCls:'avanzadaMenu',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('reUnidadesWin');
										win = module.createWindow_reUnidadesWin();
									},
									scope: this,
									windowId: 'reUnidadesWin'
								},
								{
									text: 'reImpresionLC',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('reImpresionEtiquetasLazaroWin');
										win = module.createWindow_reImpresionEtiquetasLazaroWin();
									},
									scope: this,
									windowId: 'reImpresionEtiquetasLazaroWin'
								},
								{
									text: 'Impresion Placard',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('impresionPlacardWin');
										win = module.createWindow_impresionPlacardWin();
									},
									scope: this,
									windowId: 'impresionPlacardWin'
								}
				]
            }
        };
    }
});