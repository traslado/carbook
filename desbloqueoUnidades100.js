/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.desbloqueoUnidades100', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Desbloqueo',
            iconCls: 'catMarcasDistribuidor',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Desbloqueo Tractor',
									iconCls:'catMarcasDistribuidor',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('desbloqueoTractorWin');
										win = module.createWindow_desbloqueoTractorWin();
									},
									scope: this,
									windowId: 'desbloqueoTractorWin'
								}
				]
            }
        };
    }
});