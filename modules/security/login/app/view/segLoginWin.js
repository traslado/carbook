/*
 * File: app/view/segLoginWin.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('loginApp.view.segLoginWin', {
    extend: 'Ext.window.Window',

    height: 360,
    hidden: false,
    id: 'segLoginWin',
    width: 405,
    resizable: false,
    layout: {
        type: 'fit'
    },
    closable: false,
    title: 'TRASLADO AUTOMOTRIZ (Carbook)',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    cls: 'segLoginFrm',
                    id: 'segLoginFrm',
                    layout: {
                        type: 'absolute'
                    },
                    bodyCls: 'segLoginFrm',
                    bodyPadding: 10,
                    title: '',
                    url: '../carbookBck/json/segLogin.php',
                    items: [
                        {
                            xtype: 'textfield',
                            x: 70,
                            y: 190,
                            id: 'segLoginUsuarioTxt',
                            width: 250,
                            fieldLabel: 'Usuario',
                            labelClsExtra: 'text',
                            labelSeparator: ':',
                            labelWidth: 75,
                            msgTarget: 'side',
                            name: 'segLoginUsuarioTxt',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            enforceMaxLength: true,
                            maxLength: 25
                        },
                        {
                            xtype: 'textfield',
                            x: 70,
                            y: 220,
                            id: 'segLoginPasswordTxt',
                            width: 250,
                            fieldLabel: 'Contraseña',
                            labelClsExtra: 'text',
                            labelSeparator: ':',
                            labelWidth: 75,
                            msgTarget: 'side',
                            name: 'segLoginPasswordTxt',
                            inputType: 'password',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            enforceMaxLength: true,
                            enableKeyEvents: true,
                            submitValue: false,
                            maxLength: 25,
                            listeners: {
                                keypress: {
                                    fn: me.onSegLoginPasswordTxtKeyPress,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            x: 220,
                            y: 270,
                            height: 30,
                            id: 'segLoginIngresarBtn',
                            width: 100,
                            text: 'Ingresar',
                            listeners: {
                                click: {
                                    fn: me.onSegLoginIngresarBtnClick,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            x: 68,
                            y: 270,
                            height: 30,
                            id: 'segLoginRecuperarBtn',
                            text: 'Olvidé mi contraseña',
                            listeners: {
                                click: {
                                    fn: me.onSegLoginRecuperarBtnClick,
                                    scope: me
                                }
                            }
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onSegLoginPasswordTxtKeyPress: function(field, e){
        if(e.getKey() === Ext.EventObject.ENTER){
            Ext.getCmp('segLoginIngresarBtn').fireHandler();
        }
    },

    onSegLoginIngresarBtnClick: function(button, e, eOpts) {
        console.log("entraria a password");
        var form = Ext.getCmp('segLoginFrm').getForm();

        if (form.isValid()){
            form.submit({
                params:{
                    segLoginPasswordTxt:Ext.util.MD5(Ext.getCmp('segLoginPasswordTxt').getValue())
                },
                success: function(form, action) {
                    usuValor = action.result.idUsuario;
                    form.reset();
                    Ext.data.StoreManager.lookup('segLoginDistCentroStr').load({
                        params: {
                            segUsuariosIdUsuarioHdn: action.result.idUsuario
                        }
                    });
                    Ext.create('Ext.form.Panel', {
                        title: 'Login - Selección Centro de Distribución',
						id: 'segLoginUsuCtoDistFrm',
						url: 'segInicioSesion.php',
                        height: 160,
                        width: 400,
                        floating: true,
						closable: false,
						layout: 'absolute',
						bodyCls: 'formAsWindow',
                        items: [
                        {
                            xtype: 'combo',
                            id: 'segLoginDistCentroCmb',
							name: 'segLoginDistCentroCmb',
                            width: 300,
                            x: 40,
                            y: 50,
                            valueField: 'distribuidorCentro',
                            displayField: 'descripcionCentro',
                            store: 'segLoginDistCentroStr',
                            queryMode:'local',
                            //editable: false,
                            fieldStyle: 'text-transform:uppercase',
							allowBlank: false
                        },
                        {
                            xtype: 'hiddenfield',
                            id: 'segLoginDistCentroHdn',
                            name: 'segLoginDistCentroHdn',
                            width: 300,
                            x: 40,
                            y: 50,
                            valueField: 'distribuidorCentro',
                            displayField: 'descripcionCentro',
                            store: 'segLoginDistCentroStr',
                            allowBlank: false
                        },
                        {
                            xtype: 'label',
                            text: 'Seleccione una Compañía',
                            width: 250,
                            x: 40,
                            y: 20
                        },
                        {
                            xtype: 'hiddenfield',
                            name: 'loginHdn',
                            value: '1'
                        },
                        {
                            xtype: 'button',
                            text: 'Continuar',
                            id: 'segLoginContinuarBtn',
                            width: 120,
                            height: 30,
                            x: 220,
                            y: 80,
                            listeners: {
                                click: {
                                    fn: continuarAccesoBtn
                                }
                            }
                        }
                    ]}).show();
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Login', action.result.errorMessage);
                    form.reset();
                }
            });
        }

        function continuarAccesoBtn(button, e, eOpts){
            Ext.getCmp('segLoginDistCentroHdn').setValue(Ext.getCmp('segLoginDistCentroCmb').getRawValue());
            var usuCtosFrm = Ext.getCmp('segLoginUsuCtoDistFrm').getForm();
			if (usuCtosFrm.isValid()){
				usuCtosFrm.submit({
					success: function(form, action) {
						window.location.href = "desktop.php?theme=" + action.result.tema;
					},
					failure: function(form, action) {
                    	window.location.href = "index.html";
                	}
				});
			}
        }
    },

    onSegLoginRecuperarBtnClick: function(button, e, eOpts) {
        Ext.create('Ext.form.Panel', {
            id: 'recuperarPasswordFrm',
            title: 'Recuperar contraseña',
            height: 160,
            width: 400,
            floating: true,
            closable: true,
            layout: 'absolute',
            bodyCls: 'formAsWindow',
            url: '../../carbookBck/json/segEmail.php',
            items: [
            {
                xtype: 'textfield',
                id: 'segLoginEmailTxt',
                width: 300,
                x: 50,
                y: 50,
                name: 'correoElectronico',
                allowBlank: false,
                regex: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
                regexText: '<b>Correo <br>inválido</b>',
                msgTarget: 'side'
            },
            {
                xtype: 'label',
                text: 'Escriba su correo con el que se registró:',
                width:250,
                x: 50,
                y: 20
            },
            {
                xtype: 'button',
                text: 'Enviar',
                id: 'segLoginEnviarRecuperarBtn',
                width: 120,
                height:30,
                x: 230,
                y: 80,
                listeners: {
                    click: {
                        fn: recuperarPasswordBtnClick
                    }
                }
            }
        ]}).show();

        function recuperarPasswordBtnClick(button, e, eOpts){
            var form = Ext.getCmp('recuperarPasswordFrm').getForm();
            if (form.isValid()){
                form.submit({
                    waitMsg: 'Espere ... validando el correo',
                    waitTitle: 'Recuperar contraseña',                    
                    success: function(form, action) {
                        Ext.Msg.alert('Recuperar Contraseña', action.result.msjResponse, function(btn) {
                            Ext.getCmp('recuperarPasswordFrm').destroy();
                        });                        
                    },
                    failure: function(form, action) {
                        Ext.Msg.alert('Recuperar Contraseña', '<b>'+action.result.msjResponse+'</b>', function(btn) {
                            form.reset();
                            Ext.getCmp('segLoginEmailTxt').focus();
                        });                          
                    }
                });
            }
            else
          	{
          			Ext.Msg.alert('Recuperar contraseña', '<b>Debe capturar su correo electrónico.</b>', function(btn) {
                    Ext.getCmp('segLoginEmailTxt').focus();
                });                
          	}
        }
    }

});