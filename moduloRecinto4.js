/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.moduloRecinto4', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Recinto LZC',
            iconCls: 'cxc',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Desembarque Unds',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('desL1Win');
										win = module.createWindow_desL1Win();
									},
									scope: this,
									windowId: 'desL1Win'
								},
								{
									text: 'Entrada Patio',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('lzcL2Win');
										win = module.createWindow_lzcL2Win();
									},
									scope: this,
									windowId: 'lzcL2Win'
								},
								{
									text: 'Carga Estatus Tender',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('estatusTenderWin');
										win = module.createWindow_estatusTenderWin();
									},
									scope: this,
									windowId: 'estatusTenderWin'
								},
								{
									text: 'Carga Inicial KIA/HY',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('alCinicialWin');
										win = module.createWindow_alCinicialWin();
									},
									scope: this,
									windowId: 'alCinicialWin'
								},
								{
									text: 'PDI',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('lzcPDIWin');
										win = module.createWindow_lzcPDIWin();
									},
									scope: this,
									windowId: 'lzcPDIWin'
								},
								{
									text: 'Interfeces Exportacion',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('intExpMenuWin');
										win = module.createWindow_intExpMenuWin();
									},
									scope: this,
									windowId: 'intExpMenuWin'
								},
								{
									text: 'SalidasPorTren',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('UnidadesXPlataformaWin');
										win = module.createWindow_UnidadesXPlataformaWin();
									},
									scope: this,
									windowId: 'UnidadesXPlataformaWin'
								},
								{
									text: 'SalidasPorMadrina',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('salidaMadrinaWin');
										win = module.createWindow_salidaMadrinaWin();
									},
									scope: this,
									windowId: 'salidaMadrinaWin'
								}
				]
            }
        };
    }
});