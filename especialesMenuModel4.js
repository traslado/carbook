/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.especialesMenuModel4', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Servicios Especiales',
            iconCls: 'trap010',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Generacion de Servicios Especiales',
									iconCls:'trap010',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trap010Win');
										win = module.createWindow_trap010Win();
									},
									scope: this,
									windowId: 'trap010Win'
								},
								{
									text: 'Cambio de Destino',
									iconCls:'alCambios',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('cambioDestinoWin');
										win = module.createWindow_cambioDestinoWin();
									},
									scope: this,
									windowId: 'cambioDestinoWin'
								}
				]
            }
        };
    }
});