/*
 * File: app/view/cargaTicketWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.polizaGastos.cargaTicketCard.cargaTicketWin', {
    extend: 'Ext.ux.desktop.Module',
    
    id:'cargaTicketWin',

    /*init : function(){
        this.launcher = {
            text: rhTRelLabTittle,
            iconCls:'auCatColores'
        };
    },*/
    
    createWindow_cargaTicketWin : function(){
        var desktop = this.app.getDesktop();
        var win_cargaTicketWin = desktop.getWindow('cargaTicketWin');
        if(!win_cargaTicketWin){
            // Integrar aquí los Stores que ocupará la pantalla
            var store = new MyDesktop.modules.polizaGastos.cargaTicketCard.cargaTicketNumTractorStr();
            var store = new MyDesktop.modules.polizaGastos.cargaTicketCard.cargaTicketCompaniaStr();
            var store = new MyDesktop.modules.polizaGastos.cargaTicketCard.cargaTcketGrdStr();
            var winTitle = 'Carga Ticket Card';
            
            win_cargaTicketWin = desktop.createWindow({
                    height: 460,
                    id: 'cargaTicketWin',
                    width: 718,
                    layout: {
                        type: 'fit'
                    },
                    title: 'Carga Ticket Card',
                    maximizable: false,
                    minimizable: true,
                    resizable:false,
                    iconCls:'ticketcard',                
                    dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var vinUN = "";

                                var forma = Ext.getCmp("cargaTicketFrm").getForm();

                                if(!forma.isValid()){
                                    Ext.Msg.alert(winTitle, '<b>Faltó seleccionar el Archivo.</b>');
                                }
                                else{
                                    forma.submit({
                                        waitTitle: winTitle,
                            						waitMsg: 'Espere ... cargando el archivo',
                                        success: function(form, action) {
                                            //Ext.Msg.alert(action.result.successTitle, action.result.successMessage);
                                            //var gridStr = Ext.getCmp('trap001ResultadoCargaGrdPnl').getStore();
                                            var res = action && action.result && action.result.root;

                                            if(res.length === 1){
                                                if(res[0].fail){
                                                    Ext.Msg.alert(winTitle, res[0].nose);
                                                }
                                            }else{
                                                Ext.Msg.alert(winTitle, action.result.successMessage);
                                                var gridStr = Ext.getCmp('cargaTicketGrd').getStore();

                                                gridStr.loadData(action.result.root);
                                            }
                                        },
                                        failure: function(form, action) {
                                            if(Ext.isEmpty(action.result.errorMessage))
                                            {
                                            		action.result.errorMessage = '<b>Error en el archivo.</b>'
                                            }
                                            Ext.Msg.alert(winTitle, action.result.errorMessage);
                                        }
                                    });
                                }
                            },
                            cls: '',
                            id: 'cargaTicketAceptarBtn',
                            iconCls: 'ux-btnSave',
                            text: 'Aceptar'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                var borrarFrm = Ext.getCmp('cargaTicketFrm').getForm();
                                borrarFrm.reset();

                                var connUnidadesGrd = Ext.getCmp("cargaTicketGrd").getStore();
                                connUnidadesGrd.removeAll();
                            },
                            id: 'cargaTicketCancelarBtn',
                            iconCls: 'ux-btnDel',
                            text: 'Cancelar'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    height: 417,
                    id: 'cargaTicketFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    url: '../../carbookBck/json/trCargaTicketPoliza.php?cargaTicketHdn=getCargaTicket',
                    items: [
                        {
                            xtype: 'hiddenfield',
                            x: 485,
                            y: 66,
                            id: 'cargaTicketIdViajeHdn',
                            fieldLabel: 'Label',
                            name: 'cargaTicketIdViajeHdn'
                        },
                        {
                            xtype: 'filefield',
                            x: 20,
                            y: 20,
                            id: 'cargaTicketArchivoFld',
                            fieldLabel: 'Archivo',
                            labelWidth: 70,
                            allowBlank: false,
                            name: 'cargaTicketArchivoFld',
                            buttonText: 'Buscar...',
                            listeners: {
                                change: {
                                    fn: function(filefield, value, eOpts) {
                                        var tester = new RegExp('^[^\/"]+((.xls)|(.xlsx))$');
                                        if(!tester.test(value)){
                                            Ext.Msg.alert('Cargar Ticket Card', 'Ingresa un archivo de Excel válido (*.xls, *.xlsx)', function(){
                                                filefield.reset();
                                            });
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'gridpanel',
                            x: 20,
                            y: 70,
                            height: 290,
                            id: 'cargaTicketGrd',
                            title: 'Estatus de Archivos',
                            store: 'cargaTcketGrdStr',
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'compania',
                                    text: 'Compania'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'tractor',
                                    text: 'Tractor'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'noComprobante',
                                    text: 'NoComprobante'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'fechaEvento',
                                    text: 'FechaEvento'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'litros',
                                    text: 'Litros'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'importe',
                                    text: 'Importe'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'placas',
                                    text: 'Placas'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 323,
                                    dataIndex: 'nose',
                                    text: 'Estatus Ticket'
                                }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
            
                beforeclose: {
                    fn:function(panel, eOpts) {
                        var titulo = this.title;
                        Ext.Msg.confirm(titulo,'¿Está seguro que desea salir de<br> '+ titulo+'?',function(btn){
                                if(btn == 'yes'){
                                    panel.allowDestroy = true;
                                    panel.destroy();
                                }
 
                        }, panel);
                        return false;

                    }
                }
            },
        });
        }
        win_cargaTicketWin.show();
        return win_cargaTicketWin;
    }
});