/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.avanzadasMenuModel13', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Recepcion de Unidades',
            iconCls: 'avanzadaMenu',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Alta Global de Unidades',
									iconCls:'avanzadaMenu',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trap001Win');
										win = module.createWindow_trap001Win();
									},
									scope: this,
									windowId: 'trap001Win'
								},
								{
									text: 'Facturacion Y Entrada a Patio',
									iconCls:'trap135',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('entradaPatioWin');
										win = module.createWindow_entradaPatioWin();
									},
									scope: this,
									windowId: 'entradaPatioWin'
								},
								{
									text: 'Observaciones por Serie',
									iconCls:'trap063',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trap063Win');
										win = module.createWindow_trap063Win();
									},
									scope: this,
									windowId: 'trap063Win'
								},
								{
									text: 'Cambio de Distribuidor',
									iconCls:'trap176',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('alCambioDistribuidorWin');
										win = module.createWindow_alCambioDistribuidorWin();
									},
									scope: this,
									windowId: 'alCambioDistribuidorWin'
								},
								{
									text: 'Cambios de Estatus',
									iconCls:'trap003',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trapCambioEstatusWin');
										win = module.createWindow_trapCambioEstatusWin();
									},
									scope: this,
									windowId: 'trapCambioEstatusWin'
								},
								{
									text: 'Control de Unidades Detenidas',
									iconCls:'trap474',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('detencionUnidadesWin');
										win = module.createWindow_detencionUnidadesWin();
									},
									scope: this,
									windowId: 'detencionUnidadesWin'
								},
								{
									text: 'Consulta de Unidades por Avanzada',
									iconCls:'trap276',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trap276Win');
										win = module.createWindow_trap276Win();
									},
									scope: this,
									windowId: 'trap276Win'
								},
								{
									text: 'Holds y Cambios de Destino Programados',
									iconCls:'trap176',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('alHoldsWin');
										win = module.createWindow_alHoldsWin();
									},
									scope: this,
									windowId: 'alHoldsWin'
								},
								{
									text: 'Captura de unidades con Da�os',
									iconCls:'alDanos',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('alDanosWin');
										win = module.createWindow_alDanosWin();
									},
									scope: this,
									windowId: 'alDanosWin'
								},
								{
									text: 'Reimpresion de Etiquetas',
									iconCls:'reEtiquetas',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('reEtiquetasWind');
										win = module.createWindow_reEtiquetasWind();
									},
									scope: this,
									windowId: 'reEtiquetasWind'
								},
								{
									text: 'Recepcion de llaves',
									iconCls:'alControlLlavesRecepcion',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('alControlLlavesRecepcionVw');
										win = module.createWindow_alControlLlavesRecepcionVw();
									},
									scope: this,
									windowId: 'alControlLlavesRecepcionVw'
								},
								{
									text: 'Reactivacion Unidades',
									iconCls:'avanzadaMenu',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('reUnidadesWin');
										win = module.createWindow_reUnidadesWin();
									},
									scope: this,
									windowId: 'reUnidadesWin'
								},
								{
									text: 'Impresion Placard',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('impresionPlacardWin');
										win = module.createWindow_impresionPlacardWin();
									},
									scope: this,
									windowId: 'impresionPlacardWin'
								}
				]
            }
        };
    }
});