/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.monitorUnidades13', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Monitor de Unidades',
            iconCls: 'cxc',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Monitor Unidades',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('alMonitorWin');
										win = module.createWindow_alMonitorWin();
									},
									scope: this,
									windowId: 'alMonitorWin'
								}
				]
            }
        };
    }
});