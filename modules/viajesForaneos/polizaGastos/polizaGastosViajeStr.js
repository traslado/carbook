/*
 * File: app/store/polizaGastosViajeStr.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.viajesForaneos.polizaGastos.polizaGastosViajeStr', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'polizaGastosViajeStr',
            proxy: {
                type: 'ajax',
                url: '../../carbookBck/json/trViajesTractores.php?trViajesTractoresActionHdn=getHistoricoViajes',
                reader: {
                    type: 'json',
                    root: 'root'
                }
            },
            fields: [
                {
                    name: 'descCompania'
                },
                {
                    name: 'idTractor',
                    type: 'int'
                },
                {
                    name: 'tractor'
                },
                {
                    name: 'viaje'
                },
                {
                    name: 'nombrePlazaOrigen'
                },
                {
                    name: 'nombrePlazaDestino'
                },
                {
                    name: 'kilometrosTabulados'
                },
                {
                    name: 'kilometrosSinUnidad'
                },
                {
                    name: 'numeroDistribuidores'
                },
                {
                    name: 'numTalonesValidosViaje'
                },
                {
                    name: 'numeroUnidades'
                },
                {
                    name: 'fechaEvento'
                },
                {
                    name: 'foliosTalonesViaje'
                },
                {
                    name: 'rendimiento'
                },
                {
                    name: 'idViajeTractor'
                },
                {
                    name: 'polizaGenerada'
                },
                {
                    name: 'polizaTotal'
                }
            ]
        }, cfg)]);
    }
});