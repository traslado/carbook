/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.moduloGMC66', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'moduloGMC',
            iconCls: 'cxc',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Carga Inicial GM',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('alCinicialGMWin');
										win = module.createWindow_alCinicialGMWin();
									},
									scope: this,
									windowId: 'alCinicialGMWin'
								},
								{
									text: 'carga Manifiesto GM ',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('cargaManifiestoGMWin');
										win = module.createWindow_cargaManifiestoGMWin();
									},
									scope: this,
									windowId: 'cargaManifiestoGMWin'
								}
				]
            }
        };
    }
});