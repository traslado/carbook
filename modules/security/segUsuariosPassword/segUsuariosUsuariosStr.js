/*
 * File: app/store/segUsuariosUsuariosStr.js
 *
 * This file was generated by Sencha Architect version 2.2.3.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.security.segUsuariosPassword.segUsuariosUsuariosStr', {
    extend: 'Ext.data.Store',

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //autoLoad: true,
            storeId: 'segUsuariosUsuariosStr',
            proxy: {
                type: 'ajax',
                url: '../../carbookBck/json/segUsuarios.php?segUsuariosActionHdn=getUsuarios&segUsuariosEstatusHdn=1&segUsuariosIdUsuarioHdn='+usuValor,
                reader: {
                    type: 'json',
                    root: 'root',
                    totalProperty: 'records'
                }
            },
            fields: [
                {
                    name: 'idUsuario'
                },
                {
                    name: 'nombre'
                },
                {
                    name: 'descripcionCombo'
                }
            ]
        }, cfg)]);
    }
});