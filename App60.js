/*!
 * Ext JS Library 4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */

// Prevent the backspace key from navigating back.
Ext.EventManager.on(Ext.isIE ? document : window, 'keydown', function(e, t) {
	if (e.getKey() == e.BACKSPACE && ((!/^input$/i.test(t.tagName) && !/^textarea$/i.test(t.tagName)) || t.disabled || t.readOnly)) {
		e.stopEvent();
	}
});

Ext.define('MyDesktop.App60', {
    extend: 'Ext.ux.desktop.App',

    requires: [
		'Ext.window.MessageBox',
		'Ext.ux.desktop.ShortcutModel',
		
		'MyDesktop.catGenMenuModel60',
		'MyDesktop.avanzadasMenuModel60',
		'MyDesktop.moduloComodato60',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosOrigenStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosTipoStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosMarcasCdStore',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosClasificacionStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosSimboloStr',
		'MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosWin',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276HistoricoStr',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276VinStr',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276EspecialesStr',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.alDstServiciosEspecialesWin',
		'MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276Win',
		'MyDesktop.modules.comoDato.impresionComodato.impresionComodatoStr',
		'MyDesktop.modules.comoDato.impresionComodato.impresionComodatoWin',
		'MyDesktop.modules.comoDato.localizacionUnidadesComodato.localizacionUnidadBahiaStr',
		'MyDesktop.modules.comoDato.localizacionUnidadesComodato.localizacionUnidadesStr',
		'MyDesktop.modules.comoDato.localizacionUnidadesComodato.localizacionPatioWin',
		'MyDesktop.modules.comoDato.comodatoProdStatus.prodStatusGeneralStr',
		'MyDesktop.modules.comoDato.comodatoProdStatus.prodStatusWin',
		'MyDesktop.modules.comoDato.reImpresionEtiquetas.reImpresionEtiquetasWin',
		'MyDesktop.modules.comoDato.ActializacionDeLocalizaciones.actalizacionDeLocalizacionWin',
		'MyDesktop.modules.comoDato.cambioMercado.tipoMercadoStr',
		'MyDesktop.modules.comoDato.cambioMercado.tipoMercadoWin',
		'MyDesktop.modules.comoDato.tipoSalidaComodato.alSalTatSalidaStr',
		'MyDesktop.modules.comoDato.tipoSalidaComodato.alTsalidasWin',
		
		'MyDesktop.modules.security.segUsuariosModulos.segUsuariosModulosDialogWin',
		'MyDesktop.modules.security.login.app.store.segUsuarioConfigStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlSegUsuariosStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlModulosStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlEscritorioStr',
		'MyDesktop.modules.system.panelControl.sisPanelControlWn'
    ],

    init: function() {
        // custom logic before getXYZ methods get called...

        this.callParent();

        // now ready...
    },

    getModules : function(){
        return [
            // Menues
            new MyDesktop.modules.catalogs.catalogoSimboloUnidades.catSimbolosWin(),
            new MyDesktop.modules.recepcionUnidades.consultaUnidades.trap276Win(),
            new MyDesktop.modules.comoDato.impresionComodato.impresionComodatoWin(),
            new MyDesktop.modules.comoDato.localizacionUnidadesComodato.localizacionPatioWin(),
            new MyDesktop.modules.comoDato.comodatoProdStatus.prodStatusWin(),
            new MyDesktop.modules.comoDato.reImpresionEtiquetas.reImpresionEtiquetasWin(),
            new MyDesktop.modules.comoDato.ActializacionDeLocalizaciones.actalizacionDeLocalizacionWin(),
            new MyDesktop.modules.comoDato.cambioMercado.tipoMercadoWin(),
            new MyDesktop.modules.comoDato.tipoSalidaComodato.alTsalidasWin(),
            new MyDesktop.catGenMenuModel60(),
            new MyDesktop.avanzadasMenuModel60(),
            new MyDesktop.moduloComodato60(),
            new MyDesktop.modules.security.segUsuariosModulos.segUsuariosModulosDialogWin(),
            new MyDesktop.modules.security.login.app.store.segUsuarioConfigStr()
        ];
    },

    getDesktopConfig: function () {
        var me = this, ret = me.callParent();
		
        return Ext.apply(ret, {

            contextMenuItems: [
                { text: 'Personalizar', iconCls:'personalizar', handler: me.onSettings, scope: me }
            ],

            shortcuts: Ext.create('Ext.data.Store', {
                model: 'Ext.ux.desktop.ShortcutModel',
				data: accesosDirArr
            }),

            wallpaper: 'wallpapers/Mitsubishi.jpg',
            wallpaperStretch: true
        });
    },

    // config for the start menu
    getStartConfig : function() {
        var me = this, ret = me.callParent();
        Ext.EventManager.on(document, 'keydown', function(e, t) {
    	   	if (e.getKey() == e.BACKSPACE && (!/^input$/i.test(t.tagName) || t.disabled || t.readOnly)) {
     		e.stopEvent();
    		}
   	   });

        return Ext.apply(ret, {
            title: usuNombre,
            iconCls: 'user',
            height: 300,
            toolConfig: {
                width: 100,
                items: [
                    {
                        text:'Personalizar',
                        iconCls:'personalizar',
                        handler: me.onSettings,
                        scope: me
                    },
                    '-',
                    {
                        text:'Presto',
                        iconCls:'personalizar',
                        handler: me.getCosnulta,
                        scope: me
                    },
                    '-',
                    {
                        text:'Salir',
                        iconCls:'salir',
                        handler: me.onLogout,
                        scope: me
                    }
                ]
            }
        });
    },

    getTaskbarConfig: function () {
        var ret = this.callParent();

        return Ext.apply(ret, {
            quickStart: quickStartArr,
            trayItems: [
                { xtype: 'trayclock', flex: 1 }
            ]
        });
    },

    getCosnulta: function () {
        var dlg = new MyDesktop.modules.system.panelPresto.MyWindow({
            desktop: this.desktop
        });
        dlg.show();
    },

    onLogout: function () {
        Ext.Msg.confirm('Salir', '¿Esta seguro que desea salir?<br>El trabajo que no haya guardado se <br>perder&aacute;', function(btn, text){
        	if (btn == 'yes')
				window.location = 'modules/security/login/logout.php';
			});
    },

    onSettings: function () {
        var dlg = new MyDesktop.modules.system.panelControl.sisPanelControlWn({
            desktop: this.desktop
        });
        dlg.show();
    }
});
