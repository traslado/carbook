<?php
    session_start();
    require("../carbookBck/funciones/generales.php");
    require("../carbookBck/funciones/construct.php");
	
	function getUsuarioConfig()
	{
        $sqlGetUsuarioConfigStr = "SELECT module, name, iconCls " .
								  "FROM segUsuariosDesktopTbl " . 
								  "WHERE idUsuario = " . $_SESSION['idUsuario'] . ";";

        $rs = fn_ejecuta_query($sqlGetUsuarioConfigStr);
        return json_encode($rs['root']);    
    }
?>