/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.moduloSistemas13', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Sistemas',
            iconCls: 'tractorModule',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Modificacion de Unidades',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('modificacionUnidadesWin');
										win = module.createWindow_modificacionUnidadesWin();
									},
									scope: this,
									windowId: 'modificacionUnidadesWin'
								}
				]
            }
        };
    }
});