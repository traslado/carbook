/*
 * File: app/view/complementoPolizaTalachasWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.viajesForaneos.complementoPoliza.complementoPolizaTalachasWin', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.toolbar.Separator',
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.View',
        'Ext.grid.column.Column'
    ],

    height: 440,
    id: 'complementoPolizaTalachasWin',
    width: 492,
    constrain: true,
    title: 'Comprobantes de Talachas',
    modal: true,
    resizable:false,
    iconCls:'tractorModule',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'complementoPolizaTalachasTlb',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                this.up('window').close();
                            },
                            id: 'complementoPolizaTalachasAceptarBtn',
                            iconCls: 'ux-btnSave',
                            text: 'Guardar'
                        },
                        {
                            xtype: 'tbseparator',
                            id: 'complementoPolizaTalachasSep01'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                Ext.getCmp('complementoPolizaTalachasFrm').getForm().reset();
                            },
                            id: 'complementoPolizaTalachasCancelarBtn',
                            iconCls: 'ux-btnDel',
                            text: 'Cancelar'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    border: false,
                    height: 221,
                    id: 'complementoPolizaTalachasFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    url: '../../carbookBck/json/trcomplementoPoliza.php?complementoActionHdn=addFacturaGastos',
                    items: [
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 20,
                            id: 'complementoPolizaTalachasQrTxt',
                            width: 440,
                            fieldLabel: 'Leer QR',
                            labelWidth: 90,
                            msgTarget: 'side',
                            validateOnBlur: false,
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            listeners: {
                                change: {
                                    fn: me.oncomplementoPolizaTalachasQrTxtChange,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 50,
                            id: 'complementoPolizaTalachasRfcTxt',
                            fieldLabel: 'R.F.C Emisor',
                            labelWidth: 90,
                            readOnly: true,
                            enableKeyEvents: true
                        },
                        {
                            xtype: 'textfield',
                            x: 290,
                            y: 50,
                            id: 'complementoPolizaTalachasImporteTxt',
                            width: 170,
                            fieldLabel: 'Importe',
                            labelWidth: 50,
                            readOnly: true,
                            enableKeyEvents: true
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 80,
                            id: 'complementoPolizaTalachasFolioFiscalTxt',
                            width: 400,
                            fieldLabel: 'Folio Fiscal',
                            labelWidth: 90,
                            readOnly: true,
                            enableKeyEvents: true
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 130,
                            id: 'complementoPolizaTalachasSubtotalTxt',
                            width: 200,
                            fieldLabel: 'Subtotal',
                            labelWidth: 90,
                            msgTarget: 'side',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            maskRe: /[0-9\.]/,
                            listeners: {
                                blur: {
                                    fn: me.oncomplementoPolizaTalachasSubtotalTxtBlur,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 160,
                            id: 'complementoPolizaTalachasIVATxt',
                            width: 200,
                            fieldLabel: 'IVA',
                            labelWidth: 90,
                            msgTarget: 'side',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            blankText: 'Campo Requerido',
                            enableKeyEvents: true,
                            maskRe: /[0-9\.]/,
                            listeners: {
                                blur: {
                                    fn: me.oncomplementoPolizaTalachasIVATxtBlur,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 190,
                            id: 'complementoPolizaTalachasTotalTxt',
                            width: 200,
                            fieldLabel: 'Total',
                            labelWidth: 90,
                            value: '0.00',
                            readOnly: true,
                            enableKeyEvents: true
                        },
                        {
                            xtype: 'toolbar',
                            x: 230,
                            y: 130,
                            height: 80,
                            id: 'complementoPolizaTalachasBotonesTlb',
                            width: 90,
                            vertical: true,
                            items: [
                                {
                                    xtype: 'button',
                                    handler: function(button, e) {
                                        var form = Ext.getCmp('complementoPolizaTalachasFrm').getForm();

                                        if(form.isValid()){
                                            var selection = Ext.getCmp('complementoPolizaComprobacionGrd').getView().getSelectionModel().getSelection()[0];
                                            var numCompra = 0;

                                            if(Ext.data.StoreManager.lookup('complementoPolizaTalachasStr').getCount() > 0){
                                                var record = Ext.data.StoreManager.lookup('complementoPolizaTalachasStr').getAt(
                                                Ext.data.StoreManager.lookup('complementoPolizaTalachasStr').getCount()-1
                                                );
                                                numCompra = parseInt(Ext.data.StoreManager.lookup('complementoPolizaTalachasStr').getCount())+1;
                                            } else {
                                                numCompra = 1;
                                            }

                                            form.submit({
                                                params:{
                                                    trcomplementoPolizaIdViajeHdn:Ext.getCmp('complementoPolizaIdViajeHdn').getValue(),
                                                    trcomplementoPolizaConceptoHdn:selection.data.concepto,
                                                    trcomplementoPolizaRFCHdn:Ext.getCmp('complementoPolizaTalachasRfcTxt').getValue(),
                                                    trcomplementoPolizaFolioHdn:numCompra,
                                                    trcomplementoPolizaFolioFiscalHdn:Ext.getCmp('complementoPolizaTalachasFolioFiscalTxt').getValue(),
                                                    trcomplementoPolizaSubtotalHdn:Ext.getCmp('complementoPolizaTalachasSubtotalTxt').getValue(),
                                                    trcomplementoPolizaIvaHdn:Ext.getCmp('complementoPolizaTalachasIVATxt').getValue(),
                                                    trcomplementoPolizaTotalHdn:Ext.getCmp('complementoPolizaTalachasImporteTxt').getValue(),
                                                    trcomplementoPolizaMesAfectacionHdn:Ext.getCmp('complementoPolizaMesAfectacionCmb').getValue()
                                                },
                                                success: function(form, action){
                                                    Ext.data.StoreManager.lookup('complementoPolizaTalachasStr').add({
                                                        numeroCompra:numCompra,
                                                        rfc:Ext.getCmp('complementoPolizaTalachasRfcTxt').getValue(),
                                                        subtotal:Ext.getCmp('complementoPolizaTalachasSubtotalTxt').getValue(),
                                                        iva:Ext.getCmp('complementoPolizaTalachasIVATxt').getValue(),
                                                        total:Ext.getCmp('complementoPolizaTalachasImporteTxt').getValue(),
                                                        folioFiscal:Ext.getCmp('complementoPolizaTalachasFolioFiscalTxt').getValue()
                                                    });

                                                    Ext.getCmp('complementoPolizaTalachasCancelarBtn').handler();
                                                    // ------------------- PIDE EL FOLIO PARA EL CONCEPTO
                                                        Ext.Ajax.request({
                                                            url: '../../carbookBck/json/trViajesTractores.php',            
                                                            params: {
                                                                trViajesTractoresActionHdn:'sqlGetFolio',
                                                                trViajesTractoresTipoDoctoHdn: 'PZ',
                                                                trViajesTractoresCentroHdn:'POLZ'
                                                            },            
                                                            success: function(response){
                                                                response = Ext.JSON.decode(response.responseText);
                                                                console.log("esto es la consulta"+response.records);
                                                                if (response.records > 0) {      
                                                                    Ext.getCmp('complementoPolizaTalachaRfcTxt').setValue(ciaSesVal+response.root[0].folioActual);
                                                                    Ext.getCmp('complementoPolizaTalachaRfcTxt').disable(true);
                                                                }else{
                                                                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "No se puede Obtener el Folio del Concepto");
                                                                    this.up('window').close();
                                                                }
                                                            }
                                                        });
                                                    // ---------------------------------------------------                                                    

                                                    if(Ext.data.StoreManager.lookup('complementoPolizaTalachasStr').getCount() 	){

                                                    }
                                                },
                                                failure: function(form, action){
                                                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, action.result.errorMessage);
                                                }
                                            });
                                        } else {
                                            Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "Faltan Campos Requeridos");
                                        }
                                    },
                                    cls: '',
                                    height: 25,
                                    id: 'complementoPolizaTalachasAgregarBtn',
                                    width: 78,
                                    iconCls: 'ux-btnSave',
                                    text: 'Agregar'
                                },
                                {
                                    xtype: 'button',
                                    handler: function(button, e) {
                                        var selection = Ext.getCmp('complementoPolizaTalachasGrd').getView().getSelectionModel().getSelection()[0];
                                        if(selection){
                                            Ext.Ajax.request({
                                                url: '../../carbookBck/json/trcomplementoPoliza.php',
                                                method: 'POST',
                                                params: {
                                                    complementoActionHdn:'dltFacturaGastos',
                                                    trcomplementoPolizaFolioFiscalHdn:selection.data.folioFiscal,
                                                    folioHdn: Ext.getCmp('complementoPolizaFolioTxt').getValue()
                                                },
                                                success: function(response){
                                                    Ext.getCmp('complementoPolizaTalachasGrd').store.remove(selection);
                                                    button.disable();
                                                },
                                                failure: function(response){
                                                    var jsonRsp = JSON.parse(response.responseText);
                                                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, jsonRsp.errorMessage);

                                                }
                                            });
                                        }
                                    },
                                    cls: '',
                                    disabled: true,
                                    height: 25,
                                    id: 'complementoPolizaTalachasEliminarBtn',
                                    width: 78,
                                    iconCls: 'ux-btnDel',
                                    text: 'Eliminar'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    border: false,
                    height: 331,
                    id: 'complementoPolizaTalachasPnl',
                    layout: 'absolute',
                    title: '',
                    items: [
                        {
                            xtype: 'gridpanel',
                            x: 10,
                            y: 10,
                            height: 130,
                            id: 'complementoPolizaTalachasGrd',
                            width: 460,
                            title: '',
                            enableColumnHide: false,
                            enableColumnMove: false,
                            sortableColumns: false,
                            store: 'complementoPolizaTalachasStr',
                            viewConfig: {
                                id: 'complementoPolizaTalachasGrdVw'
                            },
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    width: 70,
                                    dataIndex: 'numeroCompra',
                                    text: '# Compra'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        return parseFloat(value).toFixed(2);
                                    },
                                    width: 90,
                                    dataIndex: 'subtotal',
                                    text: 'Subtotal'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        return parseFloat(value).toFixed(2);
                                    },
                                    width: 90,
                                    dataIndex: 'iva',
                                    text: 'IVA'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        return parseFloat(value).toFixed(2);
                                    },
                                    width: 90,
                                    dataIndex: 'total',
                                    text: 'Total'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 150,
                                    dataIndex: 'rfc',
                                    text: 'RFC'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 250,
                                    dataIndex: 'folioFiscal',
                                    text: 'FolioFiscal'
                                }
                            ],
                            listeners: {
                                itemclick: {
                                    fn: me.oncomplementoPolizaTalachasGrdItemClick,
                                    scope: me
                                }
                            }
                        }
                    ]
                }
            ],
            listeners: {
                boxready: {
                    fn: me.oncomplementoPolizaTalachasWinBoxReady,
                    scope: me
                },
                beforeclose: {
                    fn: me.oncomplementoPolizaTalachasWinBeforeClose,
                    scope: me
                },
                render: {
                    fn: me.oncomplementoPolizaTalachasWinRender,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    oncomplementoPolizaTalachasQrTxtChange: function(field, newValue, oldValue, eOpts) {
        var lacturaQR = Ext.getCmp('complementoPolizaTalachasQrTxt').getValue();
        var numCaracteres = lacturaQR.length;

        if(numCaracteres >= 79){
            var qr = lacturaQR;
            var match;

            Ext.getCmp('complementoPolizaTalachasQrTxt').setReadOnly(true);
            console.log(lacturaQR);
            match = qr.match(/id¿(.*?)$/);

            Ext.Ajax.request({
                url: '../../carbookBck/json/trcomplementoPoliza.php?',
                method: 'POST',
                params: {
                    complementoActionHdn:'montoLavada',
                    montoConcepto:'rfcTraslado'
                },
                success: function(response){
                    var jsonRsp = JSON.parse(response.responseText);

                    if(jsonRsp && jsonRsp.root.length > 0){

                        var rfcEmpresa = jsonRsp.root[0].nombre;

                        match = qr.match(/rr¿(.*?)(?=\/)/);

                        if(rfcEmpresa === match[1] ){
                            Ext.getCmp('complementoPolizaTalachasQrTxt').setReadOnly(true);

                            match = qr.match(/id¿(.*?)$/);
                            if(match && match.length > 0){
                                Ext.getCmp('complementoPolizaTalachasQrTxt').setReadOnly(true);

                                match = qr.match(/id¿(.*?)$/);
                                if(match && match.length > 0){
                                    Ext.Ajax.request({
                                        url: '../../carbookBck/json/trcomplementoPoliza.php',
                                        method: 'POST',
                                        params: {
                                            complementoActionHdn:'getFacturasPoliza',
                                            trcomplementoPolizaFolioFiscalHdn:match[1].replace(/\'|\’/g, '-').toUpperCase(),
                                            folioHdn: Ext.getCmp('complementoPolizaFolioTxt').getValue()
                                        },
                                        success: function(response){
                                            var jsonRsp = JSON.parse(response.responseText);

                                            if(!jsonRsp || jsonRsp.records === 0){
                                                Ext.getCmp('complementoPolizaTalachasFolioFiscalTxt').setValue(match[1].replace(/\'|\’/g, '-').toUpperCase());
                                                match = qr.match(/tt¿(.*?)(?=\/)/);
                                                if(match && match.length > 0){
                                                    Ext.getCmp('complementoPolizaTalachasImporteTxt').setValue(parseFloat(match[1]).toFixed(2));

                                                    match = qr.match(/re¿(.*?)(?=\/)/);
                                                    if(match && match.length > 0){
                                                        Ext.getCmp('complementoPolizaTalachasRfcTxt').setValue(match[1]);

                                                        Ext.getCmp('complementoPolizaTalachasNumeroTxt').focus();
                                                    } else {
                                                        Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "No se encontró el Folio Fiscal en el QR");
                                                    }
                                                } else {
                                                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "No se encontró el Total en el QR");
                                                }
                                            } else {
                                                Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title,
                                                              "La Factura con el Folio Fiscal " +match[1].replace(/\'|\’/g, '-').toUpperCase()+" ya fue capturada");
                                                component.setValue('');
                                                component.focus();
                                                return;
                                            }
                                        },
                                        failure: function(response){
                                            Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "Error al verificar el Folio Fical");
                                            component.setValue('');
                                            Ext.getCmp('complementoPolizaTalachasImporteTxt').setValue('');
                                            Ext.getCmp('complementoPolizaTalachasRfcTxt').setValue('');
                                            component.focus();
                                            return;
                                        }
                                    });
                                } else {
                                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "No se encontró el RFC del emisor en el QR");
                                }
                            } else {
                                Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "No se encontró el RFC del emisor en el QR");
                            }
                        }else{
                            Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "El RFC de la factura no es el de la Empresa");
                        }
                    }
                },
                failure: function(response){
                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "Error al obtener el RFC de la empresa");
                    //Ext.getCmp('complementoPolizaAlimentosMontoLbl').disable(true);
                }
            });
        }
    },

    oncomplementoPolizaTalachasSubtotalTxtBlur: function(component, e, eOpts) {
        var subtotal = 0.00;
        var iva = 0.00;

        if(component.value != ''){
            subtotal = parseFloat(component.value);
        }
        if(Ext.getCmp('complementoPolizaTalachasIVATxt').getValue() != ''){
            iva = parseFloat(Ext.getCmp('complementoPolizaTalachasIVATxt').getValue());
        }

        var suma = subtotal + iva;

        if(component.value != ''){
            if(Ext.getCmp('complementoPolizaTalachasIVATxt').getValue() != ''){
                if(suma != parseFloat(Ext.getCmp('complementoPolizaTalachasImporteTxt').getValue())){
                    Ext.Msg.alert("NO", "El importe no coincide con los valores ingresados");
                    Ext.getCmp('complementoPolizaTalachasTotalTxt').setValue(
                        parseFloat(Ext.getCmp('complementoPolizaTalachasIVATxt').getValue()).toFixed(2)
                    );
                    component.setValue('');
                    component.focus();
                } else {
                    Ext.getCmp('complementoPolizaTalachasTotalTxt').setValue(suma.toFixed(2));
                }
            } else {
                Ext.getCmp('complementoPolizaTalachasTotalTxt').setValue(suma.toFixed(2));
            }
        }  else {
            Ext.getCmp('complementoPolizaTalachasTotalTxt').setValue(suma.toFixed(2));
        }
    },

    oncomplementoPolizaTalachasIVATxtBlur: function(component, e, eOpts) {
        var subtotal = 0.00;
        var iva = 0.00;

        if(component.value != ''){
            subtotal = parseFloat(component.value);
        }
        if(Ext.getCmp('complementoPolizaTalachasSubtotalTxt').getValue() != ''){
            iva = parseFloat(Ext.getCmp('complementoPolizaTalachasSubtotalTxt').getValue());
        }

        var suma = subtotal + iva;

        if(component.value != ''){
            if(Ext.getCmp('complementoPolizaTalachasSubtotalTxt').getValue() != ''){
                if(suma != parseFloat(Ext.getCmp('complementoPolizaTalachasImporteTxt').getValue())){
                    Ext.Msg.alert("NO", "El importe no coincide con los valores ingresados");
                    Ext.getCmp('complementoPolizaTalachasTotalTxt').setValue(parseFloat(Ext.getCmp('complementoPolizaTalachasSubtotalTxt').getValue()));
                    component.setValue('');
                    component.focus();
                } else {
                    Ext.getCmp('complementoPolizaTalachasTotalTxt').setValue(suma.toFixed(2));
                }
            } else {
                Ext.getCmp('complementoPolizaTalachasTotalTxt').setValue(suma.toFixed(2));
            }
        } else {
            Ext.getCmp('complementoPolizaTalachasTotalTxt').setValue(suma.toFixed(2));
        }
    },

    oncomplementoPolizaTalachasGrdItemClick: function(dataview, record, item, index, e, eOpts) {
        if(index >= 0)
            Ext.getCmp('complementoPolizaTalachasEliminarBtn').enable();
        else
            Ext.getCmp('complementoPolizaTalachasEliminarBtn').disable();
    },

    oncomplementoPolizaTalachasWinBoxReady: function(component, width, height, eOpts) {
        console.log("esta entrando");
        // ------------------- PIDE EL FOLIO PARA EL CONCEPTO
            Ext.Ajax.request({
                url: '../../carbookBck/json/trViajesTractores.php',            
                params: {
                    trViajesTractoresActionHdn:'sqlGetFolio',
                    trViajesTractoresTipoDoctoHdn: 'PZ',
                    trViajesTractoresCentroHdn:'POLZ'
                },            
                success: function(response){
                    response = Ext.JSON.decode(response.responseText);
                    console.log("esto es la consulta"+response.records);
                    if (response.records > 0) {      
                        Ext.getCmp('complementoPolizaTalachaRfcTxt').setValue(ciaSesVal+response.root[0].folioActual);
                        Ext.getCmp('complementoPolizaTalachaRfcTxt').disable(true);
                    }else{
                        Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "No se puede Obtener el Folio del Concepto");
                        this.up('window').close();
                    }
                }
            });
        // ---------------------------------------------------        
        Ext.Ajax.request({
            url: '../../carbookBck/json/trcomplementoPoliza.php',
            method: 'POST',
            params: {
                complementoActionHdn:'getDatosPoliza',
                trcomplementoPolizaCentroDistHdn:ciaSesVal,
                conceptoHdn:'6012'
            },
            success: function(response){
                var jsonRsp = JSON.parse(response.responseText);
                if(!jsonRsp || jsonRsp.root.length <= 0){
                    Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "Error al obtener el concepto de Talachas");
                    this.up('window').close();
                }
            },
            failure: function(response){
                Ext.Msg.alert(Ext.WindowMgr.get('complementoPolizaTalachasWin').title, "Error al obtener el concepto de Talachas");
                this.up('window').close();
            }
        });
    },

    oncomplementoPolizaTalachasWinBeforeClose: function(panel, eOpts) {
        var total = 0.00;
        var subtotal = 0.00;
        var iva = 0.00;

        Ext.data.StoreManager.lookup('complementoPolizaTalachasStr').each(function(record){
            total += parseFloat(record.data.total);
            subtotal += parseFloat(record.data.subtotal);
            iva += parseFloat(record.data.iva);
        });

        var selection = Ext.getCmp('complementoPolizaComprobacionGrd').getView().getSelectionModel().getSelection()[0];
        if(selection && total >= 0.00){
            selection.set('total', parseFloat(total).toFixed(2));
            selection.set('subtotal', parseFloat(subtotal).toFixed(2));
            selection.set('iva', parseFloat(iva).toFixed(2));
        }
    },

    oncomplementoPolizaTalachasWinRender: function(component, eOpts) {
        /*------------------Funcion que simula el TAB con el ENTER en los campos--------------------------------*/

        //Hago un query con los elementos que sean textfields, numberfields o combobox y obtengo un array de ellos.
        var elements = Ext.ComponentQuery.query('[xtype=textfield]').concat(Ext.ComponentQuery.query('[xtype=combobox]')).concat(Ext.ComponentQuery.query('[xtype=numberfield]'));

        //Por cada elemento lo suscribo a un evento keypress, con un eventHandler llamado nextFocus
        Ext.each(elements, function(el){
            el.on('keypress', nextFocus);
        });

        //Event Handler que se encarga de recorrer y hacer FOCO a los elementos del formulario.
        function nextFocus(field, e){
            var actual = field;

            if (e.getKey() === Ext.EventObject.ENTER) { //Si el input del teclado es Enter...
                while(actual.next() && !actual.next().isVisible()){//Recorro los elementos. Si existe el elemento siguiente y es Visible (por los hiddens fields).
                    actual = actual.next();
                }
                if(actual.next()){
                    actual.next().focus();
                }
                else{//Si estoy en el elemento final regreso al primero...NO SÉ si sea el comportamiento requerido.

                    while(actual.prev()){//Recorro mientras exista elemento
                        actual = actual.prev();
                    }
                    if(actual.isVisible()){//Si no es un hidden field le hago foco
                        actual.focus();
                    }
                    else{// de lo contrario vuelvo a recorrer hasta encontrar algún elemento visible
                        while(actual.next() && !actual.next().isVisible()){
                            actual = actual.next();
                        }
                        actual.next().focus();
                    }
                }
            }
        }
    }

});