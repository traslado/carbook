/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.catGenMenuModel4', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Catalogos',
            iconCls: 'catGeneralesMenuIco',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
					{
						text: 'Generales',
						iconCls: 'generalesCatIco',
						menu:
						{
							items:
							[
								{
									text: 'Valores Generales',
									iconCls:'genCatGenerales',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catGeneralesWin');
										win = module.createWindow_catGeneralesWin();
									},
									scope: this,
									windowId: 'catGeneralesWin'
								},
								{
									text: 'Conceptos',
									iconCls:'catConceptos',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catConceptosWin');
										win = module.createWindow_catConceptosWin();
									},
									scope: this,
									windowId: 'catConceptosWin'
								},
								{
									text: 'Bancos',
									iconCls:'catBancos',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catBancosWin');
										win = module.createWindow_catBancosWin();
									},
									scope: this,
									windowId: 'catBancosWin'
								},
								{
									text: 'Cuentas Bancarias',
									iconCls:'catCuentasBanco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catCuentasBancariasWin');
										win = module.createWindow_catCuentasBancariasWin();
									},
									scope: this,
									windowId: 'catCuentasBancariasWin'
								},
								{
									text: 'Colores Sistema',
									iconCls:'catalogosColSist',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catColoresSistemaWin');
										win = module.createWindow_catColoresSistemaWin();
									},
									scope: this,
									windowId: 'catColoresSistemaWin'
								}
							]
						}
					},
					{
						text: 'Geograficos',
						iconCls: 'catGeograficos',
						menu:
						{
							items:
							[
								{
									text: 'Paises',
									iconCls:'catPaises',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catPaisesWin');
										win = module.createWindow_catPaisesWin();
									},
									scope: this,
									windowId: 'catPaisesWin'
								},
								{
									text: 'Estados',
									iconCls:'catEstados',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catEstadosWin');
										win = module.createWindow_catEstadosWin();
									},
									scope: this,
									windowId: 'catEstadosWin'
								},
								{
									text: 'Municipios',
									iconCls:'catMunicipios',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catMunicipiosVw');
										win = module.createWindow_catMunicipiosVw();
									},
									scope: this,
									windowId: 'catMunicipiosVw'
								},
								{
									text: 'Colonias',
									iconCls:'catColonias',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catColoniasVw');
										win = module.createWindow_catColoniasVw();
									},
									scope: this,
									windowId: 'catColoniasVw'
								},
								{
									text: 'Regiones',
									iconCls:'catMarcasDistribuidor',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catRegionesWin');
										win = module.createWindow_catRegionesWin();
									},
									scope: this,
									windowId: 'catRegionesWin'
								}
							]
						}
					},
					{
						text: 'Unidades',
						iconCls: 'catalogosAutIco',
						menu:
						{
							items:
							[
								{
									text: 'Marcas Unidades',
									iconCls:'catMarcasUnidades',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catMarcasUnidadesVw');
										win = module.createWindow_catMarcasUnidadesVw();
									},
									scope: this,
									windowId: 'catMarcasUnidadesVw'
								},
								{
									text: 'Colores',
									iconCls:'auCatColores',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catColoresWin');
										win = module.createWindow_catColoresWin();
									},
									scope: this,
									windowId: 'catColoresWin'
								},
								{
									text: 'Series',
									iconCls:'catSeries',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catSeriesVw');
										win = module.createWindow_catSeriesVw();
									},
									scope: this,
									windowId: 'catSeriesVw'
								},
								{
									text: 'Clasificaciones por Marca',
									iconCls:'catalogosRefIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catClasificacionMarcaWin');
										win = module.createWindow_catClasificacionMarcaWin();
									},
									scope: this,
									windowId: 'catClasificacionMarcaWin'
								},
								{
									text: 'Simbolos Unidades',
									iconCls:'catSimbolosUnidades',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catSimbolosWin');
										win = module.createWindow_catSimbolosWin();
									},
									scope: this,
									windowId: 'catSimbolosWin'
								},
								{
									text: 'Danos',
									iconCls:'catMarcasDistribuidor',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catDanosWin');
										win = module.createWindow_catDanosWin();
									},
									scope: this,
									windowId: 'catDanosWin'
								}
							]
						}
					},
					{
						text: 'Operativos',
						iconCls: 'catOperativos',
						menu:
						{
							items:
							[
								{
									text: 'Centros de Distribucion',
									iconCls:'catCentrosDist',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catCentrosDistribucionWin');
										win = module.createWindow_catCentrosDistribucionWin();
									},
									scope: this,
									windowId: 'catCentrosDistribucionWin'
								},
								{
									text: 'Marcas Centro Distribucion',
									iconCls:'catMarcasCentro',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catMarcasCdWin');
										win = module.createWindow_catMarcasCdWin();
									},
									scope: this,
									windowId: 'catMarcasCdWin'
								},
								{
									text: 'Conceptos Centros',
									iconCls:'catConceptosCentros',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('caConceptosCentrosVw');
										win = module.createWindow_caConceptosCentrosVw();
									},
									scope: this,
									windowId: 'caConceptosCentrosVw'
								},
								{
									text: 'Distribuidores',
									iconCls:'catDistribuidores',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catDistribuidoresWin');
										win = module.createWindow_catDistribuidoresWin();
									},
									scope: this,
									windowId: 'catDistribuidoresWin'
								},
								{
									text: 'Marcas por Distribuidor',
									iconCls:'catMarcasDistribuidor',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catMarcasDiWin');
										win = module.createWindow_catMarcasDiWin();
									},
									scope: this,
									windowId: 'catMarcasDiWin'
								},
								{
									text: 'Destinos Especiales',
									iconCls:'catDestinosEspeciales',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catDestinosEspecialesWin');
										win = module.createWindow_catDestinosEspecialesWin();
									},
									scope: this,
									windowId: 'catDestinosEspecialesWin'
								},
								{
									text: 'Patios',
									iconCls:'catPatios',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catPatiosWin');
										win = module.createWindow_catPatiosWin();
									},
									scope: this,
									windowId: 'catPatiosWin'
								},
								{
									text: 'Plazas',
									iconCls:'catPlazas',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catPlazasWin');
										win = module.createWindow_catPlazasWin();
									},
									scope: this,
									windowId: 'catPlazasWin'
								},
								{
									text: 'Proveedores Gasolina',
									iconCls:'catProvedoresGas',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catProveedoresGasolinaWin');
										win = module.createWindow_catProveedoresGasolinaWin();
									},
									scope: this,
									windowId: 'catProveedoresGasolinaWin'
								},
								{
									text: 'Rutas',
									iconCls:'catRutas',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('caRutasTblWin');
										win = module.createWindow_caRutasTblWin();
									},
									scope: this,
									windowId: 'caRutasTblWin'
								},
								{
									text: 'Tarifas',
									iconCls:'catTarifas',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catTarifasWin');
										win = module.createWindow_catTarifasWin();
									},
									scope: this,
									windowId: 'catTarifasWin'
								},
								{
									text: 'Companias Tractores',
									iconCls:'catCompanias',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catCompaniasWin');
										win = module.createWindow_catCompaniasWin();
									},
									scope: this,
									windowId: 'catCompaniasWin'
								},
								{
									text: 'Tractores',
									iconCls:'catTractores',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catTractoresWin');
										win = module.createWindow_catTractoresWin();
									},
									scope: this,
									windowId: 'catTractoresWin'
								},
								{
									text: 'Choferes',
									iconCls:'serCatMecanicos',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catChoferesWin');
										win = module.createWindow_catChoferesWin();
									},
									scope: this,
									windowId: 'catChoferesWin'
								},
								{
									text: 'Catalogo Campa�as',
									iconCls:'catTractores',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('caCampaniasWin');
										win = module.createWindow_caCampaniasWin();
									},
									scope: this,
									windowId: 'caCampaniasWin'
								},
								{
									text: 'Distribuidores Exportacion',
									iconCls:'catDistribuidores',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catDisExpWin');
										win = module.createWindow_catDisExpWin();
									},
									scope: this,
									windowId: 'catDisExpWin'
								}
							]
						}
					},
					{
						text: 'Posicion Unidades',
						iconCls: 'catalogosAutIco',
						menu:
						{
							items:
							[
								{
									text: 'Grupos Patio',
									iconCls:'catalogosAutIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catGruposWin');
										win = module.createWindow_catGruposWin();
									},
									scope: this,
									windowId: 'catGruposWin'
								},
								{
									text: 'Localizacion Unidad',
									iconCls:'catalogosAutIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catLocalizacionPatioWin');
										win = module.createWindow_catLocalizacionPatioWin();
									},
									scope: this,
									windowId: 'catLocalizacionPatioWin'
								}
							]
						}
					},
					{
						text: 'Mantenimiento Tractor',
						iconCls: 'catMarcasDistribuidor',
						menu:
						{
							items:
							[
								{
									text: 'Control Mantenimiento',
									iconCls:'catMarcasDistribuidor',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trControlMantenimientoWin');
										win = module.createWindow_trControlMantenimientoWin();
									},
									scope: this,
									windowId: 'trControlMantenimientoWin'
								},
								{
									text: 'Mantenimiento Tractor',
									iconCls:'catMarcasDistribuidor',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trMantenimientoWin');
										win = module.createWindow_trMantenimientoWin();
									},
									scope: this,
									windowId: 'trMantenimientoWin'
								}
							]
						}
					},
					{
						text: 'Catalogos GMC',
						iconCls: 'catGeneralesMenuIco',
						menu:
						{
							items:
							[
								{
									text: 'Catalogo Campa�as GM',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('catCampaniaWin');
										win = module.createWindow_catCampaniaWin();
									},
									scope: this,
									windowId: 'catCampaniaWin'
								}
							]
						}
					}
				]
            }
        };
    }
});