<?php

  /************************************************************************
  * Autor: Carlos Alberto Sierra Mayoral
  * Fecha: 07-Octubre-2014
  * Tablas afectadas: trViajesTractoresTbl, trTalonesViajesTbl, trFoliosTbl
  * Descripción: Programa para dar mantenimiento a viajes Foraneos
  *************************************************************************/
	session_start();
	$_SESSION['modulo'] = "trViajesTractores";
  
  $_SESSION['usuCto'] = "CDTOL";
  $_SESSION['usuCompania'] = "TOLUCA";
  $_SESSION['idUsuario'] = 1;  
  require_once("../funciones/generales.php");
  require_once("../funciones/construct.php");
  require_once("../funciones/utilidades.php");

  $_REQUEST = trasformUppercase($_REQUEST);
	
	switch($_SESSION['idioma']){
    case 'ES':
      include_once("../funciones/idiomas/mensajesES.php");
    break;
    case 'EN':
      include_once("../funciones/idiomas/mensajesEN.php");
    break;
    default:
    include_once("../funciones/idiomas/mensajesES.php");
  }

switch($_REQUEST['trCancelacionViajesActionHdn']){
case 'getCancelacionPreviaje':
    getCancelacionPreviaje();
    break;
case 'getCancelacionAsignacion':
    getCancelacionAsignacion();    
    break;
case 'getCancelacionGastosViaje';
    getCancelacionGastosViaje();
    break;
case 'getCancelacionGastosFolio':
  getCancelacionGastosFolio();
  break;    
}
  function getCancelacionPreviaje(){

    if($_REQUEST['trCancelacionViajesIdViajeHdn'] == ""){
            $e[] = array('id'=>'trCancelacionViajesIdViajeHdn','msg'=>getRequerido());
            $a['success'] = false;
    }
    if($_REQUEST['trCancelacionIdTractorHdn'] == ""){
            $e[] = array('id'=>'trCancelacionIdTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
    }      
    $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl ".
                      "SET claveMovimiento = 'VX' ".
                      "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
    
    $rs = fn_ejecuta_query($sqlUpdViajeStr);

    $sqlUpdTalonStr = "UPDATE trTalonesViajesTbl ".
                      "SET claveMovimiento = 'TC' ".
                      "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";

    $rs = fn_ejecuta_query($sqlUpdTalonStr);                      

    $sqlUpdEsperaChoferStr =  "UPDATE trChoferesEsperaTbl ".
                              "SET claveMovimiento = 'VV' ".
                              "WHERE idTractor = '".$_REQUEST['trCancelacionIdTractorHdn']."' ";                                             
                                     
    $rs = fn_ejecuta_query($sqlUpdEsperaChoferStr);

    $sqlUpdEsperaChoferStr =  "DELETE trUnidadesEmbarcadasTbl ".
                              "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";                                             

  
    $a['errors'] = $e;
    $a['successTitle'] = getMsgTitulo();
    echo json_encode($a);
  }
  
  function getCancelacionAsignacion(){

        $a = array();
        $e = array();
        $a['success'] = true;
        if($_REQUEST['trCancelacionViajesIdViajeHdn'] == ""){
          $e[] = array('id'=>'trCancelacionViajesIdViajeHdn','msg'=>getRequerido());
          $a['success'] = false;
        }
        if($_REQUEST['trCancelacionIdTractorHdn'] == ""){
          $e[] = array('id'=>'trCancelacionIdTractorHdn','msg'=>getRequerido());
          $a['success'] = false;
        }
        if($_REQUEST['cancelacionAsignacionCompaniaHdn'] == ""){
          $e[] = array('id'=>'cancelacionAsignacionCompaniaHdn','msg'=>getRequerido());
          $a['success'] = false;
        }
        if($_REQUEST['cancelacionAsignacionContadorHdn'] == ""){
          $e[] = array('id'=>'cancelacionAsignacionContadorHdn','msg'=>getRequerido());
          $a['success'] = false;
        }
        if($_REQUEST['cancelacionAsignacionVinHdn'] == ""){
          $e[] = array('id'=>'cancelacionAsignacionVinHdn','msg'=>getRequerido());
          $a['success'] = false;
        }
        if($_REQUEST['cancelacionAsignacionChoferHdn'] == ""){
          $e[] = array('id'=>'cancelacionAsignacionChoferHdn','msg'=>getRequerido());
          $a['success'] = false;
        }
        if ($a['success'] == true) {
          //Actualiza Viajes Tractores
          $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl ".
          "SET claveMovimiento = 'VG' ".
          "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
          $rs = fn_ejecuta_query($sqlUpdViajeStr);
          //Actualiza Talones Viajes
          $sqlUpdTalonStr = "UPDATE trTalonesViajesTbl ".
          "SET claveMovimiento = 'TG', ".
          "tipoDocumento = 'TG', ".
          "tipoTalon = 'NA' ".  
          "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
          //Actualizar  alhistoricounidadestbl
          if ($_REQUEST['cancelacionAsignacionVinHdn'] != "|") {
            $vinAsignados = explode('|', substr($_REQUEST['cancelacionAsignacionVinHdn'], 0, -1));
              for($i = 0; $i<sizeof($vinAsignados);$i++) {
                $sqlHistorico = "SELECT MAX(idHistorico) FROM alhistoricounidadestbl ".
                "WHERE vin ='".$vinAsignados[$i]."'";
                
                $his = fn_ejecuta_query($sqlHistorico);
                $historico =  ($his['root'][0]['MAX(idHistorico)']);

                $sqlUpdLlavesRecepcionStr = "UPDATE alhistoricounidadestbl ".
                "SET claveMovimiento = 'CA' ".
                "WHERE idHistorico = ".$historico;
          $rs = fn_ejecuta_query($sqlUpdLlavesRecepcionStr);
              }
          }

          $rs = fn_ejecuta_query($sqlUpdTalonStr);          
          //Actualiza alultimodetalle
          
          if ($_REQUEST['cancelacionAsignacionVinHdn'] != "|") {
            $vinAsignados = explode('|', substr($_REQUEST['cancelacionAsignacionVinHdn'], 0, -1));
            for($i = 0; $i<sizeof($vinAsignados);$i++) {
              $sqlUpdLlavesRecepcionStr = "UPDATE alultimodetalletbl ud , ".
              "(SELECT vin, centroDistribucion, fechaEvento, claveMovimiento, ".
              "distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ".
              "FROM alhistoricounidadestbl hu ".
              "WHERE vin= '".$vinAsignados[$i]."' AND idHistorico = (SELECT MAX(idHistorico) ".
              "FROM alhistoricounidadestbl WHERE claveMovimiento NOT IN ".
              "(SELECT valor FROM cageneralestbl WHERE tabla = 'alhistoricounidadestbl' ".
              "AND columna = 'novalidos') AND vin = '".$vinAsignados[$i]."')) hu ".
              "SET ud.centroDistribucion = hu.centroDistribucion, ".
              "ud.fechaEvento = hu.fechaEvento, ".
              "ud.claveMovimiento = hu.claveMovimiento, ".
              "ud.distribuidor = hu.distribuidor, ".
              "ud.idTarifa = hu.idTarifa, ".
              "ud.localizacionUnidad = hu.localizacionUnidad, ".
              "ud.claveChofer = hu.claveChofer, ".
              "ud.observaciones = hu.observaciones, ".
              "ud.usuario = hu.usuario, ".
              "ud.ip = hu.ip ".
              "where ud.vin = hu.vin ";
              $rs = fn_ejecuta_query($sqlUpdLlavesRecepcionStr);

              //Actualiza la tabla de trUnidadesDetallesTalonesTbl por VIN
              $sqlUpdUnidadesDetallesStr = "UPDATE trUnidadesDetallesTalonesTbl ".
             "SET estatus = 'C' ".
             "WHERE vin='".$vinAsignados[$i]."'";

              $rs = fn_ejecuta_query($sqlUpdUnidadesDetallesStr);
            }
          }
          
          //Insertar tresperaChoferestbl 
          $sqlAddEsperaChoferStr = "INSERT INTO tresperachoferestbl (centroDistribucion, idTractor, ".
          "claveChofer, consecutivo, fechaEvento, claveMovimiento) ".
          "VALUES ('".$_SESSION['usuCto']."','".$_REQUEST['trCancelacionIdTractorHdn']."','".$_REQUEST['cancelacionAsignacionChoferHdn']."',999,'".date("Y-m-d h:m:s")."','VG')";
          $rs = fn_ejecuta_query($sqlAddEsperaChoferStr);


          if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
            $a['sql'] = $sqlAddEsperaChoferStr;
          } else {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddEsperaChoferStr;
          }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a); 
  
      }

      function getCancelacionGastosViaje(){
        $a = array();
        $e = array();
        $a['success'] = true;
        if($_REQUEST['trCancelacionViajesIdViajeHdn'] == ""){
          $e[] = array('id'=>'trCancelacionViajesIdViajeHdn','msg'=>getRequerido());
          $a['success'] = false;
        }
        if($_REQUEST['cancelacionAsignacionViajesFoliosHdn'] == ""){
          $e[] = array('id'=>'cancelacionAsignacionViajesFoliosHdn','msg'=>getRequerido());
          $a['success'] = false;
        }
        if ($a['success'] == true) {
          //Actualiza Viajes Tractores
          $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl ".
          "SET claveMovimiento = 'VV' ".
          "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
          $rs = fn_ejecuta_query($sqlUpdViajeStr);

          //Actualiza trtalonesViajestbl
          $sqlUpdTalonStr = "UPDATE trTalonesViajesTbl ".
          "SET claveMovimiento = 'TP', ".
          "tipoDocumento = 'TP', ".
          "tipoTalon = 'NA' ".  
          "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
          $rs = fn_ejecuta_query($sqlUpdTalonStr);

          //Actualiza trgastosviajetractortbl
          if ($_REQUEST['cancelacionAsignacionViajesFoliosHdn'] != "|") {
            $folio = explode('|', substr($_REQUEST['cancelacionAsignacionViajesFoliosHdn'], 0, -1));
            for($i = 0; $i<sizeof($folio);$i++) {
              $sqlUpdGastosViajeFolioStr = "UPDATE trgastosviajetractortbl ".
              "SET claveMovimiento = 'GX' ".
              "WHERE folio = ".$folio[$i];
              $rs = fn_ejecuta_query($sqlUpdGastosViajeFolioStr);
            }
          }


          if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
            $a['sql'] = $sqlAddEsperaChoferStr;
          } else {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddEsperaChoferStr;
          }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);

      }

        function getCancelacionGastosFolio(){
        $a = array();
        $e = array();
        $a['success'] = true;
        

        if($_REQUEST['trCancelacionGastosFoliosHdn'] == ""){
          $e[] = array('id'=>'trCancelacionGastosFoliosHdn','msg'=>getRequerido());
          $a['success'] = false;
        }
        if ($a['success'] == true) {

          //Actualiza trgastosviajetractortbl
                $sqlUpdGastosViajeFolioStr = "UPDATE trgastosviajetractortbl ".
                "SET claveMovimiento = 'GX' ".
                "WHERE folio = ".$_REQUEST['trCancelacionGastosFoliosHdn'];
          $rs = fn_ejecuta_query($sqlUpdGastosViajeFolioStr);


          if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
            $a['sql'] = $sqlAddEsperaChoferStr;
          } else {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddEsperaChoferStr;
          }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);

      }
?>    