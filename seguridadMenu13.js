/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.seguridadMenu13', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Seguridad',
            iconCls: 'segMenuIco',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Usuarios',
									iconCls:'segMenuIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('segUsuariosWin');
										win = module.createWindow_segUsuariosWin();
									},
									scope: this,
									windowId: 'segUsuariosWin'
								},
								{
									text: 'Cambio Contraseņa',
									iconCls:'segMenuIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('segUsuariosPasswordVw');
										win = module.createWindow_segUsuariosPasswordVw();
									},
									scope: this,
									windowId: 'segUsuariosPasswordVw'
								},
								{
									text: 'Modulos por Usuario',
									iconCls:'segMenuIco',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('segUsuariosModulosWin');
										win = module.createWindow_segUsuariosModulosWin();
									},
									scope: this,
									windowId: 'segUsuariosModulosWin'
								}
				]
            }
        };
    }
});