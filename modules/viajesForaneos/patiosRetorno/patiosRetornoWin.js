
Ext.define('MyDesktop.modules.viajesForaneos.patiosRetorno.patiosRetornoWin', {    
    extend: 'Ext.ux.desktop.Module',

    id:'patiosRetornoWin',
    

    createWindow_patiosRetornoWin : function(){
        var desktop = this.app.getDesktop();
        var win_patiosRetornoWin = desktop.getWindow('patiosRetornoWin');
        if(!win_patiosRetornoWin){

            var store = new MyDesktop.modules.viajesForaneos.patiosRetorno.bloqueoTractorStr();
            var store = new MyDesktop.modules.viajesForaneos.patiosRetorno.companiaTractorStr();
            var store = new MyDesktop.modules.viajesForaneos.patiosRetorno.patiosCentroDistribucion();
            var store = new MyDesktop.modules.viajesForaneos.patiosRetorno.tractorViajeStr();
            
            var me=this;
            

            win_patiosRetornoWin = desktop.createWindow({ 
                autoShow: true,
                height: 385,
                id: 'patiosRetornoWin',
                width: 518,
                title: 'Captura Patios Retorno',
                dockedItems: [
                    {
                        xtype: 'toolbar',
                        dock: 'top',
                        height: 35,
                        items: [
                            {
                                xtype: 'button',
                                handler: function(button, e) {
                                    console.log(Ext.getCmp('patiosPatioRetornoCmb').getValue());


                                    if(Ext.getCmp('patiosCompaniaCmb').getValue() !== null && Ext.getCmp('patiosTractorCmb').getValue()!==null  && Ext.getCmp('patiosPatioRetornoCmb').getValue()!==null && Ext.getCmp('patiosRutaCmb').getValue()!==null) {
                                        console.log(Ext.getCmp('patiosPatioRetornoCmb').getValue());
                                        Ext.Ajax.request({
                                            url: '../../carbookBck/json/trviajestractores.php?',
                                            params: {
                                                trViajesTractoresActionHdn:'guardaPatioRetorno',
                                                tractor:Ext.getCmp('patiosTractorCmb').getValue(),
                                                centroDistribucion:Ext.getCmp('patiosPatioRetornoCmb').getValue(),
                                                ruta:Ext.getCmp('patiosRutaCmb').getValue()

                                            },
                                            success: function(response){
                                                /* Ext.getCmp('int343aUndsTxa').setValue();

                                                Ext.getCmp('InterfacesAceptarBtn').disable(true);
                                                Ext.getCmp('InterfacesCancelarBtn').disable(true);
                                                Ext.Msg.alert("Interfaz i343","Interfaz i343 generada Correctamente");
                                                //response = Ext.JSON.decode(response.responseText);
                                                if(response.records > 0){
                                                }
                                                Ext.Msg.alert(titulo, response.msjResponse);*/
                                            }
                                        });
                                        Ext.Msg.alert('Patio Retorno', 'Datos Actualizados Correctamente');
                                        Ext.getCmp('patiosCompaniaCmb').setValue('');
                                        Ext.getCmp('patiosTractorCmb').setValue('');
                                        Ext.getCmp('patiosPatioRetornoCmb').setValue('');
                                        Ext.getCmp('patiosRutaCmb').setValue('');

                                        Ext.getCmp('patioRetornoDatosFrm').getForm().reset();
                                    }
                                },
                                text: 'Aceptar'
                            },
                            {
                                xtype: 'tbseparator'
                            },
                            {
                                xtype: 'button',
                                handler: function(button, e) {
                                    Ext.getCmp('patioRetornoDatosFrm').getForm().reset();
                                },
                                text: 'Cancelar'
                            }
                        ]
                    }
                ],
                items: [
                    {
                        xtype: 'form',
                        height: 322,
                        id: 'patioRetornoDatosFrm',
                        layout: 'absolute',
                        bodyPadding: 10,
                        title: '',
                        items: [
                            {
                                xtype: 'combobox',
                                x: 20,
                                y: 20,
                                id: 'patiosCompaniaCmb',
                                width: 260,
                                fieldLabel: 'Compañia',
                                name: 'patiosCompaniaCmb',
                                fieldStyle: 'text-transform:uppercase',
                                displayField: 'descCiaTractor',
                                store: 'companiaTractorStr',
                                valueField: 'compania',
                                listeners: {
                                    select: {
                                        fn: me.onPatiosCompaniaCmbSelect,
                                        scope: me
                                    }
                                }
                            },
                            {
                                xtype: 'combobox',
                                x: 290,
                                y: 20,
                                id: 'patiosTractorCmb',
                                width: 180,
                                fieldLabel: 'Tractor',
                                labelWidth: 60,
                                name: 'patiosTractorCmb',
                                displayField: 'tractor',
                                store: 'tractorViajeStr',
                                valueField: 'idTractor',
                                listeners: {
                                    select: {
                                        fn: me.onPatiosTractorCmbSelect,
                                        scope: me
                                    }
                                }
                            },
                            {
                                xtype: 'combobox',
                                x: 20,
                                y: 70,
                                id: 'patiosPatioRetornoCmb',
                                width: 450,
                                fieldLabel: 'Patio Retorno',
                                name: 'patiosPatioRetornoCmb',
                                fieldStyle: 'text-transform:uppercase',
                                displayField: 'distDesc',
                                store: 'patiosCentroDistribucion',
                                valueField: 'distribuidorCentro'
                            },
                            {
                                xtype: 'textareafield',
                                x: 20,
                                y: 120,
                                height: 153,
                                id: 'patiosRutaCmb',
                                width: 430,
                                fieldLabel: 'Ruta Seguimiento',
                                name: 'patiosRutaCmb',
                                fieldStyle: 'text-transform:uppercase'
                            }
                        ]
                    }
                ]
            });

            
            win_patiosRetornoWin.show();
        return win_patiosRetornoWin;
        }
    },

    onPatiosCompaniaCmbSelect: function(combo, records, eOpts) {
        /*if(Ext.getCmp('patiosTractorCmb').getValue() && Ext.getCmp('patiosTractorCmb').getValue() !=''){
            //liberacion de tractor
            var store = Ext.data.StoreManager.lookup('trModificacionTalonesViajeBloquearLiberarTractorStr');

            store.load({
                params:{
                    catTractoresActionHdn:'liberarTractor',
                    catTractorIdTractorHdn:Ext.getCmp('trModificacionTalonesViajeTractorCmb').getValue()
                }
            });
        }

        Ext.getCmp('trModificacionTalonesViajeTractorCmb').enable(true);
        Ext.getCmp('trModificacionTalonesViajeTractorCmb').setValue('');

        //LIMPIA TODO ANTES DE CARGAR
        Ext.data.StoreManager.lookup('trModificacionTalonesViajeTalonesStr').removeAll();
        Ext.data.StoreManager.lookup('trModificacionTalonesViajeUnidadesStr').removeAll();
        Ext.getCmp('trModificacionTalonesViajeFechaDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeChoferDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeViajeDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeOrigenDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeDestinoDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeRendimientoDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeKmDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeDistribuidoresDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeTalonesDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeUnidadesDsp').setValue('');
        Ext.getCmp('trModificacionTalonesViajeTalonesAgregarBtn').disable(true);
        Ext.getCmp('trModificacionTalonesViajeTalonesEliminarBtn').disable(true);
        Ext.getCmp('trModificacionTalonesViajeUnidadesAgregarBtn').disable(true);
        Ext.getCmp('trModificacionTalonesViajeUnidadesEliminarBtn').disable(true);*/

        Ext.data.StoreManager.lookup('tractorViajeStr').load({
            params:{
                trViajesTractoresActionHdn:'getTractoresDisponiblesViaje',
                trViajesTractoresCompaniaHdn:combo.value,
                trViajesTractoresCentroDistribucionHdn:ciaSesVal,
                //trViajesTractoresCentroDistribucionHdn:'CDTOL',
                trViajesTractoresMovDispHdn: "'VC','VG','VE' "
            },
            callback: function(records, operation, success){
                if(records.length > 0) {
                    var record = records[0];

                    var idViajeTractor=record.get('idViajeTractor');

                    console.log(idViajeTractor);


                    //Ext.getCmp('trap446ChoferHdn').setValue(record.get('idViajeTractor'));



                } else {
                   // Ext.Msg.alert("Error", "Error al obtener la información del viaje");
                }
            }
        });
    },

    onPatiosTractorCmbSelect: function(combo, records, eOpts) {
        console.log(Ext.getCmp('patiosTractorCmb').getValue());

        ///Ext.getCmp('patiosTractorCmb').setValue(combo.value);

        /*var bloqueo = false;

        //Bloquear el tractor
        var bloqLibTractor = Ext.data.StoreManager.lookup('bloqueoTractorStr');
        bloqLibTractor.load({
            params:{
                catTractoresActionHdn:'bloquearTractor',
                catTractoresIdTractorHdn:Ext.getCmp('patiosTractorCmb').getValue,
                catTractoresModuloTxt:'PATIOS RETORNO'
            }, callback: function(records, operation, success){
                if(success){
                    Ext.getCmp('trap446IdTractorHdn').setValue(combo.value);

                    if(records[0]){

                        var lecCombo = Ext.data.StoreMgr.lookup('trModificacionTalonesViajeTractorStr');
                        var cuntsec = Math.floor('0');
                        var viajeTrac = "";
                        var estatusTrac = "";
                        var numTrac = Math.floor('0');
                        var claveChofer = "";

                        lecCombo.each(function(rec) {
                            //alert(rec.get('idTractor'));
                            if(rec.get('idTractor') == Ext.getCmp('trModificacionTalonesViajeTractorCmb').getValue()){
                                viajeTrac = rec.get('idViajeTractor');
                                estatusTrac = rec.get('claveMovimiento');
                                numTrac = rec.get('tractor');
                                claveChofer = rec.get('claveChofer');
                                Ext.getCmp('trap44EstatusViajeHdn').setValue(rec.get('claveMovimiento'));
                            }
                        });
                        if(numTrac > 40){
                            if(estatusTrac === "VC") { //SI ESTA VACIO
                                Ext.MessageBox.show({
                                    title:Ext.WindowMgr.get('trModificacionTalonesViajeWin').title,
                                    msg: 'Este viaje se encuentra vacío, ¿Desea agregar nuevos talones?',
                                    buttonText: {yes: "SI",no: "NO"},
                                    fn: function(btn){
                                        if(btn === 'yes')
                                        {
                                            Ext.getCmp('trModificacionTalonesViajeTalonesAgregarBtn').enable(true);

                                            Ext.data.StoreManager.lookup('trModificacionTalonesViajeViajeStr').load({
                                                params:{
                                                    trViajesTractoresActionHdn:'getViajes',
                                                    trViajesTractoresIdTractorHdn:Ext.getCmp('trModificacionTalonesViajeTractorCmb').getValue()
                                                },
                                                callback:function(records, operation, success){
                                                    if(records.length > 0){
                                                        var record = records[0];
                                                        Ext.getCmp('trModificacionTalonesViajeChoferDsp').setValue(record.data.nombreChofer);
                                                        Ext.getCmp('trModificacionTalonesViajeViajeDsp').setValue(record.data.viaje);
                                                        Ext.getCmp('trModificacionTalonesViajeOrigenDsp').setValue(record.data.descPlazaOrigen);
                                                        Ext.getCmp('trModificacionTalonesViajeDestinoDsp').setValue(record.data.descPlazaDestino);
                                                        Ext.getCmp('trModificacionTalonesViajeRendimientoDsp').setValue(record.data.rendimiento);
                                                        Ext.getCmp('trModificacionTalonesViajeKmDsp').setValue(record.data.kilometrosTabulados);
                                                        Ext.getCmp('trModificacionTalonesViajeDistribuidoresDsp').setValue('0');
                                                        Ext.getCmp('trModificacionTalonesViajeTalonesDsp').setValue('0');
                                                        Ext.getCmp('trModificacionTalonesViajeUnidadesDsp').setValue('0');
                                                        Ext.getCmp('trModificacionTalonesViajeFechaDsp').setValue(record.data.fechaActual);
                                                        Ext.getCmp('trap446CentroDistHdn').setValue(record.data.centroDistribucion);
                                                        Ext.getCmp('trap446ChoferHdn').setValue(record.data.claveChofer);
                                                        Ext.getCmp('trap446IdViajeHdn').setValue(record.data.idViajeTractor);
                                                    } else {
                                                        Ext.Msg.alert("Error", "Error al obtener la información del viaje");
                                                    }
                                                }
                                            });

                                        } else {
                                            //Se libera porque no se usara
                                            bloqLibTractor.load({
                                                params:{
                                                    catTractoresActionHdn:'liberarTractor',
                                                    catTractorIdTractorHdn:combo.value
                                                }
                                            });
                                            Ext.getCmp('trModificacionTalonesViajeTractorCmb').reset();
                                        }
                                    }
                                });
                            } else if(estatusTrac === "VE"){ //SI ESTA ENTREGADO -CERRADO-
                                Ext.MessageBox.show({
                                    title:Ext.WindowMgr.get('trModificacionTalonesViajeWin').title,
                                    msg: 'Este viaje se encuentra cerrado, ¿Desea abrirlo?',
                                    buttonText: {yes: "SI",no: "NO"},
                                    fn: function(btn){
                                        if(btn === 'yes'){
                                            Ext.getCmp('trModificacionTalonesViajeTalonesAgregarBtn').enable(true);

                                            Ext.data.StoreManager.lookup('trModificacionTalonesViajeViajeStr').load({
                                                params:{
                                                    trViajesTractoresActionHdn:'getViajes',
                                                    trViajesTractoresIdTractorHdn:Ext.getCmp('trModificacionTalonesViajeTractorCmb').getValue()
                                                },
                                                callback: function(records, operation, success){
                                                    if(records.length > 0)
                                                    {
                                                        var record = records[0];

                                                        Ext.getCmp('trModificacionTalonesViajeChoferDsp').setValue(record.get('nombreChofer'));
                                                        Ext.getCmp('trModificacionTalonesViajeViajeDsp').setValue(record.get('idViajeTractor'));
                                                        Ext.getCmp('trModificacionTalonesViajeOrigenDsp').setValue(record.get('descPlazaOrigen'));
                                                        Ext.getCmp('trModificacionTalonesViajeDestinoDsp').setValue(record.get('descPlazaDestino'));
                                                        Ext.getCmp('trModificacionTalonesViajeRendimientoDsp').setValue(record.get('rendimiento'));
                                                        Ext.getCmp('trModificacionTalonesViajeKmDsp').setValue(record.get('kilometrosTabulados'));
                                                        Ext.getCmp('trModificacionTalonesViajeDistribuidoresDsp').setValue(record.get('numeroDistribuidores'));
                                                        Ext.getCmp('trModificacionTalonesViajeTalonesDsp').setValue(record.get('numeroTalones'));
                                                        Ext.getCmp('trModificacionTalonesViajeUnidadesDsp').setValue(record.get('numeroUnidades'));
                                                        Ext.getCmp('trModificacionTalonesViajeFechaDsp').setValue(record.get('fechaActual'));
                                                        Ext.getCmp('trap446CentroDistHdn').setValue(record.get('centroDistribucion'));
                                                        Ext.getCmp('trap446ChoferHdn').setValue(record.get('claveChofer'));

                                                        var store = Ext.data.StoreManager.lookup('trModificacionTalonesViajeTalonesStr');
                                                        store.load({
                                                            params:{
                                                                trap446IdViajeHdn:Ext.getCmp('trModificacionTalonesViajeViajeDsp').getValue()
                                                            }
                                                        });

                                                    } else {
                                                        Ext.Msg.alert("Error", "Error al obtener la información del viaje");
                                                    }
                                                }
                                            });
                                        } else {
                                            //Se libera el tractor porque no se usara
                                            bloqLibTractor.load({
                                                params:{
                                                    catTractoresActionHdn:'liberarTractor',
                                                    catTractorIdTractorHdn:combo.value
                                                }
                                            });
                                            Ext.getCmp('trModificacionTalonesViajeTractorCmb').reset();
                                        }
                                    }
                                });

                            } else if(estatusTrac === "VF"){
                                Ext.getCmp('trModificacionTalonesViajeTalonesAgregarBtn').enable(true);
                                Ext.data.StoreManager.lookup('trModificacionTalonesViajeViajeStr').load({
                                    params:{
                                        trViajesTractoresActionHdn:'getViajes',
                                        trViajesTractoresIdTractorHdn:Ext.getCmp('trModificacionTalonesViajeTractorCmb').getValue()
                                    },
                                    callback: function(records, operation, success){
                                        if(records.length > 0) {
                                            var record = records[0];

                                            Ext.getCmp('trModificacionTalonesViajeChoferDsp').setValue(record.get('nombreChofer'));
                                            Ext.getCmp('trModificacionTalonesViajeViajeDsp').setValue(record.get('idViajeTractor'));
                                            Ext.getCmp('trModificacionTalonesViajeOrigenDsp').setValue(record.get('descPlazaOrigen'));
                                            Ext.getCmp('trModificacionTalonesViajeDestinoDsp').setValue(record.get('descPlazaDestino'));
                                            Ext.getCmp('trModificacionTalonesViajeRendimientoDsp').setValue(record.get('rendimiento'));
                                            Ext.getCmp('trModificacionTalonesViajeKmDsp').setValue(record.get('kilometrosTabulados'));
                                            Ext.getCmp('trModificacionTalonesViajeDistribuidoresDsp').setValue(record.get('numeroDistribuidores'));
                                            Ext.getCmp('trModificacionTalonesViajeTalonesDsp').setValue(record.get('numeroTalones'));
                                            Ext.getCmp('trModificacionTalonesViajeUnidadesDsp').setValue(record.get('numeroUnidades'));
                                            Ext.getCmp('trModificacionTalonesViajeFechaDsp').setValue(record.get('fechaActual'));
                                            Ext.getCmp('trap446CentroDistHdn').setValue(record.get('centroDistribucion'));
                                            Ext.getCmp('trap446ChoferHdn').setValue(record.get('claveChofer'));

                                            Ext.data.StoreManager.lookup('trModificacionTalonesViajeTalonesStr').load({
                                                params:{
                                                    trap446IdViajeHdn:Ext.getCmp('trModificacionTalonesViajeViajeDsp').getValue()
                                                }
                                            });

                                        } else {
                                            Ext.Msg.alert("Error", "Error al obtener la información del viaje");
                                        }
                                    }
                                });
                            } else {
                                //Se libera el tractor porque no se usara
                                bloqLibTractor.load({
                                    params:{
                                        catTractoresActionHdn:'liberarTractor',
                                        catTractorIdTractorHdn:combo.value
                                    }
                                });
                                Ext.Msg.alert('TRAP446 - Modificación de Talones Viaje','No se puede modificar el viaje.');
                                Ext.getCmp('trModificacionTalonesViajeTractorCmb').setValue('');
                            }
                        } else {
                            if(estatusTrac == 'VP' || estatusTrac == 'VX'){
                                Ext.getCmp('trap446EsPlataformaHdn').setValue(numTrac);
                                //SE LE VA A CREAR UN VIAJE TEMPORAL
                                Ext.Msg.alert(Ext.WindowMgr.get('trModificacionTalonesViajeWin').title, 'Se le creará un viaje nuevo a este tractor');
                                Ext.getCmp('trModificacionTalonesViajeTalonesAgregarBtn').enable(true);
                                Ext.data.StoreManager.lookup('trModificacionTalonesViajeViajeStr').load({
                                    params:{
                                        trViajesTractoresActionHdn:'getViajes',
                                        trViajesTractoresIdTractorHdn:Ext.getCmp('trModificacionTalonesViajeTractorCmb').getValue()
                                    },
                                    callback: function(records, operation, success){
                                        if(records.length > 0)
                                        {
                                            var record = records[0];

                                            Ext.getCmp('trModificacionTalonesViajeChoferDsp').setValue(record.get('nombreChofer'));
                                            Ext.getCmp('trModificacionTalonesViajeOrigenDsp').setValue(record.get('descPlazaOrigen'));
                                            Ext.getCmp('trModificacionTalonesViajeRendimientoDsp').setValue(record.get('rendimiento'));
                                            Ext.getCmp('trModificacionTalonesViajeKmDsp').setValue('0.00');
                                            Ext.getCmp('trModificacionTalonesViajeDistribuidoresDsp').setValue('0');
                                            Ext.getCmp('trModificacionTalonesViajeTalonesDsp').setValue('0');
                                            Ext.getCmp('trModificacionTalonesViajeUnidadesDsp').setValue('0');
                                            Ext.getCmp('trModificacionTalonesViajeFechaDsp').setValue(record.get('fechaActual'));
                                            Ext.getCmp('trap446CentroDistHdn').setValue(record.get('centroDistribucion'));
                                            Ext.getCmp('trap446ChoferHdn').setValue(record.get('claveChofer'));

                                            Ext.data.StoreManager.lookup('trModificacionTalonesViajeCrearViajeStr').load({
                                                params:{
                                                    trViajesTractoresActionHdn:'addViajeTemp',
                                                    trViajesTractoresIdTractorHdn:combo.value,
                                                    trViajesTractoresClaveChoferHdn:Ext.getCmp('trap446ChoferHdn').getValue(),
                                                    trViajesTractoresCentroDistHdn:Ext.getCmp('trap446CentroDistHdn').getValue()
                                                },
                                                callback:function(records, operation, success){
                                                    Ext.data.StoreManager.lookup('trModificacionTalonesViajeViajeStr').load({
                                                        params:{
                                                            trViajesTractoresActionHdn:'getViajeTemp',
                                                            trViajesTractoresIdTractorHdn:combo.value
                                                        },
                                                        callback: function(records, operation, success){
                                                            var record = records[0];

                                                            Ext.getCmp('trModificacionTalonesViajeViajeDsp').setValue(record.get('idViajeTractor'));
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                //SE PUEDEN AGREGAR LOS TALONES QUE SEAN
                                Ext.getCmp('trModificacionTalonesViajeTalonesAgregarBtn').enable(true);
                                Ext.data.StoreManager.lookup('trModificacionTalonesViajeViajeStr').load({
                                    params:{
                                        trViajesTractoresActionHdn:'getViajes',
                                        trViajesTractoresIdTractorHdn:Ext.getCmp('trModificacionTalonesViajeTractorCmb').getValue()
                                    },
                                    callback: function(records, operation, success){
                                        if(records.length > 0)
                                        {
                                            var record = records[0];

                                            Ext.getCmp('trModificacionTalonesViajeChoferDsp').setValue(record.get('nombreChofer'));
                                            Ext.getCmp('trModificacionTalonesViajeViajeDsp').setValue(record.get('idViajeTractor'));
                                            Ext.getCmp('trModificacionTalonesViajeOrigenDsp').setValue(record.get('descPlazaOrigen'));
                                            Ext.getCmp('trModificacionTalonesViajeDestinoDsp').setValue(record.get('descPlazaDestino'));
                                            Ext.getCmp('trModificacionTalonesViajeRendimientoDsp').setValue(record.get('rendimiento'));
                                            Ext.getCmp('trModificacionTalonesViajeKmDsp').setValue(record.get('kilometrosTabulados'));
                                            Ext.getCmp('trModificacionTalonesViajeDistribuidoresDsp').setValue(record.get('numeroDistribuidores'));
                                            Ext.getCmp('trModificacionTalonesViajeTalonesDsp').setValue(record.get('numeroTalones'));
                                            Ext.getCmp('trModificacionTalonesViajeUnidadesDsp').setValue(record.get('numeroUnidades'));
                                            Ext.getCmp('trModificacionTalonesViajeFechaDsp').setValue(record.get('fechaActual'));
                                            Ext.getCmp('trap446CentroDistHdn').setValue(record.get('centroDistribucion'));
                                            Ext.getCmp('trap446ChoferHdn').setValue(record.get('claveChofer'));

                                            Ext.data.StoreManager.lookup('trModificacionTalonesViajeTalonesStr').load({
                                                params:{
                                                    trap446IdViajeHdn:Ext.getCmp('trModificacionTalonesViajeViajeDsp').getValue()
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    }
                } else {
                    Ext.Msg.alert(Ext.WindowMgr.get('patiosRegresoWin').title, operation.getError());
                    Ext.getCmp('patiosTractorCmb').setValue('');
                    Ext.data.StoreManager.lookup('tractorViajeStr').load({
                        params:{
                            trViajesTractoresActionHdn:'getTractoresDisponiblesViaje',
                            trViajesTractoresCompaniaHdn:Ext.getCmp('patiosTractorCmb').getValue()
                        }
                    });
                }
            }
        });*/
    }

});