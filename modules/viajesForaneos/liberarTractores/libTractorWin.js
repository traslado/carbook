
Ext.define('MyDesktop.modules.viajesForaneos.liberarTractores.libTractorWin', {
    extend: 'Ext.ux.desktop.Module',
    
    id:'libTractorWin',

    /*init : function(){
        this.launcher = {
            text: rhTRelLabTittle,
            iconCls:'auCatColores'
        };
    },*/
    
    createWindow_libTractorWin : function(){
        var desktop = this.app.getDesktop();
        var win_libTractorWin = desktop.getWindow('libTractorWin');
        if(!win_libTractorWin){
            // Integrar aquí los Stores que ocupará la pantalla
        var store = new MyDesktop.modules.viajesForaneos.liberarTractores.libTractorCompaniaStr();
        var store = new MyDesktop.modules.viajesForaneos.liberarTractores.libTractorTractorStr();
        var store = new MyDesktop.modules.viajesForaneos.liberarTractores.libTractorTalonesStr();
        var store = new MyDesktop.modules.viajesForaneos.liberarTractores.trModificacionTalonesViajeEspecialesCompaniaTractorLibStr();
        var store = new MyDesktop.modules.viajesForaneos.liberarTractores.trModificacionTalonesViajeEspecialUnidadesLibStr();
        var store = new MyDesktop.modules.viajesForaneos.liberarTractores.trModificacionTalonesViajeOrigenEspecialesLibStr();
        var store = new MyDesktop.modules.viajesForaneos.liberarTractores.libTractorBloqueoTractorStr();
        var titleWin = 'Liberacion de Tractores';
            
            win_libTractorWin = desktop.createWindow({ 
                height: 428,
                hidden: false,
                id: 'libTractorWin',
                width: 560,
                maximizable:false,
                minimizable:true,
                resizable:false,
                iconCls:'tractorModule',
                title: 'Liberacion de Tractores',
                clavesNoValidas: {
                    VD: 'VIAJE EN LISTA DE ESPERA',
                    VA: 'VIAJE DE ACOMPAÑANTE',
                    VP: 'VIAJE CON SUELDO PAGADO',
                    VU: 'VIAJE ENTREGADO CON PÓLIZA',
                    VO: 'VIAJE EN PTO. TRANSFERENCIA',
                    VI: 'ESTATUS RECIBIDO OK EN DIST.',
                    VK: 'ESTATUS RECIBIDO OK EN PATIO',
                    VR: 'ESTATUS RECIBIDO NO OK EN DIST.',
                    VT: 'ESTATUS RECIBIDO NO OK EN PATIO',
                    VV: 'PRE-VIAJE',
                    VX: 'VIAJE CANCELADO',
                    VU: 'VIAJE COMPROBADO',
                    VP: 'VIAJE PAGADO',
                    //VC: 'VIAJE VACÍO',
                    VF: 'VIAJE ASIGNADO',
                    VG: 'VIAJE CON GASTOS'//'VE':'VIAJE ENTREGADO',
                },
                items: [
                    {
                        xtype: 'form',
                        height: 361,
                        id: 'libTractorFrm',
                        defaults: {
                            blankText: 'Campo Requerido'
                        },
                        layout: 'absolute',
                        bodyPadding: 10,
                        title: '',
                        url: '../../carbookBck/json/trViajesTractores.php?trViajesTractoresActionHdn=liberarViaje',
                        items: [
                            {
                                xtype: 'combobox',
                                x: 10,
                                y: 10,
                                id: 'libTractorCompaniasCmb',
                                width: 340,
                                fieldLabel: 'Com. Tractor',
                                labelWidth: 80,
                                msgTarget: 'side',
                                name: 'libTractorCompaniasCmb',
                                fieldStyle: 'text-transform:uppercase',
                                allowBlank: false,
                                allowOnlyWhitespace: false,
                                enableKeyEvents: true,
                                maskRe: /[0-9A-Za-zÑñ\s-]/,
                                displayField: 'descCiaTractor',
                                forceSelection: true,
                                queryMode: 'local',
                                store: 'libTractorCompaniaStr',
                                valueField: 'compania',
                                listeners: {
                                    select: {
                                        fn: function(combo, records, eOpts) {
                                        
                                        var idTractor = Ext.getCmp('libTractorTractorCmb').getValue();
                                        var bloqueoStr = Ext.StoreMgr.get('libTractorBloqueoTractorStr');
                                        if (idTractor){ //Desbloquea el Tractor
                                        bloqueoStr.load({
                                            params:{
                                                catTractoresActionHdn: 'liberarTractor',
                                                catTractorIdTractorHdn: idTractor,
                                            },
                                            callback: function(records, operation, success) {
                                                if (!operation.wasSuccessful()) {
                                                    console.log('No se pudo liberar el Tractor');
                                                        }
                                                    }
                                                });
                                            }
                                            var tractorCmb = Ext.getCmp('libTractorTractorCmb');
                                            tractorCmb.reset();
                                            Ext.getCmp('libTractorViajeTxt').setValue('').setReadOnly(false);
                                            Ext.getCmp('libTractorTalonesGrd').getStore().removeAll();
                                            Ext.getCmp('libTractorTractorCmb').enable(true);
                                            tractorCmb.store.load({
                                                params:{
                                                    trViajesTractoresActionHdn: 'getTractoresDisponibles',
                                                    trViajesTractoresCompaniaHdn:combo.getValue(),
                                                    trViajesTractoresCentroDistribucionHdn:ciaSesVal,                                                    
                                                    trViajesTractoresMovDispHdn: "'VU','VE','VC' "
                                                },
                                                callback: function(records, operation, success){
                                                    tractorCmb.setReadOnly(false);
                                                }
                                            });
                                        }
                                    }
                                }
                            },
                             {
                                xtype: 'hiddenfield',
                                x: 182,
                                y: 47,
                                id: 'libTractorTractorHdn',
                                fieldLabel: 'Label',
                                name: 'libTractorTractorHdn'
                            },
                            {
                                xtype: 'hiddenfield',
                                x: 182,
                                y: 47,
                                id: 'libIDTractorTractorHdn',
                                fieldLabel: 'Label',
                                name: 'libIDTractorTractorHdn'
                            },
                            {
                                xtype: 'combobox',
                                x: 360,
                                y: 10,
                                id: 'libTractorTractorCmb',
                                width: 180,
                                fieldLabel: 'Tractor',
                                labelWidth: 80,
                                msgTarget: 'side',
                                name: 'libTractorTractorCmb',
                                allowBlank: false,
                                allowOnlyWhitespace: false,
                                enableKeyEvents: true,
                                maskRe: /[0-9]/,
                                displayField: 'tractor',
                                queryMode: 'local',
                                store: 'libTractorTractorStr',
                                valueField: 'idTractor',
                                listeners: {
                                    select: {
                                        fn: function(combo, records, eOpts) {

                                                Ext.Ajax.request({
                                                url: '../../carbookBck/json/trViajesTractores.php',
                                                params: {
                                                    trViajesTractoresActionHdn:'solicitudesPendientes',
                                                    tractor:Ext.getCmp('libTractorTractorCmb').getValue()
                                                },
                                                    success: function(response){
                                                        response = Ext.JSON.decode(response.responseText);
                                                        if(response.records !== 0){

                                                            Ext.Msg.alert('Liberación Tractores', 'Tiene Solicitudes Sin Autorizar');
                                                            Ext.getCmp('libTractorCompaniasCmb').setValue('');
                                                            Ext.getCmp('libTractorTractorCmb').setValue('');                                                             
                                  
                                                        }else{
                                                            console.log(records[0]);
                                                            var claveMovimientoViaje = records[0].get('claveMovimiento');
                                                            var window = Ext.WindowManager.get('libTractorWin');
                                                            var bloqueo= records[0].get('bloqueado');
                                                            Ext.getCmp('libTractorIdViajeTractorHdn').setValue(records[0].get('idViajeTractor'));
                                                            Ext.getCmp('libIDTractorTractorHdn').setValue(records[0].get('idtractor'));


                                                            if (claveMovimientoViaje == 'VC')
                                                            {
                                                                Ext.Msg.alert(window.title, 'Este es un viaje vacio no tiene talones');
                                                                Ext.getCmp('libTractorTractorHdn').setValue(records[0].get('tractor'));
                                                                Ext.getCmp('libTractorViajeTxt').setValue(records[0].get('viaje'));
                                                                Ext.getCmp('libTractorIdViajeTractorHdn').setValue(records[0].get('idViajeTractor'));

                                                                if(bloqueo == 1){
                                                                                Ext.Msg.alert(window.title, 'El Tractor esta bloqueado', function(){
                                                                                    combo.reset();
                                                                                    Ext.getCmp('libTractorCancelarBtn').fireHandler();
                                                                                });
                                                                }else{
                                                                //Bloquea el Tractor
                                                                var bloqueoStr = Ext.StoreMgr.get('libTractorBloqueoTractorStr');
                                                                bloqueoStr.load({
                                                                    params:{
                                                                        catTractoresActionHdn: 'bloquearTractor',
                                                                        catTractoresIdTractorHdn: combo.getValue(),
                                                                        catTractoresModuloTxt: 'LIBERACIONTRACTOR'
                                                                    },
                                                                    callback: function(records, operation, success) {
                                                                        if (!operation.wasSuccessful()) {
                                                                            Ext.Msg.alert(window.title, 'No se pudo bloquear el tractor');
                                                                            combo.getStore().reload();
                                                                        }
                                                                        else
                                                                        {
                                                                            combo.disable(true);
                                                                        }

                                                                    }
                                                                });
                                                              }
                                                            }

                                                            //////////////clave acompañante

                                                            if (claveMovimientoViaje == 'VA')
                                                            {
                                                                Ext.Msg.alert(window.title, 'Este es un viaje con Acompañante');
                                                                Ext.getCmp('libTractorTractorHdn').setValue(records[0].get('tractor'));
                                                                Ext.getCmp('libTractorViajeTxt').setValue(records[0].get('viaje'));
                                                                Ext.getCmp('libTractorIdViajeTractorHdn').setValue(records[0].get('idViajeTractor'));

                                                                if(bloqueo == 1){
                                                                                Ext.Msg.alert(window.title, 'El Tractor esta bloqueado', function(){
                                                                                    combo.reset();
                                                                                    Ext.getCmp('libTractorCancelarBtn').fireHandler();
                                                                                });
                                                                }else{
                                                                //Bloquea el Tractor
                                                                var bloqueoStr = Ext.StoreMgr.get('libTractorBloqueoTractorStr');
                                                                bloqueoStr.load({
                                                                    params:{
                                                                        catTractoresActionHdn: 'bloquearTractor',
                                                                        catTractoresIdTractorHdn: combo.getValue(),
                                                                        catTractoresModuloTxt: 'LIBERACIONTRACTOR'
                                                                    },
                                                                    callback: function(records, operation, success) {
                                                                        if (!operation.wasSuccessful()) {
                                                                            Ext.Msg.alert(window.title, 'No se pudo bloquear el tractor');
                                                                            combo.getStore().reload();
                                                                        }
                                                                        else
                                                                        {
                                                                            combo.disable(true);
                                                                        }

                                                                    }
                                                                });
                                                              }
                                                            }
                                                            //
                                                            if(claveMovimientoViaje == 'VE')
                                                            {

                                                                    var talonesStr = Ext.getCmp('libTractorTalonesGrd').getStore();
                                                                    talonesStr.load({
                                                                        params: {
                                                                            trViajesTractoresActionHdn:'getTalonesViaje',
                                                                            trViajesTractoresIdTractorHdn:combo.getValue(),
                                                                            trViajesTractoresIdViajeHdn: Ext.getCmp('libTractorIdViajeTractorHdn').getValue(),
                                                                            trViajesTractoresClaveMovimientoHdn:'!=TX',
                                                                            trViajesTractoresClaveMovViajeHdn:'VE'
                                                                        },
                                                                        callback: function(records, operation, success) {

                                                                                if(records[0]) {
                                                                                        if(bloqueo == 1){
                                                                                        Ext.Msg.alert(window.title, 'El Tractor esta bloqueado', function(){
                                                                                        combo.reset();
                                                                                            Ext.getCmp('libTractorCancelarBtn').fireHandler();
                                                                                        });
                                                                                        }else{
                                                                                        //Bloquea el Tractor
                                                                                        var bloqueoStr = Ext.StoreMgr.get('libTractorBloqueoTractorStr');
                                                                                        bloqueoStr.load({
                                                                                            params:{
                                                                                                catTractoresActionHdn: 'bloquearTractor',
                                                                                                catTractoresIdTractorHdn: combo.getValue(),
                                                                                                catTractoresModuloTxt: 'LIBERACIONTRACTOR'
                                                                                            },
                                                                                            callback: function(records, operation, success) {
                                                                                                if (!operation.wasSuccessful()) {
                                                                                                    Ext.Msg.alert(window.title, 'No se pudo bloquear el tractor');
                                                                                                    combo.getStore().reload();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    combo.disable(true);
                                                                                                }

                                                                                            }
                                                                                        });
                                                                                      }
                                                                                    Ext.getCmp('libTractorTractorHdn').setValue(records[0].get('tractor'));
                                                                                    Ext.getCmp('libTractorViajeTxt').setValue(records[0].get('viaje'));
                                                                                    Ext.getCmp('libTractorIdViajeTractorHdn').setValue(records[0].get('idViajeTractor'));
                                                                                }else {
                                                                                    Ext.Msg.alert(window.title, 'El Tractor no tiene Talones.');
                                                                                    combo.reset();
                                                                                    this.removeAll();
                                                                                }

                                                                        }
                                                                    });
                                                            }
                                                            if(claveMovimientoViaje == 'VU')
                                                            {

                                                                    var talonesStr = Ext.getCmp('libTractorTalonesGrd').getStore();
                                                                    talonesStr.load({
                                                                        params: {
                                                                            trViajesTractoresActionHdn:'getTalonesViaje',
                                                                            trViajesTractoresIdTractorHdn:combo.getValue(),
                                                                            trViajesTractoresIdViajeHdn: Ext.getCmp('libTractorIdViajeTractorHdn').getValue(),
                                                                            trViajesTractoresClaveMovimientoHdn:'!=TX',
                                                                            trViajesTractoresClaveMovViajeHdn:'VU'
                                                                        },
                                                                        callback: function(records, operation, success) {

                                                                                if(records[0]) {
                                                                                        if(bloqueo == 1){
                                                                                        Ext.Msg.alert(window.title, 'El Tractor esta bloqueado', function(){
                                                                                        combo.reset();
                                                                                            Ext.getCmp('libTractorCancelarBtn').fireHandler();
                                                                                        });
                                                                                        }else{
                                                                                        //Bloquea el Tractor
                                                                                        var bloqueoStr = Ext.StoreMgr.get('libTractorBloqueoTractorStr');
                                                                                        bloqueoStr.load({
                                                                                            params:{
                                                                                                catTractoresActionHdn: 'bloquearTractor',
                                                                                                catTractoresIdTractorHdn: combo.getValue(),
                                                                                                catTractoresModuloTxt: 'LIBERACIONTRACTOR'
                                                                                            },
                                                                                            callback: function(records, operation, success) {
                                                                                                if (!operation.wasSuccessful()) {
                                                                                                    Ext.Msg.alert(window.title, 'No se pudo bloquear el tractor');
                                                                                                    combo.getStore().reload();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    combo.disable(true);
                                                                                                }

                                                                                            }
                                                                                        });
                                                                                      }
                                                                                    Ext.getCmp('libTractorTractorHdn').setValue(records[0].get('tractor'));
                                                                                    Ext.getCmp('libTractorViajeTxt').setValue(records[0].get('viaje'));
                                                                                    Ext.getCmp('libTractorIdViajeTractorHdn').setValue(records[0].get('idViajeTractor'));
                                                                                }else {
                                                                                    Ext.Msg.alert(window.title, 'El Tractor no tiene Talones.');
                                                                                    combo.reset();
                                                                                    this.removeAll();
                                                                                }

                                                                        }
                                                                    });
                                                            }
                 
                                                        }
                                                        
                                                    }
                                                });

                                             
                                          }
                                        
                                    }
                                }
                            },
                            {
                                xtype: 'textfield',
                                x: 360,
                                y: 50,
                                id: 'libTractorViajeTxt',
                                width: 180,
                                fieldLabel: 'Viaje',
                                labelWidth: 80,
                                name: 'libTractorViajeTxt',
                                disabled:true,
                                readOnly: true,
                                enableKeyEvents: true,
                                enforceMaxLength: true,
                                maskRe: /[0-9]/,
                                maxLength: 10

                            },
                            {
                              xtype: 'button',
                              x: 50,
                                y: 50,
                                handler: function(button, e) {
                                    console.log(Ext.getCmp('libTractorTractorCmb').getValue());
                                Ext.Ajax.request({
                                    url: '../../carbookBck/json/trViajesTractores.php',
                                    params: {
                                        trViajesTractoresActionHdn:'obtenerOperador',
                                        tractor:Ext.getCmp('libTractorTractorCmb').getValue()
                                    },
                                    success: function(response){
                                        response = Ext.JSON.decode(response.responseText);
                                        if(response.records !== 0){
                                            Ext.getCmp('operadorDsp').setValue(response.root[0].nombre);
                                            Ext.getCmp('viajeDsp').setValue(response.root[0].viaje);
                                            Ext.getCmp('origenDsp').setValue(response.root[0].centroDistribucion);
                                            Ext.getCmp('destinoDsp').setValue(response.root[0].destino);

                                            console.log(Ext.getCmp('viajeDsp').getValue());
                                            //Ext.getCmp('destinoLbl').setValue(response.root[0].destino);
                                            //Ext.getCmp('rutaAutorizadaLbl').setValue(response.root[0].ruta);

                                        }
                                        //if(response.records !== 0){
                                       /* if(response.root[0].listaAsignar != '1'){
                                            Ext.Msg.alert('Unidad No Lista','Unidad No Lista Para Asignar');
                                            //  component.oldValue = null;

                                            var selection2 = Ext.getCmp('modPreviajeUnidadesGrd').getView().getSelectionModel().getSelection()[0];

                                            Ext.getCmp('modPreviajeUnidadesGrd').store.remove(selection2);
                                        }*/
                                    }
                                });
                                console.log('entraria aqui');
                                ////////////////////////////pantalla para revision de factruras precargadas
                                Ext.data.StoreManager.lookup('trModificacionTalonesViajeOrigenEspecialesLibStr').load({
                                callback: function(records, operation, success){
                                    Ext.create('Ext.window.Window', {
                                id: 'trVerificacionFacturasWin',
                                title: 'Verificacion Facturas',
                                height: 400,
                                width: 700,
                                modal:true,
                                resizable: false,
                                layout: 'fit',
                                closable: true,
                                tools: [{
                                    xtype: 'tool',
                                    handler: function(event, toolEl, owner, tool) {
                                        Ext.Ajax.request({
                                        url: '../../carbookBck/json/trViajesTractores.php',
                                        params: {
                                            trViajesTractoresActionHdn:'obtenerFacturasAutorizadas',
                                            idViajeTractorHdn:Ext.getCmp('libTractorIdViajeTractorHdn').getValue()
                                        },
                                        success: function(response){
                                            response = Ext.JSON.decode(response.responseText);
                                            if(response.records !== 0){
                                                console.log(response);
                                                console.log(response.root[0].nombre);
                                                Ext.getCmp('operadorDsp').setValue(response.root[0].nombre);
                                                Ext.getCmp('viajeDsp').setValue(response.root[0].viaje);
                                                Ext.getCmp('origenDsp').setValue(response.root[0].centroDistribucion);
                                                Ext.getCmp('destinoDsp').setValue(response.root[0].destino);

                                                console.log(Ext.getCmp('viajeDsp').getValue());
                                                //Ext.getCmp('destinoLbl').setValue(response.root[0].destino);
                                                //Ext.getCmp('rutaAutorizadaLbl').setValue(response.root[0].ruta);

                                            }
                                            //if(response.records !== 0){
                                           /* if(response.root[0].listaAsignar != '1'){
                                                Ext.Msg.alert('Unidad No Lista','Unidad No Lista Para Asignar');
                                                //  component.oldValue = null;

                                                var selection2 = Ext.getCmp('modPreviajeUnidadesGrd').getView().getSelectionModel().getSelection()[0];

                                                Ext.getCmp('modPreviajeUnidadesGrd').store.remove(selection2);
                                            }*/
                                        }
                                    });
                                    },
                                    type: 'refresh'
                                }],
                                dockedItems: [
                                    {
                                        xtype: 'form',
                                        id: 'trVerificacionFacturasFrm',
                                        layout: 'absolute',
                                        bodyPadding: 10,
                                        title: '',
                                        url: '../../carbookBck/json/trViajesTractores.php',
                                        items: [
                                            {
                                                xtype: 'combo',
                                                id: 'trModificacionTalonesViajeEspecialOrigenCmb',
                                                fieldLabel: 'Operador',
                                                labelWidth: 100,
                                                width: 420,
                                                x: 10,
                                                y: 20,
                                                allowBlank: false,
                                                hidden: true,
                                                forceSelection: true,
                                                queryMode: 'local',
                                                valueField: 'distribuidorCentro',
                                                displayField: 'distDesc',
                                                store: 'trModificacionTalonesViajeOrigenEspecialesLibStr',
                                                listeners: {
                                                    select: {
                                                        fn: function(combo, records, eOpts){
                                                            if(combo.value != null){                                                                                                                                                                                                                           
                                                                if(Ext.getCmp('trModificacionTalonesViajeEspecialCompaniaCmb').getValue() != null &&
                                                                   Ext.getCmp('trModificacionTalonesViajeEspecialFolioTxt').getValue() != ''){
                                                                    var store = Ext.data.StoreManager.lookup('trModificacionTalonesViajeEspecialUnidadesLibStr');
                                                                    store.load({
                                                                        params:{
                                                                            trViajesTractoresCentroDistHdn: combo.value,
                                                                            trViajesTractoresCompaniaRemitenteHdn:'TCO',////Ext.getCmp('trModificacionTalonesViajeEspecialCompaniaCmb').getValue(),
                                                                            trViajesTractoresFolioTxt:Ext.getCmp('trModificacionTalonesViajeEspecialFolioTxt').getValue(),                                                                            
                                                                            trViajesTractoresClaveMovimientoTalonHdn:'TO'
                                                                        },
                                                                        callback: function(records, operation, success){
                                                                            if(records.length <= 0){
                                                                                Ext.Msg.alert('Validacion Facturas', 'No existen Facturas Cargadas');
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            },                                            

                                            {
                                                xtype: 'displayfield',
                                                id: 'operadorDsp',
                                                fieldLabel: 'Operador',
                                                labelWidth: 100,
                                                width: 420,
                                                x: 10,
                                                y: 15,
                                                allowBlank: false,
                                                forceSelection: true,
                                                
                                            },
                                            {
                                                xtype: 'displayfield',
                                                id: 'viajeDsp',
                                                fieldLabel: 'Viaje',
                                                labelWidth: 100,
                                                width: 420,
                                                x: 10,
                                                y: 45,
                                                allowBlank: false,
                                                forceSelection: true,
                                                
                                            },
                                            {
                                                xtype: 'displayfield',
                                                id: 'origenDsp',
                                                fieldLabel: 'Origen',
                                                labelWidth: 100,
                                                width: 420,
                                                x: 10,
                                                y: 80,
                                                allowBlank: false,
                                                forceSelection: true,
                                                
                                            },
                                            {
                                                xtype: 'displayfield',
                                                id: 'destinoDsp',
                                                fieldLabel: 'Destino',
                                                labelWidth: 100,
                                                width: 420,
                                                x: 250,
                                                y: 80,
                                                allowBlank: false,
                                                forceSelection: true,
                                                listeners: {
                                                    change: {
                                                        fn: function(component, e, eOpts){
                                                            if(component.getValue() != '' && component.oldValue != component.getValue()){
                                                                if(Ext.getCmp('operadorDsp').getValue() != null &&
                                                                   Ext.getCmp('viajeDsp').getValue() != null){
                                                                    var store = Ext.data.StoreManager.lookup('trModificacionTalonesViajeEspecialUnidadesLibStr');
                                                                    Ext.Msg.wait("Espere validando ... ", 'Validacion Facturas');
                                                                    store.load({
                                                                        params:{
                                                                            trViajesTractoresCentroDistHdn: Ext.getCmp('trModificacionTalonesViajeEspecialOrigenCmb').getValue(),
                                                                            trViajesTractoresCompaniaRemitenteHdn:Ext.getCmp('trModificacionTalonesViajeEspecialCompaniaCmb').getValue(),
                                                                            trViajesTractoresFolioTxt:component.getValue(),
                                                                            idViajeTractorHdn:Ext.getCmp('libTractorIdViajeTractorHdn').getValue(),
                                                                            trViajesTractoresClaveMovimientoTalonHdn:'TO'
                                                                        },
                                                                        callback: function(records, operation, success){
                                                                            Ext.getCmp('trModificacionTalonesViajeEspecialSelectAllChk').setReadOnly(true);
                                                                            Ext.Msg.hide();
                                                                            if(records.length <= 0){
                                                                                Ext.Msg.alert('Validacion Facturas', 'No Existen Facturas Cargadas.');
                                                                            }
                                                                            else
                                                                            {
                                                                                    Ext.getCmp('trModificacionTalonesViajeEspecialSelectAllChk').reset();
                                                                                    Ext.getCmp('trModificacionTalonesViajeEspecialSelectAllChk').setReadOnly(false);                                                                                                                                                
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                            component.oldValue = component.getValue();
                                                        }
                                                    }
                                                }
                                                
                                            },
                                            {
                                                xtype: 'combo',
                                                id: 'trModificacionTalonesViajeEspecialCompaniaCmb',
                                                fieldLabel: 'Viaje',
                                                labelWidth: 100,
                                                width: 350,
                                                x: 10,
                                                y: 50,
                                                allowBlank: false,
                                                forceSelection: true,
                                                hidden: true,
                                                queryMode: 'local',
                                                valueField: 'compania',
                                                displayField: 'descCiaTractor',
                                                store: 'trModificacionTalonesViajeEspecialesCompaniaTractorLibStr',
                                                listeners: {
                                                    select: {
                                                        fn: function(combo, records, eOpts){
                                                            if(combo.value != null){
                                                                if(Ext.getCmp('trModificacionTalonesViajeEspecialOrigenCmb').getValue() != null &&
                                                                   Ext.getCmp('trModificacionTalonesViajeEspecialFolioTxt').getValue() != ''){
                                                                    var store = Ext.data.StoreManager.lookup('trModificacionTalonesViajeEspecialUnidadesLibStr');
                                                                    store.load({
                                                                        params:{
                                                                            trViajesTractoresCentroDistHdn: Ext.getCmp('trModificacionTalonesViajeEspecialOrigenCmb').getValue(),
                                                                            trViajesTractoresCompaniaRemitenteHdn:combo.value,
                                                                            trViajesTractoresFolioTxt:Ext.getCmp('trModificacionTalonesViajeEspecialFolioTxt').getValue(),
                                                                            trViajesTractoresClaveMovimientoTalonHdn:'TO'
                                                                        },
                                                                        callback: function(records, operation, success){
                                                                            if(records.length <= 0){
                                                                                Ext.Msg.alert('Validacion Facturas', 'No Existen Facturas Cargadas');
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'textfield',
                                                id: 'trModificacionTalonesViajeEspecialFolioTxt',
                                                x: 450,
                                                y: 8,
                                                allowBlank: false,
                                                oldValue: '',
                                                width: 180,
                                                fieldLabel: 'Tractor',
                                                hidden: true,
                                                enforceMaxLength: true,
                                                maskRe: /[0-9]/,
                                                maxLength: 9,
                                                listeners: {
                                                    blur: {
                                                        fn: function(component, e, eOpts){
                                                            if(component.getValue() != '' && component.oldValue != component.getValue()){
                                                                if(Ext.getCmp('operadorDsp').getValue() != null &&
                                                                   Ext.getCmp('viajeDsp').getValue() != null){
                                                                    var store = Ext.data.StoreManager.lookup('trModificacionTalonesViajeEspecialUnidadesLibStr');
                                                                    Ext.Msg.wait("Espere validando ... ", 'Validacion Facturas');
                                                                    store.load({
                                                                        params:{
                                                                            trViajesTractoresCentroDistHdn: Ext.getCmp('trModificacionTalonesViajeEspecialOrigenCmb').getValue(),
                                                                            trViajesTractoresCompaniaRemitenteHdn:Ext.getCmp('trModificacionTalonesViajeEspecialCompaniaCmb').getValue(),
                                                                            trViajesTractoresFolioTxt:component.getValue(),
                                                                            trViajesTractoresClaveMovimientoTalonHdn:'TO'
                                                                        },
                                                                        callback: function(records, operation, success){
                                                                            Ext.getCmp('trModificacionTalonesViajeEspecialSelectAllChk').setReadOnly(true);
                                                                            Ext.Msg.hide();
                                                                            if(records.length <= 0){
                                                                                Ext.Msg.alert('Validacion Facturas', 'No Existen Facturas Cargadas');
                                                                            }
                                                                            else
                                                                            {
                                                                                    Ext.getCmp('trModificacionTalonesViajeEspecialSelectAllChk').reset();
                                                                                    Ext.getCmp('trModificacionTalonesViajeEspecialSelectAllChk').setReadOnly(false);                                                                                                                                                
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                            component.oldValue = component.getValue();
                                                        }
                                                    }
                                                }
                                            },{
                                                xtype: 'gridpanel',
                                                x: -1,
                                                y: 120,
                                                height: 230,
                                                id: 'trModificacionTalonesViajeEspecialUnidadesGrd',
                                                width: 670,
                                                title: 'Unidades',
                                                store: 'trModificacionTalonesViajeEspecialUnidadesLibStr',
                                                /*
                                                plugins: [
                                                    Ext.create('Ext.grid.plugin.CellEditing', {
                                                        pluginId: 'trModificacionTalonesViajeEspecialCellEdt',
                                                        clicksToEdit: 1
                                                    })
                                                ],
                                                */
                                                                    dockedItems: [{
                                                                        xtype: 'toolbar',
                                                                        dock: 'top',
                                                                        border: false,
                                                                        layout: {
                                                                            type: 'hbox',
                                                                            pack: 'start'
                                                                        },
                                                                        items: [{
                                                                            xtype: 'checkboxfield',
                                                                            handler: function(checkbox, checked) {
                                                                            if (checked) {
                                                                                checkbox.getEl().down('label.x-form-cb-label').update('Si');
                                                                                Ext.getCmp('trModificacionTalonesViajeEspecialUnidadesGrd').getStore().each(function(rec) {
                                                                                    rec.set('aplicaValor', true);
                                                                                    //console.log('CHK',rec);
                                                                                });
                                                                            } else {
                                                                                checkbox.getEl().down('label.x-form-cb-label').update('No');
                                                                                Ext.getCmp('trModificacionTalonesViajeEspecialUnidadesGrd').getStore().each(function(rec) {
                                                                                    rec.set('aplicaValor', false);
                                                                                    //console.log('CHK',rec);
                                                                                });
                                                                            }
                                                                            },
                                                                            id: 'trModificacionTalonesViajeEspecialSelectAllChk',
                                                                            fieldLabel: 'Seleccionar Todo',
                                                                            name: 'seleccion',
                                                                            boxLabel: 'Si',
                                                                            checked: false,
                                                                            readOnly: true,
                                                                            inputValue: 'S',
                                                                            uncheckedValue: 'N'
                                                                        },
                                                            {
                                                                xtype: 'tbfill'
                                                            },
                                                            {
                                                                xtype: 'button',
                                                                id: 'trModificacionTalonesViajeEspecialAgregarBtn',
                                                                iconCls: 'ux-btnSave',
                                                                text: 'Guardar',
                                                                x: 173,
                                                                y: 250,
                                                                handler: function(){
                                                                    var form = Ext.getCmp('trVerificacionFacturasFrm').getForm();

                                                                    if(Ext.getCmp('destinoDsp').getValue() !=''){
                                                                        if(Ext.data.StoreManager.lookup('trModificacionTalonesViajeEspecialUnidadesLibStr').getCount() > 0){
                                                                            var store = Ext.data.StoreManager.lookup('trModificacionTalonesViajeEspecialUnidadesLibStr');
                                                                            var folioFiscal = '';
                                                                            var rfc = '';
                                                                            var fechaEvento = '';
                                                                            var concepto = '';
                                                                            store.each(function(record){
                                                                                if(record.get('aplicaValor')){
                                                                                    folioFiscal = folioFiscal + record.get('folioFiscal') + '|';
                                                                                    fechaEvento = fechaEvento + record.get('fechaEvento') + '|';
                                                                                    rfc = rfc + record.get('rfc') + '|';
                                                                                    concepto = concepto + record.get('concepto') + '|';
                                                                                    
                                                                                }
                                                                            });
                                                                            /*console.log(Ext.getCmp('trap446IdViajeHdn').getValue());
                                                                            console.log('simbolo',simbolo);
                                                                            console.log('vin',vin);
                                                                            console.log('color',color);*/
                                                                            console.log(folioFiscal);                                                                            

                                                                            Ext.Ajax.request({
                                                                                url: '../../carbookBck/json/trViajesTractores.php?',
                                                                                params: {
                                                                                folioFiscal:folioFiscal,
                                                                                trViajesTractoresActionHdn:'autorizarFacturasHdn',
                                                                                idViajeTractorHdn:Ext.getCmp('libTractorIdViajeTractorHdn').getValue(),
                                                                                centroDistribucion: ciaSesVal
                                                                            },
                                                                            success: function(response){
                                                                                response = Ext.JSON.decode(response.responseText); 
                                                                                console.log(response);                                                                               
                                                                                if (response.records > 0) {
                                                                                    Ext.getCmp('trModificacionTalonesViajeEspecialCancelarBtn').fireHandler();
                                                                                    Ext.Msg.alert('Verificacion Facturas','Procesado Correctamente.');
                                                                                    
                                                                                    /*Ext.data.StoreManager.lookup('trModificacionTalonesViajeEspecialUnidadesLibStr').removeAll();
                                                                                    this.up('trVerificacionFacturasWin').destroy();*/
                                                                                }
                                                                            }
                                                                        });
                                                                        } else {
                                                                            Ext.Msg.alert('Validacion Facturas', 'No hay unidades disponibles para este talón.');
                                                                        }
                                                                    } else {
                                                                        Ext.Msg.alert('Validacion Facturas', 'Faltan Campos Requeridos.');
                                                                    }
                                                                }
                                                            },          
                                                            {
                                                                xtype: 'button',
                                                                id: 'trModificacionTalonesViajeEspecialCancelarBtn',
                                                                iconCls: 'ux-btnCancel',
                                                                text: 'Cancelar',
                                                                x: 235,
                                                                y: 250,
                                                                handler: function(){
                                                                    Ext.data.StoreManager.lookup('trModificacionTalonesViajeEspecialUnidadesLibStr').removeAll();
                                                                    this.up('.window').destroy();
                                                                }
                                                            }                                                                                                                                                                                                                                                                                                       
                                                                                                                                ]
                                                                                                                            }],                                                                                                            
                                                columns: [
                                                    /*
                                                    {
                                                        xtype: 'gridcolumn',
                                                        renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                                            var rec = Ext.data.StoreManager
                                                            .lookup('trModificacionTalonesViajeAplicaStr').
                                                            findRecord('aplicaValor', value);
                                                            return rec.get('aplicaDesc');
                                                        },
                                                        width: 50,
                                                        dataIndex: 'aplicaValor',
                                                        text: 'Aplica',
                                                        editor: {
                                                            xtype: 'combobox',
                                                            id: 'trModificacionTalonesViajeTalonesAplicaCmb',
                                                            displayField: 'aplicaDesc',
                                                            valueField: 'aplicaValor',
                                                            forceSelection: true,
                                                            queryMode: 'local',
                                                            store: Ext.create('Ext.data.Store', {
                                                                id: 'trModificacionTalonesViajeAplicaStr',
                                                                fields: ['aplicaValor', 'aplicaDesc'],
                                                                data: [
                                                                    {'aplicaValor':'1', 'aplicaDesc':'Sí'},
                                                                    {'aplicaValor':'0', 'aplicaDesc':'No'}
                                                                ]
                                                            })
                                                        }
                                                    },
                                                    */
                                                        {
                                                            xtype: 'checkcolumn',
                                                                /*
                                                                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {                                                                                                                                                                                                          
                                                                },                                                                                                                                                                                              
                                                                */
                                                            width: 50,
                                                            dataIndex: 'aplicaValor',
                                                            listeners: {
                                                                checkchange: function(check, index, checked) {
                                                                    var record = Ext.data.StoreManager.lookup('trModificacionTalonesViajeEspecialUnidadesLibStr').getAt(index);
                                                                    console.log('CHK',check);
                                                                    console.log('Rec',record);
                                                                }
                                                            }
                                                        },
                                                    {
                                                        xtype: 'gridcolumn',
                                                        width: 130,
                                                        dataIndex: 'folioFiscal',
                                                        text: 'Folio Fiscal'
                                                    },                                                    
                                                    {
                                                        xtype: 'gridcolumn',
                                                        width: 150,
                                                        dataIndex: 'concepto',
                                                        text: 'Concepto'
                                                    },
                                                    {
                                                        xtype: 'gridcolumn',
                                                        width: 150,
                                                        dataIndex: 'total',
                                                        text: 'Total'
                                                    },
                                                    {
                                                        xtype: 'gridcolumn',
                                                        width: 150,
                                                        dataIndex: 'subTotal',
                                                        text: 'subTotal'
                                                    },
                                                    {
                                                        xtype: 'gridcolumn',
                                                        width: 150,
                                                        dataIndex: 'iva',
                                                        text: 'IVA'
                                                    },                                                    
                                                    {
                                                        xtype: 'gridcolumn',
                                                        width: 90,
                                                        dataIndex: 'fechaEvento',
                                                        text: 'Fecha Factura'
                                                    },
                                                    {
                                                        xtype: 'gridcolumn',
                                                        width: 150,
                                                        dataIndex: 'rfc',
                                                        text: 'RFC'
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    /*
                                    {
                                        xtype: 'toolbar',
                                        dock: 'bottom',
                                        layout: {
                                            type: 'hbox',
                                            pack: 'center'
                                        },
                                        items: [
                                        ]
                                    }
                                    */
                                ]
                            }).show();
console.log("valor de campo destino" + Ext.getCmp('destinoDsp').getValue());

                            if (Ext.getCmp('destinoDsp').getValue()=='') {
                                console.log("carga campo");
                                Ext.Ajax.request({
                                        url: '../../carbookBck/json/trViajesTractores.php',
                                        params: {
                                            trViajesTractoresActionHdn:'obtenerFacturasAutorizadas',
                                            idViajeTractorHdn:Ext.getCmp('libTractorIdViajeTractorHdn').getValue()
                                        },
                                        success: function(response){
                                            response = Ext.JSON.decode(response.responseText);
                                            if(response.records !== 0){
                                                console.log(response);
                                                console.log(response.root[0].nombre);
                                                Ext.getCmp('operadorDsp').setValue(response.root[0].nombre);
                                                Ext.getCmp('viajeDsp').setValue(response.root[0].viaje);
                                                Ext.getCmp('origenDsp').setValue(response.root[0].centroDistribucion);
                                                Ext.getCmp('destinoDsp').setValue(response.root[0].destino);

                                                console.log(Ext.getCmp('viajeDsp').getValue());
                                                //Ext.getCmp('destinoLbl').setValue(response.root[0].destino);
                                                //Ext.getCmp('rutaAutorizadaLbl').setValue(response.root[0].ruta);

                                            }
                                            //if(response.records !== 0){
                                           /* if(response.root[0].listaAsignar != '1'){
                                                Ext.Msg.alert('Unidad No Lista','Unidad No Lista Para Asignar');
                                                //  component.oldValue = null;

                                                var selection2 = Ext.getCmp('modPreviajeUnidadesGrd').getView().getSelectionModel().getSelection()[0];

                                                Ext.getCmp('modPreviajeUnidadesGrd').store.remove(selection2);
                                            }*/
                                        }
                                    });
                            }
                                }
                            });
//////////////////////////////////
                                },
                                id: 'libTractorVerificarBtn',
                                iconCls: 'ux-btnSave',
                                text: 'Verificar Facturas'
                            },
                            {
                                xtype: 'gridpanel',
                                x: 10,
                                y: 90,
                                height: 250,
                                id: 'libTractorTalonesGrd',
                                width: 530,
                                title: 'Talones',
                                store: 'libTractorTalonesStr',
                                columns: [
                                    {
                                        xtype: 'gridcolumn',
                                        dataIndex: 'folio',
                                        text: 'Folio'
                                    },
                                    {
                                        xtype: 'gridcolumn',
                                        dataIndex: 'numeroUnidades',
                                        text: '# Unidades'
                                    },
                                    {
                                        xtype: 'gridcolumn',
                                        width: 324,
                                        dataIndex: 'descDistribuidor',
                                        text: 'Distribuidor'
                                    }
                                ]
                            },
                            {
                                xtype: 'hiddenfield',
                                x: 182,
                                y: 47,
                                id: 'libTractorIdViajeTractorHdn',
                                fieldLabel: 'Label',
                                name: 'liberacionTractorIdViajeTractorHdn'
                            }
                        ]
                    }
                ],
                dockedItems: [
                    {
                        xtype: 'toolbar',
                        dock: 'top',
                        items: [
                            {
                                xtype: 'button',
                                handler: function(button, e) {
                                         /////validar facturas autorizadas
                                    Ext.Ajax.request({
                                                url: '../../carbookBck/json/trViajesTractores.php?',
                                                params: {
                                                trViajesTractoresActionHdn: 'validaFacturasOB7',
                                                idViajeTractor: Ext.getCmp('libTractorIdViajeTractorHdn').getValue(),
                                                ciasesval:ciaSesVal
                                            },
                                            success: function(response){
                                                response = Ext.JSON.decode(response.responseText);
                                                    Ext.Msg.hide();
                                                    if (response.records > 0) {
                                                        console.log(response.records);
                                                        Ext.Msg.alert('Liberación de Tractores','El operador tiene facturas por validar');
                                                             //Ext.getCmp('libTractorCancelarBtn').fireHandler();

                                                    }
                                                    else
                                                    {
                                                        //Ext.Msg.alert('Liberación de Tractores','El operador  no ');     
                                                        Ext.Ajax.request({
                                                url: '../../carbookBck/json/trViajesTractores.php?',
                                                params: {
                                                trViajesTractoresActionHdn: 'validaCentroDisLiberacionTractor',
                                                ciasesval:ciaSesVal
                                            },
                                            success: function(response){
                            
                                                response = Ext.JSON.decode(response.responseText);
                                                    Ext.Msg.hide();
                                                    if (response.records > 0) {

                                                          if(response.root[0].tipoLiberacion ==='C')
                                                          {
                                                            //Ext.Msg.alert('Facturas Autorizar','FALTA AUTORIZAR FACTURA');

                                                           Ext.create('MyDesktop.modules.viajesForaneos.liberarTractores.libTractorOdometroWin').show(); 
                                                 
                                                          }

                                                          else if(response.root[0].tipoLiberacion ==='S')
                                                          {



                                                            var window = Ext.WindowManager.get('libTractorWin');
                                                            var form = Ext.getCmp('libTractorFrm').getForm();

                                                                form.submit({
                                                                                                        waitTitle: titleWin,
                                                                                                        waitMsg: 'Espere ... efectuando la liberación',
                                                                    success: function(form, action){
                                                                        Ext.Msg.alert(window.title, action.result.successMessage);
                                                                        Ext.getCmp('libTractorCancelarBtn').fireHandler();
                                                                    },
                                                                    failure: function(form, action){
                                                                        Ext.Msg.alert(window.title, action.result.errorMessage);
                                                                    }
                                                                });

                                                          }
                                                           
                                                    }
                                                     else
                                                      {
                                                             Ext.Msg.alert('Liberación de Tractores','Este Centro de Distribucion no puede liberar tractores,<br> solo pueden liberar los patios que generan poliza.');
                                                             Ext.getCmp('libTractorCancelarBtn').fireHandler();

                                                      }
                                            }
                                        });                                                   
                                                    }
                                            }
                                        });
                                    ////////
                                    //var ciaSesVal = typeof(ciaSesVal)=='undefined'?'CDTOL':ciaSesVal;
                                    console.log(ciaSesVal);
                                     //console.log('before ' + combo.getValue());
                                        //if(ciaSesVal !=='CDLZC' || ciaSesVal !=='CDVER'){
                                    

                                        var window = Ext.WindowManager.get('libTractorWin');
                                        var form = Ext.getCmp('libTractorFrm').getForm();
                                        if (form.isValid()) {

                                            if(Ext.getCmp('libTractorViajeTxt').getValue() != '' ){

																						Ext.Msg.wait("Espere validando la información ... ", titleWin);
                                            

                                    }
                                    else
                                    {
                                        Ext.Msg.alert('Liberación de Tractores','No hay datos para este viaje');
                                    }
                                        
                                  }
                                },
                                id: 'libTractorAceptarBtn',
                                iconCls: 'ux-btnSave',
                                text: 'Guardar'
                            },
                            {
                                xtype: 'tbseparator'
                            },
                            {
                                xtype: 'button',
                                handler: function(button, e) {
                                    var idTractor = Ext.getCmp('libTractorTractorCmb').getValue();
                                    var form = Ext.getCmp('libTractorFrm').getForm();
                                    form.reset();
                                    Ext.getCmp('libTractorViajeTxt').setReadOnly(true);
                                    Ext.getCmp('libTractorTalonesGrd').getStore().removeAll();
                                    Ext.getCmp('libTractorTractorCmb').getStore().removeAll();
                                    Ext.getCmp('libTractorTractorCmb').enable(true);
                                    var bloqueoStr = Ext.StoreMgr.get('libTractorBloqueoTractorStr');
                                    if (idTractor){ //Desbloquea el Tractor
                                        bloqueoStr.load({
                                            params:{
                                                catTractoresActionHdn: 'liberarTractor',
                                                catTractorIdTractorHdn: idTractor,
                                            },
                                            callback: function(records, operation, success) {
                                                if (!operation.wasSuccessful()) {
                                                    console.log('No se pudo liberar el Tractor');
                                                }
                                            }
                                        });
                                    }
                                },
                                id: 'libTractorCancelarBtn',
                                iconCls: 'ux-btnDel',
                                text: 'Cancelar'
                            }
                        ]
                    }
                ],
                listeners: {
                    beforeRender: {
                        fn: function(component, eOpts) {
                                                Ext.Ajax.request({
                                                url: '../../carbookBck/json/trViajesTractores.php?',
                                                params: {
                                                trViajesTractoresActionHdn: 'validaCentroDisLiberacionTractor',
                                                ciasesval:ciaSesVal
                                            },
                                            success: function(response){
                                                response = Ext.JSON.decode(response.responseText);
                                                    if (response.records > 0) {

                                                    }
                                                     else
                                                      {
                                                        component.destroy();
                                                             Ext.Msg.alert('Liberación de Tractores','Este Centro de Distribucion no puede liberar tractores,<br> solo pueden liberar los patios que generan poliza.');

                                                      }
                                            }
                             });
                        }
                    },
                    beforeclose: {
                        fn: function(panel, eOpts) {
                            if(panel.allowDestroy){
                                delete panel.allowDestroy;
                                return true;
                            }
                            var titulo = this.title;
                            Ext.Msg.confirm(titulo,'¿Está seguro que desea salir de<br> '+ titulo+'?',function(btn){
                                if(btn == 'yes'){
                                    Ext.getCmp('libTractorCancelarBtn').fireHandler();
                                    panel.allowDestroy = true;
                                    panel.destroy();
                                }


                            }, panel);
                            return false;
                        }
                    }

                }
            });

        }
        win_libTractorWin.show();
        return win_libTractorWin;
    }
});