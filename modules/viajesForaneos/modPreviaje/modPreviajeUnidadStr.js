/*
 * File: app/store/modPreviajeUnidadStr.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeUnidadStr', {
    extend: 'Ext.data.Store',

    requires: [
        'MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
            model: 'MyDesktop.modules.viajesForaneos.modPreviaje.modPreviajeModel',
            storeId: 'modPreviajeUnidadStr',
            proxy: {
                type: 'ajax',
                url: '../../carbookBck/json/modPreViaje.php?',
                reader: {
                    type: 'json',
                    root: 'root',
                    totalProperty: 'records'
                }
            },
            fields: [
                {
                    name: 'avanzada'
                },
                {
                    name: 'vin'
                },
                {
                    name: 'distribuidor'
                }
            ]
        }, cfg)]);
    }
});