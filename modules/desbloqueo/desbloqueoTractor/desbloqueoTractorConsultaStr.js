/*
 * File: app/store/desbloqueoTractorConsultaStr.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.desbloqueo.desbloqueoTractor.desbloqueoTractorConsultaStr', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
            storeId: 'desbloqueoTractorConsultaStr',
            proxy: {
                type: 'ajax',
                url: '../../carbookBck/json/catTractores.php?catTractoresActionHdn=getTractoresBloqueados',
                reader: {
                    type: 'json',
                    root: 'root'
                }
            },
            fields: [
                {
                    name: 'idViajeTractor'
                },
                {
                    name: 'tractor'
                },
                {
                    name: 'usuario'
                },
                {
                    name: 'ip'
                },
                {
                    name: 'fechaEvento'
                },
                {
                    name: 'choferCompleto'
                },
                {
                    name: 'idTractor'
                },
                {
                    name: 'modulo'
                }
            ]
        }, cfg)]);
    }
});