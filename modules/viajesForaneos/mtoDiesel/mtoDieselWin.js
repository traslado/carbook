/*
 * File: app/view/mtoDieselWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

 Ext.define('MyDesktop.modules.viajesForaneos.mtoDiesel.mtoDieselWin', {
     extend: 'Ext.ux.desktop.Module',

     id:'mtoDieselWin',

     /*init : function(){
         this.launcher = {
             text: rhTRelLabTittle,
             iconCls:'auCatColores'
         };
     },*/

     createWindow_mtoDieselWin : function(){
         var desktop = this.app.getDesktop();
         var win_mtoDieselWin = desktop.getWindow('mtoDieselWin');
         if(!win_mtoDieselWin){
             // Integrar aquí los Stores que ocupará la pantalla
             //var store = new MyDesktop.modules.catalogs.catalogoBancos.catBancosConsultaStr();

             win_mtoDieselWin = desktop.createWindow({
                 height: 151,
                 id: 'mtoDieselWin',
                 width: 400,
                 title: 'Mantenimiento Diesel',
                 maximizable: false,
                 resizable: false,
                 minimizable: true,
                 iconCls:'mantenimientoDiesel',
                 dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                if (Ext.getCmp('mtoDieselFrm').getForm().isValid()){
                                    Ext.Msg.wait("Espere actualizando el precio del diesel ... ", 'Mantenimiento Diesel');
                                    Ext.Ajax.request({
                                        url: '../../carbookBck/json/trMantenimientoDiesel.php?',
                                        params: {
                                            trMtoDieselHdn: 'updDiesel',
                                            mtoDieselNuevoDsp: Ext.getCmp('mtoDieselNuevoDsp').getValue()
                                        },
                                        success: function(response){
                                            Ext.Msg.hide();
                                            Ext.Msg.alert('Mantenimiento Diesel','Precio actualizado correctamente.');
                                            Ext.getCmp('mtoDieselCancelarBtn').fireHandler();
                                        }
                                    });
                                }
                                else{
                                    Ext.Msg.alert("Error", "Falta Llenar campos requeridos o contienen Formato Incorrecto");
                                }
                            },
                            id: 'mtoDieselGuardarBtn',
                            iconCls: 'ux-btnSave',
                            text: 'Guardar'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                         				var precioAnt = Ext.getCmp('mtoDieselAnteriorDsp').getValue();
                         				Ext.getCmp('mtoDieselFrm').getForm().reset();   
                         				Ext.getCmp('mtoDieselAnteriorDsp').setValue(precioAnt);
                            },
                            id: 'mtoDieselCancelarBtn',
                            iconCls: 'ux-btnDel',
                            text: 'Cancelar'
                        }                        
                    ]
                }
            ],
            items: [
                {
                    xtype: 'form',
                    height: 186,
                    id: 'mtoDieselFrm',
                    layout: 'absolute',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'displayfield',
                            x: 20,
                            y: 10,
                            id: 'mtoDieselAnteriorDsp',
                            fieldLabel: 'Precio Anterior',
                            name: 'mtoDieselAnteriorDsp',
                            value: ''
                        },
                        {
                            xtype: 'textfield',
                            x: 20,
                            y: 40,
                            id: 'mtoDieselNuevoDsp',
                            width: 190,
                            fieldLabel: 'Nuevo Precio',
                            name: 'mtoDieselNuevoDsp',
                            allowBlank: false,
                            allowOnlyWhitespace: false,
                            enableKeyEvents: true,
                            enforceMaxLength: true,
                            maxLength: 7,
                            minLength: 4
                        }
                    ]
                }
            ],
            listeners: {
                render: {
                    fn: function(component, eOpts) {
                        Ext.Ajax.request({
                            url: '../../carbookBck/json/trMantenimientoDiesel.php?',
                            params: {
                                    trMtoDieselHdn: 'getMtoDiesel'
                            },
                            success: function(response){
                                response = Ext.JSON.decode(response.responseText);
                                if (response.records > 0) {
                                    Ext.getCmp('mtoDieselAnteriorDsp').setValue(response.root[0].importe);
                                }
                            }
                        });
                    }
                },
                boxready: {
                    fn: function(component, width, height, eOpts) {
                        var elements = Ext.ComponentQuery.query('[xtype=textfield]').concat(Ext.ComponentQuery.query('[xtype=combobox]')).concat(Ext.ComponentQuery.query('[xtype=numberfield]'));

                        //Por cada elemento lo suscribo a un evento keypress, con un eventHandler llamado nextFocus
                        Ext.each(elements, function(el){
                            el.on('keypress', nextFocus);
                        });

                        //Event Handler que se encarga de recorrer y hacer FOCO a los elementos del formulario.
                        function nextFocus(field, e){
                            var actual = field;

                            if (e.getKey() === Ext.EventObject.ENTER) { //Si el input del teclado es Enter...
                                while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){//Recorro los elementos. Si existe el elemento siguiente y es Visible (por los hiddens fields).
                                    actual = actual.next();
                                }
                                if(actual.next()){
                                    actual.next().focus();
                                } else {//Si estoy en el elemento final regreso al primero...NO SÉ si sea el comportamiento requerido.

                                    while(actual.prev()){//Recorro mientras exista elemento
                                        actual = actual.prev();
                                    }
                                    if(actual.isVisible()){//Si no es un hidden field le hago foco
                                        actual.focus();
                                    } else {// de lo contrario vuelvo a recorrer hasta encontrar algún elemento visible
                                        while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){
                                            actual = actual.next();
                                        }
                                        actual.next().focus();
                                    }
                                }
                            }
                        }

                    }
                },
                beforeclose: {
                    fn: function(panel, eOpts) {
                        if(panel.allowDestroy){
                            delete panel.allowDestroy;
                            return true;
                        }

                        Ext.Msg.confirm(this.title,'¿Está seguro que desea salir de<br> '+ this.title+'?',function(btn){

                            if(btn == 'yes'){
                                panel.allowDestroy = true;
                                panel.destroy();
                            }
                        }, panel);

                        return false;
                    }
                }
            }
        });

          }
          win_mtoDieselWin.show();
          return win_mtoDieselWin;
      }
  });
