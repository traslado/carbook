/*
 * File: app/view/cargaFoliosFCAWin.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyDesktop.modules.repuve.cargaFoliosFCA.cargaFoliosFCAWin', {                                                              
   extend: 'Ext.ux.desktop.Module',
    
    id:'cargaFoliosFCAWin',

    /*init : function(){
        this.launcher = {
            text: admCatGenTitle,
            iconCls:'icon-admCatGeg'
        };
    },*/
    
    createWindow_cargaFoliosFCAWin: function(){
        var desktop = this.app.getDesktop();
        var win_cargaFoliosFCAWin= desktop.getWindow('cargaFoliosFCAWin');
        if(!win_cargaFoliosFCAWin){
            var store = new MyDesktop.modules.repuve.cargaFoliosFCA.cargaFoliosFCAStr();
						var winTitle = 'Carga de Folios REPUVE';          
            
            win_cargaFoliosFCAWin= desktop.createWindow({
            height: 385,
            hidden: false,
            id: 'cargaFoliosFCAWin',
            width: 665,
            iconCls: 'cxc',
            title: winTitle,
            minimizable: true,
            maximizable: false,
            resizable: false,
            iconCls:'cxc',
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    id: 'cargaFoliosFCATbr',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'ux-btnSave',
                            text:'Guardar',
                            id: 'cargaFoliosFCAAceptarBtn',
                            listeners: {
                                click: {
                                    fn: function(button, e, eOpts) {
                                        var forma = Ext.getCmp("cargaFoliosFCAFrm");
				                            		var url = '../../carbookBck/json/cargaFoliosFCA.php?actionHdn=addFolios';
				                            		//var panel = 0;
				                            		var error = 0;
				                            		var cont = 0;			                            				
		                            				var arrFolios = [];												                    
										                    if (Ext.getCmp("cargaFoliosFCAGrd").getStore().getCount() > 0) {
												                    Ext.data.StoreManager.lookup('cargaFoliosFCAStr').each(function(rec) {
												                        if(rec.data.valido)
												                        {
												                        		arrFolios.push(rec.data);
												                        		record = rec;
												                        		cont++;
												                        		console.log('record',record.data);
												                      	}
												                    });
												                    if(cont == 0)
												                    {
												                    		error++;
												                    }
										                  	}
										                  	else
									                  		{
									                  				error++;
									                  		}
                                        
                                        if(forma.isValid() && error == 0){
                                            console.log('rec',record.data);
                                            forma.submit({
								                                waitTitle: winTitle,
								                                waitMsg: 'Espere ... efectuando registro', 
								                                url: url,                                               
                                                params:{
                                                    arrFolios: Ext.JSON.encode(arrFolios)                                                
                                                },
                                                success: function(form, action){
                                                    Ext.Msg.alert(action.result.successTitle, action.result.successMessage, function() {
                                                    		Ext.getCmp('cargaFoliosFCACancelarBtn').fireEvent('click',Ext.getCmp('cargaFoliosFCACancelarBtn'));
                                                    });                                                    
                                                },
                                                failure: function(form, action){
                                                    Ext.Msg.alert(action.result.successTitle, action.result.successMessage);
                                                }
                                            });
                                        } 
                                        else {
                                            var msg = 'Falta llenar campos requeridos.';
                                            if(error > 0)
                                            {
                                            		msg = 'Debe cargar algún archivo de excel con folios.';
                                            		if(Ext.getCmp("cargaFoliosFCAGrd").getStore().getCount() > 0){
	                                            			msg = 'Debe cargar folios válidos.';
	                                            	}
                                            }
                                            Ext.Msg.alert(winTitle, msg);
                                        }
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'tbseparator',
                            id: 'separador'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'ux-btnDel',
                            text: 'Cancelar',
                            id: 'cargaFoliosFCACancelarBtn',
                            listeners: {
                                click: {
                                    fn: function(button, e, eOpts) {
                                        Ext.getCmp('cargaFoliosFCAFrm').getForm().reset();
                                        Ext.data.StoreManager.lookup('cargaFoliosFCAStr').removeAll();                                     
                                    }
                                }
                            }
                        }
                    ]
                }
            ],
            items: [		                          
                {
                    xtype: 'form',
                    height: 530,
                    id: 'cargaFoliosFCAFrm',
                    //width: 630,
                    layout: 'absolute',
                    bodyPadding: 10,
                    //title: 'Folios',
                    items: [			                        
                        {
                            xtype: 'filefield',
                            x: 10,
                            y: 30,
                            id: 'cargaFoliosFCAArchivoFld',
                            fieldLabel: 'Archivo de Excel',
                            labelWidth: 120,
                            width: 500,
                            name: 'cargaFoliosFCAArchivoFld',
                            buttonText: 'Buscar ...',
                            accept: ".xlsx",
                            allowBlank: true,
                            listeners: {
                                change: {
                                    fn:function(filefield, value, eOpts) {
                                        var url = "../../carbookBck/json/cargaFoliosFCA.php?actionHdn=leeArchivo";
                                        var tester = new RegExp('^[^\/"]+((.xlsx))$');
                                        if(!tester.test(value)){
                                            Ext.Msg.alert(winTitle, '<b>Debe seleccionar un archivo de excel válido (*.xlsx)</b>', function(){
                                                filefield.reset();
                                            });
                                        }
                                        else
                                      	{
				                                    Ext.getCmp("cargaFoliosFCAFrm").getForm().submit({
								                                waitTitle: winTitle,
								                                waitMsg: 'Espere leyendo el archivo ... ',
								                                url: url,                                      
				                                        success: function(form, action) {
				                                            var res = action && action.result && action.result.folios.root;								
				                                            if(res.length === 1){
				                                                if(res[0].fail){
				                                                    Ext.Msg.alert(winTitle, res[0].nose);
				                                                }
				                                            }else{
				                                                Ext.Msg.alert(winTitle, action.result.msjResponse);
				                                                var gridStr = Ext.getCmp('cargaFoliosFCAGrd').getStore();
				                                                gridStr.loadData(action.result.folios.root);
				                                            }
				                                        },
				                                        failure: function(form, action) {
				                                            Ext.Msg.alert(winTitle, action.result.msjResponse);
				                                        }
				                                    });				                                        			
                                      	}
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'gridpanel',
                            x: 10,
                            y: 90,
                            height: 200,
                            id: 'cargaFoliosFCAGrd',
                            //width: 630,
                            title: 'Folios',
                            store: 'cargaFoliosFCAStr',
                            border: false,
                            enableColumnHide: false,
                            enableColumnMove: false,                                
														enableColumnResize: false,
														sortableColumns: false,				                            
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    width: 145,
                                    dataIndex: 'folioRepuve',
                                    text: 'Folio REPUVE'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 200,
                                    dataIndex: 'descrMarca',
                                    text: 'Marca'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    width: 260,
                                    dataIndex: 'descrCentroDist',
                                    text: 'Centro de Distribución'
                                }
                            ],
                            viewConfig: {
                                getRowClass: function(record, index, rowParams, store) {
                                    return (record.get('valido') == 0) ? "Inactivo .x-grid-cell" : "Activo .x-grid-cell";
                                },
                                listeners: {
                                    render: {
                                        fn: function(view) {
                                            view.tip = Ext.create('Ext.tip.ToolTip', {
                                                target: view.el,
                                                delegate: view.itemSelector,
                                                trackMouse: true,
                                                renderTo: Ext.getBody(),
                                                listeners: {
                                                    beforeshow: function(tip) {
                                                        var tooltip = '';
                                                        var folioRepuve = view.getRecord(tip.triggerElement).get('folioRepuve');
                                                        if(view.getRecord(tip.triggerElement).get('valido') == 0 && folioRepuve != null)
                                                        {
                                                        		tooltip = '<b>Inválido.</b>';
                                                        }
                                                        else
                                                      	{
                                                            if(view.getRecord(tip.triggerElement).get('valido') == 1)
                                                            {
                                                            		tooltip = '<b>Válido.</b>';
                                                            }		                                                            			
                                                      	}
                                                        if (tooltip != "") {
                                                            tip.update(tooltip);
                                                        } else {
                                                            tip.on('show', function() {
                                                                Ext.defer(tip.hide, 20, tip);
                                                            }, tip, { single: true });
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }		                                    
                            },				                            
                        }				                        
                    ],
				            listeners: {
				                afterrender: {
				                    fn: function(component, eOpts) {
				                        // Asignar el Mensaje de Ayuda al elemento
				                        function despliegaToolTip() {
				                            Ext.tip.QuickTipManager.register({
				                                target: idElemento, //ID del elemento al que se le quiere mostrar la Ayuda
				                                text: despliegaTexto,
				                                width: longitudTexto
				                            });
				                        }
				                        								                        
				                        despliegaTexto = 'Seleccione el archivo de excel que contiene los Folios de REPUVE';
				                        longitudTexto = despliegaTexto.length * 7.0;
				                        idElemento = 'cargaFoliosFCAArchivoFld';
				                        despliegaToolTip();
				                    }
				                }                
				            }					                    
                }
            ],
            listeners: {
                beforeclose: {
                    fn: function(panel, eOpts) {
                        if(panel.allowDestroy){
                            delete panel.allowDestroy;
                            return true;
                        }

                        Ext.Msg.confirm(this.title,'¿Está seguro que desea salir de<br> '+ this.title+'?',function(btn){
                            if(btn == 'yes'){
                                panel.allowDestroy = true;
                                panel.destroy();
                            }
                        }, panel);

                        return false;
                    }
                },
                boxready: {
                    fn: function(component, width, height, eOpts) {

                        /* Función que simula un TAB en inputFields con un ENTER y recorre como Linked Lists.
                        * REQUERIMIENTOS PREVIOS: settear la propiedad enableKeyEvents de los componentes a utilizar.
                        * PENDIENTES: No funciona en checkboxes por su comportamiento nativo. Desconosco aún si funciona
                        *             con varias Tab-Panel y con demás controles dentros del Form.
                        */

                        if(usuValor != 3 && usuValor != 67 && usuValor != 54 && usuValor != 78 && usuValor != 164)
                        {
                              Ext.Msg.show({
																	title: winTitle,
																	msg: '<b>Operación restringida.</b>',
																	buttons: Ext.Msg.OK,
																	closable: false,
																	icon: Ext.Msg.ERROR
																});                              
                              win_cargaFoliosFCAWin.destroy();                                      
                        }	

                        //Hago un query con los elementos que sean textfields, numberfields o combobox y obtengo un array de ellos.
                        var elements = Ext.ComponentQuery.query('[xtype=textfield]').concat(Ext.ComponentQuery.query('[xtype=combobox]')).concat(Ext.ComponentQuery.query('[xtype=numberfield]'));

                        //Por cada elemento lo suscribo a un evento keypress, con un eventHandler llamado nextFocus
                        Ext.each(elements, function(el){
                            el.on('keypress', nextFocus);
                        });

                        //Event Handler que se encarga de recorrer y hacer FOCO a los elementos del formulario.
                        function nextFocus(field, e){
                            var actual = field;

                            if (e.getKey() === Ext.EventObject.ENTER) { //Si el input del teclado es Enter...
                                while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){//Recorro los elementos. Si existe el elemento siguiente y es Visible (por los hiddens fields).
                                    actual = actual.next();
                                }
                                if(actual.next()){
                                    actual.next().focus();
                                } else{//Si estoy en el elemento final regreso al primero...NO SÉ si sea el comportamiento requerido.

                                    while(actual.prev()){//Recorro mientras exista elemento
                                        actual = actual.prev();
                                    }
                                    if(actual.isVisible()){//Si no es un hidden field le hago foco
                                        actual.focus();
                                    } else{// de lo contrario vuelvo a recorrer hasta encontrar algún elemento visible
                                        while(actual.next() && !actual.next().isVisible() && actual.next().type === 'displayfield'){
                                            actual = actual.next();
                                        }
                                        actual.next().focus();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        }
        win_cargaFoliosFCAWin.show();
        return win_cargaFoliosFCAWin;
    }
})