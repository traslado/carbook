/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.moduloComodato13', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'moduloComodato',
            iconCls: 'cxc',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Impresion de Unidades',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('impresionComodatoWin');
										win = module.createWindow_impresionComodatoWin();
									},
									scope: this,
									windowId: 'impresionComodatoWin'
								},
								{
									text: 'Cambio de Estatus',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('prodStatusWin');
										win = module.createWindow_prodStatusWin();
									},
									scope: this,
									windowId: 'prodStatusWin'
								}
				]
            }
        };
    }
});