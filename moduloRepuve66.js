/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.moduloRepuve66', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Repuve',
            iconCls: 'tractorModule',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Carga Verificacion Repuve',
									iconCls:'cxc',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('interfacesLzcRepuveWin');
										win = module.createWindow_interfacesLzcRepuveWin();
									},
									scope: this,
									windowId: 'interfacesLzcRepuveWin'
								}
				]
            }
        };
    }
});