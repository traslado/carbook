<?php
    session_start();

    if (isset($_REQUEST['loginHdn']) && $_REQUEST['loginHdn'] != '') {
        if (isset($_REQUEST['loginHdn']) && $_REQUEST['loginHdn'] == '1') {
            $_SESSION['usuCompania'] = $_REQUEST['segLoginDistCentroCmb'];
            $_SESSION['usuCiaAut'] = $_REQUEST['segLoginDistCentroHdn'];
        } else {
            $_SESSION['usuCompania'] = $_REQUEST['ciaSesVal'];
            $_SESSION['usuCiaAut']   = $_REQUEST['ciaAutVal'];

            $_SESSION['idUsuario']   = $_REQUEST['usuValor'];
            $_SESSION['usuario']     = $_REQUEST['usuClave'];
            $_SESSION['nombreUsr']   = $_REQUEST['usuNombre'];
            $_SESSION['theme']       = $_REQUEST['temaApp'];
            $_SESSION['tipoUsuario'] = $_REQUEST['tipoUsuario'];
        }

        echo json_encode(array('success'=> true, 'tema'=>$_SESSION['theme']));
    } elseif ((!isset($_SESSION['usuario'])) || (isset($_SESSION['usuario']) && $_SESSION['usuario'] == "")) {
        header('Location:index.html');
    }
?>