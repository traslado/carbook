/*!
* Ext JS Library 4.0
* Copyright(c) 2006-2011 Sencha Inc.
* licensing@sencha.com
* http://www.sencha.com/license
*/

Ext.define('MyDesktop.GtoGenMenuModel4', {
	extend: 'Ext.ux.desktop.Module',

    init : function() {

        this.launcher = {
            text: 'Gastos Viaje',
            iconCls: 'catGeneralesMenuIco',
            handler: function() {
                return false;
            },
            menu: {
                items: 
				[
								{
									text: 'Gastos Viaje Tractor',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('trap484Win');
										win = module.createWindow_trap484Win();
									},
									scope: this,
									windowId: 'trap484Win'
								},
								{
									text: 'Viajes Vacios',
									iconCls:'tractorModule',
									handler : function(button, event) {
										var me = this; 
										module = me.app.getModule('gastosViajeVacioWin');
										win = module.createWindow_gastosViajeVacioWin();
									},
									scope: this,
									windowId: 'gastosViajeVacioWin'
								}
				]
            }
        };
    }
});